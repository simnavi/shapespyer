.. shapespyer documentation master file, created by
   sphinx-quickstart on Thu Dec  9 10:33:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

**Shapespyer docs**
===================

..
   Table of Contents:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   workflow
   shape
   examples
   example1
   example2
   example3
   modules

..
   automodule:: ioports   :members: __all__

..
   autoclass:: groFile   :members:

..
   autofunction::

..
   autoexception:: # :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
