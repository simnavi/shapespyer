.. _shape.py:

**shape.py**
============

The script generates a molecular structure of the requested shape.
It takes three mandatory arguments:

``--inp <input_config_file>``

``--out <output_file_name>``

``--shape <shape>``

All other arguments are optional and control different *non-default* aspects of
both the input and output (e.g. separate directories for input and output files),
but also possible alternations in the created shape.

.. note::
    By default the output configuration file will adopt the same extension
    as the input file (normally *.gro*). In order to alter the output format, one has
    to provide an extra (optional) argument:

    ``--xout <output_extension>``

This page explains how to invoke the script on the command line, i.e. in the terminal
window, and describes all the available options that can be fed to the script in
the form of short or long arguments supplied with the relevant values where necessary.

Usage
-----

**shape.py [options]**

*Notation*
++++++++++++
*[option]    - an optional command-line parameter (excluding [] brackets if any)*

*<VALUE>     - a value to be provided by the user (excluding <> brackets if any)*

*{VAL1/VAL2} - a set of alternatives, i.e. mutually exclusive values*

*VAL1?/VAL2? - values that are planned to be added (i.e. not supported yet)*

*{VALUE\*}    - the default value in the absence of an option in the user input*

*{\*}         - no default value assumed*

*Options*
++++++++++++
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
| short form         | long form             | description                                                                                                   |
+====================+=======================+===============================================================================================================+
|  ``-h``            | ``--help``            | show this help message and exit                                                                               |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-g``            | ``--gopt``            | use 'getopt' for parsing the script arguments (parameters); if not present, use 'argparser'                   |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-y``            | ``--yaml``            | use a .yaml file to configure options                                                                         |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-d <IODIR>``    | ``--dio=<IODIR>``     | directory or list of (two) directories for input/output files {'.'\*}; example: 'input,output'                |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-b <IBOX>``     | ``--box=<IBOX>``      | input file specifying box dimensions (3 values) or cell matrix (9 values) {shape.box\*/<name>.box}            |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-i <IFILE>``    | ``--inp=<IFILE>``     | input configuration file containing molecule(s) coordinates {config_inp.gro\*/<name>.gro/<name>.xyz}          |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-o <ONAME>``    | ``--out=<ONAME>``     | base name of the output configuration file without extension {config_out\*}                                   |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-x <OEXT>``     | ``--xout=<OEXT>``     | output configuration file extension {.gro\*/.xyz/.pdb?/.dlp?/.dlm?/.dpd?}                                     |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-s <SHAPE>``    | ``--shape=<SHAPE>``   | geometrical shape to create {ring\*/rod/ball/ves/lat/lat2/lat3}                                               |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-p <NPICK>``    | ``--npick=<NPICK>``   | number of molecules to pick up from input {1\*}                                                               |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-l <LRING>``    | ``--lring=<LRING>``   | number of molecules in the largest ring within the output structure (>9) {10\*}                               |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-m <MOLIDS>``   | ``--molids=<MOLIDS>`` | list of molecule (residue) indices to pick up from input {1\*}; example: '1,3'                                |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-r <RNAMES>``   | ``--rnames=<RNAMES>`` | list of residue (molecule) names to pick up from input {ANY\*}; example: 'SDS,CTAB'                           |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-c <CAVR>``     | ``--cavr=<CAVR>``     | radius of internal cavity in the centre of the generated structure {1.0\* nm}                                 |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  ``-t <TURNS>``    | ``--turns=<TURNS>``   | number of full turns in a 'rod' (i.e. stack of rings) or a band spiral {1\* => a single ring}                 |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--alpha=<ALPHA>``   | initial azimuth angle (in XY plane) for each added molecule (0,...,360) {0.0\* degrees}                       |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--theta=<THETA>``   | initial altitude angle (w.r.t. OZ axis) for each added molecule (0,...,180) {0.0\* degrees}                   |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--sbuff=<SBUFF>``   | solvation buffer size around the generated structure {2.0* nm}                                                |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--dmin=<DMIN>``     | minimum distance between 'bone' atoms in the generated structure {0.5 * nm}                                   |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--mint=<MINT>``     | 'internal' atom indices (for each picked-up molecule), placed closest to the centre {1\*}; example: '41,40'   |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--mext=<MEXT>``     | 'external' atom indices (for each picked-up molecule), placed farther from the centre {2\*}; example: '0,16'  |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--frc=<FRC>``       | average fractions of the compounds in the structure; example: [0.4,0.6]                                       |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--nl=<NL>``         | 3D lattice number, i.e. same number of nodes in each dimension on a cubic 3D lattice {1\*}                    |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--nl=<NX>``         | lattice number in X, i.e. number of nodes in X dimension on a rectangular 3D lattice {1*}                     |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--nl=<NY>``         | lattice number in Y, i.e. number of nodes in Y dimension on a rectangular 3D lattice {1*}                     |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--nl=<NZ>``         | lattice number in Z, i.e. number of nodes in Z dimension on a rectangular 3D lattice {1*}                     |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--rev``             | reverse 'internal' <-> 'external' atom indexing in each of the picked-up molecules                            |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--pin``             | create a 'pinned-through' ball or vesicle, i.e. with holes on its sides (ad hoc)                              |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--fxz``             | use 'XZ-flattened' initial molecule orientation to minimise its 'spread' along Y axis                         |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+
|  n\/a              | ``--alignz``          | align initial molecule configuration along Z axis (only valid with 'smiles' input)                            |
+--------------------+-----------------------+---------------------------------------------------------------------------------------------------------------+

Help
----

In order to see the help page, type the following command and press 'Enter'.

.. code-block:: text

    $ shape.py -g -h

    ##################################################
    #                                                #
    #  Shapespyer - soft matter structure generator  #
    #                                                #
    #  Author: Dr Andrey Brukhno (c) 2020 - 2021     #
    #          Daresbury Laboratory, SCD, STFC/UKRI  #
    #                                                #
    ##################################################

    Using 'getopt' ...

    ============
     Notation :
    ------------
    [option]    - an optional command-line parameter (excluding [] brackets if any)
    <VALUE>     - a value to be provided by the user (excluding <> brackets if any)
    {VAL1/VAL2} - a set of alternatives, i.e. mutually exclusive values
    VAL1?/VAL2? - values that are planned to be added (i.e. not supported yet)
    {VALUE*}    - the default value in the absence of an option in the user input
    {*}         - no default value assumed
    ============

    usage: shape.py [options]

    ============
     options :
    ------------
    -h          | --help            : show this help message and exit
    -y          | --yaml            : use a .yaml file to configure options
    -g          | --gopt            : use 'getopt' for parsing the script arguments (parameters); if not present, use 'argparser'
    -d <IODIR>  | --dio=<IODIR>     : directory or list of (two) directories for input/output files {'.'*}; example: 'input,output'
    -b <IBOX>   | --box=<IBOX>      : input file specifying box dimensions (3 values) or cell matrix (9 values) {shape.box*/<name>.box}
    -i <IFILE>  | --inp=<IFILE>     : input configuration file containing molecule(s) coordinates {config_inp.gro*/<name>.gro/<name>.xyz}
    -o <ONAME>  | --out=<ONAME>     : base name of the output configuration file without extension {config_out*}
    -x <OEXT>   | --xout=<OEXT>     : output configuration file extension {.gro*/.xyz/.pdb?/.dlp?/.dlm?/.dpd?}
    -s <SHAPE>  | --shape=<SHAPE>   : geometrical shape to create {ring*/rod/ball/ves/lat/lat2/lat3}
    -p <NPICK>  | --npick=<NPICK>   : number of molecules to pick up from input {1*}
    -l <LRING>  | --lring=<LRING>   : number of molecules in the largest ring within the output structure (>9) {10*}
    -m <MOLIDS> | --molids=<MOLIDS> : list of molecule (residue) indices to pick up from input {1*}; example: '1,3'
    -r <RNAMES> | --rnames=<RNAMES> : list of residue (molecule) names to pick up from input {ANY*}; example: 'SDS,CTAB'
    -c <CAVR>   | --cavr=<CAVR>     : radius of internal cavity in the centre of the generated structure {1.0* nm}
    -t <TURNS>  | --turns=<TURNS>   : number of full turns in a 'rod' (i.e. stack of rings) or a band spiral {1* => a single ring}
        n/a     | --alpha=<ALPHA>   : initial azimuth angle (in XY plane) for each added molecule (0,...,360) {0.0* degrees}
        n/a     | --theta=<THETA>   : initial altitude angle (w.r.t. OZ axis) for each added molecule (0,...,180) {0.0* degrees}
        n/a     | --sbuff=<SBUFF>   : solvation buffer size around the generated structure {2.0* nm}
        n/a     | --dmin=<DMIN>     : minimum distance between 'bone' atoms in the generated structure {0.5 * nm}
        n/a     | --mint=<MINT>     : 'internal' atom indices (for each picked-up molecule), placed closest to the centre {1*}; example: '41,40'
        n/a     | --mext=<MEXT>     : 'external' atom indices (for each picked-up molecule), placed farther from the centre {2*}; example: '0,16'
        n/a     | --frc=<FRC>       : average fractions of the compounds in the structure; example: [0.4,0.6]
        n/a     | --nl=<NL>         : 3D lattice number, i.e. same number of nodes in each dimension on a cubic 3D lattice {1*}
        n/a     | --nx=<NX>         : lattice number in X, i.e. number of nodes in X dimension on a rectangular 3D lattice {1*}
        n/a     | --ny=<NY>         : lattice number in Y, i.e. number of nodes in Y dimension on a rectangular 3D lattice {1*}
        n/a     | --nz=<NZ>         : lattice number in Z, i.e. number of nodes in Z dimension on a rectangular 3D lattice {1*}
        n/a     | --rev             : reverse 'internal' <-> 'external' atom indexing in each of the picked-up molecules
        n/a     | --pin             : create a 'pinned-through' ball or vesicle, i.e. with holes on its sides (ad hoc)
        n/a     | --fxz             : use 'XZ-flattened' initial molecule orientation to minimise its 'spread' along Y axis
        n/a     | --alignz          : align initial molecule configuration along Z axis (only valid with 'smiles' input)
    ============

..
    automodule:: shape    :members:    :undoc-members:    :show-inheritance:
