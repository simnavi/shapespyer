designs package
===============

..
    Submodules----------

designs.protoshape module
-------------------------

.. automodule:: designs.protoshape
    :members:
    :undoc-members:
    :show-inheritance:

..
    Module contents---------------

..
    automodule:: designs    :members:    :undoc-members:    :show-inheritance:
