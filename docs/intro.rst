**Introduction**
================

**Shapespyer** package provides a *Python library* (APIs) and *Python/Bash scripts*
for automating highly parallel simulation workflows to facilitate preparation and
running atomistic and/or coarse-grain molecular simulations with the focus on
condensed soft matter systems. The package also includes tools for post-simulation
analyses.

GitLab repository: https://gitlab.com/simnavi/shapespyer

Motivation
----------

In the domain of soft matter and biomolecular simulation, the task of preparing
complex molecular structures of interest is both challenging and tedious. The naive
approach using an initial configuration with randomised positions of solute molecules
is most often fruitless due to the complexity of the (unknown) pathways that lead
to self-assembly of any particular shape of a molecular aggregate. Unfortunately,
pre-equilibration into and further relaxation of a specific molecular complex
can easily require simulation times way beyond what is reachable in a practical
simulation scenario. Therefore, *it is vital to have efficient tools facilitating
the generation and initial equilibration of pre-assembled molecular complexes with
predetermined composition and structure.* This is exactly the purpose of the
**Shapespyer** package.

Key features
------------

* **The package** comprises tools for generation of pre-assembled complex molecular
  structures of various predetermined shapes to seed molecular dynamics (MD) or
  Monte Carlo (MC) simulations aimed at studying equilibration and relaxation processes
  in soft matter and biomolecular systems.

* **Python Library with APIs** for creation and manipulation of homo- and hetero-
  geneous molecular aggregates (complexes). The library provides general-purpose
  APIs tailored for 'ease-of-use' in stand-alone (external) scripts.

* **Automated highly parallel workflows** for preparation and running of MD simulations
  (mostly using Gromacs MD engine, but extendable for using other engines).

* **Helper tools** for semi-automated analyses of cluster formation
  and evolution, including cluster number and size distributions, but also
  gyration radii and momenta of inertia projected onto the three principal axes.

..
  For more details see *README.md*.
