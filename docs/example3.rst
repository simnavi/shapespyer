.. _SDS-CTAB:

**Example 3: mixed SDS-CTAB**
=============================

First, change to ``shapespyer/examples/SDS-CTAB``. 
If starting from ``shapespyer/examples``:

.. code-block:: text

  $ cd SDS-CTAB
  $ ls inp
  config_smiles_SDS-C10TAB.gro  smiles_SDS-CTA10.sml


1. Generating a 'rod' with random molecule distribution
-------------------------------------------------------

In this example the user could merely use the previously created 3D configurations for 
the two template molecules: SDS and C(10)TAB. We suggest to combine the ones generated from 
SMILES strings, e.g. ``config_smiles_SO4C12.gro`` and ``config_smiles_NC13H30.gro`` from 
the previous examples. The combined ``.gro`` file is found in subdirectory ``inp``:

.. code-block:: text

  $ ls inp/config_smiles*gro
  config_smiles_SDS-C10TAB.gro

.. note::
  We deliberately chose the configurations that are ordered in accordance with the convention
  of indices starting from the central atom in the molecules' hydrophilic head group.

Using this file as input we can generate a mixed molecular assembly with a specified composition:

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_SDS-C10TAB.gro -o configS -x .gro --shape='rod' -r 'SDS,CTA' --frc=[50,50] -l 15 -t 8  > out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log

where the option ``--frc=[50,50]`` sets the average fractions of the two compounds in the resulting 
structure, i.e. 50/50 in this case.

The actual numbers of SDS and CTA10 molecules can be found in ``out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log``:

.. code-block:: text

  $ grep "Molecule distribution" out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log
  shape.py::main(): Molecule distribution = [88, 90]

That is, the created 'rod' consists of 88 SDS and 90 CTA10 molecules. 

Check the generated 'rod' structure in VMD.

a) Along the Z axiz:

.. image:: images/SDS-CTAB10-rodS-N15-t8-T178-88x90-z0.png
  :width: 300
  :alt: CTAB ring configuration viewed along Z axiz

b) In the XY plane (from above):

.. image:: images/SDS-CTAB10-rodS-N15-t8-T178-88x90-xy.png
  :width: 300
  :alt: CTAB ring configuration - XY in plane view

Let's reduce its inner cavity by alowing for smaller distance between the terminal C-atoms:

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_SDS-C10TAB.gro -o configS-dmin04 -x .gro --shape='rod' -r 'SDS,CTA' --frc=[50,50] -l 15 -t 8 --dmin=0.4  > out/shape_SDS-CTA10_rodS_n15_t8-dmin04.log

  $ grep "Molecule distribution" out/shape_SDS-CTA10_rodS_n15_t8-dmin04.log
  shape.py::main(): Molecule distribution = [83, 85]

The final compact 'rod' structure is illustrated below.

a) Along the Z axiz:

.. image:: images/SDS-CTAB10-rodS-N15-t8-T168-83x85-z0.png
  :width: 300
  :alt: CTAB ring configuration viewed along Z axiz

b) In the XY plane (from above):

.. image:: images/SDS-CTAB10-rodS-N15-t8-T168-83x85-xy.png
  :width: 300
  :alt: CTAB ring configuration - XY in plane view


2. Replicating the rod and placing the replicas on a lattice
------------------------------------------------------------

Now we can place replicas of the latter rod assembly on a simple cubic lattice:

.. code-block:: text

  $ shape.py --dio=out,out -i configS-dmin04_rod_SDS-CTA_2-15-frc_ndte-8.gro -o configS_lattice -x .gro --shape='lat' -r 'SDS,CTA' --nl 2 > out/shape_SDS-CTA10_latS_n15_t8-dmin04-nl2.log

Check the new structure in VMD:

a) Along the X axiz:

.. image:: images/SDS-CTAB10-rodS-N15-t8-T168-83x85-latC-2x2x2-yz.png
  :width: 500
  :alt: mixed SDS-CTAB rods configuration on a cubic lattice - YZ in plane view

...

