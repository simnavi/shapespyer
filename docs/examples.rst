.. _examples:

**Examples**
============

The provided hands-on examples can be found in directory ``shapespyer/examples``.


Prerequisites
-------------

In order to be able to successfully progress through the example (tutorial) cases,
the user need to ensure that

- Gromacs version 2018+ (using modern ``gmx`` wrapper) is installed
- Sufficient computational resources are available on the system: 
  at least 8 logical CPU threads and RAM of at least 8 GB.
- Alternatively, the user might want to run simulations on a remote HPC cluster (perhaps, after 
  the system preparation done on a local workstation), in which case the script ``gmx-equilibrate.bsh`` 
  needs to be either called from the HPC queue submission script(s) or modified by the user accordingly.
- Upon downloading and unpacking the package, the user is advised to add the ``shapespyer/scripts`` directory 
  to user's environment variable called ``PATH``, so that all the scripts can be automatically found 
  and launched by the system. For example, assuming that the package was unpacked into user's ``$HOME`` 
  directory, one can enter the following command in a terminal, or add it to user's ``.bashrc`` file:
- Advanced users who wish to use the objects and methods (API) provided by Shapespyer directly
  in their own Python scripts, are advised to also add to the ``PATH`` variable the path to their
  local ``shapespyer`` installation, thereby enabling imports from the Shapespyer library.


``export PATH=$PATH:$HOME/shapespyer/scripts``



Exemplar workflow tasks
-----------------------

The directory contains a number of subdirectories with exemplar case studies using
the Shapespyer workflow with inputs and outputs for relatively simple surfactant systems,
covering the following tasks:

0. Getting 3D coordinates for a molecule from a SMILES string

1. Arranging molecules in a planar '*ring*' (or disc), configuration (start with SDS example)

2. Generating a spherical micelle, or a monolayer '*ball*' (start with SDS example)

3. Generating a spherical vesicle, or a (multi-)layered '*ball*'

4. Generating a cylindrical stack, or a monolayer '*rod*' topped with caps

5. Placing previously generated structures on a lattice

6. Staged equilibration and production runs followed by structure and solution analyses


Examples' (sub)directory structure
----------------------------------

Each case study is self-contained and resides in its own subdirectory wherein the user will 
find the following (or similar) subdirectories:

'**inp**' containing the required input files:

- ``*.sml`` - SMILES strings (e.g. ``smiles.sml``)
- ``*.gro`` - molecule 3D coordinates (e.g. ``SDS-single.gro``)

'**ref-out**' containing sample output files generated based on inputs in '**inp**' directory

'**usr-equil**' where user can run a relatively short equilibration on pre-generated structures

'**ref-equil**' containing reference files to compare with

'**ref-equil/toppar**' providing Gromacs-formatted files that define topologies (``<species>.itp``),
force-fields (``forcefield.itp``) and MD simulation parameters (``equil*.mdp``), 
but also seeding coordinate files for ionic species (``single_ion*.gro``).


The workflow is illustrated with test-bed case studies on surfactants in aqueous solution.

**Example1:** :doc:`SDS <example1>`

**Example2:** :doc:`CTAB <example2>`

**Example3:** :doc:`mixed SDS-CTAB <example3>`


Usage patterns
--------------

The main script ``shape.py`` accepts a number of options (or flags) and variable arguments
which determine its function, inputs and outputs. The user is advised to refer to the
:doc:`Usage page <shape> or check the output of ``shape.py --help`` command.

When ``shape.py`` is invoked from the command line (i.e. in a terminal window) it takes
exactly one input file (see below) and produces a single output file with a generated
molecular configuration. The script allows for three main usage patterns:

1. Generating 3D molecule configuration(s) (i.e. atomic X,Y,Z coordinates), given a (set of)
   valid SMILES string(s) found in an ``.sml`` file as input. Depending of the user-specified
   output file extension, the coordinate are stored in ether ``.gro`` or ``.pdb`` format
   (recently DL_POLY and DL_MESO formats have been added, which are still experimental).

2. Generating a molecular aggregate in one of the shapes listed above, starting with a (set of)
   single (template) molecule configurations read-in from a ``.gro`` or ``.pdb`` input file.
   (Recently the support of DL_POLY and DL_MESO formatted inputs has been added, still
   experimental). The available output formats are as above.

3. Replicating and placing molecular aggregates (from input) in a 3D lattice arrangement
   (cubic, FCC or hexagonal). The available input and output formats are as above.


File naming convention
----------------------

``shape.py`` provides default values for most of its options and input parameters which
are designated by the asterisk (*) symbol next to the default values in the output of
``shape.py --help``. In relation to the non-default parameter values, ``shape.py`` adopts
the following file naming convention:

- Default values, where available, are never reflected in the output file names!

- If the user provided a full name for the output file (option ``-o`` or ``--out``)
  which ends with a recognised 4-letter extension (e.g. ``.gro`` or ``.pdb``), **and**
  option ``-x`` (or ``-ext``) is not present in the command, the provided file name
  is considered 'fully user-specified' and is not altered, so non-default parameter values
  have no effect on the output file name.

- Otherwise, if option ``-x`` (or its longer version ``--ext``) followed by a recognised
  file extension (e.g. '.gro' or '.pdb') is given, then the string that follows option
  ``-o`` (``--out``) is considered as *base name* for the output, which will be appended
  by a number of suffixes, reflecting any non-default parameter values used.
