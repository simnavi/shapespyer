
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Workflow &#8212; shapespyer 0.1.6 (Beta) documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/nature.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="shape.py" href="shape.html" />
    <link rel="prev" title="Introduction" href="intro.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="shape.html" title="shape.py"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="intro.html" title="Introduction"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">shapespyer 0.1.6 (Beta) documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href=""><strong>Workflow</strong></a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="workflow">
<h1><strong>Workflow</strong><a class="headerlink" href="#workflow" title="Permalink to this headline">¶</a></h1>
<p>Below we describe how the package can be used in practice to prepare, run and analyse
simulations of soft matter systems.</p>
<p>The underlying semi-automated workflow follows a conventional simulation routine and
comprises the following three major stages:</p>
<section id="simulation-setup">
<h2>Simulation setup<a class="headerlink" href="#simulation-setup" title="Permalink to this headline">¶</a></h2>
<p>Preparing a molecular system for MD or MC simulation requires three types of input:</p>
<ul class="simple">
<li><p><em>initial configuration</em> file containing the coordinates of all the species present in
the system as well as the simulation cell specification (3D dimensions or cell matrix);</p></li>
<li><p><em>topology</em> and <em>force-field</em> input files (sometimes combined into a single file) providing
a detailed description of the intra-molecular structure(s) and standardised interactions
between all the particles (atoms or CG beads) plus other possible force contributions such
as external or restraining fields;</p></li>
<li><p><em>simulation control</em> file that specifies thermodynamic conditions and other simulation
parameters, such as the temporal length and the number of MD steps, any restraints on
atomic positions and so on.</p></li>
</ul>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>At this stage the primary objective of the <strong>Shapespyer</strong> package is to facilitate creation
of a number of predetermined, well-shaped molecular arrangements such as rings (discs), rods
(stacks), micelles (single-layer balls), vesicles (multi-layer balls) and lattice structures.
The generated molecular coordinates are stored into an ‘initial’ configuration file to seed
a simulation.</p>
<p>Although template topology, force-field and simulation control files for simple surfactants
(e.g. SDS and DTAB/CTAB) are provided, in general, the task of collecting these files is left for
the user! Most often they can be found in the databases supplied with general-purpose MD packages,
or alternatively they can be generated with the aid of many other popular toolchains.</p>
</div>
<section id="generating-the-initial-structure-with-shape-py">
<h3>Generating the initial structure with <strong>shape.py</strong><a class="headerlink" href="#generating-the-initial-structure-with-shape-py" title="Permalink to this headline">¶</a></h3>
<p>The <strong>Shapespyer</strong> workflow requires only one input file to start with - an input coordinate
file. The data in this file should be ordered so that the distinct (i.e. different) molecules
are either present only once or grouped together, which is a common formatting convention with
MD and MC packages, e.g. Gromacs.</p>
<p>Provided the initial configuration file (<em>.gro</em> or <em>.pdb</em>) containing coordinates of
the species of interest, one can use the script called <strong>shape.py</strong> to generate
a molecular aggregate of a specified shape and size - for all the options and arguments
of the script see the <a class="reference internal" href="shape.html"><span class="doc">Usage page</span></a>.</p>
<p>The script takes the template molecules’ coordinates from the input file, say <code class="docutils literal notranslate"><span class="pre">config_inp.gro</span></code>,
and, based on the user specified options, creates an output file, say <code class="docutils literal notranslate"><span class="pre">config_out.gro</span></code>,
with coordinates for a molecular structure in one of the following shapes:</p>
<ul class="simple">
<li><p><strong>ring</strong> : a planar disk-like configuration with a circular arrangement of molecules around
the centre (located at the origin)</p></li>
<li><p><strong>ball</strong> : a spherically shaped molecular arrangement with a small void in the centre (to avoid
particle clashes in the inner region); this shape corresponds to a single-layered micellar structure
characteristic of surfactants, where <em>hydrophobic (hydrocarbon dominated) tails</em> normally point
towards the centre and <em>hydrophilic head-groups</em> stick out towards the exterior</p></li>
<li><p><strong>vesicle</strong> : also a spherically shaped assembly but with molecules arranged in a multi-layer
structure around the centre; again, the <em>hydrophobic tails</em> of molecules are supposed to be hidden
within the interior, effectively screened from the aqueous solution by the <em>hydrophilic heads</em>
to form both internal and external interfaces</p></li>
<li><p><strong>rod</strong> : a cylindrically shaped stack of rings finished with semi-spherical caps at the bottom
and at the top - the cylinder is aligned along the Z axis; this molecular arrangement can seed
simulations of larger <em>rigid-rod</em> or <em>worm-like</em> micellar structures typically formed in highly
concentrated solutions of surfactants</p></li>
<li><p><strong>lattice</strong> : molecules or whole molecular complexes (of the above shapes) are placed in the nodes
of a 3D lattice - either cubic or hexagonal, which is useful for preparing homogeneous or
bicontinuous phases</p></li>
</ul>
<p><strong>NOTE:</strong> An extended set of less common or more intricate structural units can be added and
made available on user’s request (for example, ‘conical rod’ shapes, fused ‘bi-vesicles’,
bicontinuous phases etc).</p>
<p>To learn how to use the script in practice the user is referred to a few simple but instructive
<a class="reference internal" href="examples.html"><span class="doc">Examples</span></a>.</p>
</section>
<section id="adding-small-ionic-species-and-water">
<h3>Adding small ionic species and water<a class="headerlink" href="#adding-small-ionic-species-and-water" title="Permalink to this headline">¶</a></h3>
<p>After the initial structure has been prepared, it is usually necessary to add free ions
(counterions/salt) and water molecules to fill in empty space in the simulation box.
This can be done with the aid of the following scripts (found in the directory called <code class="docutils literal notranslate"><span class="pre">scripts</span></code>).</p>
<ul class="simple">
<li><p><strong>gmx-add-ions-solv.bsh</strong> : this helper script allows to add a specified number of ions of
a particular type (by default specs are taken from file <code class="docutils literal notranslate"><span class="pre">single-ion.gro</span></code>) plus, optionally,
the necessary amount of water; one has to run the script a few times if several different
ionic species need to be added before the solvation step.
Examples of its usage are given below (in the first case we add 1380 ions of a default type):</p></li>
</ul>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ gmx-add-ions-solv.bsh config_out.gro 1380
$ gmx-add-ions-solv.bsh config_out.gro 2130 NA-single-ion.gro
$ gmx-add-ions-solv.bsh config_out.gro 2130 CL-single-ion.gro solvate
</pre></div>
</div>
<ul class="simple">
<li><p><strong>gmx-setup-equil.bsh</strong> : this is the more general script that uses <strong>gmx-add-ions-solv.bsh</strong>
and can add both counterions and salt pairs in a single round, finishing by adding water into
the system. Below we provide the help page for this script:</p></li>
</ul>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ gmx-setup-equil.bsh -h

- The script generates Gromacs input based on an initial configuration file -

=============
Main usage:
=============

&gt; gmx-setup-equil.bsh -h
&gt; gmx-setup-equil.bsh --help
&gt; gmx-setup-equil.bsh &lt;config_name&gt; &lt;solute_name&gt; &lt;solute_atoms&gt; [c-ion_name] [salt_pairs [salt_ion1 salt_ion2]]

-----------------------
mandatory parameters:
-----------------------
config_name  - the initial configuration file name (must be found in the current directory)
solute_name  - the primary solute molecule name    (must be found in the topology database)
solute_atoms - the number of atoms per solute molecule

----------------------
optional parameters:
----------------------
c-ion_name   - the primary solute counterion name  (must be found in the topology database)
salt_pairs   - the number of salt pairs to add = the number of solute molecules if -1 given
salt_ion1    - the positive salt ion name (must be found in the topology database; def: NAT)
salt_ion2    - the negative salt ion name (must be found in the topology database; def: CLA)
</pre></div>
</div>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>The script will also create the template topology file, e.g. <code class="docutils literal notranslate"><span class="pre">&lt;task_name&gt;.top</span></code>, specific
for the system thereby created. If all the necessary topology and force-field files (<em>.itp</em>)
are already present in the directory <code class="docutils literal notranslate"><span class="pre">tops</span></code> and the file with simulation parameters, e.g.
&lt;task_name&gt;.mdp is also present in the current (working) directory, the setting up stage
will be accomplished by this time; otherwise a waring message will be printed advising the
user to provide the missing input files.</p>
</div>
</section>
</section>
<section id="equilibration-stage">
<h2>Equilibration stage<a class="headerlink" href="#equilibration-stage" title="Permalink to this headline">¶</a></h2>
<p>When all the input files are ready, one can start an equilibration simulation by invoking the
script called <strong>gmx-equilibrate.bsh</strong>. Note that for a successful launch all the necessary
topology/force-field (<em>.itp</em>) files must be found in the directory called <code class="docutils literal notranslate"><span class="pre">tops</span></code>, whereas
the configuration and simulation parameter files must be located in the current (working)
directory from where the simulation is launched.</p>
<p>Below is the help page for the script:</p>
<div class="highlight-text notranslate"><div class="highlight"><pre><span></span>$ gmx-equilibrate.bsh -h

- The script runs Gromacs with input based on initial .gro, .top and .mdp files -

=============
Main usage:
=============

&gt; gmx-equilibrate.bsh -h
&gt; gmx-equilibrate.bsh --help
&gt; gmx-equilibrate.bsh [-nohup] &lt;task_name&gt; &lt;stage#1&gt; [&lt;stage#2&gt; &lt;np_tot&gt; &lt;stage_name&gt;]

-----------------------
optional switch -nohup
-----------------------

launching terminal-non-binding command &#39;nohup gmx-equilibrate.bsh &lt;args&gt; &amp;&#39;

-----------------------
mandatory parameters:
-----------------------
task_name  - the base file name (the prefix before file extensions)
stage#1/#2 - the stage index or two indeces (initial and final; at least one required)

----------------------
optional parameters: in cases where exactly 3 mandatory arguments are also given
----------------------

np_tot     - the total number of threads (combining OpenMPI and OpenMP if possible) [8]
stage_name - generic stage suffix to add to task_name [&#39;eq&#39;]
</pre></div>
</div>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Stage with index <code class="docutils literal notranslate"><span class="pre">0</span></code> must always correspond to energy minimisation of the initial configuration
in order to remove any possible clashes (VdW overlaps) of particles, which otherwise would result
in an abrupt termination of the entire simulation. What sort of equilibration stages follow after
the energy minimisation stage, e.g. equilibration in <em>NVT</em> / <em>NpT</em> ensembles with specific
thermostat/barostat or additional restraints applied, is fully defined by the simulation
parameters provided in the corresponding control files (<em>.mdp</em>).</p>
</div>
</section>
<section id="post-processing-analyses">
<h2>Post-processing / analyses<a class="headerlink" href="#post-processing-analyses" title="Permalink to this headline">¶</a></h2>
<p>The following scripts are provided to aid the post-simulation analyses of the generated simulation
trajectories (see the contents of subdirectory <code class="docutils literal notranslate"><span class="pre">scripts</span></code>).</p>
<ul class="simple">
<li><p><strong>ana-solvent.py3</strong></p></li>
<li><p><strong>gmx-ana-solvation.bsh</strong></p></li>
<li><p><strong>gmx-ana-gyration.bsh</strong></p></li>
<li><p><strong>gmx-ana-clusters.bsh</strong></p></li>
<li><p><strong>gmx-center-cluster.bsh</strong></p></li>
</ul>
<p>To familiarise with the scripts, users are advised to check their help pages!</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#"><strong>Workflow</strong></a><ul>
<li><a class="reference internal" href="#simulation-setup">Simulation setup</a><ul>
<li><a class="reference internal" href="#generating-the-initial-structure-with-shape-py">Generating the initial structure with <strong>shape.py</strong></a></li>
<li><a class="reference internal" href="#adding-small-ionic-species-and-water">Adding small ionic species and water</a></li>
</ul>
</li>
<li><a class="reference internal" href="#equilibration-stage">Equilibration stage</a></li>
<li><a class="reference internal" href="#post-processing-analyses">Post-processing / analyses</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro.html"
                          title="previous chapter"><strong>Introduction</strong></a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="shape.html"
                          title="next chapter"><strong>shape.py</strong></a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/workflow.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="shape.html" title="shape.py"
             >next</a> |</li>
        <li class="right" >
          <a href="intro.html" title="Introduction"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">shapespyer 0.1.6 (Beta) documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href=""><strong>Workflow</strong></a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2020-2024, Dr Andrey Brukhno.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.4.0.
    </div>
  </body>
</html>