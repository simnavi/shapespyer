**Workflow**
============

Below we describe how the package can be used in practice to prepare, run and analyse
simulations of soft matter systems.

The underlying semi-automated workflow follows a conventional simulation routine and
comprises the following three major stages:

Simulation setup
----------------

Preparing a molecular system for MD or MC simulation requires three types of input:

- *initial configuration* file containing the coordinates of all the species present in
  the system as well as the simulation cell specification (3D dimensions or cell matrix);

- *topology* and *force-field* input files (sometimes combined into a single file) providing
  a detailed description of the intra-molecular structure(s) and standardised interactions
  between all the particles (atoms or CG beads) plus other possible force contributions such
  as external or restraining fields;

- *simulation control* file that specifies thermodynamic conditions and other simulation
  parameters, such as the temporal length and the number of MD steps, any restraints on
  atomic positions and so on.

.. note::
    At this stage the primary objective of the **Shapespyer** package is to facilitate creation
    of a number of predetermined, well-shaped molecular arrangements such as rings (discs), rods
    (stacks), micelles (single-layer balls), vesicles (multi-layer balls) and lattice structures.
    The generated molecular coordinates are stored into an 'initial' configuration file to seed
    a simulation.

    Although template topology, force-field and simulation control files for simple surfactants
    (e.g. SDS and DTAB/CTAB) are provided, in general, the task of collecting these files is left for
    the user! Most often they can be found in the databases supplied with general-purpose MD packages,
    or alternatively they can be generated with the aid of many other popular toolchains.

Generating the initial structure with **shape.py**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **Shapespyer** workflow requires only one input file to start with - an input coordinate
file. The data in this file should be ordered so that the distinct (i.e. different) molecules
are either present only once or grouped together, which is a common formatting convention with
MD and MC packages, e.g. Gromacs.

Provided the initial configuration file (*.gro* or *.pdb*) containing coordinates of
the species of interest, one can use the script called **shape.py** to generate
a molecular aggregate of a specified shape and size - for all the options and arguments
of the script see the :doc:`Usage page <shape>`.

The script takes the template molecules' coordinates from the input file, say ``config_inp.gro``,
and, based on the user specified options, creates an output file, say ``config_out.gro``,
with coordinates for a molecular structure in one of the following shapes:

- **ring** : a planar disk-like configuration with a circular arrangement of molecules around
  the centre (located at the origin)

- **ball** : a spherically shaped molecular arrangement with a small void in the centre (to avoid
  particle clashes in the inner region); this shape corresponds to a single-layered micellar structure
  characteristic of surfactants, where *hydrophobic (hydrocarbon dominated) tails* normally point
  towards the centre and *hydrophilic head-groups* stick out towards the exterior

- **vesicle** : also a spherically shaped assembly but with molecules arranged in a multi-layer
  structure around the centre; again, the *hydrophobic tails* of molecules are supposed to be hidden
  within the interior, effectively screened from the aqueous solution by the *hydrophilic heads*
  to form both internal and external interfaces

- **rod** : a cylindrically shaped stack of rings finished with semi-spherical caps at the bottom
  and at the top - the cylinder is aligned along the Z axis; this molecular arrangement can seed
  simulations of larger *rigid-rod* or *worm-like* micellar structures typically formed in highly
  concentrated solutions of surfactants

- **lattice** : molecules or whole molecular complexes (of the above shapes) are placed in the nodes
  of a 3D lattice - either cubic or hexagonal, which is useful for preparing homogeneous or
  bicontinuous phases

**NOTE:** An extended set of less common or more intricate structural units can be added and
made available on user's request (for example, 'conical rod' shapes, fused 'bi-vesicles',
bicontinuous phases etc).

To learn how to use the script in practice the user is referred to a few simple but instructive
:doc:`Examples <examples>`.

Adding small ionic species and water
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

After the initial structure has been prepared, it is usually necessary to add free ions
(counterions/salt) and water molecules to fill in empty space in the simulation box.
This can be done with the aid of the following scripts (found in the directory called ``scripts``).

- **gmx-add-ions-solv.bsh** : this helper script allows to add a specified number of ions of
  a particular type (by default specs are taken from file ``single-ion.gro``) plus, optionally,
  the necessary amount of water; one has to run the script a few times if several different
  ionic species need to be added before the solvation step.
  Examples of its usage are given below (in the first case we add 1380 ions of a default type):

.. code-block:: text

    $ gmx-add-ions-solv.bsh config_out.gro 1380
    $ gmx-add-ions-solv.bsh config_out.gro 2130 NA-single-ion.gro
    $ gmx-add-ions-solv.bsh config_out.gro 2130 CL-single-ion.gro solvate

- **gmx-setup-equil.bsh** : this is the more general script that uses **gmx-add-ions-solv.bsh**
  and can add both counterions and salt pairs in a single round, finishing by adding water into
  the system. Below we provide the help page for this script:

.. code-block:: text

    $ gmx-setup-equil.bsh -h

    - The script generates Gromacs input based on an initial configuration file -

    =============
    Main usage:
    =============

    > gmx-setup-equil.bsh -h
    > gmx-setup-equil.bsh --help
    > gmx-setup-equil.bsh <config_name> <solute_name> <solute_atoms> [c-ion_name] [salt_pairs [salt_ion1 salt_ion2]]

    -----------------------
    mandatory parameters:
    -----------------------
    config_name  - the initial configuration file name (must be found in the current directory)
    solute_name  - the primary solute molecule name    (must be found in the topology database)
    solute_atoms - the number of atoms per solute molecule

    ----------------------
    optional parameters:
    ----------------------
    c-ion_name   - the primary solute counterion name  (must be found in the topology database)
    salt_pairs   - the number of salt pairs to add = the number of solute molecules if -1 given
    salt_ion1    - the positive salt ion name (must be found in the topology database; def: NAT)
    salt_ion2    - the negative salt ion name (must be found in the topology database; def: CLA)


.. note::
    The script will also create the template topology file, e.g. ``<task_name>.top``, specific
    for the system thereby created. If all the necessary topology and force-field files (*.itp*)
    are already present in the directory ``tops`` and the file with simulation parameters, e.g.
    <task_name>.mdp is also present in the current (working) directory, the setting up stage
    will be accomplished by this time; otherwise a waring message will be printed advising the
    user to provide the missing input files.

Equilibration stage
-------------------

When all the input files are ready, one can start an equilibration simulation by invoking the
script called **gmx-equilibrate.bsh**. Note that for a successful launch all the necessary
topology/force-field (*.itp*) files must be found in the directory called ``tops``, whereas
the configuration and simulation parameter files must be located in the current (working)
directory from where the simulation is launched.

Below is the help page for the script:

.. code-block:: text

    $ gmx-equilibrate.bsh -h

    - The script runs Gromacs with input based on initial .gro, .top and .mdp files -

    =============
    Main usage:
    =============

    > gmx-equilibrate.bsh -h
    > gmx-equilibrate.bsh --help
    > gmx-equilibrate.bsh [-nohup] <task_name> <stage#1> [<stage#2> <np_tot> <stage_name>]

    -----------------------
    optional switch -nohup
    -----------------------

    launching terminal-non-binding command 'nohup gmx-equilibrate.bsh <args> &'

    -----------------------
    mandatory parameters:
    -----------------------
    task_name  - the base file name (the prefix before file extensions)
    stage#1/#2 - the stage index or two indeces (initial and final; at least one required)

    ----------------------
    optional parameters: in cases where exactly 3 mandatory arguments are also given
    ----------------------

    np_tot     - the total number of threads (combining OpenMPI and OpenMP if possible) [8]
    stage_name - generic stage suffix to add to task_name ['eq']

.. note::
    Stage with index ``0`` must always correspond to energy minimisation of the initial configuration
    in order to remove any possible clashes (VdW overlaps) of particles, which otherwise would result
    in an abrupt termination of the entire simulation. What sort of equilibration stages follow after
    the energy minimisation stage, e.g. equilibration in *NVT* / *NpT* ensembles with specific
    thermostat/barostat or additional restraints applied, is fully defined by the simulation
    parameters provided in the corresponding control files (*.mdp*).


Post-processing / analyses
--------------------------

The following scripts are provided to aid the post-simulation analyses of the generated simulation
trajectories (see the contents of subdirectory ``scripts``).

- **ana-solvent.py3**

- **gmx-ana-solvation.bsh**

- **gmx-ana-gyration.bsh**

- **gmx-ana-clusters.bsh**

- **gmx-center-cluster.bsh**

To familiarise with the scripts, users are advised to check their help pages!
