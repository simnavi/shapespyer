.. _SDS:

**Example 1: SDS**
==================

First, change to ``shapespyer/examples/SDS``. If starting from ``shapespyer`` directory:

.. code-block:: text

  $ cd examples/SDS
  $ ls inp
  config_C12SO4_smiles.gro  config_SDS_smiles.gro  config_SO4C12_smiles.gro  config_SO4C12_smiles_az.gro  
  SDS-single1.gro  SDS-single2.gro  smiles_SDS.sml


A few sample coordinate files are provided (in Gromacs format) as well as an ``*.sml`` file containing 
a couple of alternative representations of SDS in SMILES format. The latter can be used as input for 
``shape.py`` script to generate an *idealised* configuration of the molecule (see below).

To check the contents of, say, ``inp/SDS-single1.gro``:

.. code-block:: text

  $ cat inp/SDS-single1.gro
  SDS single molecule (sample 1)
  42
    1SDS      S    1   5.130   5.590   3.097
    1SDS    OS1    2   5.004   5.494   3.077
    1SDS    OS2    3   5.230   5.495   3.139
    1SDS    OS3    4   5.095   5.684   3.199
    1SDS    OS4    5   5.162   5.653   2.967
    1SDS     C1    6   4.890   5.493   3.165
    1SDS    H11    7   4.798   5.497   3.101
    1SDS    H12    8   4.885   5.582   3.229
    1SDS     C2    9   4.873   5.366   3.257
    1SDS    H21   10   4.877   5.275   3.193
    1SDS    H22   11   4.958   5.360   3.326
    1SDS     C3   12   4.740   5.370   3.341
    1SDS    H31   13   4.656   5.377   3.270
    1SDS    H32   14   4.741   5.465   3.399
    1SDS     C4   15   4.703   5.256   3.443
    1SDS    H41   16   4.734   5.157   3.404
    1SDS    H42   17   4.756   5.273   3.539
    1SDS     C5   18   4.550   5.251   3.473
    1SDS    H51   19   4.494   5.257   3.377
    1SDS    H52   20   4.525   5.344   3.528
    1SDS     C6   21   4.488   5.131   3.550
    1SDS    H61   22   4.496   5.147   3.660
    1SDS    H62   23   4.378   5.133   3.532
    1SDS     C7   24   4.538   4.988   3.516
    1SDS    H71   25   4.581   4.984   3.414
    1SDS    H72   26   4.447   4.923   3.515
    1SDS     C8   27   4.636   4.929   3.620
    1SDS    H81   28   4.737   4.969   3.600
    1SDS    H82   29   4.600   4.968   3.716
    1SDS     C9   30   4.638   4.777   3.640
    1SDS    H91   31   4.555   4.736   3.580
    1SDS    H92   32   4.732   4.735   3.599
    1SDS    C10   33   4.616   4.722   3.784
    1SDS   H101   34   4.514   4.752   3.813
    1SDS   H102   35   4.613   4.612   3.781
    1SDS    C11   36   4.706   4.766   3.902
    1SDS   H111   37   4.752   4.863   3.883
    1SDS   H112   38   4.633   4.785   3.983
    1SDS    C12   39   4.811   4.672   3.963
    1SDS   H121   40   4.800   4.567   3.935
    1SDS   H122   41   4.806   4.677   4.073
    1SDS   H123   42   4.913   4.704   3.935
   8.24000   8.24000   8.24000


.. note::
  User is advised to check both initial (input) and final (output) 
  molecular structures (.gro, .xyz, .pdb) visually with the aid of a graphical 
  visualisation software such as **VMD** (https://www.ks.uiuc.edu/Research/vmd/).


0. 3D configuration from a SMILES string
----------------------------------------

In order to generate the 3D chemical structure (coordinates) of a molecule,
taking a particular SMILES string from an ``.sml`` file, user has to check 
the contents of the file (``smiles_SDS.sml`` in this case) and note the name 
or the chemical formula for the chosen SMILES string:

.. code-block:: text

  C12SO4   SDS   CCCCCCCCCCCCOS(=O)(=O)[O-]
  ^^^^^^   ^^^   ^^^^^^^^^^^^^^^^^^^^^^^^^^
  chem.f.  name  SMILES string defining the route for coordinates' generation

Then user can generate the molecule 3D coordinates by entering the following 
command from within the current directory:

.. code-block:: text

  $ shape.py --dio=inp,. -i smiles_SDS.sml -o config -x .gro --shape='smiles' -r SDS > shape_smiles_for_SDS.log

In this case the fist SMILES string with name SDS is used, which corresponds to formula 'C12SO4'.

Alternatively, user could pick up a specific SMILES string corresponding to a specific chemical formula 
found in ``inp/smiles_SDS.sml``:

.. code-block:: text

  $ shape.py --dio=inp,. -i smiles_SDS.sml -o config -x .gro --shape='smiles' -r 'C12SO4' > shape_smiles_for_C12SO4.log
  $ shape.py --dio=inp,. -i smiles_SDS.sml -o config -x .gro --shape='smiles' -r 'SO4C12' > shape_smiles_for_SO4C12.log

In order to align the generated molecule along the OZ axis, one can use option ``--alignz``:

.. code-block:: text

  $ shape.py --dio=inp,. -i smiles_SDS.sml -o config -x .gro --shape='smiles' -r SO4C12 --alignz > shape_smiles_for_SO4C12_az.log

In each case the resulting coordinate file is saved in the current directory (``./``):

.. code-block:: text

  $ ls *smiles*
  shape_smiles_for_SDS.log  config_SDS_smiles.gro
  shape_smiles_for_SO4C12.log  config_SO4C12_smiles.gro 
  shape_smiles_for_SO4C12_az.log  config_SO4C12_smiles_az.gro

.. note::
  The canonical SMILES string for SDS starts with the terminal CH3 group, whereas the Gromacs topology file 
  'SDS.itp' used below follows a different convention - indexing atoms starting with S-atom. Therefore, 
  below we are using 'config_SO4C12_smiles.gro' generated for 'SO4C12' SMILES string.

.. note::
  Another caveat about the Gromacs topology file 'SDS.itp' is that the O-atoms ordering in the sulfate (SO4) group 
  is still different, even based on the SMILES string 'SO4C12': the O-atom connecting to the hydrocarbon tail is 
  the 4-th within the SO4 group in SMILES, whereas it is the first O-atom in 'SDS.itp' topology file. Therefore, 
  for the simulations below to proceed well, it is *crucial to swap the two lines for the corresponfing O-atoms!*

.. code-block:: text

  $ printf '%s\n' 4m7 7-m4- w q | ed -s config_SO4C12_smiles.gro
  $ printf '%s\n' 4m7 7-m4- w q | ed -s config_SO4C12_smiles_az.gro

It is advisable to store the newly created coordinate files in a separate directory to possibly use them later on:

.. code-block:: text

  $ mkdir outsml
  $ mv *smiles* outsml/


1. Generating a ring
--------------------

*Ring* is the simplest molecular arrangement that ``shape.py`` script can generate. Rings serve also as 
the main '*building blocks*' when creating other assemblies, such as spheres, cylinders and vesicles.
Therefore, it is important to master shaping up rings to perfection.

The following command will take file ``inp/SDS-single1.gro`` as input, create a molecular ring
structure and store the output file with the coordinates in subdirectory ``out``.

.. code-block:: text

  $ shape.py --dio=inp,out -i SDS-single1.gro -o config1 -x .gro -s ring -r SDS -l 16 > shape_ring_for_SDS-single1_l16.log

Here we use the following options:

**--dio** option specifies two directories (folders) where to (i) find input configuration files
and (o) store output configuration files;

**-i** provides the name of the input file with the template coordinates of an SDS molecule;

**-o** and **-x** define the *base-name* and *extension* of the output file that
will contain the coordinates of all the generated molecules placed in the requested (ring)
configuration;

**-s** stands for the *shape* specification, which takes **ring** as an argument in this case;

**-r** stands for the *residue name(s)* to be found and matched in the input configuration file
(**SDS** in this case);

**-l** takes a whole number as an argument, specifying the number of molecules in the ring.

By default, the command given above will throw some information on the screen, related
to the progress made by the script (plus error messages if any), which might clutter the terminal
view. It is, however, easy to redirect this output to a *log file* by appending the command with
'**2&>1 > shape.log**' instruction (both **stdout** and **stderr** outputs will be flushed into
**shape.log** file).

The generated structure (``out/config1_SDS_ring_lr16.gro``) can be checked in a graphical
MD visualisation software like **VMD**. Below we show its two views.

a) In the XY plane (from above):

.. image:: images/SDS-ring1-N16.png
  :width: 300
  :alt: SDS ring configuration - XY in plane view

b) Along the Z axiz:

.. image:: images/SDS-ring1-N16-z0.png
  :width: 300
  :alt: SDS ring configuration viewed along Z axiz

Compare this configuration with the one generated for the second SDS sample input:

.. code-block:: text

  $ shape.py --dio=inp,out -i SDS-single2.gro -o config2 -x .gro --shape='ring' -r SDS -l 16 > shape_ring_for_SDS-single2_l16.log

a) In the XY plane (from above):

.. image:: images/SDS-ring2-N16.png
  :width: 300
  :alt: SDS ring configuration 2 - XY in plane view

b) Along the Z axiz:

.. image:: images/SDS-ring2-N16-z0.png
  :width: 300
  :alt: SDS ring configuration 2 viewed along Z axiz

.. note::
  If the original molecular configuration is not straight, the resulting 
  structure will bear its 'orientation footprint'.

There is, however, option ``--fxz`` that allows for automatically flattening the ring(s) by minimizing 
the molecules' thickness in Z dimension, even when the original molecular configuration is not straight:

.. code-block:: text

  $ shape.py --dio=inp,out -i SDS-single1.gro -o config1 -x .gro --shape='ring' -r SDS -l 16 --fxz > shape_ring_for_SDS-single1_l16_fxz.log
  $ shape.py --dio=inp,out -i SDS-single2.gro -o config2 -x .gro --shape='ring' -r SDS -l 16 --fxz > shape_ring_for_SDS-single2_l16_fxz.log

a) Sample 1 along the Z axiz:

.. image:: images/SDS-ring1-N16-fxz-z0.png
  :width: 300
  :alt: SDS ring configuration 3 viewed along Z axiz

b) Sample2 along the Z axiz:

.. image:: images/SDS-ring2-N16-fxz-z0.png
  :width: 300
  :alt: SDS ring configuration 3 viewed along Z axiz

Compare the previous configurations with the outcome of the following command:

.. code-block:: text

  $ shape.py --dio=inp,out -i config_SO4C12_smiles_az.gro -o configS -x .gro --shape='ring' -r SDS -l 16 --mint=41 --mext=0 > shape_ring_for_SO4C12-smiles_lr16_mint41.log

a) In the XY plane (from above):

.. image:: images/SDS-ringS-N16.png
  :width: 300
  :alt: SDS ring configuration 3 - XY in plane view

b) Along the Z axiz:

.. image:: images/SDS-ringS-N16-z0.png
  :width: 300
  :alt: SDS ring configuration 3 viewed along Z axiz

In the latter case we used the perfectly straight configuration generated 
from SMILES starting with the head S-atom (index 0) and finishing with 
the end H-atom (index 41), so that the vector connecting them is almost 
perfectly aligned with the backbone (so we did not need to use ``--fxz`` option).

.. note::
  The two extra options in the latter command (``--mint=41 --mext=0``) allow for 
  specifying the indices of the two atoms that define the molecule's 'bone' vector 
  bridging between the 'interior' and 'exterior' surfaces of the generated structure. 
  This convention adopted in Shapespyer enables additional flexibility for fine 
  tuning the resulting molecular shape(s).

.. note::

  By default, Shapespyer uses the following 'bone' indices for SDS: ``--mint=38 --mext=0``, 
  starting with 0 for 'S' atom and 38 for the last heavy atom, i.e. 'C' in the terminal 
  'CH3' group. User is advised to always check the indexing convention in the input files 
  at hand and, if necessary, redefine the 'bone' vector by specifying ``--mint`` and 
  ``--mext`` explicitly.


2. Generating a ball
--------------------

To generate a spherical micelle, we simply replace the argument ``ring`` for ``ball``. 
Noting that the straightened molecular configuration above allows for placing SDS molecules 
closer to each other around the center, we can use the ``--dmin`` option to reduce 
the minimal distance between the interior 'bone' atoms:

.. code-block:: text

  $ shape.py --dio=inp,out -i config_SO4C12_smiles_az.gro -o configS -x .gro -s ball -r SDS -l 16 --fxz > shapeS-SDS-ball-lr16-fxz.log

Note that here ``-l 16`` corresponds to the *max number of molecules on the equatorial ring* in the ball arrangement.
Since the script does not take in the total number of molecules from the input, some trial and error might be necessary 
to obtain the micelle size close to the desired one. The number of molecules that were generated can be found in 
the last line of the log file - it is 80 in this case. 

The resulting molecular structure ``configS_SDS_ball_lr16_fxz.gro`` is illustrated below.

.. image:: images/SDS80-NAT80-wTIP3-eq0-ini.png
  :width: 300
  :alt: SDS ball configuration


3. Generating a rod
-------------------

Next we generate a *rod* structure - a cylindrically shaped micelle with semi-spherical caps.
This time we use keyword ``rod`` (instead of ``ring`` or ``ball``) and add the number of *turns*
along its length by using an extra option '``-t 10``'. Note that the number of molecules necessary
to form the caps is automatically adjusted to fit the arrangement.

.. code-block:: text

  $ shape.py --dio=inp,out -i config_SO4C12_smiles_az.gro -o configS -x .gro -s rod -r SDS -l 16 -t 10 --fxz > shapeS-SDS-rod-lr16-nt10-fxz.log

The resulting molecular structure ``configS_SDS_rod_lr16_nt10_fxz.gro`` is illustrated below.

.. image:: images/SDS-rodS-N16-dmin4A-fxz.png
  :width: 300
  :alt: SDS rod configuration


4. Staged equilibration for a 'ball'
------------------------------------

In the following exercise we will briefly equilibrate a small SDS micelle ('ball') in aqueous solution and 
perform analyses of (i) the water content inside the micelle and (ii) the radius of gyration of the micelle. 
By doing so we will be able to judge about the efficiency of the equilibration process in terms of the molecular 
structure and the solvation patterns.


4.1 Using *position restraints* to 'support' the initial structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since the initial micelle structure obtained above is, evidently, far from optimum (being rather sparse 
and spread), one has to make sure that it does not get appart due to initially unbalanced internal forces 
and strong interactions with solvent (water) molecules which will be inserted in the system at randomised positions. 
To this end, a few initial stages during equilibration will employ a biasing mechanism with *restraints on atom positions* 
to effectively restrict the diffusion of a chosen set of molecules (SDS) to within a spherical region around 
the origin, i.e. the center of the simulation box where the micelle's center of mass is placed initially. 

The specs for 'position restraints' are given at the end of the force-field file for SDS species (``equil0/toppar/SDS.itp``):

.. code-block:: text

  #ifdef POSRES
  [ position_restraints ]
  ;atom p.form geom. radius f.constant (defined in .mdp)
      1     2    1   2.0   POSRES_FC_LIP
  #endif

In this case the position restraining potential is applied atom-wise to 'S' atoms on SDS molecules and has 
a spherically symmetric 'flat-bottom' form where a harmonic restraining force only acts on an atom if it is 
found too far away from its *reference position*, i.e. outside of a sphere of the user-defined radius (2.0 nm). 
The reference positions for all affected atoms are specified in a separate file: ``equil0/SDS80-NAT80-wTIP3-posres.gro``,
in our case all being placed in the center of the simulation box, i.e. coinciding with the center of the micelle. 
The restraints are activated in the simulation control ``.mdp`` file. For example, check the first line in 
``equil0/toppar/equil1.mdp``:

.. code-block:: text

  define = -DPOSRES -DPOSRES_FC_LIP=200.0

The user is advised to refer to the `Gromacs documentation <https://manual.gromacs.org/current/reference-manual/functions/restraints.html>`_ for more details. 

.. note::
  During the equilibration process the restraining forces are made progressively weaker by halving the force constant 
  (POSRES_FC_LIP) for each consecutive stage, so that no restraints are applied at the final (5-th) stage:

.. code-block:: text

  $ grep DPOSRES equil0/toppar/equil?.mdp
  equil0/toppar/equil0.mdp:;define                  = -DPOSRES -DPOSRES_FC_LIP=1000.0
  equil0/toppar/equil1.mdp:define                  = -DPOSRES -DPOSRES_FC_LIP=200.0
  equil0/toppar/equil2.mdp:define                  = -DPOSRES -DPOSRES_FC_LIP=100.0
  equil0/toppar/equil3.mdp:define                  = -DPOSRES -DPOSRES_FC_LIP=50.0
  equil0/toppar/equil4.mdp:define                  = -DPOSRES -DPOSRES_FC_LIP=20.0


4.2 Setting up the system
~~~~~~~~~~~~~~~~~~~~~~~~~

To start with, copy the ``ball`` structure that was generated earlier to directory ``equil`` to seed 
an equilibration simulation.

.. code-block:: text

  $ ls out/configS*ball*
  configS_SDS_ball_lr16_fxz.gro

  $ mkdir equil
  $ cp out/configS_SDS_ball_lr16_fxz.gro equil/

In order to set up staged equilibration one needs, first, to collect the force-field and topology files for 
each component of the system. For this example all the necessary .itp files are provided in ``equil0/toppar``: 

.. code-block:: text

  $ ls ../equil0/toppar/*itp
  forcefield.itp CLA.itp NAT.itp SDS.itp TIP3.itp

Additionally, for ionic species such as sodium (NAT) and chloride (CLA) ions, trivial .gro files with coordinates 
are necessary:

.. code-block:: text

  $ ls ../equil0/toppar/*gro
  single-ionCLA.gro single-ionNAT.gro

  $ cp -r ../equil0/toppar ./

Now we are ready to prepare the solvated SDS system. For guidance on the usage of the script, check its 'help':

.. code-block:: text

  $ gmx-setup-equil.bsh --help

  - The script generates Gromacs input based on an initial configuration file -

  =============
   Main usage: 
  =============

  > gmx-setup-equil.bsh -h
  > gmx-setup-equil.bsh --help
  > gmx-setup-equil.bsh <config_name> <solute_name> <solute_atoms> [c-ion_name] [salt_pairs [salt_ion1 salt_ion2]]

  -----------------------
   mandatory parameters:
  -----------------------
   config_name  - the initial configuration file name (must be found in the current directory)
   solute_name  - the primary solute molecule name    (must be found in the topology database)
   solute_atoms - the number of atoms per solute molecule

  ----------------------
   optional parameters:
  ----------------------
   c-ion_name   - the primary solute counterion name  (must be found in the topology database)
   salt_pairs   - the number of salt pairs to add = the number of solute molecules if -1 given
   salt_ion1    - the positive salt ion name (must be found in the topology database; def: NAT)
   salt_ion2    - the negative salt ion name (must be found in the topology database; def: CLA)


Thus, the setup with Na+ ions (counter-ions for S-atoms on SDS head groups) and water (flexible TIP3 model) 
can be done as follows (one can check the number of atoms per SDS molecule either in ``SDS.itp`` or ``SDS80.gro``):

.. code-block:: text

  $ gmx-setup-equil.bsh configS_SDS_ball_lr16_fxz.gro SDS 42 NAT

As a result of executing this command, a number of new files will be generated:

.. code-block:: text

  $ ls
  SDS80.gro  SDS80-NAT80.gro  SDS80-NAT80-wspce.gro  SDS80-NAT80-wTIP3.gro SDS80-NAT80-wTIP3.top toppar

Finally, before launching the equilibration runs we need to copy ``.mdp`` files from ``toppar`` to the current directory:

.. code-block:: text

  $ for i in {0..6}; do cp toppar/equil${i}.mdp SDS80-NAT80-wTIP3-eq${i}.mdp; done

  $ ls *mdp
  SDS80-NAT80-wTIP3-eq0.mdp  SDS80-NAT80-wTIP3-eq1.mdp  SDS80-NAT80-wTIP3-eq2.mdp  SDS80-NAT80-wTIP3-eq3.mdp  
  SDS80-NAT80-wTIP3-eq4.mdp  SDS80-NAT80-wTIP3-eq5.mdp  SDS80-NAT80-wTIP3-eq6.mdp

and to accomplish the setup for position restraints:

.. code-block:: text

  $ cp ../eqil0/SDS80-NAT80-wTIP3-posres.gro ./

For the equilibration script to work correctly, we also need to copy the initial ``.gro`` file for the system
under the name corresponding to the 0-th (energy minimization) step:

.. code-block:: text

  $ cp SDS80-NAT80-wTIP3.gro SDS80-NAT80-wTIP3-eq0-ini.gro

Now we have all the files necessary for the 5 stages of a short equilibration round (under one nanosecond). 
It is advisable, however, to backup the initial setup files to be able to restore them if necessary:

.. code-block:: text

  $ mkdir initial
  $ cp * initial


4.3 Launching equilibration, plus sanity checks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Check ``help`` for the equilibration script:

.. code-block:: text

  $ gmx-equilibrate.bsh -h

  - The script runs Gromacs with input based on initial .gro, .top and .mdp files -

  =============
   Main usage: 
  =============

  > gmx-equilibrate.bsh -h
  > gmx-equilibrate.bsh --help
  > gmx-equilibrate.bsh [-nohup] <task_name> <stage#1> [<stage#2> <np_tot> <stage_name>]

  -----------------------
   optional switch -nohup
  -----------------------

   this is one way of launching terminal non-binding command 'nohup gmx-equilibrate.bsh <args> &'

  -----------------------
   mandatory parameters:
  -----------------------
   task_name  - the base file name (the prefix before file extensions)
   stage#1/#2 - the stage index or two indeces (initial and final; at least one required)

  ----------------------
   optional parameters: in cases where exactly 3 mandatory arguments are also given
  ----------------------

   np_tot     - the total number of threads (combining OpenMPI and OpenMP if possible) [8]
   stage_name - generic stage suffix to add to task_name ['eq']

Accordingly, one can launch the first 0-5 stages of equilibration on 16 parallel threads by entering 
the following command (which can take a while, depending on your computer power and memory resources):

.. code-block:: text

  $ gmx-equilibrate.bsh -nohup 'SDS80-NAT80-wTIP3' 0 5 16 'eq'
  nohup: appending output to 'nohup.out'

To check if the simulation has started successfully, use command 'top':

.. code-block:: text

  $ top
  PID    USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM  TIME+ COMMAND 
  732870 user      20   0 2323104 163732  15536 R  1559   0.1  01:03.93 gmx 

Look for the line(s) with 'gmx' in the last column - normally it should appear at the top of the table, 
as one of the currently executed programs with high priority. If it is not found in the table, then 
something went wrong!

To monitor the progress of execution, enter:

.. code-block:: text

  $ tail -l 11 nohup.out; echo
  Using 16 MPI threads
  Using 1 OpenMP thread per tMPI thread

  starting mdrun 'SDS solution with NAT counterions'
  250000 steps,    500.0 ps.
  step 212500, remaining wall clock time:    92 s          vol 0.75  imb F  4% pme/F 0.35 

The output should look similar to the above, hinting on the remaining time for the currently running stage -
it will be updated each time one enters the 'tail' command.

.. note::
  The script will iterate through the equilibration stages by running each stage in the current directory.
  When the simulation for a particular stage is finished, the results will be moved into a subdirectory 
  corresponding to that stage: 'eq0' ... 'eq5'. 

.. note::
  If the simulation is terminated abnormally, the files for the failed stage are still kept in the directory 
  where it started, so that rerunning the equilibration script starting with the last stage should be able to pick 
  it up where it stopped. If the rerun does not go well and also stops, the user is advised to restart that stage 
  from scratch! To do so, the user must make sure that all the initial files in the current directory are restored -
  check for the presence of: <BaseName>-eq#-ini.gro <BaseName>-eq#.top <BaseName>-eq#.mdp and remove any other files 
  for that stage if present.

The simulation runs (with 16 threads) will take about an hour. After these are finished one should find a subdirectory 
tree similar to the reference in ``equil0``.


4.4 Post-simulation analyses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first type of structure anlysis one can do is for the evolution of the radius of gyration and its components, for which 
the user can utilise the script called ``gmx-ana-gyration.bsh``:

.. code-block:: text

  $ gmx-ana-gyration.bsh -h

  - Radius of gyration and its components for solute species based on .gro or .xtc files

  =============
   Main usage:
  =============

  > gmx-ana-gyration.bsh -h
  > gmx-ana-gyration.bsh --help
  > gmx-ana-gyration.bsh <task_name> [<species_index> <config_extension>]

  Example: gmx-ana-gyration.bsh SDS80-NAT80-wTIP3-eq1 2 gro

In order to use it on the results from a particular simulation stage, one has to switch to the corresponding subdirectory, 
for example:

.. code-block:: text

  $ cd eq0
  $ gmx-ana-gyration.bsh SDS80-NAT80-wTIP3-eq0 2 gro

.. note::
  The second argument for the script specifies the index of the species to be analysed as defined in the Gromacs index file (``.ndx``). 
  In most cases solute has index 2 (after [System] and [Other]), which is the default value assumed by the script.

.. note::
  The third argument can be either ``gro``, ``xtc`` or ``trr``. Evidently, only one configuration is analysed with ``gro`` 
  format while the entire simulation trajectory is taken as input in the latter two cases.

Another helper script called ``gmx-ana-gyration-dirs.bsh`` automates this analysis:

.. code-block:: text

  $ gmx-ana-gyration-dirs.bsh -h

  - Radius of gyration and its components for solute species based on .gro or .xtc files
  - Going through directories matching a pattern

  =============
   Main usage:
  =============

  > gmx-ana-gyration-dirs.bsh -h
  > gmx-ana-gyration-dirs.bsh --help
  > gmx-ana-gyration-dirs.bsh <dir_pattern> <task_name> [<species_index> <config_extension>]

  Example: gmx-ana-gyration-dirs.bsh eq? SDS80-NAT80-wTIP3-eq1 2 gro

As the help page suggests, the script will go through all subdirectories matching the given pattern
and call for ``gmx-ana-gyration.bsh`` accordingly. As a result of running either of the scripts over
all the subdirs ``eq?``, we find the following two files generated in each of the subdirectories:

  ``SDS80-NAT80-wTIP3-eq?-GyrRXYZ.xvg`` - the radius of gyration data

  ``SDS80-NAT80-wTIP3-eq?-GyrRXYZ-ACF.xvg`` - the corresponding autocorrelation functions data

Both files can be directly opened and processed in Grace (xmgrace) plotting software.

The radius of gyration evolution through the final configurations from each of the equilibration
stages is illustrated below.

.. image:: images/SDS80-NAT80-wTIP3-eq05-GyrRXYZ.png
  :width: 600
  :alt: Rg equilibration for SDS micelle

We see that the size of the micelle appears to converge rather quickly within just under one
nanosecond of equilibration.

The question remains though: whether the micelle adopts its natural compact structure with very
little water content in its interior? - In order to check this we can employ the solvation analysis
utility called ``ana-solvent.py3``:

.. code-block:: text

  $ ana-solvent.py3 --help

    Extracting solvent molecules bound to within a sphere or a spherical layer.
    Optionally, also extracting solvation shell subset for specific atomic species.

    ======
    Usage:
    ======

    ana-solvent.py3 [-d <dio>] -i <inp> -o <out> -x <ext> -s <solvent> -l <nsatm> -r <rs> [-c <cluster> -a <atoms>]

    --dio=<dio> : in/out directory (optional) {.}
    --box=<box> : input  file with box dimensions: Lx Ly Lz {config.box}
    --inp=<inp> : input  file with system coordinates where solute cluster is centered at origin {config.gro}
    --out=<out> : output file base name, to be automatically extended with appropriate suffices {config}
    --ext=<ext> : output file extension {.gro}

    --solvent=<solvent_name>
                : name of solvent molecules to extract {SOL}
    --nsatm=<n_solvent_atoms>
                : number of atoms per solvent molecule {3}
    --cluster=<solute_names>
                : name(s) of solute(s) forming a cluster (comma-delimited) {N/A}
    --atoms=<atom_names>
                : name(s) of solvated atom(s), one per solute in the cluster (comma-delimited) {N/A}
    --rs=<r_ext[,r_int[,r_shell]>
                : radius/radii of bounding sphere(s) centered at origin {1.0}; if more than one entry given:
                : the first entry must always be the radius of the external bounding sphere,
                : the second entry must be the radius of the internal bounding sphere,
                : the third entry must be the radius of shells centered at atom(s) specified in <atoms>;
                : i.e. it is necessary if at least one entry is given by <cluster> and <atoms> options

In the simplest use case the script finds and extracts all solvent molecules within a sphere of
a given radius (nm) at the center of the simulation box. Therefore, before using it one has to
rearrange the original configuration so that a cluster of solute molecules is placed accordingly.

For the purpose of illustarting the usage of the script, we can run it on the final configuration
of the energy minimisation stage (i.e. in ``eq0``) where the micelle's center of mass deviates
very little from the box center:

.. code-block:: text

  $ ana-solvent.py3 -d eq0 -b SDS80-NAT80-wTIP3-eq0-box.gro -i SDS80-NAT80-wTIP3-eq0.gro -o SDS80-NAT80-wTIP3-eq0 -x .gro -s TIP3 -l 3 -r 1.0

  Input file extension: '.gro'

  Doing: input 'SDS80-NAT80-wTIP3-eq0.gro' => output 'SDS80-NAT80-wTIP3-eq0_TIP3_Rc_1.0nm.gro'

  Reading GRO (box) file 'eq0/SDS80-NAT80-wTIP3-eq0-box.gro' ...
  File 'eq0/SDS80-NAT80-wTIP3-eq0-box.gro' successfully read: nlines = 1

  Reading GRO file 'eq0/SDS80-NAT80-wTIP3-eq0.gro' ...
  Extracting solvent 'TIP3' molecules of 3 atoms within a spherical slice, R = [-1e-12, 1.0] ...

  Title: 'SDS solution with NAT counterions'

  File 'eq0/SDS80-NAT80-wTIP3-eq0.gro' successfully read: nlines = 40375; natms = 252 / 3 = nmols = 84.0; Rc(rest) = 0.0035703150797072123 =?= 0.0035703150797072123

  Writing molecule 1TIP3 into GRO file 'eq0/SDS80-NAT80-wTIP3-eq0_TIP3_Rc_1.0nm.gro' ...
  File 'eq0/SDS80-NAT80-wTIP3-eq0_TIP3_Rc_1.0nm.gro' successfully written: nlines = 254 & natms = 252 / 252

As we see, the script created a new configuration file ``SDS80-NAT80-wTIP3-eq0_TIP3_1.0-nm.gro``
containing only the extracted water molecules. Below we visualise its contents overlayed on top
of the cross-sectional view of SDS micelle:

.. image:: images/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3-rw2.0nm-yh-xz0.png
  :width: 400
  :alt: Water content in the 1 nm interior of SDS micelle

It is more convenient, however, to use another script, called ``gmx-ana-solvation.bsh``,
which automates the job by calling the above script in a few useful scenarios.

.. code-block:: text

  $ gmx-ana-solvation.bsh -h

    - Extracting solvent within a sphere at the origin, with input from .gro file
    - Prior the solvation analysis the input is processed to center the solute cluster

    ============
     Main usage
    ============

    > gmx-ana-solvation.bsh -h
    > gmx-ana-solvation.bsh --help
    > gmx-ana-solvation.bsh <task_name> [<solvent> <natoms> <range>]
    > gmx-ana-solvation.bsh <task_name> [<solvent> <natoms> <range> [<cluster> [<atoms>]]]

    ---------------------
     mandatory parameter
    ---------------------
     task_name  - the base file name excluding file extension(s)

    ---------------------
     optional parameters
    ---------------------
     solvent    - solvent name {SOL}

     natoms     - number of atoms per solvent molecule {3};

     range      - a set of radii specifying the spherical layer(s)
                  and possibly the width of the solvation shell
                  in the following format: 'Rext[,Rint[,dR/Rshl]]'
                  {defaults: Rext = 1.0, Rint = 0.0}

     cluster    - solute name(s) for species forming a cluster (comma-delimited)

     atoms      - atom name(s), one per solute species (comma-delimited)

    ------
    NOTE1: If ony the solvent name is provided, the default solute species
           to be treated as a cluster will be the first one found, which
           corresponds to entry #2 in the <task_name>.ndx file.

    NOTE2: If more than one solute name provided, the joint group for the respective
           solutes must be found in the <task_name>.ndx file and be named as follows:
           '<solute1>_<solute2>' (i.e. the solute names must be joined by underscore).
           In this case all the specified solute species will be considered to form
           a cluster which will be centered at the origin.

    NOTE3: If the atom name(s) NOT given, i.e. solvent and solute names are specified,
           solvent molecules are extracted from within the specified spherical
           layer [Rint, Rext] with the solute cluster's COM placed at the origin;
           In this case 'range' should include either ONLY ONE value (Rext)
           or ALL THREE values, where the last one must be either a (whole) number
           of subdivisions (Nsub) to be considered between Rint and Rext,
           or the radius increment (dR < 1 nm) for stepwise iteration.

    NOTE4: With the atom name(s) given, i.e. the 6-th (last) argument is present,
           solvent molecules are exrtacted from within the solvation shells (Rshell)
           around the atoms with the given name(s) found on the corresponding solute(s).
    ------

    ===============
     Prerequisites
    ===============
     The following files must be found in the current directory:

     <task_name>.mdp / <task_name>_noSOL.mdp (system without solvent)
     <task_name>.top / <task_name>_noSOL.top (system without solvent)
     <task_name>.tpr
     <task_name>.gro
     <task_name>.ndx

    ----------
     Examples
    ----------
    1. Single round of extraction of water molecules bound by sphere of radius 3.0 nm:
       gmx-ana-solvation.bsh SDS80-NAT80-wTIP3-eq1 TIP3 3 3.0

    2. Stepwise extraction of water molecules in spheres of radii in [1.0,3.0] with dR=0.5:
       gmx-ana-solvation.bsh SDS80-NAT80-wTIP3-eq1 TIP3 3 3,1,0.5

    3. Single round of extraction of water in solvation shells within 0.5 nm by C17 atoms
       on SDS species within spherical layer between Rint=1.0 and Rext=3.0 nm:
       gmx-ana-solvation.bsh SDS80-NAT80-wTIP3-eq1 TIP3 3 3,1,0.5 SDS C17

One can automate this routine even further by running the script in a loop over all
the subdirectories ``eq*``:

.. code-block:: text

  $ for i in {0..5}; do cd eq${i}; gmx-ana-solvation.bsh SDS80-NAT80-wTIP3-eq${i} TIP3 3  3.0,0.5,0.25; cd - ; done

This command results in a number of configuration files created in each of the subdirectories,
corresponding to the given cavity sizes. Check it, for example, in ``eq0``:

.. code-block:: text

  $ ls eq0/*_TIP3_*gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_0.5nm.gro   eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_2.0nm.gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_0.75nm.gro  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_2.25nm.gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_1.0nm.gro   eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_2.5nm.gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_1.25nm.gro  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_2.75nm.gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_1.5nm.gro   eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_3.0nm.gro
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc_1.75nm.gro  eq0/SDS80-NAT80-wTIP3-eq0_TIP3_Rc_1.0nm.gro

Additionally, the data for the number of water molecules and the corresponding number of atoms for each Rc have also been collected:

.. code-block:: text

  $ ls eq0/*_TIP3_*dat
  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc-Natms.dat  eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3_Rc-Nmols.dat

  $ cat eq0/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3-Rc-NMOLS.dat
  # Radius of spherical cavity / nm       # Number of solvent molecules 'TIP3' in cavity
  .500    20
  .750    63
  1.000   84
  1.250   99
  1.500   171
  1.750   314
  2.000   560
  2.250   895
  2.500   1359
  2.750   1940
  3.000   2741

The data collected over all the equilibration stages are illustarted in the following graph.

.. image:: images/SDS80-NAT80-wTIP3-eq05-CC-PBC_TIP3-Rc-NMOLS.png
  :width: 600
  :alt: Water content witin SDS micelle (2 nm)

We can also verify visually in VMD that all the water molecules that are initially present within the micelle interior 
at stages ``eq0`` and ``eq1`` are displaced towards its exterior in the final configurations from the later stages.

**eq0:** Water content within 2 nm of the SDS micelle

.. image:: images/SDS80-NAT80-wTIP3-eq0-CC-PBC_TIP3-rw2.0nm-yh-xz.png
  :width: 400
  :alt: Water content within the SDS micelle at stage eq0

**eq1:**

.. image:: images/SDS80-NAT80-wTIP3-eq1-CC-PBC_TIP3-rw2.0nm-yh-xz.png
  :width: 400
  :alt: Water content within the SDS micelle at stage eq1

**eq2:**

.. image:: images/SDS80-NAT80-wTIP3-eq2-CC-PBC_TIP3-rw2.0nm-yh-xz.png
  :width: 400
  :alt: Water content within the SDS micelle at stage eq2

**eq3:**

.. image:: images/SDS80-NAT80-wTIP3-eq3-CC-PBC_TIP3-rw2.0nm-yh-xz.png
  :width: 400
  :alt: Water content within the SDS micelle at stage eq3



