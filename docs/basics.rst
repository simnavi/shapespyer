basics package
==============

..
    Submodules----------

basics.input module
-------------------

.. automodule:: basics.input
    :members:
    :undoc-members:
    :show-inheritance:

..
   Module contents
   ---------------

..
   automodule:: basics    :members:    :undoc-members:    :show-inheritance:
