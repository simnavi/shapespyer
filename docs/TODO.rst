**TO DO list**
==============

0. Unify the code style and benchmarks [ALL]:
---------------------------------------------

0.1 Coding recommendations (to be followed!):
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 - Preferably, use PyCharm IDE (community or educational edition) - helps a lot!
 - CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
 - Hint on method/function return data type(s), same for the interface arguments
 - One empty line between functions/methods & groups of interrelated imports
 - Two empty lines between Classes & after all the imports done
 - Classes and methods/functions must finish with a closing comment: '# end of <its name>'
 - Meaningful DocStrings right after the definition (def) of Class/method/function/module
 - Comments must be meaningful and start with '# ' (hash symbol followed by a space)
 - Insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:
 # AB: This is an insightful comment describing a crucial or difficult to follow piece of code

0.2 Testing and benchmarking (CI/CD):
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 - Unit-tests to be produced from every single case-study included in 'examples' subdirectory
 - Purified (=simplified) versions of these are to be included in 'bench' subdirectory [ALL]
 - Create benchmarks (=unit-tests) as part of the CI/CD merging workflow in the repo [AB - 30 Nov'23]

0.3 Git commits, branches and pull/push rules:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 - In case there is no 'master' branch in your local repository (clone), create it:
   while the 'main' branch is in focus (git checkout main; git checkout -b master)
 - Only pull from / push to the remote origin (GitLab) while the 'master' is in focus
   (git checkout master; git pull origin main / git push origin master)
   NOTE: pulling from 'origin main' to local 'master'
   whereas pushing to 'origin master' from local 'master'
 - All development tasks must be done in local branches (a branch per a single task) 
   and only committed when ready (i.e. when the task is done) to be included in the remote (GitLab)
 - After committing to your local branch (git commit -m 'Commit description'), 
   immediately change to the 'master' branch (git checkout master),
   pull from the remote (git pull), try to merge your branch (git merge 'branch_name'), 
   resolve the popping up conflicts if any, and only then push to the remote origin 
   (git push origin master)
 - After successfully pushing to remote 'origin master', go online (GitLab)
   to create a merge request (master -> main) for the newly pushed commit,
   and then see if it is successful (passed the CI/CD tests and was approved)!

1. Introduce topology features in Molecule class [AB, MD - see specific deadlines below]:
-----------------------------------------------------------------------------------------
 - bonds, angles, dihedrals (torsion angles) [MD ~ 30 Nov'23+]
 - make sure these are unified between Smiles and Molecule classes
 - Mariam: working on topology descriptors (bonds, angles, dihedrals) in stage.protospecies.Molecule()
    - bonds:  [ia, ib, r0, (func, func_pars)]
    - angles: [ia, ib, ic, a0, (func, func_pars)]
    - dihedrals: [ia, iab, ic, id, (func, func_pars)]
 - internal FF: hard spheres for crude MC [AB ~ 30 Nov'23]
 - methods for altering these, based on Crankshaft/Pivot Monte Carlo (MC) [AB ~ Xmas'23]
 - method for a molecule rotation MC move
 - method for a molecule translation MC move
 - methods for regrowth and reptation MC moves?
 - try to make all the above consistent and compatible with MDverse / MDAnalysis package
 
2. Introduce a module / class for coarse-graining [AB,MS ~ Xmas'23+]:
----------------------------------------------------------------------
 - use YAML or Json(?) formatted input file(s)
 - see if XML input could also be supported (for compatibility with DL_CGMAP and VOTCA packages)
 - provide compatibility with DPD models (DL_MESO package)

3. Introduce module(s) / classe(s) for force-fields [AB,MD,CY - 30 Nov'23 + Xmas'23+]:
---------------------------------------------------------------------------
 - create Python API ports (wrapper functions/methods?) to MoSDeF package(s) -> OPLS-AA
 - general force-field generation via calling DL_FIELD (a black-box for now) -> Amber/CHARMM/OPLS
 - DL_FIELD API: atomic 3D XYZ configuration plus FF format -> topology specs plus FF returned
 - DLF force-fields: written into a file of a particular format (*DL_POLY* / Gromacs / NAMD) [CY ~ 31 Jan'23]
 - Shapespyer API: return topology in the above format (bonds, angles, dihedrals)
 - Soft deadline: end of January (for whatever progress)

4. Introduce API porting from/to SasView (SAScalc) [AB - Xmas'23+]:
--------------------------------------------------------------------
 - remains to see what needs to be done (work with JD & TS) ...

5. Provide support for DPD (DL_MESO) workflows [MS - deadline?]:
----------------------------------------------------------------
 - start with scripts for a basic workflow, similar to existing bash-scripts (gmx-*.bsh)
 - Python equivalents of workflow scripts for DPD / DL_MESO:
 - stage.protosims.apiDPD [MS ~ Xmas'23], simMC [AB ~ Xmas'23] / apiDLM [AB ~ 28 Feb'24] 

6. Provide support for MC (DL_MONTE) workflows [AB - deadline?]:
----------------------------------------------------------------
 - link with DLMONTE_Python package via APIs etc
 - use DLMONTE_Python workflows
 - see if we can extend these ...

7. Provide support for NAMD workflows [VL - see below]:
-------------------------------------------------------
 - start with mono- and bi-layer structure generation (see designs/protoshape.py)
 - POPC test with a single molecule: add Charmm-36 native FF (atom names convention)
 - NAMD workflow: psfgen / CgenFF script(s) for generating NAMD topology and force-field files.
 - introduce atom naming pertaining to NAMD / Charmm convention 
 - in the fisrt instance use pasfgen / CgenFF scripts withoitn the workflows like in bash script.
 - add a mono- / bi-layer 2d grids in class Lattice (module desings) for placing molecules onto.
 - Soft deadline for mono- / bi-layer method : Xmas 2023
 - Soft deadline for CgenFF/psfgen scripts in the workflow: end of November 
