.. _CTAB:

**Example 2: CTAB**
===================

First, change to ``shapespyer/examples/CTAB``. If starting from ``shapespyer`` directory:

.. code-block:: text

  $ cd examples/CTAB
  $ ls inp
  config_smiles_CTA.gro  config_smiles_NC13H30.gro  CTA10-single.gro  smiles_CTA10.sml

A few sample coordinate files are provided (in Gromacs format) as well as an ``.sml`` file containing 
a few alternative representations of C(10)TAB in SMILES format. The latter can be used as input for ``shape.py``
script to generate a 'idealised' configuration of the molecule (see below).

To check the contents of, say, ``CTA10-single.gro``:

.. code-block:: text

  $ cat inp/CTA10-single.gro
  C(10)TAB single molecule
  44
      2CTA     N1    1   4.013   3.960   1.148
      2CTA     C1    2   4.140   3.922   1.209
      2CTA    H11    3   4.176   4.000   1.277
      2CTA    H12    4   4.221   3.900   1.142
      2CTA    H13    5   4.126   3.835   1.272
      2CTA     C2    6   4.051   4.091   1.089
      2CTA    H21    7   4.146   4.082   1.037
      2CTA    H22    8   4.067   4.164   1.169
      2CTA    H23    9   3.977   4.129   1.021
      2CTA     C3   10   3.995   3.847   1.052
      2CTA    H31   11   3.977   3.756   1.109
      2CTA    H32   12   4.087   3.829   0.996
      2CTA    H33   13   3.914   3.862   0.984
      2CTA     C4   14   3.881   3.973   1.231
      2CTA    H41   15   3.825   3.881   1.221
      2CTA    H42   16   3.819   4.050   1.187
      2CTA     C5   17   3.888   3.999   1.383
      2CTA    H51   18   3.951   4.087   1.404
      2CTA    H52   19   3.928   3.898   1.409
      2CTA     C6   20   3.764   3.999   1.475
      2CTA    H61   21   3.729   3.894   1.490
      2CTA    H62   22   3.684   4.058   1.428
      2CTA     C7   23   3.793   4.057   1.618
      2CTA    H71   24   3.705   4.119   1.647
      2CTA    H72   25   3.879   4.126   1.614
      2CTA     C8   26   3.814   3.953   1.728
      2CTA    H81   27   3.891   3.878   1.699
      2CTA    H82   28   3.720   3.897   1.744
      2CTA     C9   29   3.852   4.022   1.861
      2CTA    H91   30   3.772   4.096   1.880
      2CTA    H92   31   3.949   4.078   1.850
      2CTA    C10   32   3.861   3.925   1.980
      2CTA   H101   33   3.969   3.896   1.990
      2CTA   H102   34   3.807   3.831   1.953
      2CTA    C11   35   3.798   3.985   2.111
      2CTA   H111   36   3.711   3.923   2.138
      2CTA   H112   37   3.762   4.088   2.090
      2CTA    C12   38   3.899   3.995   2.224
      2CTA   H121   39   3.968   4.072   2.194
      2CTA   H122   40   3.941   3.902   2.191
      2CTA    C19   41   3.888   3.999   2.386
      2CTA   H191   42   3.789   4.049   2.376
      2CTA   H192   43   3.970   4.056   2.341
      2CTA   H193   44   3.912   3.978   2.491
     8.24000   8.24000   8.24000

.. note::
  User is advised to check both initial (input) and final (output) 
  molecular structures (.gro, .xyz, .pdb) visually with the aid of a graphical 
  visualisation software such as **VMD** (https://www.ks.uiuc.edu/Research/vmd/).


0. 3D configuration from a SMILES string
----------------------------------------

In order to generate 3D chemical structure (coordinates) of a molecule,
taking a particular SMILES string from an ``.sml`` file, user has to check 
the contents of the file (``smiles_CTA10.sml`` in this case) and note the name 
or the chemical formula for the chosen SMILES string:

.. code-block:: text
  
  NC13H30  CTA  [N+](C)(C)(C)CCCCCCCCCC
  ^^^^^^^  ^^^  ^^^^^^^^^^^^^^^^^^^^^^^
  chem.f.  name  SMILES string defining the route for coordinates' generation

Then user can generate the molecule 3D coordinates by entering the following 
command from within the current directory:

.. code-block:: text

  $ shape.py --dio=inp,. -i smiles_CTA10.sml -o config -x .gro --shape='smiles' -r 'CTA' > shape_smiles_for_CTAB10.log

In this case the fist SMILES string with name 'CTA' is used.

Alternatively, user could specify a specific SMILES string with the chemical formula:

.. code-block:: text

  $ shape.py --dio=inp,. -i smiles_CTA10.sml -o config -x .gro --shape='smiles' -r 'NC13H30' > shape_smiles_for_NC13H30.log

In each case the resulting coordinate file is saved in the current directory (``./``):

.. code-block:: text

  $ ls *smiles*
  config_smiles_CTA.gro  config_smiles_NC13H30.gro
  shape_smiles_for_CTAB10.log  shape_smiles_for_NC13H30.log

It is advisable to store the newly created coordinate files in a separate directory to possibly use them later on:

.. code-block:: text

  $ mkdir outsml
  $ mv *smiles* outsml/

.. note::
  The canonical SMILES string for CTAB starts with the terminal CH3 group, whereas the Gromacs topology files (.itp)
  follow a different convention - indexing atoms starting with 'N+' atom.


1. Generating a ring
--------------------

*Ring* is the simplest molecular arrangement that ``shape.py`` script can generate. Rings serve also as 
the main '*building blocks*' when creating other assemblies, such as spheres, cylinders and vesicles.
Therefore, it is important to master shaping up rings to perfection.

The following command will take file ``inp/CTA10-single.gro`` as input , create a molecular ring
structure and store the output file with the coordinates in subdirectory ``out``.

.. code-block:: text

  $ shape.py --dio=inp,out -i CTA10-single.gro -o config -x .gro --shape='ring' -r 'CTA' -l 10  > out/shape_CTA10_ring_n10.log

Here we use the following options:

**--dio** option specifies two directories (folders) where to (i) find input configuration files
and (o) store output configuration files;

**-i** provides the name of the input file with the template coordinates of a C(10)TAB molecule;

**-o** and **-x** define the *base-name* and *extension* of the output file that
will contain the coordinates of all the generated molecules placed in the requested (ring)
configuration;

**-s** stands for the *shape* specification, which takes **ring** as an argument in this case;

**-r** stands for the *residue name(s)* to be found and matched in the input configuration file
(**CTA** in this case);

**-n** takes a whole number as an argument, specifying the number of molecules in the ring.

By default, the command given above will throw some information on the screen, related
to the progress made by the script (plus error messages if any), which might clutter the terminal
view. It is, however, easy to redirect this output to a *log file* by appending the command with
'**2&>1 > shape.log**' instruction (both **stdout** and **stderr** outputs will be flushed into
**shape.log** file).

The generated structure (``out/config_ring_CTA-10.gro``) can be checked in a graphical
MD visualisation software like **VMD**. Below we show its two views.

a) In the XY plane (from above):

.. image:: images/CTAB10-ring-N10.png
  :width: 300
  :alt: CTAB ring configuration - XY in plane view

b) Along the Z axiz:

.. image:: images/CTAB10-ring-N10-z.png
  :width: 300
  :alt: CTAB ring configuration viewed along Z axiz

To reduce the minimum distance between molecules on the inner radius from 0.5 (default) to 0.4 nm:

.. code-block:: text

  $ shape.py --dio=inp,out -i CTA10-single.gro -o config_dmin04 -x .gro --shape='ring' -r 'CTA' -l 10 --dmin=0.4  > out/shape_CTA10_ring_n10-dmin04.log

a) In the XY plane (from above):

.. image:: images/CTAB10-ring-N10-dmin04.png
  :width: 300
  :alt: CTAB ring configuration 2 - XY in plane view

Generate a 15-membered ring of C(10)TAB:

.. code-block:: text

  $ shape.py --dio=inp,out -i CTA10-single.gro -o config -x .gro --shape='ring' -r 'CTA' -l 15  > out/shape_CTA10_ring_n15.log

a) In the XY plane (from above):

.. image:: images/CTAB10-ring-N15.png
  :width: 300
  :alt: CTAB ring configuration - XY in plane view

b) Along the Z axiz:

.. image:: images/CTAB10-ring-N15-z.png
  :width: 300
  :alt: CTAB ring configuration viewed along Z axiz

To flatten the ring and reduce the minimum distance between molecules on the inner radius from 0.5 (default) to 0.4 nm:

.. code-block:: text

  $ shape.py --dio=inp,out -i CTA10-single.gro -o config_dmin04 -x .gro --shape='ring' -r 'CTA' -l 15 --dmin=0.4 --fxz  > out/shape_CTA10_ring_n15-fxz-dmin=04.log

a) In the XY plane (from above):

.. image:: images/CTAB10-ring-N15-fxz.png
  :width: 300
  :alt: CTAB ring configuration 2 - XY in plane view

b) Along the Z axiz:

.. image:: images/CTAB10-ring-N15-fxz-z0.png
  :width: 300
  :alt: CTAB ring configuration 2 viewed along Z axiz

Generate a 15-membered ring with 'idealised' (stretched out) CTAB molecules:

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_NC13H30.gro -o configS -x .gro --shape='ring' -r 'CTA' -l 15 --dmin=0.4  > out/shape_CTA10_ringS_n15-dmin04.log

a) In the XY plane (from above):

.. image:: images/CTAB10-ringS-N15-dmin04.png
  :width: 300
  :alt: CTAB ring configuration 3 - XY in plane view

b) Along the Z axiz:

.. image:: images/CTAB10-ringS-N15-dmin04-z.png
  :width: 300
  :alt: CTAB ring configuration 3 viewed along Z axiz

.. note::
  By default, Shapespyer uses the following 'backbone' indices for CTAB: ``--mint=40 --mext=0``, starting with 0 for 'N'
  atom and 40 for the last heavy atom, i.e. 'C' in the terminal 'CH3' group. User is advised to always check the indexing 
  convention in the input files at hand and, if necessary, redefine the 'bone' vector by specifying ``--mint`` and 
  ``--mext`` options explicitly.


2. Generating a ball
--------------------

To generate a spherical micelle, we simply replace the argument ``ring`` for ``ball`` and increase
the number of molecules to 16. Note that this number corresponds to the *max number of molecules
in the largest ring* fitting the required *ball arrangement*.

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_NC13H30.gro -o configS -x .gro --shape='ball' -r 'CTA' -l 15 --dmin=0.4  > out/shape_CTA10_ballS_n15-dmin04.log

The number of molecules that were generated can be found in the last line of the log file -
it is 52 in this case (at this stage the script cannot take in the total number of molecules from the input, 
so some trial and error might be necessary to obtain the micelle size close to the desired one). 

Generate a 'ball' with 16  molecules on its '*equator*':

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_NC13H30.gro -o configS -x .gro --shape='ball' -r 'CTA' -l 16 --dmin=0.4  > out/shape_CTA10_ballS_n16-dmin04.log

Upon checking the output file 'out/configS_rod_CTA-15_ndte-8.gro' (look for the index of the last molecule), the resulting structure contains 62 molecules.

.. image:: images/CTAB10-ballS-N16-T62-z0.png
  :width: 300
  :alt: CTAB ball configuration

...

3. Generating a rod
-------------------

This time we use keyword ``rod`` (instead of ``ring`` or ``ball``) and add the number of *turns*
along its length by using an extra option '``-t 10``'. Note that the number of molecules necessary
to form the caps is automatically adjusted to fit the arrangement.

.. code-block:: text

  $ shape.py --dio=inp,out -i config_smiles_NC13H30.gro -o configS -x .gro --shape='rod' -r 'CTA' -l 15 -t 8 --dmin=0.4  > out/shape_CTA10_rodS_n15_t8_m168-dmin04.log

Upon checking the output file ``out/configS_rod_CTA-15_ndte-8.gro`` (look for the index of the last molecule), the resulting structure contains 168 molecules.

The resulting molecular structure is illustrated below.

.. image:: images/CTAB10-rodS-N15-T168-z0.png
  :width: 300
  :alt: CTAB rod configuration

...


4. Staged equilibration for a 'rod'
-----------------------------------

For this exercise it is more interesting (and physically sensible) to use a CTAB molecule with a longer, 
whence more hydrophobic, hydrocarbon tail of 16 'CH*' groups. Therefore, we expect the user to repeat 
the above steps for 'C(16)TAB' for which the SMILES input is found in subdirectory 'CTAB16/inp', whereas
the initial configuration for a 'rod' structure is 'CTAB16/out/configS_rod_CTA-n16_t8-dmin4A.gro' (for reference).

Next, switch to ``CTAB16``, copy the initial 'rod' structure to directory ``equil`` and prepare files
for equilibration runs as follows.

.. code-block:: text

  $ cd CTAB16
  $ ls out/configS*rod*
  out/configS_rod_CTA-n16_t8-dmin4A.gro

  $ mkdir equil
  $ cp out/configS_rod_CTA-n16_t8-dmin4A.gro equil/CTA178.gro
  $ cd equil
  $ ls
  CTA178.gro

The force-field and topology files for the system (the necessary ``.itp`` files) are provided in ``equil0/toppar``: 

.. code-block:: text

  $ ls ../equil0/toppar/*itp
  forcefield.itp CLA.itp BRA.itp NAT.itp CTA.itp CTA10.itp TIP3.itp

Additionally, for ionic species such as bromide (BRA), sodium (NAT) and chloride (CLA) ions, trivial .gro files 
with coordinates are necessary:

.. code-block:: text

  $ ls ../equil0/toppar/*gro
  single-ionBRA.gro single-ionCLA.gro single-ionNAT.gro

  $ cp -r ../equil0/toppar ./

Now we are ready to prepare the solvated CTAB system. For guidance on the usage of the script, check its 'help':

.. code-block:: text

  $ gmx-setup-equil.bsh --help

  - The script generates Gromacs input based on an initial configuration file -

  =============
   Main usage: 
  =============

  > gmx-setup-equil.bsh -h
  > gmx-setup-equil.bsh --help
  > gmx-setup-equil.bsh <config_name> <solute_name> <solute_atoms> [c-ion_name] [salt_pairs [salt_ion1 salt_ion2]]

  -----------------------
   mandatory parameters:
  -----------------------
   config_name  - the initial configuration file name (must be found in the current directory)
   solute_name  - the primary solute molecule name    (must be found in the topology database)
   solute_atoms - the number of atoms per solute molecule

  ----------------------
   optional parameters:
  ----------------------
   c-ion_name   - the primary solute counterion name  (must be found in the topology database)
   salt_pairs   - the number of salt pairs to add = the number of solute molecules if -1 given
   salt_ion1    - the positive salt ion name (must be found in the topology database; def: NAT)
   salt_ion2    - the negative salt ion name (must be found in the topology database; def: CLA)


So, the setup with Br- ions (counter-ions for N+ atoms on CTA head groups), and water (flexible TIP3 model) 
can be done as follows (one can check the number of atoms per CTAB molecule either in 'CTA.itp' or 'CTA178.gro'):

.. code-block:: text

  $ gmx-setup-equil.bsh CTA178.gro CTA 62 BRA

.. note::
  To add, for example, 100 NaCl salt pairs one could add '``100 NAT CLA``' at the end of the command.

As a result of executing this command, a number of new files will be generated:

.. code-block:: text

  $ ls
  CTA178.gro  CTA178-BRA178-wspce.gro CTA178-BRA178-wTIP3.gro  CTA178-BRA178-wTIP3-prest.gro  CTA178-BRA178-wTIP3.top toppar

.. note::
  One can notice '``CTA178-BRA178-wTIP3-prest.gro``' file in the list. Similar to the SDS example, this file contains
  reference coordinates for the position-restraining potential(s) to be used in this case on heavy atoms of CTAB molecules. 
  The user is advised to also check '``toppar/CTA.itp``' file for additional details (look the the very bottom of the file 
  and refer to the Gromacs documentation for reference on the 'flat-bottom' restraining potentials). 

.. note::
  The purpose of position-restraints in this case is to maintain the initial 'rod' structure while the positions of water 
  (and ionic species, if any) are relaxed.


Finally, before launching the equilibration runs we need to copy .mdp files from 'toppar' to the current directory:

.. code-block:: text

  $ for i in {0..6}; do cp toppar/equil${i}.mdp CTA178-BRA178-wTIP3-eq${i}.mdp; done

  $ ls *mdp
  CTA178-BRA178-wTIP3-eq0.mdp  CTA178-BRA178-wTIP3-eq1.mdp  
  CTA178-BRA178-wTIP3-eq2.mdp  CTA178-BRA178-wTIP3-eq3.mdp  
  CTA178-BRA178-wTIP3-eq4.mdp  CTA178-BRA178-wTIP3-eq5.mdp  
  CTA178-BRA178-wTIP3-eq6.mdp

For the equilibration script to work correctly, we also need to copy the initial .gro file for the system
under the name corresponding to the 0-th (energy minimization) step:

.. code-block:: text

  $ cp CTA178-BRA178-wTIP3.gro CTA178-BRA178-wTIP3-eq0-ini.gro

Now we have all the files necessary for the 5 stages of brief equilibration (under one nanosecond)! 
However, it is advisable to backup the initial setup files:

.. code-block:: text

  $ mkdir initial
  $ cp * initial

Check ``help`` for the equilibration script:

.. code-block:: text

  $ gmx-equilibrate.bsh -h

  - The script runs Gromacs with input based on initial .gro, .top and .mdp files -

  =============
   Main usage: 
  =============

  > gmx-equilibrate.bsh -h
  > gmx-equilibrate.bsh --help
  > gmx-equilibrate.bsh [-nohup] <task_name> <stage#1> [<stage#2> <np_tot> <stage_name>]

  -----------------------
   optional switch -nohup
  -----------------------

   this is one way of launching terminal non-binding command 'nohup gmx-equilibrate.bsh <args> &'

  -----------------------
   mandatory parameters:
  -----------------------
   task_name  - the base file name (the prefix before file extensions)
   stage#1/#2 - the stage index or two indeces (initial and final; at least one required)

  ----------------------
   optional parameters: in cases where exactly 3 mandatory arguments are also given
  ----------------------

   np_tot     - the total number of threads (combining OpenMPI and OpenMP if possible) [8]
   stage_name - generic stage suffix to add to task_name ['eq']


Accordingly, one can launch the first 0-5 stages of equilibration on 16 parallel threads by entering 
the following command (which can take a while, depending on your computer power and memory resources):

.. code-block:: text

  $ gmx-equilibrate.bsh -nohup CTA178-BRA178-wTIP3 0 5 16
  nohup: appending output to 'nohup.out'

To check if the simulation has started successfully, use command 'top':

.. code-block:: text

  $ top
  PID    USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM  TIME+ COMMAND 
  732870 user      20   0 2323104 163732  15536 R  1559   0.1  01:03.93 gmx 

Look for the line(s) with 'gmx' in the last column - normally it should appear at the top of the table, 
as one of the currently executed programs with high priority. If it is not found in the table, then 
something went wrong!

To monitor the progress of execution, enter:

.. code-block:: text

  $ tail -l 11 nohup.out; echo
  Using 16 MPI threads
  Using 1 OpenMP thread per tMPI thread

  starting mdrun 'CTA solution with BRA counterions and NAT-CLA salt'
  250000 steps,    500.0 ps.
  step 212500, remaining wall clock time:    92 s          vol 0.75  imb F  4% pme/F 0.35 

The output should look similar to the above, hinting on the remaining time for the currently running stage 
(it will be updated each time one enters the 'tail' command).

.. note::
  The script will iterate through the equilibration stages by running each stage in the current directory.
  When the simulation for a particular stage is finished, the results will be moved into a subdirectory 
  corresponding to that stage: 'eq0' ... 'eq5'. 

.. note::
  If the simulation is terminated abnormally, the files for the failed stage are still kept in the directory 
  where it started, so that rerunning the equilibration script starting with the last stage should be able to pick 
  it up where it stopped. If the rerun does not go well and also stops, the user is advised to restart that stage 
  from scratch! To do so, the user must make sure that all the initial files in the current directory are restored -
  check for the presence of: <BaseName>-eq#-ini.gro <BaseName>-eq#.top <BaseName>-eq#.mdp and remove any other files 
  for that stage if present.


4.1 Post-simulation analyses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first type of structure anlysis one can do is for the evolution of the radius of gyration and its components, for which 
the user can utilise the script called ``gmx-ana-gyration.bsh``:

.. code-block:: text

  $ gmx-ana-gyration.bsh -h

  - Radius of gyration and its components for solute species based on .gro or .xtc files

  =============
   Main usage:
  =============

  > gmx-ana-gyration.bsh -h
  > gmx-ana-gyration.bsh --help
  > gmx-ana-gyration.bsh <task_name> [<species_index> <config_extension>]

  Example: gmx-ana-gyration.bsh CTA178-BRA178-wTIP3-eq1 2 gro

In order to use it on the results from a particular simulation stage, one has to switch to the corresponding subdirectory, 
for example:

.. code-block:: text

  $ cd eq0
  $ gmx-ana-gyration.bsh CTA178-BRA178-wTIP3-eq0 2 gro

.. note::
  The second argument for the script specifies the index of the species to be analysed as defined in the Gromacs index file (``.ndx``). 
  In most cases solute has index 2 (after [System] and [Other]), which is the default value assumed by the script.

.. note::
  The third argument can be either ``gro``, ``xtc`` or ``trr``. Evidently, only one configuration is analysed with ``gro`` 
  format while the entire simulation trajectory is taken as input in the latter two cases.

Another helper script called ``gmx-ana-gyration-dirs.bsh`` automates this analysis:

.. code-block:: text

  $ gmx-ana-gyration-dirs.bsh -h

  - Radius of gyration and its components for solute species based on .gro or .xtc files
  - Going through directories matching a pattern

  =============
   Main usage:
  =============

  > gmx-ana-gyration-dirs.bsh -h
  > gmx-ana-gyration-dirs.bsh --help
  > gmx-ana-gyration-dirs.bsh <dir_pattern> <task_name> [<species_index> <config_extension>]

  Example: gmx-ana-gyration-dirs.bsh eq? CTA178-BRA178-wTIP3-eq1 2 gro

As the help page suggests, the script will go through all subdirectories matching the given pattern and call for 
``gmx-ana-gyration.bsh`` accordingly. As a result of running either of the scripts over all the subdirs ``eq?``, we 
find the following two files generated in each of the subdirectories: 

  ``CTA178-BRA178-wTIP3-eq?-GyrRXYZ.xvg`` - the radius of gyration data

  ``CTA178-BRA178-wTIP3-eq?-GyrRXYZ-ACF.xvg`` - the corresponding autocorrelation functions data

Both files can be directly opened and processed in Grace (xmgrace) plotting software.

The radius of gyration evolution through the final configurations from each of the equilibration stages is illustrated below.

.. image:: images/CTA178-BRA178-wTIP3-eq05-GyrRXYZ.png
  :width: 600
  :alt: Rg equilibration for SDS micelle

We see that the size of the micelle appears to converge rather quickly within just under one nanosecond of equilibration.
We also notice how the radius of gyration around the OX axis drops revealing the assymetry of the rod.

...


