#!/usr/bin/env python3

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#               ver. 0.1.7 (beta)                #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: MSc Mariam Demir (c) Oct - Dec 2023  #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#      (YAML IO, InputParser, topology analyses) #
#                                                #
#  Contrib: Dr Michael Seaton (c) 2024           #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (DL_POLY/DL_MESO DPD w-flows, tests)  #
#                                                #
#  Contrib: Dr Valeria Losasso (c) 2024          #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#        (PDB IO, Bilayer, NAMD w-flows, tests)  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by developer's initials as follows:

# import this to be able to check for the 'installed' status of am external package
# one will also need to import sys (but that's done anyway)
# import importlib.util

# system modules (for parsing arguments, options, handling IO/errors)
# import os, sys, getopt

# import typing
# from typing import IO
# import math
# import matplotlib.pyplot as plt
# import struct
# import array as arr

# import numpy as np
# from numpy import sin, cos, random
# from numpy import exp, log

# local project modules
# import shapes.basics.globals as org
# from shapes.basics.globals import *

# all the above is imported by way of the below
#from shapes.basics.help import *
#from shapes.basics.globals import *
from shapes.basics.input import *
from shapes.basics.functions import pbc

from shapes.ioports.ioxyz import *
from shapes.ioports import iogro, iopdb, ioconfig, iofield
from shapes.stage.protovector import Vec3

from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
from shapes.stage.protomoleculeset import MoleculeSet as MolSet
from shapes.stage.protomolecularsystem import MolecularSystem as MolSys

from shapes.designs.protoshape import Ring, Ball, Rod
from shapes.designs.protolayer import Bilayer
from shapes.designs.protolattice import Lattice
from shapes.designs.smiles import smlFile, Smiles


print("\n##################################################")
print("#                                                #")
print("#  Shapespyer - soft matter structure generator  #")
print("#               ver. 0.1.7 (beta)                #")
print("#                                                #")
print("#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #")
print("#          Daresbury Laboratory, SCD, STFC/UKRI  #")
print("#                                                #")
print("##################################################\n")

#sname = "shape.py"
#sname = sys.argv[0]
sname = os.path.basename(sys.argv[0])

nargs = len(sys.argv) - 1

if nargs < 1:
    #print(f"Too few options / arguments!\n")
    print(f"{sname}:: At least one option is expected!\n")
    print(f"{sname}:: Try '{sname} --help'\n")
    sys.exit(1)

### MAIN ###

def main(argv):

    sname = os.path.basename(argv[0])

    # Molecule.readSF(argv[1])
    # sys.exit(0)

    # DEFAULTS & CONVENTIONS
    dinp = '.'
    dout = '.'
    sxyz = '.xyz'
    spdb = '.pdb'
    sdlp = '.dlp'
    sdlm = '.dlm'
    sgro = '.gro'
    ssml = '.sml'

    exts = [sgro, sxyz, ssml, spdb, sdlp, sdlm] #, spdb, sdpd]  # supported file extensions so far

    # SUPPORTED SHAPES
    shapes = ['ring', 'rod', 'ball', 'ves', 'balp', 'vesp', 'lat', 'lat2', 'lat3', 'smiles', 'waves', 'bilayer','dens']  #, 'band', 'mix'

    # AB: default surface XYZ file
    fwav = 'config_wav.xyz'

    # AB: default GROMACS INPUT / OUTPUT
    # finp = 'config_inp.gro'
    # fout = 'config_out.gro'
    # fsml = 'smiles.sml'

    # AB: default DL_POLY INPUT / OUTPUT
    # fcfg = 'CONFIG'
    # fhst = 'HISTORY'
    # ftrj = 'TRAJOUT'

##### - COMMAND-LINE / YAML INPUT / HELP - START

    #print(f"\n{sname}::main(): Initialising input parser ...")
    inpPars = InputParser()  # AB: initialising with defaults

    print(f"\n{sname}::main(): Parsing input parameters (from CLI or YAML) ...")
    inpPars.parseCLI(argv)
    print(f"\n{sname}::main(): Parsing input parameters - DONE\n")

##### - COMMAND-LINE / YAML INPUT / HELP - END

##### - ARGUMENTS ANALYSIS - START

    # AB: retain the sign only in inpPars.getSolvBuff()
    BUFF = abs(inpPars.getSolvBuff()) # nm - rescale for Angstroems!
    DMIN = inpPars.getMinDist() # nm - rescale for Angstroems!

    if inpPars.getShapeRadius() < DMIN:
        print(f"{sname}::main(): Input cavity radius < " + str(DMIN) + " nm - reset!\n")
        inpPars.setShapeRadius(DMIN)

    print(f"{sname}::main(): Check globals & defaults:\n"
          f"Pi = {Pi}, 2*pi = {TwoPi}, TINY = {TINY}, HUGE = {HUGE}\n"
          f"BUFF = {BUFF}, DMIN = {DMIN}, RMIN = {inpPars.getShapeRadius()},\n")

    if len(inpPars.getMolIDs()) > 0:
        inpPars.setMolID(inpPars.getMolIDs()[0])
    else:
        inpPars.delMolIDs()
        inpPars.setMolIDs(inpPars.getMolID())

    if len(inpPars.getMolNames()) >= 1:
        inpPars.setMolResnm(inpPars.getMolNames()[0])

    if inpPars.getShapeType() not in shapes:
        print(f"{sname}::main(): Unrecognised option for shape: {inpPars.getShapeType()}\n")
        print(f"{sname}::main(): Try: " + sname + " --help\n")
        sys.exit(1)

    if inpPars.getShapeLayers()[0] > 1:
        if inpPars.getShapeType()[:3] == 'bal':
            inpPars.setShapeType('ves')
            print(f"\n{sname}::main(): Requested number of layers {inpPars.getShapeLayers()[0]} > 1 "
                  f" for 'ball' shape -> reset to 'ves' (vesicle) shape!..")
        if not inpPars.getShapeType()=='ves':
            inpPars.setShapeLayers(1)
    elif inpPars.getShapeType()=='ves':
        inpPars.setShapeLayers(2)

    ishape = 1  # ring
    if inpPars.getShapeType() == 'rod':  ishape = 2
    if inpPars.getShapeType() == 'ball': ishape = 3
    if inpPars.getShapeType() == 'balp': ishape =-3
    if inpPars.getShapeType() == 'mix':  ishape = 4
    if inpPars.getShapeType() == 'ves':  ishape = 5
    if inpPars.getShapeType() == 'vesp': ishape =-5
    if inpPars.getShapeType() == 'lat':  ishape = 6
    if inpPars.getShapeType() == 'lat2': ishape =-6
    if inpPars.getShapeType() == 'lat3': ishape = 7
    if inpPars.getShapeType() == 'waves': ishape = -2
    if inpPars.getShapeType() == 'smiles': ishape = -1
    if inpPars.getShapeType() == 'bilayer': ishape = 8

    if abs(ishape) > 5 or ishape in {4, -1}:
        inpPars.setFlagRandom(False)
        inpPars.delMolFracs()

    if abs(ishape) in {3,5}:
        if inpPars.getShapeFill()[:5]=='rings' and inpPars.getShapeNmols() > 0:
            inpPars.setShapeNmols(0)
        if inpPars.getShapeFill()=='fibo' and inpPars.getShapeLring() > 0:
            inpPars.setShapeLring(0)
        if inpPars.getShapeFill()=='fibo' and inpPars.getShapeNmols() < 1\
          and inpPars.getShapeRadius() < DMIN+TINY:
            print(f"\n{sname}::main(): Requested number of molecules "
                 f"'--nmols={inpPars.getShapeNmols()}' < 1 "
                 f"for '--shape={inpPars.getShapeType()}' with '--fill={inpPars.getShapeFill()}' "
                 f"and '--cavr={inpPars.getShapeRadius()}' (empty structure)"
                 f"- FULL STOP!!!\n")
            sys.exit(1)
        elif inpPars.getShapeFill()[:5]=='rings' and inpPars.getShapeLring() < 1\
          and inpPars.getShapeRadius() < DMIN+TINY:
            print(f"\n{sname}::main(): Requested number of molecules on equatorial ring "
                 f"'--lring={inpPars.getShapeLring()}' < 1 "
                 f"for '--shape={inpPars.getShapeType()}' with '--fill={inpPars.getShapeFill()}' "
                 f"and '--cavr={inpPars.getShapeRadius()}' (empty structure)"
                 f"- FULL STOP!!!\n")
            sys.exit(1)
        if inpPars.getShapeNmols() + inpPars.getShapeLring() < 1 \
          and inpPars.getShapeRadius() < DMIN+TINY:
            print(f"\n{sname}::main(): Requested number of molecules "
                 f"'--nmols={inpPars.getShapeNmols()}' and '--lring={inpPars.getShapeLring()}' < 1 "
                 f"for '--shape={inpPars.getShapeType()}' "
                 f"and '--cavr={inpPars.getShapeRadius()}' (empty structure)"
                 f"- FULL STOP!!!\n")
            sys.exit(1)

    if abs(ishape) == 3 or abs(ishape) == 5 or ishape in {1, 2}:
        inpPars.setFlagPin(ishape < 0)
        ishape = abs(ishape)
        if inpPars.getShapeFill()[:5] == 'rings' and inpPars.getShapeLring() > 1:
            # min radius of the interior surface
            inpPars.setShapeRadius(max(inpPars.getShapeRadius(), DMIN * abs(inpPars.getShapeLring()) / TwoPi))
            lring = int(TINY + inpPars.getShapeRadius() * TwoPi / DMIN)
            if lring != inpPars.getShapeLring():
                print(f"\n{sname}::main(): WARNING! resetting molecule number for the largest ring (equator) "
                      f"{inpPars.getShapeLring()} -> {lring} - due to Dmin / Rmin constraints!")
                inpPars.setShapeLring(lring)
        elif inpPars.getShapeFill() == 'fibo' and inpPars.getShapeNmols() > 1:
            nmols = inpPars.getShapeNmols()
            # min radius of the interior surface
            inpPars.setShapeRadius(max(inpPars.getShapeRadius(), np.sqrt(DMIN**2 * float(nmols) / (3.04 * Pi))))
            nmols = int(TINY + 3.04 * Pi * inpPars.getShapeRadius()**2 / DMIN**2)
            if nmols != inpPars.getShapeLring():
                print(f"\n{sname}::main(): WARNING! resetting molecule number for the structure "
                      f"{inpPars.getShapeNmols()} -> {nmols} - due to Dmin / Rmin constraints!")
                inpPars.setShapeNmols(nmols)
        elif inpPars.getShapeFill()[:5] == 'rings':
            inpPars.setShapeLring(max(abs(inpPars.getShapeLring()), 1))
        elif inpPars.getShapeFill() == 'fibo':
            inpPars.setShapeNmols(max(abs(inpPars.getShapeNmols()), 1))
    else:
        inpPars.setShapeLring(max(abs(inpPars.getShapeLring()), 1))

    iext = inpPars.getInpFile()[-4:]
    if inpPars.getInpFile()[:6] != 'CONFIG' and inpPars.getInpFile()[:5] != 'FIELD':
        print(f"{sname}::main(): Input file extension: '" + iext + "'\n")
    if (iext not in exts) and inpPars.getInpFile()[:6] != 'CONFIG' and inpPars.getInpFile()[:5] != 'FIELD':
        if iext[1] == '.':
            print(f"{sname}::main(): Unsupported input extension: '" + iext + "' [.xyz/.pdb/.gro; optional .dlp/.dlm for DL_POLY/DL_MESO CONFIG/FIELD]\n")
        else:
            print(f"{sname}::main(): Unsupported input file-name: '" + inpPars.getInpFile() + "' [no extension => DL_POLY/DL_MESO 'CONFIG', DL_MESO 'FIELD']\n")
        sys.exit(2)
    # else :
    #   print(f"{sname}::main(): Using input file: '"+finp+"'\n")

    if iext == ssml:
        if ishape == -1:
            inpPars.setFlagSmiles(True)
        else:
            print(f"{sname}::main(): Unsupported input file-name: '" + inpPars.getInpFile() +
                  "' for shape '"+ inpPars.getShapeType() +"' - try '-i smiles.sml -s smiles' ...\n")
            sys.exit(2)

    print(f"{sname}::main(): Requested molecule names and ids: {inpPars.getMolNames()} -> {inpPars.getMolResnm()} & "
          f"{inpPars.getMolIDs()} -> {inpPars.getMolID()} ({inpPars.getMolNpick()})\n")

    print(f"{sname}::main(): Requested molecule 'bone' indices: {inpPars.getMolMint()} ... {inpPars.getMolMext()} & "
          f"({inpPars.getMolNpick()})\n")

    if inpPars.getMolNpick() != len(inpPars.getMolNames()) or inpPars.getMolNpick() != len(inpPars.getMolIDs()):
        mpick = max(len(inpPars.getMolNames()),len(inpPars.getMolIDs()))
        print(f"{sname}::main(): Number of requested unique molecules ({inpPars.getMolNpick()}) =/= "
              f"number of given molecule names ({len(inpPars.getMolNames())}) or "
              f"number of given molecule indices ({len(inpPars.getMolIDs())}) - "
              f"setting it to {mpick}")
        inpPars.setMolNpick(mpick)


    sfrc  = ''
    fmax  = 0
    # AB: get a copy of the input for fractions as it's going to be adjusted below!
    inpFracs = []
    for frcs in inpPars.getMolFracs():
        inpFracs.append(frcs.copy())
    msurf = len(inpFracs)
    print(f"{sname}::main(): Species' fractions from input = {inpPars.getMolFracs()}")
    if msurf > inpPars.getShapeLayers()[0]:  # 2:
        # print(f"\n{sname}::main(): Surfaces with {msurf} layers (leaflets) > 2 "
        #       f"are not supported yet - FULL STOP!!!")
        print(f"\n{sname}::main(): Number of fraction sets {msurf} > {inpPars.getShapeLayers()[0]} "
              f"number of (mono-)layers - FULL STOP!!!")
        sys.exit(1)
    elif msurf > 0:
        fmax = max([ len(frcs) for frcs in inpPars.getMolFracs() ])
        if fmax > inpPars.getMolNpick():
            print(f"{sname}::main(): Max number of species' fractions ({fmax}) > "
                  f"number of species to be picked ({inpPars.getMolNpick()}) - FULL STOP!!!")
            sys.exit(1)
        fmin = min([ len(frcs) for frcs in inpPars.getMolFracs() ])
        if fmin > 0:
            for frcs in inpFracs:
                fsum = 0
                for frc in frcs:
                    fsum += frc
                sfrc1 = '-'
                for i in range(0, len(frcs)):
                    frcs[i] /= fsum
                    sfrc += sfrc1+str(int(frcs[i]*100.0))
                    sfrc1 = 'x'
                for i in range(1, len(frcs)):
                    frcs[i] = frcs[i-1] + frcs[i]
            sfrc = sfrc[1:]
            print(f"{sname}::main(): Species' fractions from input = {inpFracs} / "
                  f"{[ sum(frcs) for frcs in inpFracs ]} -> '{sfrc}'")
            # print(f"{sname}::main(): Species' fractions from input = {inpPars.getMolFracs()} / "
            #       f"{[ sum(frcs) for frcs in inpPars.getMolFracs() ]} -> '{sfrc}'")
        else:
            print(f"{sname}::main(): Min number of species' fractions ({fmin}) < 2 "
                  f"does not make sense for random placement ({inpPars.getMolNpick()}) - FULL STOP!!!")
            sys.exit(1)
    else:
        msurf = 1
        print(f"{sname}::main(): Number of layers to generate msurf = {msurf} "
              f"{len(inpPars.getMolFracs())} -> {inpPars.getMolFracs()} ...")

    mpick = inpPars.getMolNpick()-1

    if inpPars.getFlagRandom():
        mpick = -mpick

    setname = ''
    unique = []
    for rnm in inpPars.getMolNames():
        if rnm not in unique:
            setname += rnm+'-'
            unique.append(rnm)
    rnames = setname[:-1]
    inpPars.setMolResnm(setname[:-1])
    inpPars.setMolNames(unique)

    print(f"\n{sname}::main(): names suffix = '{inpPars.getMolResnm()}' =?= '{rnames}' (rnames) "
          f" based on unique names '{inpPars.getMolNames()}' ...")

    mols_in = []
    print(f"{sname}::main(): Created {mols_in} - empty input list of species")
    mols_out = []
    print(f"{sname}::main(): Created {mols_out} - empty (output) list of species\n")

    if not inpPars.getFlagSmiles():
        for i in range(inpPars.getMolNpick()):
            mols_out.append(MolSet(i, 0, sname=inpPars.getMolNames()[i], stype='output'))
            print(f"{sname}::main(): Added {mols_out[i]} to the output list of species\n")

### - Output file naming - START

    oext = ''
    print(f"{sname}::main(): Initial user-provided output name = {inpPars.getOutFile()}")
    if isinstance(inpPars.getOutFile(),str):
        if len(inpPars.getOutFile()) > 4:
            # MS: if directly calling file CONFIG or starts as such, assume it is DL_POLY or DL_MESO
            if inpPars.getOutFile()[:6] == 'CONFIG':
                print(f"{sname}::main(): User-provided output file name '{inpPars.getOutFile()}' => DL_POLY/DL_MESO format")
                # MS: 'dummy' extension added here to avoid full file name construction below
                #     but distinguishing between DL_POLY/DL_MESO based on any specified DPD length unit 
                oext = sdlp if inpPars.getLenDPD()==0.1 else sdlm
                inpPars.setOutBase(inpPars.getOutFile())
            else:
                oext = inpPars.getOutFile()[-4:]
                print(f"{sname}::main(): User-provided output file name '{inpPars.getOutFile()}' => extension '{oext}'\n")
                inpPars.setOutBase(inpPars.getOutFile()[:-4])
            if (oext not in exts):
                print(f"{sname}::main(): Unsupported output file extension '{oext}' - FULL STOP!!!")
                sys.exit(0)
            inpPars.setOutExt(oext)
    else:
        if inpPars.getOutExt() == sdlp or inpPars.getOutExt() == sdlm:
            print(f"{sname}::main(): User did not provide the output file name - will construct it as 'CONFIG_' + 'base name'")
            fout = 'CONFIG_' + str(inpPars.getOutBase())
        else: 
            print(f"{sname}::main(): User did not provide the output file name - will construct it as 'base name' + 'extension'")
            fout = str(inpPars.getOutBase() + inpPars.getOutExt())
        inpPars.setOutFile(fout)

    if len(oext) < 4:  # the extension was provided by user separately => construct the full file name ...
        if isinstance(inpPars.getOutBase(),str) and len(inpPars.getOutBase()) > 0:
            print(f"{sname}::main(): User-provided output file name base "
                  f"'{inpPars.getOutBase()}' (given separately)")
            if isinstance(inpPars.getOutExt(),str) and len(inpPars.getOutExt()) > 0 and inpPars.getOutExt()[0] == '.':
                if inpPars.getOutExt() == sdlp or inpPars.getOutExt() == sdlm:
                    print(f"{sname}::main(): User-provided output file extension "
                          f"'{inpPars.getOutExt()}' (given separately: will append 'CONFIG_' to beginning of file name)")
                else: 
                    print(f"{sname}::main(): User-provided output file extension "
                          f"'{inpPars.getOutExt()}' (given separately or by default)")
            else:
                print(f"{sname}::main(): Unsupported output file extension '{inpPars.getOutExt()}' - FULL STOP!!!")
                sys.exit(0)
        else:
            print(f"{sname}::main(): Unsupported output file extension '{inpPars.getOutExt()}' - FULL STOP!!!")
            sys.exit(0)

        if len(inpPars.getOutExt()) > 0 and (inpPars.getOutExt() not in exts):
            print(
                f"{sname}::main(): Unsupported output extension: '" + inpPars.getOutExt() + "' [.xyz/.pdb/.gro; optional .dlp/.dlm for DL_POLY/DL_MESO CONFIG]\n")
            sys.exit(3)
        # else :
        #   print(f"{sname}::main(): Using output file: '"+fout+"' (extension: '"+oext+"')\n")

        # AB: add unique solute (residue) names as a suffix to the output file name
        #inpPars.setOutBase(inpPars.getOutBase() + '_' + inpPars.getMolResnm() + '_' + inpPars.getShapeType())
        inpPars.setOutBase(inpPars.getOutBase() + '_' + inpPars.getMolResnm())

        if len(inpPars.getMolMint()) > 0:
            smint = ''
            for m in inpPars.getMolMint():
                smint +=str(m) + '-'
            smint = smint[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_mi' + smint)
        if len(inpPars.getMolMext()) > 0:
            smext = ''
            for m in inpPars.getMolMext():
                smext +=str(m) + '-'
            smext = smext[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_mx' + smext)

        if inpPars.getMolFracs():
            inpPars.setOutBase(inpPars.getOutBase() + '_frc' + sfrc)
        # if inpPars.getFlagRandom():
        #     inpPars.setOutBase(inpPars.getOutBase() + '_rnd')

        # AB: add unique solute (residue) names as a suffix to the output file name
        inpPars.setOutBase(inpPars.getOutBase() + '_' + inpPars.getShapeType())

        # AB: commenting this out because npick should be deprecated as an input parameter
        # AB: npick is now determined from the lengths of input lists, so is still used 'internally'
        # if inpPars.getMolNpick() > 1:
        #     #if npick != len(mint):
        #     #    print(f"{sname}::main(): Number of requested molecules ({npick}) =/= "
        #     #          f"number of 'bone' internal indices ({len(mint)})")
        #     #    sys.exit(1)
        #     #if npick != len(mext):
        #     #    print(f"{sname}::main(): Number of requested molecules ({npick}) =/= "
        #     #          f"number of 'bone' external indices ({len(mext)})")
        #     #    sys.exit(1)
        #     if abs(ishape) > 5:
        #         inpPars.setOutBase(inpPars.getOutBase() + '_np' + str(inpPars.getMolNpick()) + '_lat' + str(inpPars.getNlatx()) + 'x' + str(inpPars.getNlaty()) + 'x' + str(inpPars.getNlatz()))
        #     elif ishape in {-1,-2}:
        #         inpPars.setOutBase(inpPars.getOutBase() + '_np' + str(inpPars.getMolNpick()))
        #     else:
        #         inpPars.setOutBase(inpPars.getOutBase() + '_np' + str(inpPars.getMolNpick()) + '_lr' + str(inpPars.getShapeLring()))
        # elif ishape in {-1,-2}:
        if ishape in {-1, -2}:
            pass
            #inpPars.setOutBase(inpPars.getOutBase() + '_' + inpPars.getShapeType())
        elif ishape == 8:
            inpPars.setOutBase(inpPars.getOutBase() + '_ns' + str(inpPars.getNside()) + '_zsep' + str(inpPars.getZsep()))
        elif abs(ishape) > 5: # and (ishape != 8):
            inpPars.setOutBase(inpPars.getOutBase() + '_lat' + str(inpPars.getNlatx()) + 'x' + str(inpPars.getNlaty()) + 'x' + str(inpPars.getNlatz()))
        elif inpPars.getShapeLring()+inpPars.getShapeNmols() > 1:
            if inpPars.getShapeLring() > 1:
                inpPars.setOutBase(inpPars.getOutBase()+ '_lr' + str(inpPars.getShapeLring()))
            if inpPars.getShapeLayers()[0] > 2:
                inpPars.setOutBase(inpPars.getOutBase() + '_ml' + str(inpPars.getShapeLayers()[0]))
            elif ishape != 5 and inpPars.getShapeLayers()[0] > 1:
                inpPars.setOutBase(inpPars.getOutBase() + '_ml' + str(inpPars.getShapeLayers()[0]))
        else:
            rm = round(inpPars.getShapeRadius() * 10., 2)
            srm = "{:<5}".format(rm).strip().rstrip('0')
            if srm[-1:] == '.': srm = srm[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_cr' + srm + 'A')

        if inpPars.getShapeTurns() == -1: inpPars.setShapeTurns(1)

        if abs(inpPars.getShapeTurns()) > 1:  # non-default TURNS (for a 'rod')
            inpPars.setOutBase(inpPars.getOutBase() + '_nt' + str(inpPars.getShapeTurns()))
        # AB: deprecated - ?
        # if inpPars.getShapeTurns() > 1:
        #     inpPars.setOutBase(inpPars.getOutBase() + '_ndte' + str(inpPars.getShapeTurns()))
        # elif inpPars.getShapeTurns() < -1:
        #     inpPars.setOutBase(inpPars.getOutBase() + '_ndto' + str(abs(inpPars.getShapeTurns())))
        #     inpPars.setShapeNstep(0)
        # if inpPars.getShapeNstep() > 0:
        #     inpPars.setOutBase(inpPars.getOutBase() + '_nstu' + str(inpPars.getShapeNstep()))
        # elif inpPars.getShapeNstep() < 0:
        #     inpPars.setOutBase(inpPars.getOutBase() + '_nstd' + str(abs(inpPars.getShapeNstep())))

        if inpPars.getAlpha() > TINY:  # non-default LEFT
            inpPars.setOutBase(inpPars.getOutBase() + '_azml' + str(inpPars.getAlpha()))
        elif inpPars.getAlpha() < -TINY:  # non-default RIGHT
            inpPars.setOutBase(inpPars.getOutBase() + '_azmr' + str(abs(inpPars.getAlpha())))
        else:
            inpPars.setAlpha(0.0)

        if inpPars.getTheta() > TINY:  # non-default UP
            inpPars.setOutBase(inpPars.getOutBase() + '_altu' + str(inpPars.getTheta()))
        elif inpPars.getTheta() < -TINY:  # non-default DOWN
            inpPars.setOutBase(inpPars.getOutBase() + '_altd' + str(abs(inpPars.getTheta())))
        else:
            inpPars.setTheta(0.0)

        if inpPars.getFlagFxz():
            inpPars.setOutBase(inpPars.getOutBase() + '_fxz')
        if inpPars.getFlagRev():
            inpPars.setOutBase(inpPars.getOutBase() + '_rev')
        if inpPars.getFlagPin():
            inpPars.setOutBase(inpPars.getOutBase() + '_pin')

        if inpPars.getFlagAlignZ():  # aligning along OZ
            inpPars.setOutBase(inpPars.getOutBase() + '_az')

        if inpPars.getShapeOrigin() != 'cog':  # non-default origin
            inpPars.setOutBase(inpPars.getOutBase() + '_' + inpPars.getShapeOrigin())

        if len(inpPars.getShapeOffset()) > 1:
            print(f"{sname}::main(): Required origin offset = {inpPars.getShapeOffset()}\n")
            soff = ''
            for val in inpPars.getShapeOffset():
                sval = "{:<5}".format(val).strip().rstrip('0')
                soff += sval + '-'
            soff = soff[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_off')

        #if inpPars.getMinDist() != 0.5:  # non-default DMIN
        if abs(DMIN - 0.5) > TINY:  # non-default DMIN
            dm = DMIN
            if dm < 1.0:
                dm *= 10.0
            sdm = "{:<5}".format(dm).strip().rstrip('0')
            if sdm[-1:] == '.': sdm = sdm[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_dm' + sdm + 'A')

        #if inpPars.getSolvBuff() != 1.0:  # non-default BUFF
        #    sb = inpPars.getSolvBuff() * 10.0
        if abs(BUFF - 1.0) > TINY:  # non-default BUFF
            sb = BUFF * 10.0
            sbf = "{:<5}".format(sb).strip().rstrip('0')
            if sbf[-1:] == '.': sbf = sbf[:-1]
            inpPars.setOutBase(inpPars.getOutBase() + '_sb' + sbf + 'A')

        # MS: add 'CONFIG_' to beginning of output file name if for DL_POLY/DL_MESO (don't use extension in filename)
        if inpPars.getOutExt() == sdlp or inpPars.getOutExt() == sdlm:
            if inpPars.getOutBase()[:6] != 'CONFIG':
                inpPars.setOutBase('CONFIG_'+str(inpPars.getOutBase()))
            fout = str(inpPars.getOutBase())
        else:
            fout = str(inpPars.getOutBase() + inpPars.getOutExt())
            if len(inpPars.getOutExt()) < 1:
                inpPars.setOutExt(fout[-4:])
                if '.' not in inpPars.getOutExt():
                    inpPars.setOutExt('')

        print(f"\n{sname}::main(): base name = '{inpPars.getOutBase()}' => '{fout}' (fout) ...")

        if inpPars.getOutFile() != inpPars.getOutBase()+inpPars.getOutExt():
        # if inpPars.getOutFile() != 'config_out.gro':
        #     print(f"\n{sname}::main(): user-provided output file name = '{inpPars.getOutFile()}'")
        # else:
            # MS: omit file extension if .dlp or .dlm is selected and add 'CONFIG_' to beginning instead
            if inpPars.getOutExt() == sdlp or inpPars.getOutExt()==sdlm:
                inpPars.setOutFile(inpPars.getOutBase())
            else:
                inpPars.setOutFile(inpPars.getOutBase()+inpPars.getOutExt())
            print(f"\n{sname}::main(): input-based output file name = '{inpPars.getOutFile()}'")

    #oext = inpPars.getOutExt()

    ### - Output file naming - DONE

    rmin = inpPars.getShapeRadius()  # min radius of the interior surface
    #rmin = max(inpPars.getShapeRadius(), DMIN * inpPars.getShapeLring() / TwoPi)
    print(f"{sname}::main(): Using Nmol = " + str(max(inpPars.getShapeLring(),inpPars.getShapeNmols())))  # \n")
    print(f"{sname}::main(): Using Nlay = " + str(inpPars.getShapeLayers()[0]))  # \n")
    print(f"{sname}::main(): Using Dmin = " + str(DMIN) + " for min separation between heavy atoms")  # \n")
    print(f"{sname}::main(): Using Rmin = " + str(rmin) + " for target internal radius (if applicable)")  # \n")
    print(f"{sname}::main(): Using Rbuf = " + str(inpPars.getSolvBuff()) + " for solvation buffer\n")

    # AB: dump the input for reference
    inpPars.dumpYaml()

##### - ARGUMENTS ANALYSIS - DONE

    ierr  = 0
    matms = 0
    natms = 0
    mmols = 0

    gbox = []
    cbox = []
    cell = []
    mesh = []

    # Remark / comment lines from the input file if any
    rems_inp = []
    rems_add = ""

    # SMILES style input for molecules to be generated from scratch
    smiles = []
    snames = []

    # Length / distance scaling factor (seems only needed for SMILES: A -> nm)
    #rscale = 1.0

    if os.path.isdir(inpPars.getInpPath()):

        dfinp = str(inpPars.getInpPath() + '/' + inpPars.getInpFile())
        dfout = str(inpPars.getOutPath() + '/' + inpPars.getOutFile())

        if os.path.isfile(dfinp):

            print(f"{sname}::main(): Doing: input '" + inpPars.getInpFile() + "' => output '" + inpPars.getOutFile() + "'\n")

            # Reading input for molecules

            if inpPars.getInpFile()[-4:] == ssml:
            #if inpPars.getFlagSmiles():

                fsml = smlFile(dfinp)
                fsml.readInSmiles(rems_inp, smiles, snames, gbox,
                                  inpPars.getMolNames(), inpPars.getMolIDs()) #, verbose=True)
                rems_add = " from SMILES"

                # if inpPars.getOutExt() == sgro:
                #    rscale = 0.1  # angstrom -> nm

                dbkinks = []  # list of elements that can produce 'kinks' due to double bounds
                #dbkinks = ['C'] #  ['C','O','P'] # ['C','O','P']
                if len(dbkinks) > 0:
                    print(f"{sname}::main(): Will generate molecules from scratch with SMILES: {smiles} "
                          f"with double bond 'kinks' for atoms {dbkinks}\n")
                else:
                    print(f"{sname}::main(): Will generate molecules from scratch with SMILES: {smiles} "
                          f"without double bond 'kinks'\n")

                for i in range(len(smiles)):
                    smile = ''
                    if isinstance(smiles[i], list):
                        # AB: in case of more than one SMILES string
                        for j in range(len(smiles[i])):
                            # AB: in case of more than one isomer(s) with the same chemical formula
                            smile = smiles[i][j][0]
                            smlnm = smiles[i][j][1]
                            chemf = smiles[i][j][2]

                            if j == 0:  # create only one MoleculeSet per chemical formula
                                mols_out.append(MolSet(i, 0, sname=smlnm, stype='output'))
                                print(f"{sname}::main(): Added {mols_out[i]} to the output list of species")

                            print(f"\n{sname}::main(): Calling  Smiles().getMolecule('{smile}', '{smlnm}') ...\n")
                            mols_out[i].addItem(Smiles(smile = smile, name = smlnm).getMolecule(smile = smile,
                                                                                                aname = smlnm[:5],
                                                                                                sdkinks = dbkinks,
                                                                                                putHatoms = True,
                                                                                                withTopology = False, #True,
                                                                                                alignZ = inpPars.getFlagAlignZ(),
                                                                                                verbose = False))
                            print(f"\n{sname}::main(): Added new molecule {i} based on SMILES '{smile}' \n"
                                  f" {mols_out[i][-1]}', chemf = '{chemf}', natoms = {len(mols_out[i][-1])} ...\n")
                            #mols_out[i][-1].refresh() #updateRvecs()
                            #print(f"\n{sname}::main(): Refreshed molecule {i} based on SMILES '{smile}' \n"
                            #      f" {mols_out[i][-1]}', chemf = '{chemf}', natoms = {len(mols_out[i][-1])} ...\n")
                            #sys.exit(0)
                    else:
                        # AB: in case of only one SMILES string (seems to be deprecated)
                        smile = smiles[i][0]
                        smlnm = smiles[i][1]
                        chemf = smiles[i][2]

                        mols_out.append(MolSet(0, 0, sname=smlnm, stype='output'))
                        print(f"{sname}::main(): Added {mols_out[i]} to the output list of species")

                        print(f"\n{sname}::main(): Calling  Smiles().getMolecule('{smile}', '{smlnm}') ...\n")
                        mols_out[i].addItem(Smiles(smile = smile, name = smlnm).getMolecule(smile = smile,
                                                                                            aname = smlnm[:5],
                                                                                            sdkinks = dbkinks,
                                                                                            putHatoms = True,
                                                                                            verbose = False))
                        print(f"\n{sname}::main(): Added new molecule {i} based on SMILES '{smile}' - \n"
                              f" name = '{mols_out[i][-1].name}', chemf = '{chemf}', natoms = {len(mols_out[i][-1])} ...\n")

            elif inpPars.getInpFile()[-4:] == sgro:

                fgro = iogro.groFile(dfinp)
                fgro.readInMols(rems_inp, mols_in, gbox, tuple(inpPars.getMolNames()), tuple(inpPars.getMolIDs()))

                hbox = 0.0
                hmax = 0.0
                if len(gbox) > 0:
                    hbox = np.array(gbox) * 0.5
                    hmax = np.amax(hbox)
                    print(f"{sname}::main(): Read-in GRO box: " + '({:>8.3f}{:>8.3f}{:>8.3f})'.format(*gbox) + \
                          "; " + '({:>8.3f}{:>8.3f}{:>8.3f})'.format(*hbox))  # +"\n")

                    cbox = np.array(gbox)
                    inpPars.setFlagBox(True)
                    if not inpPars.getFlagBox():
                        gbox[0] = 0.0
                        gbox[1] = 0.0
                        gbox[2] = 0.0

                    print(f"{sname}::main(): Read-in INI box: " + '({:>8.3f}{:>8.3f}{:>8.3f})'.format(*gbox) + \
                          "; " + '({:>8.3f}{:>8.3f}{:>8.3f})'.format(*hbox) + "\n")

            elif inpPars.getInpFile()[-4:] == sxyz:
                #TODO: REFACTOR in new style:
                #TODO: object lists to replace arrays
                #TODO: reading template molecules for each species from separate files

                # atms_inp = []
                # atms_inp.append([])
                # axyz_inp = []
                # axyz_inp.append([] * 3)
                # atms_out = []
                # atms_out.append([])
                # axyz_out = []
                # axyz_out.append([] * 3)

                atms_inp = []
                atms_inp.append([])
                axyz_inp = []
                axyz_inp.append([] * 3)
                print(f"{sname}::main(): Created atms_inp of " + str(len(atms_inp)) + " item(s) & axyz_inp of " + \
                      str(len(axyz_inp)) + " item(s) x " + str(axyz_inp[len(axyz_inp) - 1]) + "\n")

                read_mol_xyz(dfinp, rems_inp, atms_inp, axyz_inp)

                matms = len(atms_inp)
                natms = matms

            elif inpPars.getInpFile()[:6] == 'CONFIG' or inpPars.getInpFile()[-4:] == sdlp or inpPars.getInpFile()[-4:] == sdlm:
                #MS: currently assumes entire CONFIG file contains a single template molecule
                #MS: molecule name either taken from user input (if provided) or given default name
                #TODO: REFACTOR to use after reading in FIELD file to separate out and name individual molecules

                fconf = ioconfig.CONFIGFile(dfinp)
                mol_name = tuple(inpPars.getMolNames()) if len(inpPars.getMolNames())>0 else ('MOLECULE')

                # rescale positions to nm according to user input (default 0.1 for angstroms in DL_POLY, user selects for DL_MESO/DPD)

                lenscale = inpPars.getLenDPD()
                fconf.readInMols(rems_inp, mols_in, gbox, mol_name, lenscale)

            elif inpPars.getInpFile()[:5] == 'FIELD':
                #MS: reads DL_MESO FIELD file to find template molecule
                #MS: currently rejects DL_POLY FIELD files as these can only be used with a CONFIG file
                #TODO: REFACTOR to use contents of either type of FIELD file with a CONFIG file (see above)

                ffld = iofield.FIELDFile(dfinp)

                # rescale positions to nm according to user input

                lenscale = inpPars.getLenDPD()
                ffld.readInMols(rems_inp, mols_in, gbox, tuple(inpPars.getMolNames()), lenscale)

            elif inpPars.getInpFile()[-4:]==spdb:

                fpdb = iopdb.pdbFile(dfinp)
                
                # MS: automatically rescales positions and box size from angstroms to nm
                #     (removes restriction to only use PDB files for bilayers)
                
                fpdb.readInMols(rems_inp, mols_in, gbox, tuple(inpPars.getMolNames()), tuple(inpPars.getMolIDs()))

                #if ishape != 8:  # AB: any better solution?
                #    DMIN *= 10.0
                #    rmin *= 10.0
                
                #     for mols in mols_in:
                #         for mol in mols:
                #             for atm in mol.items:
                #                 atm.setRvec(atm.getRvec()*0.1)
                #     if isinstance(gbox, list) and len(gbox) > 2:
                #         gbox[0] *= 0.1
                #         gbox[1] *= 0.1
                #         gbox[2] *= 0.1
                #         cbox = np.array(gbox)
                #         inpPars.setFlagBox(True)
                #         if not inpPars.getFlagBox():
                #             gbox[0] = 0.0
                #             gbox[1] = 0.0
                #             gbox[2] = 0.0

            else:
                print(f"{sname}::main(): Unrecongnised input format: '" +
                      inpPars.getOutFile() + "' [no extension => DL_POLY/DL_MESO CONFIG]\n")
                sys.exit(2)

            if not inpPars.getFlagSmiles():

##### - Ordering species - START
                # AB: making sure the order of species is as specified in the input
                slen = -1
                if inpPars.getOutExt() == spdb: slen = 4
                mols_inp = []
                for i in range(len(inpPars.getMolNames())):
                    for j in range(len(mols_in)):
                        if slen == 4:
                            if (inpPars.getMolNames()[i])[:slen] == (mols_in[j][0].getName())[:slen]:
                                mols_inp.append(mols_in[j])
                                mols_inp[-1].indx = i
                                break
                        elif inpPars.getMolNames()[i] == mols_in[j][0].getName():
                            mols_inp.append(mols_in[j])
                            mols_inp[-1].indx = i
                            break
                if len(mols_in) != len(mols_inp):
                    print(f"\n{sname}::main(): Numbers of read-in and expected "
                          f"distinct molecules (species) differ - FULL STOP!!! ")
                    sys.exit(1)

                print(f"{sname}::main(): Read-in {mols_in}")

                mols_in = mols_inp
##### - Ordering species - DONE

                print(f"{sname}::main(): Ordered {mols_inp}")

                mmols = len(mols_in)
                if mmols > 0:
                    print(f"{sname}::main(): Read-in M_mols = " + str(mmols) +
                          ", Mol_Names = " + str([mol.name for mol in mols_in[:]]) +
                          ", N_mols = " + str([len(molset) for molset in mols_in]) +
                          ", N_atms = " + str([len(molset[0]) for molset in mols_in]) +
                          ", M_atms = " + str(sum(len(mols) for molset in mols_in for mols in molset)) + "\n")
                
##### - Figuring out bonding connections within molecule(s) - experimental!
                # print(f"\n{sname}::main(): *** Topology guessing based on .gro file (experimental) - START ***")
                #
                # #mols_in[0][0].assignBonds()
                # mols_in[0][0].guessBondsFromDistances()
                # mols_in[0][0].guessBondsFromAtomNames()
                #
                # print(f"\n{sname}::main(): *** Topology guessing based on .gro file (experimental) - END ***\n")
##### - Figuring out bonding connections within molecule(s) - DONE

##### - Density calculation - START
                if inpPars.getShapeType() == 'dens':
                    #and isinstance(inpPars.getDensNames(), list) \
                    #and len(inpPars.getDensNames()) > 0:

                    print(f"\n{sname}::main(): DensNames = {inpPars.getDensNames()} - "
                          f"doing density calcs ...")

                    rgrid = inpPars.getDensRange()

                    rlen = 1.0
                    molsys = MolSys(sname='mols_inp', stype='input',
                                    molsets=mols_in, vbox=list(gbox[:3]))

                    mtot = molsys.getMass()
                    isElemMass = molsys.setMassElems()
                    if not isElemMass:
                        print(f"\n{sname}::main(): MolSys failed to reset mass (a.u.) of all "
                              f"{int(mtot)} atoms -> {molsys.getMass()} "
                              f"(isMassElems = {molsys.isMassElems})")
                    else:
                        print(f"\n{sname}::main(): MolSys resetting mass (a.u.) of all "
                              f"{int(mtot)} atoms -> {molsys.getMass()} "
                              f"(isMassElems = {molsys.isMassElems})")

                    #if inpPars.getInpFile()[-4:] == sgro:
                    #rcom, rcog = molsys.getRvecs(isupdate=True)
                    rcom = molsys.items[0].getRcom(isupdate=True)
                    rcog = molsys.items[0].getRcog(isupdate=True)
                    print(f"\n{sname}::main(): MolSys(inp) initial Rcom[0] = {rcom}")
                    print(f"{sname}::main(): MolSys(inp) initial Rcog[0] = {rcog}")

                    rcom, rcog = molsys.getRvecs(isupdate=True)
                    print(f"\n{sname}::main(): MolSys(inp) initial Rcom[:] = {rcom}")
                    print(f"{sname}::main(): MolSys(inp) initial Rcog[:] = {rcog}")

                    # AB: center the system at the origin
                    bmin, bmax = molsys.getDims()
                    bbox = np.array(bmax) - np.array(bmin)
                    borg = (np.array(bmax) + np.array(bmin))*0.5
                    rcob = Vec3(*borg)

                    # cbox = bbox.copy()
                    # box0 = cbox.copy()
                    hbox = Vec3(*gbox)*0.5

                    molsys.moveBy(-hbox)

                    rcom = molsys.items[0].getRcom(isupdate=True)
                    rcog = molsys.items[0].getRcog(isupdate=True)
                    print(f"\n{sname}::main(): MolSys(inp) initial Rcom[0] = {rcom}")
                    print(f"{sname}::main(): MolSys(inp) initial Rcog[0] = {rcog}")

                    rcom, rcog = molsys.getRvecs(isupdate=True)
                    print(f"\n{sname}::main(): MolSys(inp) initial Rcom[:] = {rcom}")
                    print(f"{sname}::main(): MolSys(inp) initial Rcog[:] = {rcog}")

                    if inpPars.getMolNames()[-1] == 'TIP3':
                        rcom_pbc = Vec3()
                        rcog_pbc = Vec3()
                        mass_mset= 0.0
                        for im, molset in enumerate(molsys.items[:-1]):
                            # AB: testing / debugging
                            # rcom_mset = Vec3()
                            # rcog_mset = Vec3()
                            # for mol in molset:
                            #     rcom_mol, rcog_mol = mol.getRvecs(isupdate=True, box=cbox)
                            #     rcom_mset += rcom_mol * mol.getMass()
                            #     rcog_mset += rcog_mol
                            # rcom_mset /= molset.getMass()
                            # rcog_mset /= float(len(molset.items))

                            rcom0, rcog0 = molset.items[0].getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                            rcom_mset = rcom0.copy() * molset.items[0].getMass()
                            rcog_mset = rcog0.copy()
                            for mol in molset[1:]:
                                rcom, rcog = mol.getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                                rvec = rcom0 + pbc(rcom - rcom0, cbox)
                                rcom0 = rcom.copy()
                                mol.moveBy(rvec - rcom)
                                rcom_mset += rcom * mol.getMass()
                                rcog_mset += rcog
                            rcom_mset /= molset.getMass()
                            rcog_mset /= float(len(molset.items))

                            #rcom_mset, rcog_mset = molset.getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                            print(f"\n{sname}::main(): MolSet *PBC* Rcom[{im}] = {rcom_mset} "
                                  f"for {molset.name} of {molset.getMass()} m.a.u.- initial")
                            print(f"{sname}::main(): MolSet *PBC* Rcog[{im}] = {rcog_mset} "
                                  f"for {molset.name} of {molset.nitems} atoms  - initial")

                            mass_mset+= molset.getMass()
                            rcom_pbc += rcom_mset * molset.getMass()
                            rcog_pbc += rcog_mset
                        rcom_pbc /= mass_mset
                        rcog_pbc /= float(len(molsys.items[:-1]))
                        print(f"\n{sname}::main(): MolSet *PBC* Rcom[:] = {rcom_pbc} - initial")
                        print(f"{sname}::main(): MolSet *PBC* Rcog[:] = {rcog_pbc} - initial")
                    else:
                        rcom_pbc, rcog_pbc = molsys.getRvecs(isupdate=True, box=cbox)#, isMolPBC=True)
                        print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rcom_pbc} - initial")
                        print(f"{sname}::main(): MolSys *PBC* Rcog = {rcog_pbc} - initial")

                    # AB: just a test
                    rpbc = molsys.getRcomPBC(cbox)
                    print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rpbc} "
                          f"<-> Rcom[0] = {molsys.items[0].getRcomPBC(cbox)} ")
                    rpbc = molsys.getRcogPBC(cbox)
                    print(f"{sname}::main(): MolSys *PBC* Rcog = {rpbc} "
                          f"<-> Rcog[0] = {molsys.items[0].getRcogPBC(cbox)} ")

                    if inpPars.getShapeOrigin() == 'cob':
                        org = hbox
                    elif inpPars.getShapeOrigin() == 'com':
                        org = rcom_pbc
                    else:
                        org = rcog_pbc

                    molsys.moveBy(-org)
                    org = Vec3()

                    # AB: just a test
                    rpbc = molsys.getRcomPBC(cbox)
                    print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rpbc} "
                          f"<-> Rcom[0] = {molsys.items[0].getRcomPBC(cbox)} ")
                    rpbc = molsys.getRcogPBC(cbox)
                    print(f"{sname}::main(): MolSys *PBC* Rcog = {rpbc} "
                          f"<-> Rcog[0] = {molsys.items[0].getRcogPBC(cbox)} ")

                    if inpPars.getMolNames()[-1] == 'TIP3':
                        rcom_pbc = Vec3()
                        rcog_pbc = Vec3()
                        mass_mset= 0.0
                        for im, molset in enumerate(molsys.items[1:]):
                            rcom_mset, rcog_mset = molset.getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                            print(f"\n{sname}::main(): MolSet *PBC* Rcom[{im}] = {rcom_mset} "
                                  f"for {molset.name} of {molset.getMass()} m.a.u.- final")
                            print(f"{sname}::main(): MolSet *PBC* Rcog[{im}] = {rcog_mset} "
                                  f"for {molset.name} of {molset.nitems} atoms - final")
                            mass_mset+= molset.getMass()
                            rcom_pbc += rcom_mset * molset.getMass()
                            rcog_pbc += rcog_mset
                        rcom_pbc /= mass_mset
                        rcog_pbc /= float(len(molsys.items))
                        print(f"\n{sname}::main(): MolSet *PBC* R'com[:] = {rcom_pbc} - final")
                        print(f"{sname}::main(): MolSet *PBC* R'cog[:] = {rcog_pbc} - final")
                    else:
                        rcom_pbc, rcog_pbc = molsys.getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                        print(f"\n{sname}::main(): MolSys *PBC* R'com = {rcom_pbc} - final")
                        print(f"{sname}::main(): MolSys *PBC* R'cog = {rcog_pbc} - final")

                    # AB: just a test
                    rpbc = molsys.getRcomPBC(cbox)
                    print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rpbc} - total")
                    rpbc = molsys.getRcogPBC(cbox)
                    print(f"{sname}::main(): MolSys *PBC* Rcog = {rpbc} - total")

                    inpName = inpPars.getInpFile()[:-4]
                    sframe  = ''
                    if 'frame' in inpName:
                        iframe = inpName.find('frame')
                        sframe = '_'+inpName[iframe:]
                        inpName= inpName[:iframe]

                    fsfx = 'Rc'+str(rgrid[0])+'-'+str(rgrid[1])+'-'+str(rgrid[2])+'nm_'+inpPars.getShapeOrigin()
                    if 'msys' in inpPars.getDensNames():
                        inpName += fsfx+'_msys'+sframe
                        inpPars.getDensNames().remove('msys')
                        molsys.radialDensities(rorg=org, rmin=rgrid[1], rmax=rgrid[0], dbin=rgrid[2],
                                               clist=inpPars.getMolNames(),
                                               dlist=inpPars.getDensNames(),
                                               bname=inpName)
                                               #bname=inpPars.getInpFile()[:-4]+fsfx+'_msys')
                    if 'mset' in inpPars.getDensNames():
                        inpName += fsfx+'_mset_' #+molset.name+sframe
                        inpPars.getDensNames().remove('mset')
                        for molset in molsys.items:
                            inpNameSet = inpName + molset.name + sframe
                            molsys.radialDensities(rorg=org, rmin=rgrid[1], rmax=rgrid[0], dbin=rgrid[2],
                                                   clist=inpPars.getMolNames(),
                                                   dlist=inpPars.getDensNames(),
                                                   bname=inpNameSet)
                                                   #bname=inpPars.getInpFile()[:-4]+fsfx+'_mset_'+molset.name)

                    # AB: output initial PBC config
                    mols_out = molsys.items
                    rems = "GRO coords for molecule set " + setname[:-1] + rems_add
                    fout = dfout
                    fout = fout.replace('.gro', '_PBCmols.gro')
                    fgrow = iogro.groFile(fout, 'w')
                    fgrow.writeOutMols(rems, cbox, mols_out)

                    # AB: Exit for now
                    sys.exit(0)
##### - Density calculation - DONE

##### - Backbone directors - START
                # AB: check for the (back-)bone specs (mint/mext) and reset if needed
                isreset_mint = False
                if len(inpPars.getMolMint()) < len(mols_in):
                    print(f"{sname}::main(): WARNING! number of mint indices {len(inpPars.getMolMint())} "
                          f"< {len(mols_in)} (number of species read in) - resetting ...")
                    inpPars.delMolMint()
                    isreset_mint = True
                isreset_mext = False
                if len(inpPars.getMolMext()) < len(mols_in):
                    print(f"{sname}::main(): WARNING! number of mext indices {len(inpPars.getMolMext())} "
                          f"< {len(mols_in)} (number of species read in) - resetting ...")
                    inpPars.delMolMext()
                    isreset_mext = True
                if (ishape != 4 and abs(ishape) < 6) or (ishape == 8): 

                    # AB: figure out the (back-)bone vectors for all species where needed
                    for m in range(len(mols_in)):
                        # AB: assuming atom indexing starts from 'head-group'
                        # AB: take the first (usually 'heavy') atom as (back-)bone 'head'
                        mbeg = 0
                        mend = mols_in[m][0].nitems - 1
                        if isreset_mext:
                            # AB: alternative 'defaults'
                            # if mols_in[m][0].name == 'SDS':
                            #     mbeg = 5  # the first C(H2) on SDS
                            # elif mols_in[m][0].name in {'CTA', 'DTA'}:
                            #     mbeg = 13  # the first C(H2) on CTAB(s)
                            # elif ...
                            if mols_in[m][0].name in inpPars.getMolNames():
                                for ia in range(mols_in[m][0].nitems):
                                    if mols_in[m][0][ia].name[0:1] in ['S', 'N', 'C']:
                                        mbeg = ia
                                        break
                            inpPars.setMolMext(mbeg)
                        else:
                            mbeg = inpPars.getMolMext()[m]
                        # MS: if using full atom model of known molecules, can immediately identify last
                        #     'heavy' atom in (back)-bone - otherwise (especially for CG representations)
                        #     need to scan through particles (backwards) to find last one with a 'heavy' atom
                        if isreset_mint:
                            mlen = len(mols_in[m][0])
                            if mols_in[m][0].name == 'SDS' and mlen == 42:
                                mend = 38 # terminal C(H3) on SDS
                            elif mols_in[m][0].name == 'CTA' and mlen == 44:
                                mend = 40  # terminal C(H3) on C(10)TAB
                            elif mols_in[m][0].name == 'DTA' and mlen == 50:
                                mend = 46  # terminal C(H3) on C(12)TAB=DTAB
                            elif mols_in[m][0].name in inpPars.getMolNames():
                                for ia in range(mols_in[m][0].nitems):
                                    if mols_in[m][0][mend - ia].name[0:1] in ['C', 'N', 'S']:
                                        mend -= ia
                                        break
                            inpPars.setMolMint(mend)
                        else:
                            mend = inpPars.getMolMint()[m]

                        isBackOrder = inpPars.getFlagRev()
                        if mend < mbeg:
                            isBackOrder = not isBackOrder
                            mend = inpPars.getMolMext()[m]
                            mbeg = inpPars.getMolMint()[m]

                        for n in range(len(mols_in[m])):
                            mols_in[m][n].setBoneBeg(mbeg)
                            mols_in[m][n].setBoneEnd(mend)
                            mols_in[m][n].setBoneOrder(isBackOrder)

                    print(
                        f"{sname}::main(): Setting molecule 'bone' indices: {inpPars.getMolMint()} ... {inpPars.getMolMext()} ->  "
                        f"{str([mol.getBoneInt() for mols in mols_in for mol in mols])} ... "
                        f"{str([mol.getBoneExt() for mols in mols_in for mol in mols])} & "
                        f"({inpPars.getMolNpick()} / {mpick})\n")
##### - Backbone directors - DONE

##### - Shape generation - START
                # rmin = max(inpPars.getShapeRadius(), DMIN * inpPars.getShapeLring() / TwoPi)  # min radius of the interior surface
                # print(f"{sname}::main(): Using Dmin = '" + str(DMIN) + "' for min C-atom separation")  # \n")
                # print(f"{sname}::main(): Using Rmin = '" + str(rmin) + "' for internal radius")  # \n")
                # print(f"{sname}::main(): Using Rbuf = '" + str(inpPars.getSolvBuff()) + "' for solvation buffer \n")

                if ishape == -2:  # surface placement
                # TODO: rework to use any input file (not just .xyz) for atom coordinates (and put into shapes.designs/protoshapes.py as a class)
                # TODO: coordinate with reworking of shapes.ioports/ioxyz.py into similar form as other input files

                    atms_inp = []
                    axyz_inp = []
                    print(f"{sname}::main(): Created atms_inp of " + str(len(atms_inp)) + " item(s) & axyz_inp of " + \
                          str(len(axyz_inp)) + " item(s) x 3\n")

                    dfinp = str(inpPars.getInpPath() + '/' + fwav)
                    read_mol_xyz(dfinp, rems_inp, atms_inp, axyz_inp)

                    matms = len(atms_inp)
                    natms = len(axyz_inp)

                    print(f"{sname}::main(): Total number of mesh atoms  = {matms} read-in from '{dfinp}'")
                    print(f"{sname}::main(): Total number of mesh points = {natms} read-in from '{dfinp}'")

                    # dimin = 7.0  # angstrom
                    # DMIN2 = dimin*dimin
                    # DMIN4 = 0.25 # nm
                    
                    # MS: all distances now in nm (including coordinates read in from xyz file)
                    inpPars.setMinDist(0.7)  # nm (7 angstroms)
                    DMIN2 = inpPars.getMinDist() * inpPars.getMinDist()
                    DMIN4 = 0.25 # nm
                    atms_inc = []
                    axyz_inc = []
                    mmols = len(mols_in)
                    print(f"\n{sname}::main(): Total # mols = {mmols}  with # atoms = {len(mols_in[m][0].items)} "
                          f"read-in from input")

                    xmin = HUGE
                    ymin = HUGE
                    zmin = HUGE
                    xmax = -HUGE
                    ymax = -HUGE
                    zmax = -HUGE

                    print(f"\n{sname}::main(): Number of layers to generate msurf = {msurf} ...")

                    natms = 0
                    latms = 0
                    for ia in range(0,matms,3):
                        is_inc = True
                        for ib in range(len(axyz_inc)):
                            dxyz2 = (axyz_inp[ia][0]-axyz_inc[ib][0])**2 + \
                                    (axyz_inp[ia][1]-axyz_inc[ib][1])**2 + \
                                    (axyz_inp[ia][2]-axyz_inc[ib][2])**2
                            if is_inc and dxyz2 < DMIN2:
                                is_inc = False
                                break
                        if is_inc:
                            latms += 1
                            atms_inc.append(atms_inp[ia])
                            axyz_inc.append(axyz_inp[ia])
                            for isf in range(msurf):
                                mplace = 1
                                if len(inpFracs) > 0: mplace = len(inpFracs[isf])
                                if latms == 1:
                                    print(f"\n{sname}::main(): ... generating layer {isf} with mplace = {mplace} ...")
                                iam = ia
                                iap = ia + isf + 1
                                if isf > 0:
                                    iam  = iap
                                    iap -= 1
                                    atms_inc.append(atms_inp[iam])
                                    axyz_inc.append(axyz_inp[iam])
                                else:
                                    atms_inc.append(atms_inp[iap])
                                    axyz_inc.append(axyz_inp[iap])
                                tvec = Vec3(axyz_inp[iap][0]-axyz_inp[iam][0],
                                            axyz_inp[iap][1]-axyz_inp[iam][1],
                                            axyz_inp[iap][2]-axyz_inp[iam][2])
                                m = 0
                                if mplace > 1:
                                    frnd = random.random()
                                    while frnd > inpFracs[0][m]:
                                        m += 1

                                mols_in[m][0].alignBoneToVec(tvec,
                                                             is_flatxz=inpPars.getFlagFxz(),
                                                             is_invert=inpPars.getFlagRev(),
                                                             be_verbose=True)
                                inpPars.delMolMint()
                                inpPars.delMolMext()  # Next two lines append to list:
                                inpPars.setMolMint(mols_in[m][0].bone_int)  # index of backbone interior atom - 38 'C12' for SDS (CHARMM-36)
                                inpPars.setMolMext(mols_in[m][0].bone_ext)  # index of backbone exterior atom - 0 for 'S1' or 5 'C1'  for SDS (CHARMM-36)
                                vec0 = np.array([axyz_inp[iap][0],axyz_inp[iap][1],axyz_inp[iap][2]])  # nm

                                vec0 -= tvec.arr3()*DMIN4/tvec.norm()
                                latms2 = latms/2
                                if (latms2 - int(latms2)) > 0.0:
                                    vec0 -= tvec.arr3()*DMIN4*0.5/tvec.norm()

                                mlast = len(mols_out[m].items)
                                mols_out[m].addItem(Molecule(mindx=mlast, aname=mols_in[m].name, atype='output'))
                                iatms = len(mols_in[m][0].items)
                                for i in range(iatms):
                                    vec2 = mols_in[m][0].items[i].rvec.arr3() - mols_in[m][0].items[inpPars.getMolMext()].rvec.arr3()
                                    vec1 = vec2 + vec0
                                    if xmin > vec1[0] : xmin = vec1[0]
                                    if ymin > vec1[1] : ymin = vec1[1]
                                    if zmin > vec1[2] : zmin = vec1[2]
                                    if xmax < vec1[0] : xmax = vec1[0]
                                    if ymax < vec1[1] : ymax = vec1[1]
                                    if zmax < vec1[2] : zmax = vec1[2]
                                    # add new molecule to the output
                                    mols_out[m].items[mlast].addItem(
                                        Atom(aname=mols_in[m][0].items[i].name, atype=mols_in[m][0].items[i].type,
                                             aindx=natms, arvec=list(vec1)))
                                    #print(f"{sname}:: Added {mols_out[m].items[mlast].items[-1]}")
                                    natms += 1

                    matms = len(atms_inc)

                    print(f"\n{sname}::main(): Total number of mesh points = {len(axyz_inc)} included from '{dfinp}'\n")
                    print(f"\n{sname}::main(): Total number of mesh atoms  = {len(atms_inc)} included from '{dfinp}'\n")
                    print(f"\n{sname}::main(): Total number of mols atoms  = {natms} generated for '{dfinp}'\n")

                    fname = 'config-waves_n'+str(natms)+'.xyz'
                    fmode = 'w'
                    fcode = 'utf-8'
                    try:
                        fio = open(fname, fmode, encoding=fcode)
                    except (IOError, EOFError) as err:
                        print(f"{sname}:: Oops! Could not open file '{fname}'"
                              f" in mode '{fmode}' - FULL STOP!!!\n")
                        sys.exit(2)
                    except:
                        print(f"{sname}:: Oops! Unknown error while opening file '{fname}' "
                              f"in mode '{fmode}' - FULL STOP!!!\n")
                        sys.exit(3)
                    ntot = matms
                    fio.write(str(ntot) + "\n")
                    fio.write(f"XYZ coordinates for atoms on a surface'\n")
                    for m in range(ntot):
                        aname = atms_inc[m]
                        line = '{:>4}'.format(aname) + ''.join('{:>14.5f}{:>15.5f}{:>15.5f}'.format(*axyz_inc[m]))
                        fio.write(line + "\n")
                    fio.close()

                    inpPars.setSolvBuff(0.0)
                    gbox[0] = (xmax-xmin)
                    gbox[1] = (ymax-ymin)
                    gbox[2] = (zmax-zmin)

                    cbox = np.array(gbox)

                    #sys.exit(0)

                # elif ishape == 5:  # spherical vesicle - ORIGINAL
                #
                #     Ball(isvesicle=True, rint=rmin, dmin=DMIN, ovec=Vec3(),
                #          mols_inp=mols_in, mols_out=mols_out).make(
                #                                               nlring=inpPars.getShapeLring(),
                #                                               frcl=inpFracs,
                #                                               is_flatxz=inpPars.getFlagFxz(),
                #                                               is_invert=inpPars.getFlagRev(),
                #                                               is_pinned=inpPars.getFlagPin())
                #                                               # AB: irrelevant here: alpha=alpha, theta=theta)
                #
                #     print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                #     print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")
                #
                #     bmax = max(gbox[:3])
                #     for j in range(min(3,len(gbox))): gbox[j] = bmax

                elif ishape == 5:  # spherical vesicle - NEW

                    sfx = 'nmr'
                    if inpPars.getShapeFill()[:5] == 'rings':
                        rmin0 = rmin
                        ballO = Ball(isvesicle=True, layers=inpPars.getShapeLayers(),
                                     rint=rmin0, dmin=DMIN, ovec=Vec3(),
                                     mols_inp=mols_in, mols_out=mols_out)
                        ballO.makeRings(nlring=inpPars.getShapeLring(),
                                        frcl=inpFracs,
                                        fill=inpPars.getShapeFill(),
                                        is_flatxz=inpPars.getFlagFxz(),
                                        is_invert=inpPars.getFlagRev(),
                                        is_pinned=inpPars.getFlagPin())

                    elif inpPars.getShapeFill() == 'fibo':
                        rmin0 = rmin
                        ballO = Ball(isvesicle=True, layers=inpPars.getShapeLayers(),
                                     rint=rmin0, dmin=DMIN, ovec=Vec3(),
                                     mols_inp=mols_in, mols_out=mols_out)
                        ballO.makeFibo(nmols=inpPars.getShapeNmols(),
                                       frcl=inpFracs,
                                       is_flatxz=inpPars.getFlagFxz(),
                                       is_invert=inpPars.getFlagRev(),
                                       is_pinned=inpPars.getFlagPin())
                        sfx = 'nmf'
                    else:
                        print(f"\n{sname}::main(): Unsupported option '--fill={inpPars.getShapeFill()}' "
                              f"- FULL STOP!!!")
                        sys.exit(1)

                    sub = '_' + inpPars.getShapeType() # + '_'
                    sfx += str(sum([len(ms) for ms in mols_out]))
                    dfout = dfout.replace(sub, sub + '_' + sfx) # )
                    print(f"\n{sname}::main(): Replacing '{sub}' -> '{sub + '_' + sfx}' "
                          f"in the output file name ...")

                    rmin = ballO.getRint()
                    if abs(rmin - rmin0) > TINY and inpPars.getShapeLring() < 2:
                    # AB: in case '--cavr' option was provided and rmin changed
                        subr = '_'+inpPars.getShapeType()+'_cr'
                        if subr in dfout:
                            rm = round(rmin * 10., 2)
                            srm = "{:<5}".format(rm).strip().rstrip('0')
                            if srm[-1:] == '.': srm = srm[:-1]
                            sfr = '_cr' + srm + 'A'
                            idx0 = dfout.index(subr)
                            idx1 = dfout[idx0+1:].index('_')+idx0+1
                            idx2 = dfout[idx1+1:].find('A')+idx1+2
                            if idx2 > len(dfout):
                                idx2 = -1
                            elif idx2 < 0:
                                idx2 = dfout[idx1+1:].rfind('.')+idx1+1
                            subr = dfout[idx1:idx2]
                            subn = '_cr'+srm+'A'
                            print(f"\n{sname}::main(): Replacing '{subr}' -> '{subn}' "
                                  f"in the output file name ...")
                            dfout = dfout.replace(subr, subn)

                    nmball = sum([len(ms) for ms in mols_out])
                    if ballO.getNmols() != nmball:
                        print(f"\n{sname}::main(): Inconsistent Nmols = {ballO.getNmols()} != {nmball} !..")

                    print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}")

                    bmax = max(gbox[:3])
                    for j in range(min(3,len(gbox))): gbox[j] = bmax

                # elif ishape == 3:  # ball = spherical micelle - ORIGINAL
                #
                #     Ball(rint=rmin, dmin=DMIN, ovec=Vec3(),
                #          mols_inp=mols_in, mols_out=mols_out).make(
                #                                               nlring=inpPars.getShapeLring(),
                #                                               frcl=inpFracs,
                #                                               is_flatxz=inpPars.getFlagFxz(),
                #                                               is_invert=inpPars.getFlagRev(),
                #                                               is_pinned=inpPars.getFlagPin())
                #                                               # AB: irrelevant here: alpha=alpha, theta=theta)
                #
                #     print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                #     print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")
                #
                #     bmax = max(gbox[:3])
                #     for j in range(min(3,len(gbox))): gbox[j] = bmax
                #     #for j in range(len(gbox)): gbox[j] = bmax

                elif ishape == 3:  # ball = spherical micelle - NEW

                    sfx = 'nmr'
                    if inpPars.getShapeFill()[:5] == 'rings':
                        rmin0 = rmin
                        ballO = Ball(rint=rmin0, dmin=DMIN, ovec=Vec3(),
                                     mols_inp=mols_in, mols_out=mols_out)
                        ballO.makeRings(nlring=inpPars.getShapeLring(),
                                        frcl=inpFracs,
                                        fill=inpPars.getShapeFill(),
                                        is_flatxz=inpPars.getFlagFxz(),
                                        is_invert=inpPars.getFlagRev(),
                                        is_pinned=inpPars.getFlagPin())

                    elif inpPars.getShapeFill() == 'fibo':
                        rmin0 = rmin
                        ballO = Ball(rint=rmin0, dmin=DMIN, ovec=Vec3(),
                                     mols_inp=mols_in, mols_out=mols_out)
                        ballO.makeFibo(nmols=inpPars.getShapeNmols(),
                                       frcl=inpFracs,
                                       is_flatxz=inpPars.getFlagFxz(),
                                       is_invert=inpPars.getFlagRev(),
                                       is_pinned=inpPars.getFlagPin())
                        sfx = 'nmf'
                    else:
                        print(f"\n{sname}::main(): Unsupported option '--fill={inpPars.getShapeFill()}' "
                              f"- FULL STOP!!!")
                        sys.exit(1)

                    sub = '_' + inpPars.getShapeType() # + '_'
                    sfx += str(sum([len(ms) for ms in mols_out]))
                    dfout = dfout.replace(sub, sub + '_' + sfx) #)
                    print(f"\n{sname}::main(): Replacing '{sub}' -> '{sub + '_' + sfx}' "
                          f"in the output file name ...")

                    rmin = ballO.getRint()
                    if abs(rmin - rmin0) > TINY and inpPars.getShapeLring() < 2:
                    # AB: in case '--cavr' option was provided and rmin changed
                        subr = '_'+inpPars.getShapeType()+'_cr'
                        #print(f"\n{sname}::main(): Replacing '{rmin0}' -> '{rmin}' "
                        #      f" in {subr}")
                        if subr in dfout:
                            rm = round(rmin * 10., 2)
                            srm = "{:<5}".format(rm).strip().rstrip('0')
                            if srm[-1:] == '.': srm = srm[:-1]
                            sfr = '_cr' + srm + 'A'
                            idx0 = dfout.index(subr)
                            idx1 = dfout[idx0+1:].index('_')+idx0+1
                            idx2 = dfout[idx1+1:].find('A')+idx1+2
                            if idx2 > len(dfout):
                                idx2 = -1
                            elif idx2 < 0:
                                idx2 = dfout[idx1+1:].rfind('.')+idx1+1
                            subr = dfout[idx1:idx2]
                            subn = '_cr'+srm+'A'
                            print(f"\n{sname}::main(): Replacing '{subr}' -> '{subn}' "
                                  f"in the output file name ...")
                            dfout = dfout.replace(subr, subn)

                    nmball = sum([len(ms) for ms in mols_out])
                    if ballO.getNmols() != nmball:
                        print(f"\n{sname}::main(): Inconsistent Nmols = {ballO.getNmols()} != {nmball} !..")

                    print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")

                    bmax = max(gbox[:3])
                    for j in range(min(3,len(gbox))): gbox[j] = bmax
                    #for j in range(len(gbox)): gbox[j] = bmax

                elif ishape == 2:  # rod/cylinder with caps

                    # Rod(nturns=inpPars.getShapeTurns(), rint=rmin, dmin=DMIN, ovec=Vec3(),
                    #     mols_inp=mols_in, mols_out=mols_out).makeNew(
                    Rod(nturns=inpPars.getShapeTurns(), rint=rmin, dmin=DMIN, ovec=Vec3(),
                        mols_inp=mols_in, mols_out=mols_out).makeNew(
                                                             nlring=inpPars.getShapeLring(),
                                                             frcs=inpFracs,
                                                             is_flatxz=inpPars.getFlagFxz(),
                                                             is_invert=inpPars.getFlagRev())
                                                             # AB: irrelevant here: alpha, theta, is_pinned = is_pinned)
                    sfx   = 'nmr'
                    sub   = '_'+inpPars.getShapeType() # +'_'
                    sfx  += str(sum([len(ms) for ms in mols_out]))
                    dfout = dfout.replace(sub, sub + '_' + sfx)  #)
                    print(f"\n{sname}::main(): Replacing '{sub}' -> '{sub + '_' + sfx}' "
                          f"in the output file name ...")

                    print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")

                elif ishape == 8:
                    Bilayer(mols_inp=mols_in, mols_out=mols_out,nside=inpPars.getNside()).make(zsep=inpPars.getZsep(), dmin=DMIN)
                    print(f"\n{sname}::main(): Total number of molecules OUT = {sum(len(mols) for mols in mols_out)}")
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")

                elif abs(ishape) > 5: #and (ishape != 8):  # lattice(s)

                    Lattice(nx=inpPars.getNlatx(), ny=inpPars.getNlaty(), nz=inpPars.getNlatz(),
                            ovec=Vec3()).make(gbox=gbox,
                            mols_inp=mols_in, mols_out=mols_out,
                            alpha=inpPars.getAlpha(), theta=inpPars.getTheta(),
                            hbuf=inpPars.getSolvBuff(), ishape=ishape)

                    for mols in mols_out:
                        print(mols, len(mols))
                    print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}")
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")

                else:  # ring/stack/spiral (depending on input)
                    Ring(rint=rmin, dmin=DMIN, ovec=Vec3(),
                         mols_inp=mols_in, mols_out=mols_out).makeNew(
                                                              alpha=inpPars.getAlpha(),
                                                              theta=inpPars.getTheta(),
                                                              nmols=inpPars.getShapeLring(),
                                                              frcs=inpFracs,
                                                              is_flatxz=inpPars.getFlagFxz(),
                                                              is_invert=inpPars.getFlagRev())
                                                              #is_pinned=inpPars.getFlagPin())
                                                              # AB: irrelevant here: is_pinned = is_pinned)

                    print(f"\n{sname}::main(): Total number of molecules = {sum(len(mols) for mols in mols_out)}\n") ###
                    print(f"{sname}::main(): Molecule distribution = {[len(mols) for mols in mols_out]}\n")
##### - Shape generation - DONE

##### - Structure placement - START
            if abs(ishape) < 6 or ishape == 8: # not for the lattice cases (???)
                cbox = np.array([0.0, 0.0, 0.0])
                rlen = 1.0

                molsys = MolSys(sname='mols_out', stype='output',
                                molsets=mols_out, vbox=gbox)

                mtot = molsys.getMass()
                isElemMass = molsys.setMassElems()
                if not isElemMass:
                    print(f"\n{sname}::main(): MolSys failed to reset mass (a.u.) of all "
                          f"{int(mtot)} atoms -> {molsys.getMass()} "
                          f"(isMassElems = {molsys.isMassElems})")
                else:
                    print(f"\n{sname}::main(): MolSys resetting mass (a.u.) of all "
                          f"{int(mtot)} atoms -> {molsys.getMass()} "
                          f"(isMassElems = {molsys.isMassElems})")

                # AB: Rescale to nm and get Rcom & Rcog for the entire molecular system
                if inpPars.getFlagSmiles():
                    # MS: convert angstroms used for SMILES creation to appropriate lengthscale for
                    #     required output file - nm for GRO files, DPD lengthscales for DL_MESO CONFIG
                    #     (lenscale will retain angstroms for xyz, PDB and DL_POLY CONFIG)
                    if inpPars.getOutFile()[-4:]==sgro:
                        rcom, rcog = molsys.getRvecsScaled(rscale=0.1, isupdate=True)
                    else:
                        lenscale = inpPars.getLenDPD()  # defaults to 0.1 (nm)
                        rlen = 1.0/lenscale
                        rcom, rcog = molsys.getRvecsScaled(rscale=0.1*rlen, isupdate=True)
                elif inpPars.getOutFile()[-4:] != sgro:
                    # MS: if not using GRO file as output, convert nm to angstroms
                    #     (xyz, PDB, DL_POLY CONFIG) or DPD lengthscales (DL_MESO CONFIG)
                    lenscale = inpPars.getLenDPD()
                    rlen = 1.0/lenscale
                    rcom, rcog = molsys.getRvecsScaled(rscale=rlen, isupdate=True)
                else:
                    # MS: if using GRO file as output, retain nm as lengthscale
                    #     but still calculate Rcom and Rcog for entire system
                    # rcom = molsys.items[0].getRcom(isupdate=True)
                    # rcog = molsys.items[0].getRcog(isupdate=True)
                    # print(f"\n{sname}::main(): MolSys initial Rcom[0] = {rcom}")
                    # print(f"{sname}::main(): MolSys initial Rcog[0] = {rcog}")
                    # if len(molsys) > 1:
                    #     rcom = molsys.items[1].getRcom(isupdate=True)
                    #     rcog = molsys.items[1].getRcog(isupdate=True)
                    #     print(f"\n{sname}::main(): MolSys initial Rcom[1] = {rcom}")
                    #     print(f"{sname}::main(): MolSys initial Rcog[1] = {rcog}")
                    rcom, rcog = molsys.getRvecs(isupdate=True)

                # MS: all distances and box sizes from this point onwards using and reporting
                # in correct units for output file
                print(f"\n{sname}::main(): MolSys initial Rcom = {rcom}")
                print(f"{sname}::main(): MolSys initial Rcog = {rcog}")

                # AB: center the system at the origin and apply solvation buffer
                bmin, bmax = molsys.getDims()
                bbox = np.array(bmax) - np.array(bmin)
                borg = (np.array(bmax) + np.array(bmin))*0.5
                rcob = Vec3(*borg)

                cbox = bbox.copy()
                box0 = cbox.copy()

                # AB: allow for BUFF=0.0 (exactly)
                sbuf = BUFF  # abs(inpPars.getSolvBuff())
                if sbuf > 0.00001:
                    if ishape == 8:
                        cbox[2] += sbuf*rlen
                    else:
                        cbox += np.array([sbuf, sbuf, sbuf])*rlen

                    box0 = cbox.copy()
                    if inpPars.getSolvBuff() < -TINY:
                        # AB: set box size(s) to max (only if max differs < 1%)
                        maxb = max(cbox)
                        maxr = 1.01
                        if maxb / cbox[0] < maxr:
                            cbox[0] = maxb
                        if maxb / cbox[1] < maxr:
                            cbox[1] = maxb
                        if maxb / cbox[2] < maxr:
                            cbox[2] = maxb

                hbox = cbox * 0.5
                obox = hbox  # set to [0.,0.,0.] in non-Gromacs cases
                if rlen > 1.0 : obox = np.array([0.0]*3)

                if len(inpPars.getShapeOffset()) == 3:
                    rvoff = inpPars.getShapeOffset()
                    if abs(rvoff[0])+abs(rvoff[1])+abs(rvoff[2]) < TINY:
                        shift =-Vec3(*obox)
                    else:
                        shift = Vec3(*rvoff)
                    # print(f"\n{sname}::main(): MolSys will move structure "
                    #       f"by offset {np.array(shift)} ...")
                else:
                    shift = Vec3()

                print(f"\n{sname}::main(): MolSys rcob = {list(rcob)}\n"
                      f"{sname}::main(): MolSys bbox = {bbox}\n"
                      f"{sname}::main(): MolSys sbox = {box0} -> {cbox}\n"
                      f"{sname}::main(): MolSys hbox = {hbox}\n"
                      f"{sname}::main(): MolSys orig = {obox} + {list(shift)}")

                # AB: output initial config
                # rems = "GRO coords for molecule set " + setname[:-1] + rems_add
                # fout = dfout
                # fout = fout.replace('.gro', '_ini.gro')
                # fgrow = iogro.groFile(fout,'w')
                # fgrow.writeOutMols(rems, cbox, mols_out)

                # AB: just a test
                rcom_pbc, rcog_pbc = molsys.getRvecs(isupdate=True, box=cbox, isMolPBC=True)
                print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rcom_pbc}")
                print(f"{sname}::main(): MolSys *PBC* Rcog = {rcog_pbc}")

                # AB: output initial PBC config
                # rems = "GRO coords for molecule set " + setname[:-1] + rems_add
                # fout = dfout
                # fout = fout.replace('.gro', '_pbc0.gro')
                # fgrow = iogro.groFile(fout, 'w')
                # fgrow.writeOutMols(rems, cbox, mols_out)

                if inpPars.getShapeOrigin() == 'cob':
                    molsys.moveBy(shift-rcob)
                    print(f"\n{sname}::main(): MolSys placing bounding box "
                          f"at {np.array(shift+obox)} ...")
                    rcom += shift-rcob
                    rcog += shift-rcob
                elif inpPars.getShapeOrigin() == 'cog':
                    molsys.moveBy(shift-rcog)
                    print(f"\n{sname}::main(): MolSys placing structure COG "
                          f"at {np.array(shift+obox)} ...")
                    rcob += shift-rcog
                    rcom += shift-rcog
                elif inpPars.getShapeOrigin() == 'com':
                    molsys.moveBy(shift-rcom)
                    print(f"\n{sname}::main(): MolSys placing structure COM "
                          f"at {np.array(shift+obox)} ...")
                    rcob += shift-rcom
                    rcog += shift-rcom

                rcom, rcog = molsys.getRvecs(isupdate=True, box=cbox, isMolPBC=True)

                # AB: output final PBC config
                # rems = "GRO coords for molecule set " + setname[:-1] + rems_add
                # fout = dfout
                # fout = fout.replace('.gro', '_pbc1.gro')
                # fgrow = iogro.groFile(fout, 'w')
                # fgrow.writeOutMols(rems, cbox, mols_out)

                # AB: just a test
                rpbc = molsys.getRcomPBC(cbox)
                print(f"\n{sname}::main(): MolSys *PBC* Rcom = {rpbc}")
                rpbc = molsys.getRcogPBC(cbox)
                print(f"{sname}::main(): MolSys *PBC* Rcog = {rpbc}")

                print(f"\n{sname}::main(): MolSys final Rcob = {rcob+obox}")
                print(f"{sname}::main(): MolSys final Rcom = {rcom+obox}")
                print(f"{sname}::main(): MolSys final Rcog = {rcog+obox}\n")
##### - Structure placement - DONE

##### - Output - START
            if inpPars.getOutFile()[-4:] == sgro:
                # print(f"{sname}::main(): GRO input '"+finp+"' => GRO output '"+fout+"'\n")
###
# AB: Shape generation and structure placement bits have been moved above!
###
                # AB: output the final structure
                # rems = rems_inp[0]
                rems = "GRO coords for molecule set " + setname[:-1] + rems_add
                fgrow = iogro.groFile(dfout,'w')
                fgrow.writeOutMols(rems, cbox, mols_out)
                #print(f"\n{sname}::main(): Written output file '{dfout}' =?= '{inpPars.getOutFile()}' ...")

            elif inpPars.getOutFile()[-4:] == sxyz:
                print(f"{sname}::main(): GRO input '" + inpPars.getInpFile() + "' => XYZ output '" + inpPars.getOutFile() + "'\n")
            elif inpPars.getOutFile()[-4:] == spdb:
                rems = "PDB coords for molecule set " + setname[:-1] + rems_add
                fpdb = iopdb.pdbFile(dfout,'w')
                fpdb.writeOutMols(rems, cbox, mols_out)
            elif inpPars.getOutFile()[:6] == 'CONFIG' or inpPars.getOutFile()[-4:] == sdlp or inpPars.getOutFile()[-4:] == sdlm:
                rem = "CONFIG coords for molecule set " + setname[:-1] + rems_add
                fconfw = ioconfig.CONFIGFile(dfout,'w')
                fconfw.writeOutMols(rem, cbox, mols_out)
            else:
                print(f"{sname}::main(): Unrecognised output format: '" + inpPars.getOutFile() + "' [no extension => DL_POLY/DL_MESO CONFIG]\n")
                sys.exit(3)
##### - Output - DONE

        else:
            print(f"{sname}::main(): input file not found: '{inpPars.getInpFile()}'".format(dfinp))
            sys.exit(2)
    else:
        print(f"{sname}::main(): directory not found: '{inpPars.getInpPath()}'".format(inpPars.getInpPath()))
        sys.exit(2)

# end of main(argv)


if __name__ == "__main__":
    main(sys.argv)

sys.exit(0)

### END OF SCRIPT ###
