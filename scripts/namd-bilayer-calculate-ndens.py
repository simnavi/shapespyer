#!/usr/bin/env python3
"""
The script calculates the number densities of every single atom, averaged over a DCD trajectory.

Usage:
  namd-bilayer-calculate-ndens.py -d <dcd_file> -t <topology_file>
  namd-bilayer-calculate-ndens.py (-h | --help)

Options:
  -h --help                Show this screen.
  -d <dcd_file>            Input DCD file.
  -t <topology_file>       Input topology file.
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#  Contrib: Dr Valeria Losasso (c) 2024          #
#           BSc Saul Beck (c) 2024               #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"


import numpy as np
from shapes.ioports.iodcd import DCDFile
from docopt import docopt

# AB: The following import only works upon installing Shapespyer:
# pip3 install $PATH_TO_shapespyer
from shapes.basics.functions import timing


class NumberDensityCalculator:
    """
    A class to calculate number densities for atoms along the z-axis of a simulation box.
    This class reads a DCD trajectory and topology file, and calculates the number of atoms per bin in the z-dimension, normalised by the volume of each slice.
    """
    
    def __init__(self, dcd_reader, topology_file, bin_size=0.5):
        """
        Initialise the number density calculator with the DCDReader object and the topology file.
        
        :param dcd_reader: A custom DCDReader object (from SB & AB 's DCDReader.py script) that loads and reads frames from a DCD trajectory.
        :param topology_file: A topology file containing atom and residue information.
        :param bin_size: Size of each bin in the z-direction (default 0.5 Angstroms).
        """
        self.dcd_reader = dcd_reader  # Trajectory data from DCDReader
        self.topology_file = topology_file  # Topology file path
        self.bin_size = bin_size  # Size of each bin along z-axis for density calculations
        self.atom_residue_pairs = []  # List to store (residue_name, atom_name) pairs from the topology
        self.z_lengths = []  # List to store z-dimension lengths from each frame in the trajectory
        self.densities = {}  # Dictionary to store density data for each atom-residue pair
        self.atom_residue_pairs = self.parse_topology()  # Parse topology to get atom-residue pairs
        self.calculate_number_densities()  # Calculate the number densities for the trajectory

    @timing
    def parse_topology(self):
        """
        Parse the topology file to extract atom-residue pairs.
        
        :return: A list of (residue_name, atom_name) tuples.
        """
        atom_residue_pairs = []
        with open(self.topology_file, 'r') as f:
            in_atom_section = False  # Flag to indicate if the parser is in the atom section of the topology file
            for line in f:
                if "!NATOM" in line:
                    in_atom_section = True  # Start reading atom section
                    continue
                if "!NBOND" in line:
                    in_atom_section = False  # End reading atom section
                    break
                if in_atom_section:
                    fields = line.split()
                    if len(fields) > 4:  # Ensure that we are parsing valid lines
                        atom_name = fields[4]  # Atom name is in the 5th column
                        residue_name = fields[3]  # Residue name is in the 4th column
                        atom_residue_pairs.append((residue_name, atom_name))  # Append the atom-residue pair
        return atom_residue_pairs
    # end of parse_topology()

    @timing
    def calculate_number_densities(self):
        """
        Calculate the number densities of atoms along the z-axis for each frame in the DCD trajectory.
        This function bins atom positions in the z-dimension and normalises the counts by the volume of the bin slice.
        """
        max_z_length = 0  # Track the maximum z-length across all frames

        # Iterate over each frame in the DCD trajectory
        for frame in self.dcd_reader.frames:
            cell_params = frame[0]
            a = cell_params.a
            b = cell_params.b
            c = cell_params.c
            box_size = [a, b, c]  # The box size is stored in the first element of the frame
            z_length = c  # z-dimension length of the simulation box
            self.z_lengths.append(z_length)  # Store the z-length for later averaging

            area = box_size[0] * box_size[1]  # Area of the xy-plane
            num_bins = int(z_length / self.bin_size)  # Number of bins based on z_length and bin size
            max_z_length = max(max_z_length, z_length)  # Update the maximum z-length

            # Create a zero-initialised density array for each atom-residue pair
            frame_densities = {pair: np.zeros(num_bins) for pair in self.atom_residue_pairs}

            coordinates = frame[1]  # Atom coordinates (2nd element of the frame)
            # Assign atoms to z-bins and count the occurrences
            for atom_index, (residue_name, atom_name) in enumerate(self.atom_residue_pairs):
                z_coord = coordinates[atom_index, 2]  # Get the z-coordinate of the atom
                bin_index = int(z_coord / self.bin_size)  # Calculate the bin index
                frame_densities[(residue_name, atom_name)][bin_index] += 1

            # Normalise the bin counts by the volume of the slice
            for pair, density in frame_densities.items():
                for i in range(num_bins):
                    volume_of_slice = area * self.bin_size  # Volume of each bin slice
                    if volume_of_slice > 0:  # Avoid division by zero
                        frame_densities[pair][i] /= volume_of_slice

            # Store the densities for this frame
            for pair, density in frame_densities.items():
                if pair not in self.densities:
                    self.densities[pair] = []  # Initialise if not present
                self.densities[pair].append(density)

        # Determine the total number of bins based on the maximum z_length across all frames
        self.total_bins = int(max_z_length / self.bin_size)
    # end of calculate_number_densities()

    @timing
    def save_results(self):
        """
        Save the averaged number densities for each atom-residue pair into separate files.
        The results are averaged across all frames and written to .dat files for each atom-residue pair.
        """
        average_z_length = np.mean(self.z_lengths)  # Average z-dimension length across all frames

        # Loop over all atom-residue pairs and save their densities
        for pair, densities_per_frame in self.densities.items():
            residue_name, atom_name = pair

            # Interpolate each frame's density to match the maximum bin count (self.total_bins)
            densities_resampled = []
            for density in densities_per_frame:
                resampled_density = np.interp(
                    np.linspace(0, len(density), self.total_bins),
                    np.arange(len(density)),
                    density
                )
                densities_resampled.append(resampled_density)

            # Convert the resampled densities into a NumPy array (shape: num_frames x total_bins)
            densities_array = np.array(densities_resampled)

            # Average densities across frames for each bin
            average_density = np.mean(densities_array, axis=0)
            shift = -(len(average_density) // 2) - 1

            average_densities = np.roll(average_density, shift)
            # Generate z-values corresponding to the bin positions
            z_values = np.linspace(0, average_z_length, len(average_density))
            mid_z = average_z_length/2
            z_values = [(i if i < mid_z else i - average_z_length) for i in z_values]
            z_values = sorted(list(z_values))
            # Save the data to a file
            file_name = f"{atom_name}_{residue_name}.dat"
            with open(file_name, 'w') as f:
                z1 = None
                for z, d in zip(z_values, average_densities):
                    if np.isnan(d):  # Replace NaN values with 0
                        d = 0.0
                    if z != z1: 
                       f.write(f"{z:.8f} {d:.8f}\n")
                       z1 = z
            print(f"Saved {file_name}")
    # end of save_results()

# end of class NumberDensityCalculator


@timing
def main():
    """
    Main function to parse command line arguments and run the number density calculation.
    """
    args = docopt(__doc__)  # Parse arguments using docopt

    # Load the DCD trajectory
    dcd_reader = DCDFile(args['-d'])

    # Initialise the number density calculator
    calculator = NumberDensityCalculator(dcd_reader, args['-t'])

    # Save the results to files
    calculator.save_results()

if __name__ == "__main__":
    main()

