#!/bin/bash

################################################
#                                              #
#  Setup workflow for creating Gromacs inputs  #
#  based on initial configuration file (.gro)  #
#  for a complex multi-component system:       #
#  solutes [+ couter-ions] [+ salt] + solvent  #
#                                              #
#  Author: Andrey Brukhno, DL/STFC, July 2020  #
#  amended: June 2023, February 2024           #
#                                              #
################################################

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
cat <<EOF

///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
//   Workflow for running Gromacs MD jobs for complex multi-component systems    //
//   by Dr Andrey Brukhno (c) 2020-2024, Daresbury Laboratory, SCD, STFC, UKRI   //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////

- The script generates Gromacs inputs based on initial configuration file -

=============
 Main usage: 
=============

> ${0##*/} -h
> ${0##*/} --help
> ${0##*/} <config_name> <solute_names> <solute_atom_nums> [c-ion_names] [salt_pairs_num [salt_ion1 salt_ion2]]

-----------------------
 mandatory parameters:
-----------------------
 config_name    - initial configuration file name (must be found in the current directory)
 solute_name(s) - solute species names (comma-delimited; must be found in the topology database)
 solute_atom(s) - number of atoms per distinct solute molecule, i.e. species (comma-delimited)

----------------------
 optional parameters:
----------------------
 c-ion_name(s)  - solute counter-ion names (comma-delimited; must be found in the topology database)
 salt_pairs_num - number of salt pairs to add (-1 => number of solute molecules;  def: 0)
 salt_ion_name1 - positive salt ion name (must be found in the topology database; def: NAT)
 salt_ion_name2 - negative salt ion name (must be found in the topology database; def: CLA)

EOF
   exit 0
fi

echo
echo "////////////////////////////////////////////////////////////////////////////////"
echo "//                                                                            //"
echo "//   Workflow for creating Gromacs inputs for complex multicomponent systems //"
echo "//   by Dr Andrey Brukhno - 2020-2024, Daresbury Laboratory, SCD, STFC, UKRI  //"
echo "//                                                                            //"
echo "////////////////////////////////////////////////////////////////////////////////"
echo
echo

if [ $# -lt 3 ]; then
  #echo
  echo "Provide at least 3 arguments: config_file_name, solute_names, solute_atom_numbers (plus, possibly, couterion_names, salt_pairs_number, salt_ion_names)."
  echo
  exit
fi

# where to look for the extra topology (force-field) files
TOPDIR="./toppar"

# regexp for non-negative integers
NUMRX='^[0-9]+$'

# regexp for integers with sign
INTRX='^[+-]?[0-9]+$'

# regexp for floats with sign
FLTRX='^[+-]?[0-9]+([.][0-9]+)?$'

# number of solute molecules, atoms per molecule,
# total number of atoms in the solute species and (counter-) ions
let "NMOLS = 0"
let "MATMS = 0"
let "NATMS = 0"
let "NIONS = 0"

# file name for for the initial configuration (perhaps, generated with gen-shape.py3)
CONFI="$1"

# take the first word in the second line of $CONFI (must be the atom number in a .gro file)
let "NATMS = $(sed -n '2{p;q}' $CONFI)"

# count the actual number of atoms in config file (i.e. lines matching the regex pattern below)
#let "NFATM = $(grep -E '^[[:blank:]]+[0-9]+\w+[[:blank:]]+(\w+[[:blank:]]+)*' $CONFI | wc -l)"
let "NFATM = $(grep -c -E '^[[:blank:]]+[0-9]+\w+[[:blank:]]+(\w+[[:blank:]]+)*' $CONFI)"

if [ $NATMS -ne $NFATM ]; then
  echo
  echo "The number of atoms in species '$MNAME' ${NFATM} =/= ${NATMS} specified in the configuration file."
  echo
  exit
fi

IFS=',' read -ra SNAMES <<< "$(echo $2 | sed 's/^\[//;s/\]$//')"
IFS=',' read -ra SATOMS <<< "$(echo $3 | sed 's/^\[//;s/\]$//')"

let "NSPECS = ${#SNAMES[@]}"
if [ $NSPECS -ne ${#SATOMS[@]} ]; then
  echo
  echo "Specified number of species names (${NSPECS}) =/= number of atom numbers (${#SATOMS[@]}) - FULL STOP!"
  echo
  exit 1
fi

echo "Requested solute name(s): [${SNAMES[*]}] # ${#SNAMES[@]}"
echo "Solute(s) atom number(s): [${SATOMS[*]}] # ${#SATOMS[@]}"

# ion name for the primary solute counter-ions (NAT for SDS; BRA for CTAB)
STIONS=()
NTIONS=()
echo "Initially species C-ions: # ${#STIONS[@]} - [${STIONS[*]}] # (${NTIONS[*]} ${#NTIONS[@]} )"

if [ $# -gt 3 ]; then
  IFS=',' read -ra STIONS <<< "$(echo $4 | sed 's/^\[//;s/\]$//')"
  echo "Requested counter-ion(s): [${STIONS[*]}] # ${#STIONS[@]}"
  if [ $NSPECS -ne ${#STIONS[@]} ]; then
    #echo
    echo "Specified number of species names (${NSPECS}) =/= number of counter-ions (${#STIONS[@]}) - FULL STOP!"
    echo
    exit 1
  fi
  for i in $(seq -s ' ' 0 $(expr ${#STIONS[@]} - 1)); do
    if [ $(grep -c ":" <<< ${STIONS[$i]}) -gt 0 ]; then
      NTIONS+=(${STIONS[$i]#*:})
      STIONS[$i]=${STIONS[$i]%%:*}
    else
      NTIONS+=(0)
    fi
  done
fi
unset IFS

echo "Species C-ions: # ${#STIONS[@]} - [${STIONS[*]}] # (${NTIONS[*]} ${#NTIONS[@]} )"

# number of the salt pairs
let "NSALT = 0"

# names for the salt ions to add (default: NAT / CLA for NaCl)
PNAME="NAT"
NNAME="CLA"
if [ $# -gt 4 ]; then
  #if ! [[ $5 =~ $NUMRX ]] || [[ $4 -lt 0 ]] 2>/dev/null; then
  if ! [[ $5 =~ $INTRX ]]; then
    #echo
    echo "The 4-th parameter must be the salt pair number (give integer number or skip)."
    echo "Then you can also provide 2 names for salt ions (default: NAT / CLA for NaCl)."
    echo
    exit 1
  #elif [ $5 -lt 0 ]; then
  #  let "NSALT = NMOLS"
  else
    NSALT=$5
  fi

  if [ $# -eq 6 ]; then
    #echo
    echo "Provide 2 names for salt ions (default: NAT / CLA for NaCl, if the pair number =/= 0)."
    echo
    exit
  elif [ $# -eq 7 ]; then
    PNAME="$6"
    NNAME="$7"
#  else
#    PNAME="NAT"
#    NNAME="CLA"
  fi
fi

let "NWARN = 0"

echo
echo "Now creating topology template based on the parameters given ..."
echo

cat > template.top <<EOF
;
; Combined CHARMM FF topology file for GROMACS MD engine
;
; Generated by ${0##*/} script
; which is part of the general simulation workflow SimNavi
; Author: Andrey Brukhno, DL/STFC, July 2020
;
; Including the following force-field bits from the database:
#include "${TOPDIR}/forcefield.itp"
EOF

if [ ! -d ${TOPDIR} ]; then
  echo
  echo "WARNING: '${TOPDIR}' subdirectory not found here - to be resolved later on (not by this script)."
  echo
  let NWARN++
fi

TNAME=""
CNAME=""
ADDON=""
SNMOLS=()
MNAME="${SNAMES[0]}"
for i in $(seq 0 $(expr ${#SNAMES[@]} - 1)); do
  #echo "${i}-th species name: ${SNAMES[${i}]}"
  #echo "${i}-th species atoms: ${SATOMS[${i}]}"

  MNAME="${SNAMES[$i]}"
  MATMS="${SATOMS[$i]}"

  # count the number of lines containing the solute name
  let "NSATM = $(sed -e '1,2d' ${CONFI} | grep -c ${MNAME})"
  let "NMOLS = $NSATM / $MATMS"
  if [ $NMOLS -lt 1 ]; then
    echo
    echo "The requested solute molecule number < 1 - FULL STOP!"
    echo
    exit
  fi
  SNMOLS+=(${NMOLS})

  TNAME="${TNAME}${MNAME}${NMOLS}-"
  CNAME="${CNAME}${MNAME}${NMOLS}-"

  if [ ! -f ${TOPDIR}/${MNAME}.itp ]; then
    echo
    echo "WARNING: force-field file '${MNAME}.itp' included but not found! - Check '${TOPDIR}' subdirectory."
    echo
    let NWARN++
  fi

  cat >> template.top <<EOF
#include "${TOPDIR}/${MNAME}.itp"
EOF

  INAME=""
  if [[ ${#STIONS[@]} -gt 0 ]]; then
    INAME="${STIONS[${i}]}"
    #echo "INAME = $INAME -> NTIONS = ${NTIONS[$i]}"
    [[ ${NTIONS[$i]} -eq 0 ]] && let "NTIONS[$i] = $NMOLS"
    [[ $(grep -c "$INAME" <<< "$ADDON") -lt 1 ]] && ADDON="${ADDON} ${INAME}"
    echo "#STIONS = ${#STIONS[@]} => INAME = $INAME -> NTIONS = ${NTIONS[$i]} ..."
  fi
done

ADDON=${ADDON:1:${#ADDON}}
SYSTEM="${SNAMES} solution"
if [ "$ADDON" != "" ]; then
  SYSTEM="$SYSTEM with $ADDON counterions"
fi

echo "Found molecule number(s): [${SNMOLS[*]}] <-> [${SNAMES[*]}]"
echo "Counter-ions to be added: [${NTIONS[*]}] <-> [${STIONS[*]}]"
echo "Salt pairs to be added:   $NSALT [$PNAME $NNAME]"
echo

TNAME=${TNAME::-1}
CNAME=${CNAME::-1}
#echo "New config name: ${TNAME}"
#echo "New config name: ${CNAME}"

if [ $NSALT -gt 0 ]; then
  let "PSUM = 0"
  let "NSUM = 0"
  for i in $(seq -s ' ' 0 $(expr ${#STIONS[@]} - 1)); do
    if [ "${STIONS[$i]}" == "${PNAME}" ]; then
      let "NTIONS[$i] += $NSALT"
      let "PSUM = 1"
      echo "Adding $NSALT ${STIONS[$i]} extra ions (salt)"
    fi
    if [ "${STIONS[$i]}" == "${NNAME}" ]; then
      let "NTIONS[$i] += $NSALT"
      let "NSUM = 1"
      echo "Adding $NSALT ${STIONS[$i]} extra ions (salt)"
    fi
  done

  if [ ${PSUM} -eq 0 ]; then
    STIONS+=($PNAME)
    NTIONS+=($NSALT)
    echo "Adding $NSALT $PNAME salt ions"
  fi
  if [ ${NSUM} -eq 0 ]; then
    STIONS+=($NNAME)
    NTIONS+=($NSALT)
    echo "Adding $NSALT $NNAME salt ions"
  fi
  SYSTEM="${SYSTEM} and ${PNAME}-${NNAME} salt"
fi

SSIONS=()
SFIONS=()
NFIONS=()
for i in $(seq 0 $(expr ${#STIONS[@]} - 1)); do
  INAME=${STIONS[$i]}

  for j in $(seq $(expr ${i} + 1) $(expr ${#STIONS[@]} - 1)); do
    if [ "$INAME" == "${STIONS[$j]}" ] && [ ${NTIONS[$j]} -gt 0 ]; then
      let "NTIONS[$i] += ${NTIONS[$j]}"
      let "NTIONS[$j] = 0"
    fi
  done
  NIONS=${NTIONS[$i]}

  [[ $NIONS -eq 0 ]] && continue

  SSIONS+=("$INAME:$NIONS")
  SFIONS+=($INAME)
  NFIONS+=($NIONS)

  TNAME="${TNAME}-${INAME}${NIONS}"

  if [ ! -f ${TOPDIR}/${INAME}.itp ]; then
    echo
    echo "WARNING: force-field file '${PNAME}.itp' included but not found! - Check '${TOPDIR}' subdirectory."
    echo
    let NWARN++
  fi

  if [ $(grep -c "${INAME}.itp" template.top) -lt 1 ]; then
    cat >> template.top <<EOF
#include "${TOPDIR}/${INAME}.itp"
EOF
  fi
done

#echo "SNAMES = [${SNAMES[*]}]"
#echo "SNMOLS = [${SNMOLS[*]}]"
#echo "STIONS = [${SFIONS[*]}] <- [${STIONS[*]}]"
#echo "NTIONS = [${NFIONS[*]}] <- [${NTIONS[*]}]"
echo
echo "Summary of added ions: [${SSIONS[*]}] <- [${STIONS[*]}]:[${NTIONS[*]}]"
unset STIONS
unset NTIONS

WNAME="TIP3"
if [ ! -f ${TOPDIR}/${WNAME}.itp ]; then
  echo
  echo "WARNING: force-field file '${WNAME}.itp' included but not found! - Check '${TOPDIR}' subdirectory."
  echo
  let NWARN++
fi

echo
echo "The solute will be solvated with '${WNAME}' water model - standard in CHARMM FF."
echo "The task-name '${TNAME}-w${WNAME}' will be used - more actions to follow."
#echo

cat >> template.top <<EOF
#include "${TOPDIR}/${WNAME}.itp"

; System title
[ system ]
${SYSTEM}

; System composition:
; species   #
[ molecules ]
EOF

for i in $(seq 0 $(expr ${#SNAMES[@]} - 1)); do
  cat >> template.top <<EOF
${SNAMES[${i}]}   ${SNMOLS[${i}]}
EOF
done

for i in $(seq 0 $(expr ${#SFIONS[@]} - 1)); do
  if [ ${NFIONS[$i]} -gt 0 ]; then
    cat >> template.top <<EOF
${SFIONS[$i]}   ${NFIONS[$i]}
EOF
  fi
done

if [ $NWARN -gt 0 ]; then
  echo
  echo "Template topology file has been generated with CRITICAL WARNINGS # ${NWARN}!"
  echo
  exit
else
  echo
  echo "Template topology file has been generated!"
  echo
fi

cp -v $CONFI $CNAME.gro

echo
echo "Now working on configuration file ${CNAME}.gro ..."
#echo

let "NFATM = 0"
let "MIONS = 0"
MTIONS=$(expr ${#SFIONS[@]} - 1)
for i in $(seq 0 ${MTIONS}); do
  INAME="${SFIONS[${i}]}"
  NIONS=${NFIONS[$i]}
  let "MIONS += NIONS"

  if [ $NIONS -gt 0 ]; then
    if [ -f $TOPDIR/single-ion${INAME}.gro ]; then
      if [ $i -lt $MTIONS ]; then
        echo
        echo "Adding ${NIONS} '${INAME}' ions to configuration ..."
        echo
        gmx-add-ions-solv.bsh $CNAME $NIONS ${TOPDIR}/single-ion${INAME}.gro

        mv -v ${CNAME}-ions${NIONS}.gro ${CNAME}-${INAME}${NIONS}.gro
        CNAME="$CNAME-${INAME}${NIONS}"
      else
        echo
        echo "Adding ${NIONS} '${INAME}' ions and solvent '${WNAME}' to configuration ..."
        echo
        gmx-add-ions-solv.bsh $CNAME $NIONS ${TOPDIR}/single-ion${INAME}.gro SOLV

        mv -v ${CNAME}-ions${NIONS}.gro ${CNAME}-${INAME}${NIONS}.gro
        mv -v ${CNAME}-ions${NIONS}-wspce.gro ${CNAME}-${INAME}${NIONS}-wspce.gro
        mv -v ${CNAME}-ions${NIONS}-w${WNAME}.gro ${CNAME}-${INAME}${NIONS}-w${WNAME}.gro
        CNAME="$CNAME-${INAME}${NIONS}-w${WNAME}"
      fi

      let "NFATM = $(sed -e '1,2d' ${CNAME}.gro | grep -c ${INAME})"
      if [ $NFATM -lt $NIONS ]; then
        #echo
        echo "The number of added ions '$INAME' ${NFATM} =/= ${NIONS} requested - FULL STOP!"
        echo
        exit
      fi
    else
      echo
      echo "Template file '$TOPDIR/single-ion${INAME}.gro' not found - FULL STOP!"
      echo
      exit
    fi
  fi
done

if [ ${MIONS} -eq 0 ]; then
  # add solvent only!
  gmx-add-ions-solv.bsh $CNAME 0 ${TOPDIR}/single-ions.gro SOLV
  CNAME="$CNAME-w${WNAME}"
fi

NATMS="$(sed -n '2{p;q}' ${CNAME}.gro)"
echo
echo "Number of atoms in file '${CNAME}.gro ' = $NATMS"

SNATOMS=""
let "MNATOMS = 0"
for i in $(seq 0 $(expr ${#SNAMES[@]} - 1)); do
  SNATOMS="${SNATOMS}${SATOMS[$i]} * ${SNMOLS[$i]} + "
  MNATOMS=$(echo "${MNATOMS} + ${SATOMS[$i]} * ${SNMOLS[$i]}" | bc)
done
echo "Number of atoms in solutes = ${SNATOMS::-3} = $MNATOMS"
echo "Number of ions added = $MIONS"

#if [ $MIONS -eq 0 ]; then 
#  # add solvent only!
#  gmx-add-ions-solv.bsh $CNAME 0 ${TOPDIR}/single-ions.gro SOLV
#fi

let "NWATM = $NATMS - $MNATOMS - $MIONS"
let "NFATM = $(sed -e '1,2d' ${CNAME}.gro | grep -c TIP3)"
if [ $NFATM -ne $NWATM ]; then
  echo
  echo "The number of found atoms '$WNAME' ${NFATM} =/= ${NWATM} = $NATMS - $MNATOMS - $MIONS - $NSALT - $NSALT calculated - FULL STOP!"
  echo
  exit
fi

if [ $NWATM -gt 0 ]; then

  let "NWMOL = $NWATM / 3"
  cat >> template.top <<EOF
${WNAME}   ${NWMOL}
EOF
  TNAME="${TNAME}-w${WNAME}"
  if ! [ $TNAME == $CNAME ]; then
    echo "The task-name and config-name mismatch: '$TNAME' =/= ${CNAME} - check the contents!"
    echo
    exit
  fi
else
  echo "The number of solvent atoms '$WNAME' ${NWATM} = 0 - no solvent was added!"
  echo
  exit
fi

echo

#mv -v template.top ${TNAME}.top
mv -v template.top ${CNAME}.top

echo

#rm -fv \#* *-bak*
rm -f \#* *-bak*

echo
echo "${0##*/} : ALL DONE!"
echo

exit

