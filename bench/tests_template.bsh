#!/bin/bash

SNAME="${0##*/}"
OTIME=''
BENCH=''

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
cat <<EOF

///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
//   Workflow for running Gromacs MD jobs for complex multi-component systems    //
//   by Dr Andrey Brukhno (c) 2020-2024, Daresbury Laboratory, SCD, STFC, UKRI   //
//                                                                               //
//   Aggregator for CI/CD testing of shape.py - structure generation script      //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////

 The script aggregates all the test runs of 'shape.py' via calls 
 of 'test_shape.bsh' from within the current directory. 
 In some cases extra comparisons of equivivalent outputs are done.

 Adding new tests must be done as follows:

 1. Run shape.py with arguments not covered by the current set of tests.

 2. If all goes alright and the developer is satisfied that the outputs 
 are correct to the best of their judgement, then run 'test_shape.bsh' 
 with the same arguments which will create a NEW TEST. - Then run it again 
 by repeating the same command, to ascertain that the test would pass next time.

 3. Finally, the command that generated the new test ('test_shape.bsh ...') 
 can be added to this script.

 Usage
 =====

 Cleaning to remove any temporary file(s) after unsuccessful test runs:
 '-c' or '--clean' option

 > $SNAME --clean

 Invoking 'time' command for benchmarking:
 '-b' / '--bench' option (if used, must be the first option)

 > $SNAME -b [other_option(s)]

EOF
  exit 0
else
  if [ "$1" == "-b" ] || [ "$1" == "--bench" ]; then
    OTIME='time '
    BENCH='-b'
    shift
  fi
  if [ "$1" == "-c" ] || [ "$1" == "--clean" ]; then
    rm CLI* YAML* CONFIG* *yaml *log *txt *diff
    exit 0
  fi
fi

#./test_shape.bsh <inp_file_1> <out_ext_1> <mol_name(s)_1> <shape_1> "<shape_parameters_1>"

#./test_shape.bsh <inp_file_2> <out_ext_2> <mol_name(s)_2> <shape_2> "<shape_parameters_2>"

# In cases where the two previous tests (with different inputs) must generate identical outputs,
# add the following:
#
#DIFF="$(diff -q ref/YAML_<out_file_1> ref/CLI_<out_file_2>)"
#
#if [ "$DIFF" != "" ]; then 
#  echo
#  echo "TEST FAILED! - Files must not differ: "
#  echo "ref/YAML_<out_file_1>"
#  echo "ref/CLI_<out_file_2>"
#  #echo
#fi

exit 0

