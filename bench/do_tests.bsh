#!/bin/bash

OTIME=''
BENCH=''
SNAME="${0##*/}"
TDIRS="$(find "./" -mindepth 1 -maxdepth 1 -type d | sed 's/.\///g' | sed 's/\///g')"
#TDIRS='SDS CTA CONFIG-dlp FIELD-dlm PDB NAMD'

#AB: just a test
#echo
#echo "TEST_DIRS = '$TDIRS' "
#echo
#for d in $TDIRS; do cd $d; pwd; ls -l; cd -; done
#exit

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
cat <<EOF

///////////////////////////////////////////////////////////////////////////////////
//                                                                               //
//   Workflow for running Gromacs MD jobs for complex multi-component systems    //
//   by Dr Andrey Brukhno (c) 2020-2024, Daresbury Laboratory, SCD, STFC, UKRI   //
//                                                                               //
//   Aggregator for CI/CD testing of shape.py - structure generation script      //
//                                                                               //
///////////////////////////////////////////////////////////////////////////////////

 Without options the script runs the tests found in this directory by cycling through
 all subdirectories (shapespyer/bench/*/) and invoking script 'tests.bsh' therein.

 Upon completion, it checks for failures by searching for subdirectory 'fails'
 in each test subdirectory (shapespyer/bench/*/fails/).

 Usage
 =====

 Cleaning to remove any temporary file(s) after unsuccessful test runs:
 '-c' or '--clean' option

 > $SNAME --clean

 Removing backed-up tests (*bkp subdirectories and files):
 '-bkp' option after option '-c' / '--clean'

 > $SNAME -c -bkp

 Invoking 'time' command for benchmarking:
 '-b' / '--bench' option (if used, must be the first option)

 > $SNAME -b [other_option(s)]

 Tests can be run selectively by adding the corresponding directory name(s):
 '-d' / '--dirs' option (if used, must be the first or second option)

 > $SNAME [-b] -d '<dir_names>' [other_option(s)]

 where <dir_names> stands for directory names delimited by either space ' ' of colon ':'.

 Creating new tests or renewing the existing ones:
 '-n' / '--new' / '--renew' option (if used, must be the last option)

 > $SNAME [-b] [-d '<dir_names>'] -n

 in which case the existing input and reference files are backed-up with suffix '-bkp'.

EOF
  exit 0
else
  if [ "$1" == "-b" ] || [ "$1" == "--bench" ]; then
    OTIME='time '
    BENCH='-b'
    shift
    echo
    echo "Using 'time' command for benchmarking..."
    echo
  fi
  if [ "$1" == "-d" ] || [ "$1" == "--dirs" ]; then
    shift
    if [ "$1" != " " ]; then
      TDIRS="$(echo $1 | sed 's/:/ /g')"
      shift
      #echo "TEST_DIRS = '$TDIRS' "
      #echo
      #exit
    fi
  fi

  echo
  echo "Working on tests in ${TDIRS}"
  echo

  if [ "$1" == "-c" ] || [ "$1" == "--clean" ]; then
    shift
    RB=""
    if [ "$1" == "-bkp" ] || [ "$1" == "bkp" ]; then
      RB="RB"
    fi
    # cleaning test subdirectories (from temporary files only)
    #for d in $TDIRS; do cd $d; echo "In '$d' : rm CLI* YAML* *yaml *log *txt *diff"; rm CLI* YAML* *yaml *log *txt *diff; cd -; done
    for d in $TDIRS; do
      cd $d
      echo "In $d : removing CLI* YAML* *yaml *log *txt *diff"
      rm CLI* YAML* *yaml *log *txt *diff*
      if [ ! -z "$RB" ]; then
        echo "In $d : removing backups..."
        rm -rv *bkp
      fi
      echo
      cd ../
    done

    echo "In bench (./) : removing *log *txt *diff*"
    rm *log *txt *diff*
    echo
    echo "Cleaning done, but check for subdir 'fails' in each subdir!"
    exit 0

  elif [ "$1" == "-u" ] || [ "$1" == "--update" ]; then
    # updating reference log file(s)
    for d in $TDIRS; do
      cd $d
      if [ -f tests.txt ]; then
        echo "In $d : tests.ref -> tests.ref-prv"
        mv tests.ref tests.ref-prv
        echo "In $d : tests.txt -> tests.ref"
        mv tests.txt tests.ref
      fi
      echo
      cd ../
    done
    exit 0

  elif [ "$1" == "-n" ] || [ "$1" == "--new" ] || [ "$1" == "--renew" ]; then
    # creating new references for test(s)
    for d in $TDIRS; do
      cd $d
      if [ -d ref ]; then
        echo "In $d : backing-up ref -> ref-bkp (will be renewed!)"
        mv ref ref-bkp
        mv tests.ref tests.ref-bkp
      fi
      if [ -f tests.ref ]; then
        echo "In $d : tests.ref -> tests.ref-bkp"
        mv tests.ref tests.ref-bkp
      fi

      mv -v inp inp-bkp
      cp -r inp-bkp inp
      echo "In $d : removing inp/*yaml (will be renewed!)"
      rm inp/*yaml

      echo "In $d : renewing reference outputs..."
      ${OTIME}./tests.bsh ${BENCH} > tests.new 2>&1

      echo "In $d : check the sizes of input and reference directories:"
      du -hs inp-bkp
      du -hs inp
      du -hs ref-bkp
      du -hs ref

      [[ -f ./new_conf.diffs ]] && rm -v ./new_conf.diffs
      [[ -f ./new_yaml.diffs ]] && rm -v ./new_yaml.diffs
      [[ -f ./new_logs.diffs ]] && rm -v ./new_logs.diffs

      # check the new outputs against the old ones:
      #for f in $(ls -A 'ref-bkp'); do [[ ! "${f:(-4)}" =~ ^(yaml|.yml|.log)$ ]] && [[ "$(diff -q ref-bkp/${f} ref/${f})" != "" ]] && echo "File '${f}' altered upon renewal!"; done;
      for f in $(ls -A 'ref-bkp'); do
        if [ -f ref/${f} ]; then
          if [[ ! "${f:(-4)}" =~ ^(yaml|.yml|.log)$ ]]; then
            if [ "$(diff -q ref-bkp/${f} ref/${f})" != "" ]; then
              echo "File '${f}' altered upon renewal!" >> ./new_conf.diffs
            fi
          elif [[ "${f:(-4)}" =~ ^(yaml|.yml)$ ]]; then
            if [ "$(diff -q ref-bkp/${f} ref/${f})" != "" ]; then
              echo "File '${f}' altered upon renewal!" >> ./new_yaml.diffs
            fi
          elif [[ "${f:(-4)}" == ".log" ]]; then
            if [ "$(diff -q ref-bkp/${f} ref/${f})" != "" ]; then
              echo "File '${f}' altered upon renewal!" >> ./new_logs.diffs
            fi
          fi
        else
          echo "File '${f}' missing upon renewal!" >> ./new_conf.diffs
        fi
      done

      if [ -f ./new_conf.diffs ]; then
        echo
        echo "In $d : some of new configs DIFFER - check report in 'new_conf.diffs'..."
        echo
      fi

      echo "In $d : running tests..."
      ${OTIME}./tests.bsh ${BENCH} > tests.txt 2>&1
      if [ -d fails ]; then
        echo "In $d : some of new tests FAILED - check subdirectory 'fails'..."
      else
        mv -v tests.txt tests.ref
        echo "In $d : ALL new tests PASSED..."
      fi
      echo
      cd ../
    done
    exit 0

  elif [ "$1" == "-r" ] || [ "$1" == "--revert" ]; then
    # reverting previous backup(s)
    for d in $TDIRS; do
      cd $d
      echo "In $d : reverting previous backup..."
      if [ ! -d inp-bkp ] || [ ! -d ref-bkp ]; then
        echo "In $d : either 'inp-bkp' or 'ref-bkp' not found... skipping"
      else
        if [ -d inp-bkp ]; then
          if [ ! -z "$(ls -A 'inp-bkp')" ]; then
            rm -r inp
            mv -v inp-bkp inp
          fi
        #else
        #  echo "In $d : subdirectory 'inp-bkp' not found..."
        fi
        if [ -d ref-bkp ]; then
          if [ ! -z "$(ls -A 'ref-bkp')" ]; then
            rm -r ref
            mv -v ref-bkp ref
          fi
        #else
        #  echo "In $d : subdirectory 'ref-bkp' not found..."
        fi
        if [ -f tests.ref-bkp ]; then
          mv tests.ref-bkp tests.ref
        else
          echo "In $d : log file 'tests.ref-bkp' not found..."
        fi
      fi
      echo
      cd ../
    done
    exit 0

  fi
fi

# reverting previous backing-up
#for d in $TDIRS; do cd $d; echo "Now in $d"; rm -r ref; mv -v ref-bkp ref; mv tests.ref-bkp tests.ref; rm -r inp; mv -v inp-bkp inp; echo ; cd ../; done

[[ -f ./tests_diff.log ]] && rm -v ./tests_diff.log

#for d in SDS CTA CONFIG-dlp FIELD-dlm PDB NAMD; do cd $d; ./tests.bsh > tests.txt; [[ -f tests.ref ]] && [[ -f tests.txt ]] && [[ $(diff -q tests.ref tests.txt) ]] && echo "DIFF in $d - check for failures!" || echo "DONE in $d - PASSED..."; cd -; done
for d in $TDIRS; do
  cd $d
  #pwd
  echo "In $d : running tests..."
  ${OTIME}./tests.bsh ${BENCH} > tests.txt 2>&1
  if [ -f tests.ref ] && [ -f tests.txt ]; then
    DIFF="$(diff -q tests.ref tests.txt)"
    if [ "$DIFF" != "" ]; then
      echo "DIFF in $d - checking for failures..."
      echo "DIFF in $d - checking for failures..." >> ../tests_diff.log
      echo "$(diff tests.ref tests.txt)" >> ../tests_diff.log
      echo "..." >> ../tests_diff.log
      if [ -d ./fails ]; then
        echo "Found 'fails' in $d - FAILED..."
      else
        echo "DONE in $d - PASSED... but log files differ!"
      fi
    else
      echo "DONE in $d - PASSED..."
    fi
  else
    echo "LOST in $d - FAILED... log files tests.* not found!"
  fi
  echo
  cd ../
done

exit 0
