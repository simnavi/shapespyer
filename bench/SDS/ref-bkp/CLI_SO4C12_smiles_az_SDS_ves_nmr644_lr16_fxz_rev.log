
##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################


shape.py::main(): Parsing input parameters (from CLI or YAML) ...

InputParser.parseCLI(): Got inp_path = 'inp' and out_path = '.'

InputParser.parseCLI(): Verbose mode is OFF

shape.py::main(): Parsing input parameters - DONE

shape.py::main(): Input cavity radius < 0.5 nm - reset!

shape.py::main(): Check globals & defaults:
Pi = 3.141592653589793, 2*pi = 6.283185307179586, TINY = 1e-12, HUGE = 1e+100
BUFF = 1.0, DMIN = 0.5, RMIN = 0.5,

shape.py::main(): Input file extension: '.gro'

shape.py::main(): Requested molecule names and ids: ['SDS'] -> SDS & [1] -> 1 (1)

shape.py::main(): Requested molecule 'bone' indices: [] ... [] & (1)

shape.py::main(): Species' fractions from input = []
shape.py::main(): Number of layers to generate msurf = 1 0 -> [] ...

shape.py::main(): names suffix = 'SDS' =?= 'SDS' (rnames) ...
shape.py::main(): Created [] - empty input list of species
shape.py::main(): Created [] - empty (output) list of species

shape.py::main(): Added MoleculeSet => { index: 0, name: 'SDS', type: 'output', mass: 0.0, charge: 0.0, nitems: 0;
 rvec: None;
 rcog: None } to the output list of species

shape.py::main(): Initial user-provided output name = None
shape.py::main(): User did not provide the output file name - will construct it as 'base name' + 'extension'
shape.py::main(): User-provided output file name base 'CLI_SO4C12_smiles_az' (given separately)
shape.py::main(): User-provided output file extension '.gro' (given separately or by default)

shape.py::main(): base name = 'CLI_SO4C12_smiles_az_SDS_ves_lr16_fxz_rev' => 'CLI_SO4C12_smiles_az_SDS_ves_lr16_fxz_rev.gro' (fout) ...

shape.py::main(): input-based output file name = 'CLI_SO4C12_smiles_az_SDS_ves_lr16_fxz_rev.gro'
shape.py::main(): Using Nmol = 16
shape.py::main(): Using Nlay = 2
shape.py::main(): Using Dmin = 0.5 for min separation between heavy atoms
shape.py::main(): Using Rmin = 1.2732395447351628 for target internal radius (if applicable)
shape.py::main(): Using Rbuf = 1.0 for solvation buffer

InputParser.dumpYaml(): 'yaml' has been successfully imported - proceeding ...
shape.py::main(): Doing: input 'SO4C12_smiles_az.gro' => output 'CLI_SO4C12_smiles_az_SDS_ves_lr16_fxz_rev.gro'

groFile.readInMols(): Ready for reading GRO file 'inp/SO4C12_smiles_az.gro' ...
groFile.readInMols(): Reading GRO file 'inp/SO4C12_smiles_az.gro' from line # 0 (file is_open = True)...
groFile.readInMols(): GRO title: 'GRO coords for molecule set SO4C12 from SMILES'

groFile.readInMols(): Adding new molecular species - 0, mspec = 0, nmols = 0
groFile.readInMols(): Atom => { index: 1, name: 'S1', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.589, 0.618, 2.263] }
groFile.readInMols(): Atom => { index: 2, name: 'O2', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.681, 0.618, 2.388] }
groFile.readInMols(): Atom => { index: 3, name: 'O3', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.506, 0.737, 2.265] }
groFile.readInMols(): Atom => { index: 4, name: 'O4', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.506, 0.5, 2.265] }
groFile.readInMols(): Atom => { index: 5, name: 'O5', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.676, 0.618, 2.135] }
groFile.readInMols(): Atom => { index: 6, name: 'C6', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.592, 0.618, 2.02] }
groFile.readInMols(): Atom => { index: 7, name: 'H61', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.529, 0.707, 2.021] }
groFile.readInMols(): Atom => { index: 8, name: 'H62', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.529, 0.529, 2.021] }
groFile.readInMols(): Atom => { index: 9, name: 'C7', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.678, 0.618, 1.892] }
groFile.readInMols(): Atom => { index: 10, name: 'H71', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.741, 0.529, 1.891] }
groFile.readInMols(): Atom => { index: 11, name: 'H72', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.741, 0.707, 1.891] }
groFile.readInMols(): Atom => { index: 12, name: 'C8', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.587, 0.618, 1.768] }
groFile.readInMols(): Atom => { index: 13, name: 'H81', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.524, 0.707, 1.77] }
groFile.readInMols(): Atom => { index: 14, name: 'H82', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.524, 0.529, 1.77] }
groFile.readInMols(): Atom => { index: 15, name: 'C9', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.673, 0.618, 1.641] }
groFile.readInMols(): Atom => { index: 16, name: 'H91', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.736, 0.529, 1.64] }
groFile.readInMols(): Atom => { index: 17, name: 'H92', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.736, 0.707, 1.64] }
groFile.readInMols(): Atom => { index: 18, name: 'C10', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.582, 0.618, 1.517] }
groFile.readInMols(): Atom => { index: 19, name: 'H101', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.519, 0.707, 1.518] }
groFile.readInMols(): Atom => { index: 20, name: 'H102', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.519, 0.529, 1.518] }
groFile.readInMols(): Atom => { index: 21, name: 'C11', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.668, 0.618, 1.389] }
groFile.readInMols(): Atom => { index: 22, name: 'H111', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.731, 0.529, 1.388] }
groFile.readInMols(): Atom => { index: 23, name: 'H112', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.731, 0.707, 1.388] }
groFile.readInMols(): Atom => { index: 24, name: 'C12', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.577, 0.618, 1.265] }
groFile.readInMols(): Atom => { index: 25, name: 'H121', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.514, 0.707, 1.267] }
groFile.readInMols(): Atom => { index: 26, name: 'H122', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.514, 0.529, 1.267] }
groFile.readInMols(): Atom => { index: 27, name: 'C13', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.664, 0.618, 1.138] }
groFile.readInMols(): Atom => { index: 28, name: 'H131', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.726, 0.529, 1.137] }
groFile.readInMols(): Atom => { index: 29, name: 'H132', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.726, 0.707, 1.137] }
groFile.readInMols(): Atom => { index: 30, name: 'C14', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.572, 0.618, 1.014] }
groFile.readInMols(): Atom => { index: 31, name: 'H141', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.509, 0.707, 1.015] }
groFile.readInMols(): Atom => { index: 32, name: 'H142', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.509, 0.529, 1.015] }
groFile.readInMols(): Atom => { index: 33, name: 'C15', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.659, 0.618, 0.887] }
groFile.readInMols(): Atom => { index: 34, name: 'H151', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.722, 0.529, 0.885] }
groFile.readInMols(): Atom => { index: 35, name: 'H152', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.722, 0.707, 0.885] }
groFile.readInMols(): Atom => { index: 36, name: 'C16', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.567, 0.618, 0.763] }
groFile.readInMols(): Atom => { index: 37, name: 'H161', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.505, 0.707, 0.764] }
groFile.readInMols(): Atom => { index: 38, name: 'H162', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.505, 0.529, 0.764] }
groFile.readInMols(): Atom => { index: 39, name: 'C17', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.654, 0.618, 0.635] }
groFile.readInMols(): Atom => { index: 40, name: 'H171', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.717, 0.529, 0.634] }
groFile.readInMols(): Atom => { index: 41, name: 'H172', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.717, 0.707, 0.634] }
groFile.readInMols(): Atom => { index: 42, name: 'H173', type: 'SDS', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.589, 0.618, 0.547] }
groFile.readInMols(): In total 1 'SDS' of 42 atom(s) found ... 

groFile.readInMols(): Read-in Mmols = 1, Matms = [42], MolNames = ['SDS']
groFile.readInMols(): File 'inp/SO4C12_smiles_az.gro' successfully read: lines = 44 & natms = 42

shape.py::main(): Read-in GRO box: (   1.236   1.237   2.841); (   0.618   0.618   1.420)
shape.py::main(): Read-in INI box: (   1.236   1.237   2.841); (   0.618   0.618   1.420)

shape.py::main(): Read-in [MoleculeSet => { index: 0, name: 'SDS', type: 'input', mass: 0.0, charge: 0.0, nitems: 1;
 rvec: None;
 rcog: None }]
shape.py::main(): Ordered [MoleculeSet => { index: 0, name: 'SDS', type: 'input', mass: 0.0, charge: 0.0, nitems: 1;
 rvec: None;
 rcog: None }]
shape.py::main(): Read-in M_mols = 1, Mol_Names = ['SDS'], N_mols = [1], N_atms = [42], M_atms = 42

shape.py::main(): WARNING! number of mint indices 0 < 1 (number of species read in) - resetting ...
shape.py::main(): WARNING! number of mext indices 0 < 1 (number of species read in) - resetting ...
shape.py::main(): Setting molecule 'bone' indices: [38] ... [0] ->  [0] ... [38] & (1 / 0)

Ball.makeNew(info): Number of molecules on 'equator':  lring = 16 <-> Rmin = 1.2732395447351628
Ball.makeNew(info): Number of rings (turns) in a ball: turns = 9
Ball.makeNew(info): Number of (mono-)layers in a ball: layers = 0 / 2

Ball.makeNew(info): Estimated molecule number = 80
Ball.makeNew(info): Placing 0-th ring of 1 mols at zorg = -1.2732395447351628, theta = -90.0, rmin = 7.796343665038751e-17
Ball.makeNew(info): Placing 1-th ring of 6 mols at zorg = -1.1763199553648058, theta = -67.5, rmin = 0.48724767920221645
Ball.makeNew(info): Placing 2-th ring of 11 mols at zorg = -0.9003163161571061, theta = -45.0, rmin = 0.9003163161571062
Ball.makeNew(info): Placing 3-th ring of 14 mols at zorg = -0.48724767920221634, theta = -22.5, rmin = 1.1763199553648058
Ball.makeNew(info): Placing 4-th ring of 16 mols at zorg = 0.0, theta = 0.0, rmin = 1.2732395447351628
Ball.makeNew(info): Placing 5-th ring of 14 mols at zorg = 0.48724767920221634, theta = 22.5, rmin = 1.1763199553648058
Ball.makeNew(info): Placing 6-th ring of 11 mols at zorg = 0.9003163161571061, theta = 45.0, rmin = 0.9003163161571062
Ball.makeNew(info): Placing 7-th ring of 6 mols at zorg = 1.1763199553648058, theta = 67.5, rmin = 0.48724767920221645
Ball.makeNew(info): Placing 8-th ring of 1 mols at zorg = 1.2732395447351628, theta = 90.0, rmin = 7.796343665038751e-17

Ball.makeNew(info): Outer layer (1) rmin -> 1.2732395447351628 + 1.6292970877037745 + 0.5 * 1.0 = 3.4025366324389372
Ball.makeNew(info): Number of molecules on 'equator':  lring = 42 <-> Rmin = 3.4025366324389372
Ball.makeNew(info): Number of rings (turns) in a ball: turns = 22
Ball.makeNew(info): Number of (mono-)layers in a ball: layers = 1 / 2

Ball.makeNew(info): Estimated molecule number = 564
Ball.makeNew(info): Placing 0-th ring of 1 mols at zorg = -3.4025366324389372, theta = -90.0, rmin = 2.0834527979489792e-16
Ball.makeNew(info): Placing 1-th ring of 6 mols at zorg = -3.3645331095158606, theta = -81.42857142857143, rmin = 0.5071217704461483
Ball.makeNew(info): Placing 2-th ring of 12 mols at zorg = -3.251371476649802, theta = -72.85714285714286, rmin = 1.0029152785340292
Ball.makeNew(info): Placing 3-th ring of 18 mols at zorg = -3.0655795777250194, theta = -64.28571428571429, rmin = 1.47630531656707
Ball.makeNew(info): Placing 4-th ring of 24 mols at zorg = -2.81130769675162, theta = -55.71428571428572, rmin = 1.9167171333491029
Ball.makeNew(info): Placing 5-th ring of 29 mols at zorg = -2.494235847378917, theta = -47.14285714285715, rmin = 2.314312656651836
Ball.makeNew(info): Placing 6-th ring of 33 mols at zorg = -2.121446890776436, theta = -38.571428571428584, rmin = 2.6602102594915116
Ball.makeNew(info): Placing 7-th ring of 37 mols at zorg = -1.7012683162194693, theta = -30.000000000000014, rmin = 2.946683160999274
Ball.makeNew(info): Placing 8-th ring of 39 mols at zorg = -1.2430862187394265, theta = -21.428571428571445, rmin = 3.16733202993766
Ball.makeNew(info): Placing 9-th ring of 41 mols at zorg = -0.7571356292708862, theta = -12.857142857142874, rmin = 3.3172279351858656
Ball.makeNew(info): Placing 10-th ring of 42 mols at zorg = -0.2542718809733996, theta = -4.2857142857143025, rmin = 3.3930224499161734
Ball.makeNew(info): Placing 11-th ring of 42 mols at zorg = 0.25427188097339753, theta = 4.285714285714269, rmin = 3.3930224499161734
Ball.makeNew(info): Placing 12-th ring of 41 mols at zorg = 0.7571356292708841, theta = 12.85714285714284, rmin = 3.3172279351858664
Ball.makeNew(info): Placing 13-th ring of 39 mols at zorg = 1.2430862187394243, theta = 21.42857142857141, rmin = 3.167332029937661
Ball.makeNew(info): Placing 14-th ring of 37 mols at zorg = 1.7012683162194675, theta = 29.99999999999998, rmin = 2.9466831609992754
Ball.makeNew(info): Placing 15-th ring of 33 mols at zorg = 2.121446890776434, theta = 38.57142857142855, rmin = 2.6602102594915134
Ball.makeNew(info): Placing 16-th ring of 29 mols at zorg = 2.4942358473789157, theta = 47.14285714285712, rmin = 2.3143126566518375
Ball.makeNew(info): Placing 17-th ring of 24 mols at zorg = 2.8113076967516193, theta = 55.71428571428569, rmin = 1.9167171333491047
Ball.makeNew(info): Placing 18-th ring of 18 mols at zorg = 3.065579577725018, theta = 64.28571428571426, rmin = 1.476305316567072
Ball.makeNew(info): Placing 19-th ring of 12 mols at zorg = 3.2513714766498016, theta = 72.85714285714283, rmin = 1.0029152785340307
Ball.makeNew(info): Placing 20-th ring of 6 mols at zorg = 3.3645331095158606, theta = 81.4285714285714, rmin = 0.5071217704461497
Ball.makeNew(info): Placing 21-th ring of 1 mols at zorg = 3.4025366324389372, theta = 89.99999999999997, rmin = 1.7193750843805985e-15

shape.py::main(): Replacing '_ves' -> '_ves_nmr644' in the output file name ...

shape.py::main(): Total number of molecules = 644
shape.py::main(): Molecule distribution = [644]

shape.py::main(): MolSys resetting mass (a.u.) of all 27048 atoms -> 170909.87200000233 (isMassElems = True)

shape.py::main(): MolSys initial Rcom = Vec3[-3.6e-07, 1.23e-06, -0.00120563]
shape.py::main(): MolSys initial Rcog = Vec3[-1e-06, 3.45e-06, -0.00296969]

MolecularSystem.getDims(): min_xyz, min_ids = [-5.144812421153638, -5.134239188374526, -5.153063912899253] @ (14323, 14743, 3361)
MolecularSystem.getDims(): min_xyz, min_ids = [5.144812421153638, 5.134239188374526, 5.153063912899254] @ (13441, 13861, 27007)

shape.py::main(): MolSys rcob = [0.0, 0.0, 8.881784197001252e-16]
shape.py::main(): MolSys bbox = [10.28962484 10.26847838 10.30612783]
shape.py::main(): MolSys sbox = [11.28962484 11.26847838 11.30612783] -> [11.28962484 11.26847838 11.30612783]
shape.py::main(): MolSys hbox = [5.64481242 5.63423919 5.65306391]
shape.py::main(): MolSys orig = [5.64481242 5.63423919 5.65306391] + [0.0, 0.0, 0.0]

shape.py::main(): MolSys placing structure COG at [5.64481242 5.63423919 5.65306391] ...

shape.py::main(): MolSys final Rcob = Vec3[5.64481343, 5.63423573, 5.6560336]
shape.py::main(): MolSys final Rcom = Vec3[5.64481307, 5.63423697, 5.65482797]
shape.py::main(): MolSys final Rcog = Vec3[5.64481242, 5.63423919, 5.65306391]

groFile.writeOutMols(): Ready for writing GRO file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Writing GRO file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' from line # 0 ...
groFile.writeOutMols(): Writing molecule '1SDS' into GRO file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '2SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '3SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '4SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '5SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '6SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '7SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '8SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '9SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '10SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '20SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '30SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '40SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '50SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '60SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '70SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '80SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '90SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '100SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '200SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '300SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '400SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '500SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '600SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): Appending molecule '644SDS' to file './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' ...
groFile.writeOutMols(): File './CLI_SO4C12_smiles_az_SDS_ves_nmr644_lr16_fxz_rev.gro' successfully written: lines = 27051 : n_mols = 644 & n_atms = 27048 / 27048

