
##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################


shape.py::main(): Parsing input parameters (from CLI or YAML) ...

InputParser.parseCLI(): Got inp_path = 'inp' and out_path = '.'

InputParser.parseCLI(): Verbose mode is OFF

shape.py::main(): Parsing input parameters - DONE

shape.py::main(): Input cavity radius < 0.5 nm - reset!

shape.py::main(): Check globals & defaults:
Pi = 3.141592653589793, 2*pi = 6.283185307179586, TINY = 1e-12, HUGE = 1e+100
BUFF = 1.0, DMIN = 0.5, RMIN = 0.5,

shape.py::main(): Input file extension: '.gro'

shape.py::main(): Requested molecule names and ids: ['DTA'] -> DTA & [1] -> 1 (1)

shape.py::main(): Requested molecule 'bone' indices: [] ... [] & (1)

shape.py::main(): Species' fractions from input = []
shape.py::main(): Number of layers to generate msurf = 1 0 -> [] ...

shape.py::main(): names suffix = 'DTA' =?= 'DTA' (rnames)  based on unique names '['DTA']' ...
shape.py::main(): Created [] - empty input list of species
shape.py::main(): Created [] - empty (output) list of species

shape.py::main(): Added MoleculeSet => { index: 0, name: 'DTA', type: 'output', mass: 0.0, charge: 0.0, nitems: 0;
 rvec: None;
 rcog: None } to the output list of species

shape.py::main(): Initial user-provided output name = None
shape.py::main(): User did not provide the output file name - will construct it as 'base name' + 'extension'
shape.py::main(): User-provided output file name base 'CLI_NC15H34_smiles_az' (given separately)
shape.py::main(): User-provided output file extension '.gro' (given separately or by default)

shape.py::main(): base name = 'CLI_NC15H34_smiles_az_DTA_ring_lr15' => 'CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' (fout) ...

shape.py::main(): input-based output file name = 'CLI_NC15H34_smiles_az_DTA_ring_lr15.gro'
shape.py::main(): Using Nmol = 15
shape.py::main(): Using Nlay = 1
shape.py::main(): Using Dmin = 0.5 for min separation between heavy atoms
shape.py::main(): Using Rmin = 1.1936620731892151 for target internal radius (if applicable)
shape.py::main(): Using Rbuf = 1.0 for solvation buffer

InputParser.dumpYaml(): 'yaml' has been successfully imported - proceeding ...
shape.py::main(): Doing: input 'NC15H34_smiles_az.gro' => output 'CLI_NC15H34_smiles_az_DTA_ring_lr15.gro'

groFile.readInMols(): Ready for reading GRO file 'inp/NC15H34_smiles_az.gro' ...
groFile.readInMols(): Reading GRO file 'inp/NC15H34_smiles_az.gro' from line # 0 (file is_open = True)...
groFile.readInMols(): GRO title: 'GRO coords for molecule set NC15H34 from SMILES'

groFile.readInMols(): Adding new molecular species - 0, mspec = 0, nmols = 0
groFile.readInMols(): Atom => { index: 1, name: 'N1', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.674, 0.659, 1.94] }
groFile.readInMols(): Atom => { index: 2, name: 'C2', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.821, 0.67, 1.934] }
groFile.readInMols(): Atom => { index: 3, name: 'H21', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.853, 0.667, 1.83] }
groFile.readInMols(): Atom => { index: 4, name: 'H22', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.865, 0.586, 1.989] }
groFile.readInMols(): Atom => { index: 5, name: 'H23', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.852, 0.764, 1.98] }
groFile.readInMols(): Atom => { index: 6, name: 'C3', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.619, 0.779, 2.005] }
groFile.readInMols(): Atom => { index: 7, name: 'H31', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.7, 0.846, 2.033] }
groFile.readInMols(): Atom => { index: 8, name: 'H32', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.563, 0.75, 2.094] }
groFile.readInMols(): Atom => { index: 9, name: 'H33', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.552, 0.83, 1.936] }
groFile.readInMols(): Atom => { index: 10, name: 'C4', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.636, 0.54, 2.017] }
groFile.readInMols(): Atom => { index: 11, name: 'H41', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.577, 0.57, 2.103] }
groFile.readInMols(): Atom => { index: 12, name: 'H42', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.726, 0.489, 2.051] }
groFile.readInMols(): Atom => { index: 13, name: 'H43', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.578, 0.473, 1.954] }
groFile.readInMols(): Atom => { index: 14, name: 'C5', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.621, 0.649, 1.803] }
groFile.readInMols(): Atom => { index: 15, name: 'H51', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.512, 0.641, 1.807] }
groFile.readInMols(): Atom => { index: 16, name: 'H52', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.661, 0.56, 1.755] }
groFile.readInMols(): Atom => { index: 17, name: 'C6', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.66, 0.774, 1.723] }
groFile.readInMols(): Atom => { index: 18, name: 'H61', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.718, 0.841, 1.786] }
groFile.readInMols(): Atom => { index: 19, name: 'H62', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.57, 0.825, 1.689] }
groFile.readInMols(): Atom => { index: 20, name: 'C7', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.744, 0.732, 1.601] }
groFile.readInMols(): Atom => { index: 21, name: 'H71', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.797, 0.639, 1.623] }
groFile.readInMols(): Atom => { index: 22, name: 'H72', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.817, 0.81, 1.578] }
groFile.readInMols(): Atom => { index: 23, name: 'C8', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.652, 0.71, 1.48] }
groFile.readInMols(): Atom => { index: 24, name: 'H81', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.578, 0.791, 1.476] }
groFile.readInMols(): Atom => { index: 25, name: 'H82', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.601, 0.615, 1.489] }
groFile.readInMols(): Atom => { index: 26, name: 'C9', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.736, 0.712, 1.35] }
groFile.readInMols(): Atom => { index: 27, name: 'H91', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.806, 0.628, 1.352] }
groFile.readInMols(): Atom => { index: 28, name: 'H92', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.792, 0.805, 1.344] }
groFile.readInMols(): Atom => { index: 29, name: 'C10', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.643, 0.699, 1.228] }
groFile.readInMols(): Atom => { index: 30, name: 'H101', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.574, 0.783, 1.227] }
groFile.readInMols(): Atom => { index: 31, name: 'H102', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.586, 0.606, 1.235] }
groFile.readInMols(): Atom => { index: 32, name: 'C11', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.727, 0.699, 1.099] }
groFile.readInMols(): Atom => { index: 33, name: 'H111', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.796, 0.614, 1.101] }
groFile.readInMols(): Atom => { index: 34, name: 'H112', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.783, 0.792, 1.092] }
groFile.readInMols(): Atom => { index: 35, name: 'C12', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.633, 0.686, 0.978] }
groFile.readInMols(): Atom => { index: 36, name: 'H121', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.564, 0.77, 0.976] }
groFile.readInMols(): Atom => { index: 37, name: 'H122', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.577, 0.593, 0.984] }
groFile.readInMols(): Atom => { index: 38, name: 'C13', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.717, 0.685, 0.848] }
groFile.readInMols(): Atom => { index: 39, name: 'H131', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.786, 0.601, 0.85] }
groFile.readInMols(): Atom => { index: 40, name: 'H132', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.773, 0.779, 0.841] }
groFile.readInMols(): Atom => { index: 41, name: 'C14', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.624, 0.673, 0.727] }
groFile.readInMols(): Atom => { index: 42, name: 'H141', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.555, 0.757, 0.725] }
groFile.readInMols(): Atom => { index: 43, name: 'H142', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.568, 0.58, 0.734] }
groFile.readInMols(): Atom => { index: 44, name: 'C15', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.708, 0.672, 0.597] }
groFile.readInMols(): Atom => { index: 45, name: 'H151', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.777, 0.588, 0.599] }
groFile.readInMols(): Atom => { index: 46, name: 'H152', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.764, 0.765, 0.59] }
groFile.readInMols(): Atom => { index: 47, name: 'C16', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.615, 0.66, 0.476] }
groFile.readInMols(): Atom => { index: 48, name: 'H161', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.545, 0.744, 0.474] }
groFile.readInMols(): Atom => { index: 49, name: 'H162', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.558, 0.566, 0.483] }
groFile.readInMols(): Atom => { index: 50, name: 'H163', type: 'DTA', mass: 1.0, charge: 0.0;
 rvec: Vec3[0.674, 0.659, 0.384] }
groFile.readInMols(): In total 1 'DTA' of 50 atom(s) found ... 

groFile.readInMols(): Read-in Mmols = 1, Matms = [50], MolNames = ['DTA']
groFile.readInMols(): File 'inp/NC15H34_smiles_az.gro' successfully read: lines = 52 & natms = 50

shape.py::main(): Read-in GRO box: (   1.353   1.373   2.719); (   0.677   0.686   1.359)
shape.py::main(): Read-in INI box: (   1.353   1.373   2.719); (   0.677   0.686   1.359)

shape.py::main(): Read-in [MoleculeSet => { index: 0, name: 'DTA', type: 'input', mass: 0.0, charge: 0.0, nitems: 1;
 rvec: None;
 rcog: None }]
shape.py::main(): Ordered [MoleculeSet => { index: 0, name: 'DTA', type: 'input', mass: 0.0, charge: 0.0, nitems: 1;
 rvec: None;
 rcog: None }]
shape.py::main(): Read-in M_mols = 1, Mol_Names = ['DTA'], N_mols = [1], N_atms = [50], M_atms = 50

shape.py::main(): WARNING! number of mint indices 0 < 1 (number of species read in) - resetting ...
shape.py::main(): WARNING! number of mext indices 0 < 1 (number of species read in) - resetting ...
shape.py::main(): Setting molecule 'bone' indices: [46] ... [0] ->  [46] ... [0] & (1 / 0)


shape.py::main(): Total number of molecules = 15

shape.py::main(): Molecule distribution = [15]


shape.py::main(): MolSys resetting mass (a.u.) of all 750 atoms -> 3426.5579999999995 (isMassElems = True)

shape.py::main(): MolSys initial Rcom = Vec3[-0.0, 0.0, -0.02582744]
shape.py::main(): MolSys initial Rcog = Vec3[-0.0, -0.0, -0.02591966]

MolecularSystem.getDims(): min_xyz, min_ids = [-2.774791703287195, -2.802325155872256, -0.1889197483037118] @ (410, 557, 3)
MolecularSystem.getDims(): min_xyz, min_ids = [2.817873315960246, 2.8117354908392773, 0.1565010838694511] @ (10, 210, 14)

shape.py::main(): MolSys rcob = [0.021540806336525442, 0.004705167483510575, -0.01620933221713035]
shape.py::main(): MolSys bbox = [5.59266502 5.61406065 0.34542083]
shape.py::main(): MolSys sbox = [6.59266502 6.61406065 1.34542083] -> [6.59266502 6.61406065 1.34542083]
shape.py::main(): MolSys hbox = [3.29633251 3.30703032 0.67271042]
shape.py::main(): MolSys orig = [3.29633251 3.30703032 0.67271042] + [0.0, 0.0, 0.0]

Applying PBC to molecules in set 'DTA': box = [6.59266502 6.61406065 1.34542083] & isMolPBC = True
Molecule.updateRvecs(): Molecule 0 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 1 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 2 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 3 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 4 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 5 has been restored w.r.t. PBC ...

shape.py::main(): MolSys *PBC* Rcom = Vec3[-0.0, 0.0, -0.02582744]
shape.py::main(): MolSys *PBC* Rcog = Vec3[-0.0, -0.0, -0.02591966]

shape.py::main(): MolSys placing structure COG at [3.29633251 3.30703032 0.67271042] ...

Applying PBC to molecules in set 'DTA': box = [6.59266502 6.61406065 1.34542083] & isMolPBC = True
Molecule.updateRvecs(): Molecule 0 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 1 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 2 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 3 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 4 has been restored w.r.t. PBC ...
Molecule.updateRvecs(): Molecule 5 has been restored w.r.t. PBC ...

shape.py::main(): MolSys *PBC* Rcom = Vec3[0.0, 0.0, 9.222e-05]
shape.py::main(): MolSys *PBC* Rcog = Vec3[-0.0, -0.0, 0.0]

shape.py::main(): MolSys final Rcob = Vec3[3.31787332, 3.31173549, 0.68242074]
shape.py::main(): MolSys final Rcom = Vec3[3.29633251, 3.30703032, 0.67280263]
shape.py::main(): MolSys final Rcog = Vec3[3.29633251, 3.30703032, 0.67271042]

groFile.writeOutMols(): Ready for writing GRO file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Writing GRO file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' from line # 0 ...
groFile.writeOutMols(): Writing molecule '1DTA' into GRO file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '2DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '3DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '4DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '5DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '6DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '7DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '8DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '9DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '10DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): Appending molecule '15DTA' to file './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' ...
groFile.writeOutMols(): File './CLI_NC15H34_smiles_az_DTA_ring_lr15.gro' successfully written: lines = 753 : n_mols = 15 & n_atms = 750 / 750

