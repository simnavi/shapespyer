#
# SMILES for SDS molecule
#
# Hydrogens are added automatically:
C12SO4  SDS  CCCCCCCCCCCCOS(=O)(=O)[O-]
SO4C12  SDS  S([O-])(=O)(=O)OCCCCCCCCCCCC
#
# Including hydrogens in the chemical formula:
C12H25SO4  SDS  CCCCCCCCCCCCOS(=O)(=O)[O-]
SO4C12H25  SDS  S([O-])(=O)(=O)OCCCCCCCCCCCC
#
# END OF SMILES
#
Box
1.0 1.0 1.0

