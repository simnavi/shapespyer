======================
Mixed SDS-CTAB example
======================

This directory contains exemplar inputs and outputs for the Shapespyer workflow
in the case of mixed SDS-CTAB10 system:

1. Generating a 'rod' with random molecule distribution

2. Replicating the rod and placing the replicas on a lattice


Directory structure
===================

'inp' contains the required input files:

*.sml - SMILES strings (e.g. smiles_CTA10.sml)
*.gro - molecule 3D coordinates (e.g. CTA10-single.gro)

'out' contains sample output files generated based on inputs in 'inp' directory


1. Generating a 'rod' with random molecule distribution
-------------------------------------------------------

In this example the user could merely copy the previously created 3D configurations for 
the two template molecules: SDS and C(10)TAB. We suggest to combine the ones generated from 
SMILES strings, e.g. 'config_smiles_SO4C12.gro' and 'config_smiles_NC13H30.gro' from 
the previous examples. The combined .gro file is found in subdirectory 'inp':

$ ls inp/config_smiles*gro
config_smiles_SDS-C10TAB.gro

NOTE: We deliberately chose the configurations that are ordered in accordance with the convention
of indices starting from the central atom in the molecules' hydrophilic head group.

Using this file as input we can generate a mixed molecular assembly with a specified composition:

$ ../../shape.py -g --dio=inp,out -i config_smiles_SDS-C10TAB.gro -o configS -x .gro --shape='rod' -r 'SDS,CTA' --frc=[50,50] -n 15 -t 8  > out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log

where the option '--frc=[50,50]' sets the average fractions of the two compounds in the resulting 
structure, i.e. 50/50 in this case.

Check the actual numbers of SDS and CTA10 molecules in 'out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log':

$ grep "Molecule distribution" out/shape_SDS-CTA10_rodS_n15_t8-dmin05.log
shape.py::main(): Molecule distribution = [88, 90]

That is, the created 'rod' consists of 88 SDS and 90 CTA10 molecules. 

Let's try to reduce its inner cavity by alowing for smaller distance between the terminal C-atoms:

$ ../../shape.py -g --dio=inp,out -i config_smiles_SDS-C10TAB.gro -o configS-dmin04 -x .gro --shape='rod' -r 'SDS,CTA' --frc=[50,50] -n 15 -t 8 --dmin=0.4  > out/shape_SDS-CTA10_rodS_n15_t8-dmin04.log

$ grep "Molecule distribution" out/shape_SDS-CTA10_rodS_n15_t8-dmin04.log
shape.py::main(): Molecule distribution = [83, 85]


2. Replicating the rod and placing the replicas on a lattice
------------------------------------------------------------

Now we can place replicas of the latter rod assembly on a simple cubic lattice:

$ ../../shape.py -g --dio=out,out -i configS-dmin04_rod_SDS-CTA_2-15-frc_ndte-8.gro -o configS_lattice -x .gro --shape='lat' -r 'SDS,CTA' --nl 2 > out/shape_SDS-CTA10_latS_n15_t8-dmin04-nl2.log


