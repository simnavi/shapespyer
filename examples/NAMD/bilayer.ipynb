{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9937e9f9-4fd7-4aa6-9e3e-8713eca5e83c",
   "metadata": {},
   "source": [
    "## Example: POPC bilayer"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0da7432a-9b5b-4329-9557-d78f6ce527f9",
   "metadata": {},
   "source": [
    "POPC is a zwitterionic phospholipid used commonly as the main component of a mammalian bilayer membrane. \n",
    "\n",
    "![POPC molecule](POPC.png)\n",
    "\n",
    "In this tutorial, we will generate a POPC bilayer with Shapespyer. We will then:\n",
    "\n",
    "1) Create the first topology file (in .psf format, as read by NAMD) based on the CHARMM36 force field\n",
    "2) Solvate the structure and add ions\n",
    "3) Create the topology of the final structure\n",
    "4) Generate input files for minimisation, equilibration and production runs\n",
    "5) Run the simulations!\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69bf42ea-0556-4e57-9538-730b74f939e3",
   "metadata": {},
   "source": [
    "### Prerequisites and necessary imports:\n",
    "---\n",
    "Before attempting to follow this tutorial, user has to make sure that the necessary packages are installed in their environment: `numpy`, `matplotlib`, `nglview`, `ipywidgets`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08f860ec-fcc1-4266-bd4e-e1b5b3a68cea",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "# visualisation\n",
    "import nglview as nv\n",
    "from nglview import NGLWidget\n",
    "from nglview.datafiles import PDB, XTC\n",
    "# math and plotting tools\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "# layout control\n",
    "from ipywidgets import Layout, Box\n",
    "\n",
    "box_layout = Layout(display='flex',\n",
    "                    flex_flow='column',\n",
    "                    width='100%') #save the box_layout for all visualisations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9e8ca15-5911-40e3-b7c2-494ba7d17686",
   "metadata": {},
   "outputs": [],
   "source": [
    "# need to add 'shapespyer' and 'shapespyer/scripts' to the user's PATH variable (do it only once!). Replace it with yours\n",
    "!export PATH=$PATH:$HOME/Desktop/shapespyer_with_branch/shapespyer/scripts\n",
    "!export PATH=$PATH:$HOME/Desktop/shapespyer_with_branch/shapespyer/\n",
    "# list the contents of dir\n",
    "!ls "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fcaf14dc-4da8-43d9-a8be-13432097c0aa",
   "metadata": {},
   "source": [
    "A sample coordinate file is provided (in PDB format). This can be used as input for shape.py script to generate a 3D configuration of the POPC bilayer.\n",
    "\n",
    "One can check the contents of POPC.pdb:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "694a3e4e-4e15-4cd2-a5bb-8406480d994b",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat POPC.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "451c02c4-7e6b-4131-9bce-8f2328c35fdf",
   "metadata": {},
   "source": [
    "NOTE: User is advised to check both initial (input) and final (output) molecular structures (.gro, .xyz, .pdb) visually with the aid of graphical visualisation tools such as NGL Viewer (https://github.com/nglviewer/nglview), or VMD (https://www.ks.uiuc.edu/Research/vmd/).\n",
    "\n",
    "Here we use NGL Viewer to visulaise inp/SDS-single1.gro. Alternatively, one could view the molecule in VMD."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5a002fc-d3d8-42cf-af41-c028ae4f4485",
   "metadata": {},
   "outputs": [],
   "source": [
    "view = nv.show_structure_file('POPC.pdb')\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d9ab7d8-afa5-4b66-b9c3-2e57406b7f30",
   "metadata": {},
   "source": [
    "## 0.   Generating a bilayer with shape.py\n",
    "\n",
    "Let's look at the relevant options in shape.py by running its help feature:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d32568d-8d6e-469e-ac7e-37cb5b071f9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "!shape.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96ae83ad-6b3f-49a4-9c06-69cca9467871",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "We will use the following options:\n",
    "\n",
    "- **dio**  :  The option specifies two directories (folders) where to (i) find input configuration files and (o) store output configuration files; in this case, it will look for input and output in the same directory\n",
    "- **-i** : provides the name of the input file with the template coordinates of a single POPC molecule;\n",
    "- **-o ** : base-name of the output file that will contain the coordinates of all the generated molecules placed in the requested (bilayer) configuration;\n",
    "- **--shape** stands for the shape specification, which takes bilayer as an argument in this case;\n",
    "- **-x** : extension (format) of the output file\n",
    "- **-r** stands for the residue name(s) to be found and matched in the input configuration file (POPC in this case);\n",
    "- **--nside** : number of POPC molecules to be repeated on each side of the membrane\n",
    "- **--origin cog** : the center of geometry of the bilayer will be placed at the origin (0, 0, 0).\n",
    "\n",
    "The last possible option (we will not use it here, as it assumes a prior knowledge of the system) is **zsep**, which is the distance (in nm) along the z axis between the head atoms of the two layers of the bilayer. If not specified, it is defined as double the distance between the \"head\" and the \"tail\" atom of the POPC molecule, plus the default **dmin** (0.5 nm) to avoid clashes.\n",
    "\n",
    "We use therefore the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "968b7861-dcb3-4f3a-b01c-ad2e71d5080f",
   "metadata": {},
   "outputs": [],
   "source": [
    "!shape.py --dio=.,. -i POPC.pdb -o config -x .pdb --shape=\"bilayer\" -r 'POPC' --nside 10 --origin cog"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af4bda6d-08d7-4cab-a802-9fb6e8231d7b",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "**NOTE:** By default, the command given above will throw some information on the screen (as above) -- the output is related to the progress made by the script (plus error messages if any), which might clutter the terminal view. However, it is easy to redirect all the output into a log file by appending the command with &> shape.log instruction, in which case both stdout and stderr outputs will be flushed into shape.log file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52157aad-c80f-4493-b257-b24b030674d3",
   "metadata": {},
   "source": [
    "Let us visualise the generated bilayer structure (`config_POPC_bilayer_ns10_zsep0.pdb`) in NGL Viewer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c2188529-cd2e-40cd-ab7c-b7933eddda93",
   "metadata": {},
   "outputs": [],
   "source": [
    "view = nv.show_structure_file('config_POPC_bilayer_ns10_zsep0.pdb', gui=True)\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ada46b25-ffbd-433d-bc15-8d339c706e6c",
   "metadata": {},
   "source": [
    "**NOTE:** With the parameter ``gui=True`` in the `view` API \n",
    "(add it as the second argument in ``nv.show_structure_file()`` call), \n",
    "the **NGL Viewer GUI** pops up, providing the user with handy controls \n",
    "to manipulate the visualisation properties!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76426559-8d41-4c21-a56e-d3e2b1e5ee0f",
   "metadata": {},
   "source": [
    "## 1.   Creating the first topology for the generated structure\n",
    "\n",
    "Let's look at the relevant options in the relevant script namd-write-psf.py by running its help feature:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3228fde4-ed15-4968-900c-d1b072b77af1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd-write-psf.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69084977-c8b3-4403-9601-f52f0f2d8114",
   "metadata": {},
   "source": [
    "The script will use a) the PDB file of the bilayer and b) the general atom type and bond description in the CHARMM generalised force field files contained in the `toppar` directory, to build a topology `psf` file which will include information about mass and partial charges of the single atoms, bonds, angles, dihedrals and impropers for the whole structure.  \n",
    "The script needs an input file (if not specified, it will be taken from the YAML file generated by shape.py) to be specified with the flag **-in**, and the name of an output file with \".psf\" extension. In this case, we are using the PDB generated by `shape.py`, so we can run the script through the following command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3539e8a4-0946-47bc-be76-591fd1cb3514",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd-write-psf.py --out bilayer.psf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b32ec658-556c-439d-a415-e23a8498316f",
   "metadata": {},
   "source": [
    "## 2.   Solvating the bilayer and adding ions\n",
    "\n",
    "To prepare the bilayer for the simulation, the system must be solvated first, i.e. water has to be added to the top and the bottom leaflet. Then, if the system is charged (the topology files contains all the partial charges, so it will be worked out from there), counterions will need to be added. Finally, an extra ionic strength is commonly added. The `solvate.py` scripts performs all of these functions.\n",
    "Let's look at the relevant options in `solvate.py` by running its help feature:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e56dd346-c4e9-4b95-ab2d-28efcd554353",
   "metadata": {},
   "outputs": [],
   "source": [
    "!solvate.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a56ec89-b204-4cb8-9f77-68a0b615eaee",
   "metadata": {},
   "source": [
    "The script will take the input file name (the generated PDB of the bilayer) from the YAML file. The name of the input `psf` will have to be indicated manually with the option **--inptop**. Optionally, we can indicate:\n",
    "- the name the pdb file of the solvated system with the option **--out**;\n",
    "- the thickness (in Angstroms) of the water layer along the z axis (**--zlayer**)\n",
    "- the cation type (**--cation**)\n",
    "- the anion type (**--anion**)\n",
    "- the final salt concentration we want (if 0, neutralisation if needed will be performed anyway) (**--saltconc**)\n",
    "\n",
    "Let's try for example to add water with a layer of 15 Angstroms and a salt concentration 0.15 mM:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbcb0943-951d-405b-b79d-de8bbdb650bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "!solvate.py --inptop bilayer.psf --zlayer 15 --saltconc 0.15"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf2fa102-4ec4-46e4-be5e-fc13c173d1e5",
   "metadata": {},
   "source": [
    "The file `ionized.pdb` has been now created. Let's look at what has been added to the initial bilayer file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86a4c99f-47b5-4523-9800-f4cadfee1d74",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat ionized.pdb | grep -v POPC"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2da2b708-f7b9-4dc4-b9d9-f8305874ee9e",
   "metadata": {},
   "source": [
    "Water molecules (TIP3), 6 Na+ atoms (SOD) and 6 Cl- atoms (CLA) have been added. We can now visualise the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d9f8dd7-ff16-449a-afad-755019796dc8",
   "metadata": {},
   "outputs": [],
   "source": [
    "view = nv.show_structure_file('ionized.pdb', gui=True)\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37cabe2c-92d3-4eab-803f-fdc10ee90578",
   "metadata": {},
   "source": [
    "## 3.   Creating the topology for the solvated/ionized system\n",
    "\n",
    "This is needed to run the simulation. We are going to run again namd-write-psf.py. This time we need to indicate our input file, as it's not generated through `shape.py`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce9fc849-0bf0-4823-aad2-1465b91c3f95",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd-write-psf.py --in ionized.pdb --out ionized.psf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "977d0390-2376-473c-983f-bced99956f7e",
   "metadata": {},
   "source": [
    "We are now ready to start the simulation. This will involve 3 steps:\n",
    "- energy minimisation\n",
    "- equilibration, with the lipid heads kept in place by a harmonic constraint, so to equilibrate the hydrophobic lipid tails\n",
    "- production run\n",
    "\n",
    "We will need to write 3 NAMD input files for these steps. Templates are provided, and we can use these as a starting point and generate our own with a choice of parameters. Let's look at them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cee40dd-baea-489f-8166-a363f9b2ac78",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae00aeaf-9d7f-499d-9eb4-ef00960644b1",
   "metadata": {},
   "source": [
    "`min_template.conf`, `equil_template.conf` and `prod_template.conf` are the 3 template files to be used. The first one, `min_template.conf`, will require also two more parameters/inputs: \n",
    "- the initial dimension of the system and its center, to provide a \"cell\" for the simulation\n",
    "- a file which is equivalent to `ionized.pdb` but contains the information about the atoms to be constrained.\n",
    "\n",
    "These two will be calculated automatically upon creation of the custom minimisation file.\n",
    "The help function of the script `namd-write-files.py` suggests us the custom parameters we can use to modify the templates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e1409ad-437b-41e8-88ac-81752ff182a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd-write-files.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54f90d04-9d73-434b-8cdb-2f12911c773e",
   "metadata": {},
   "source": [
    "- **out_min**, **out_equil** and **out_prod** are the names for the 3 NAMD input files we need. \n",
    "- **struct** is the base-name of our inputs (we should have a `.pdb` and a `.psf` file with this root in our working directory).\n",
    "- **temp** is the temperature we want to perform the simulation at\n",
    "- **steps_min**, **steps_equil** and **steps_prod** are the number of simulation steps (default timestep: 1fs) we want for the 3 stages.\n",
    "\n",
    "Let's run the script with a low number of steps for each stage in order to speed up the tutorial (in real life, longer minimisations/equilibrations/productions are needed!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d71096d7-2350-40dd-b32f-eba010da7323",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd-write-files.py --out_min min.conf --out_equil equil.conf --out_prod prod.conf --struct ionized --temp 300 --steps_min 1000 --steps_equil 10000 --steps_prod 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4c3bc10-555e-4bf6-86ef-ceb2de906710",
   "metadata": {},
   "source": [
    "Have a look at the files that have been created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5f21182-68f0-4639-9ace-c7252346f784",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -nrt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51910302-634a-4a8f-8ccb-701c73c2f336",
   "metadata": {},
   "source": [
    "`min.conf`, `equil.conf` and `prod.conf` are the NAMD inputs that have been created with our custom parameters. `ionized.fix` is instead the equivalent of `ionized.pdb` where we have constrained the phosphate atoms with a force constant of 1 kcal/mol:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9dfa5ae-97c2-482f-9eb0-0640fb64bfc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -22 ionized.fix"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70327fe7-20d8-4068-a943-8aeea91075ae",
   "metadata": {},
   "source": [
    "The P atom (last line) has the third to last column set to 1.00 instead of 0.00, this will tell NAMD where to apply the positional restraint."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3705185d-8d04-41bb-8ebd-13a1c8302c3c",
   "metadata": {},
   "source": [
    "Let's have a look at the relevant sections of `min.conf`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4234dd51-61ef-4dfd-ba6f-803116364537",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat min.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa5cda57-f40f-461a-a923-573f8e31fec6",
   "metadata": {},
   "source": [
    "`cellBasisVector1`, `cellBasisVector2`, `cellBasisVector3` and `cellOrigin` have been automatically calculated and inserted based on the size of the system. `ionized.fix`  is used in the `constraints` section. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80790b2a-fb7d-48c1-bbe5-0787d2aedc51",
   "metadata": {},
   "source": [
    "Let's have a look at the differences between `min.conf` and `equil.conf`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9a0910c-be82-4b1a-b63e-e28b0c468fb6",
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff min.conf equil.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dc9ba1f-a4a3-485f-8762-28c43054bb9c",
   "metadata": {},
   "source": [
    "`outputName` is changed; in `equil.conf`, an \"input\" section has appeared with `inputname`, `bincoordinates`, `binvelocities` and `extendedSystem` taking coordinates, velocities and box size from the previous run, so the cell parameters disappear. The exponent for the force constant of the restraints is lower, and we have `run` as command in place of `minimize`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2b70668-ed10-40a7-993e-51a416832f15",
   "metadata": {},
   "source": [
    "Let's look now at the differences between `equil.conf` and `prod.conf`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a0fceda-445d-4b69-91f2-307b22c94bfc",
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff equil.conf prod.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b4df42c-1a44-4cea-95fb-c4568316a774",
   "metadata": {},
   "source": [
    "Inputs and outputs obviously change, but the important thing is that the constraints section disappears in `prod.conf`, so we have now a fully unbiased production run."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "557646a3-583c-403b-8628-a6953b6aa7ef",
   "metadata": {},
   "source": [
    "Let's run the 3 stages (assuming you have the `namd2` binary somewhere (replace your $PATH), we don't need to parallelise as we have requested very few steps so to be run on a laptop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaeae9d8-daaa-464d-a8e4-7cd234940ec3",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd2 min.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dc60e51-f8ac-4e20-9e1a-4ffef9afb351",
   "metadata": {},
   "source": [
    "The minimisation stage has ended, let's look into the directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be988de6-3020-4474-ad51-95f8cf564839",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -nrt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdc8a8b8-0416-44de-8632-85fc3119dd4c",
   "metadata": {},
   "source": [
    "**min.dcd** will be the trajectory of the minimisation, visible with VMD. **min.xsc**, **min.coor** and **min.vel** are the restart files used by `equil.conf`. Let's run the equilibration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fcb68676-e42f-49b2-a252-673a53302cb2",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd2 equil.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab1d5fcc-6025-4b9d-931e-9ade188bc63c",
   "metadata": {},
   "source": [
    "The equilibration stage has ended, let's look into the directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9cecf34-d2c4-4a18-9cc0-8e3bbd98d1d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls -nrt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53265052-e0a2-4677-9a43-c767befb57fb",
   "metadata": {},
   "source": [
    "**equil.dcd** will be the trajectory of the equilibration, visible with VMD. **equil.xsc**, **equil.coor** and **equil.vel** are the restart files used by `prod.conf`. Let's run the production:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf6fec16-75fe-4799-a8b5-48e7e4a771da",
   "metadata": {},
   "outputs": [],
   "source": [
    "!namd2 prod.conf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad44ae68-afe8-462c-8bf7-7136b46b948a",
   "metadata": {},
   "source": [
    "We can now use `prod.dcd` for all our analyses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08531876-acc1-4969-8852-82e38c44b7d3",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
