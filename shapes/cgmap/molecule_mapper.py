import yaml
import json
from pathlib import Path

class MoleculeMapper:
    @classmethod
    def from_file(cls, filepath):
        """
        Create a MoleculeMapper instance from either a JSON or YAML file.

        Parameters
        ----------
        filepath : str or Path
            Path to the mapping file (either .json or .yaml/.yml)

        Returns
        -------
        MoleculeMapper
            Initialised molecule mapper instance

        Raises
        ------
        ValueError
            If the file format is not supported or file cannot be parsed
        """
        filepath = Path(filepath)
        
        if not filepath.exists():
            raise FileNotFoundError(f"Mapping file not found: {filepath}")
            
        with open(filepath, 'r') as f:
            if filepath.suffix.lower() in ['.yaml', '.yml']:
                try:
                    mapping_data = yaml.safe_load(f)
                except yaml.YAMLError as e:
                    raise ValueError(f"Error parsing YAML file: {e}")
            elif filepath.suffix.lower() == '.json':
                try:
                    mapping_data = json.load(f)
                except json.JSONDecodeError as e:
                    raise ValueError(f"Error parsing JSON file: {e}")
            else:
                raise ValueError(f"Unsupported file format: {filepath.suffix}")
                
        return cls(mapping_data)

    def __init__(self, mapping_data):
        """
        Initialize molecular mapping from configuration data.

        Parameters
        ----------
        mapping_data : dict
            Configuration data containing bead definitions, bond connections,
            and angle constraints for molecular mapping
        """
        self.beads = mapping_data.get('beads', [])
        self.bonds = mapping_data.get('bonds', [])
        self.angles = mapping_data.get('angles', [])
        
        self.bead_dict = {bead['name']: bead for bead in self.beads}
        self._validate_mapping()
        
        # Get unique residue names from beads
        self.residue_names = list(set(bead['residue'] for bead in self.beads))
        
        if not self.residue_names:
            print("Warning: No residue names specified in mapping file. Will accept all residues.")
        else:
            print(f"Loaded mapping with {len(self.beads)} beads for residues: {', '.join(self.residue_names)}")
    
    def save_to_file(self, filepath, format='yaml'):
        """
        Save the molecular mapping to a file in either YAML or JSON format.

        Parameters
        ----------
        filepath : str or Path
            Path where the mapping file should be saved
        format : str, optional
            Format to save the file in ('yaml' or 'json'), defaults to 'yaml'

        Raises
        ------
        ValueError
            If the specified format is not supported
        """
        filepath = Path(filepath)
        data = self.to_dict()
        
        with open(filepath, 'w') as f:
            if format.lower() == 'yaml':
                yaml.dump(data, f, default_flow_style=False, sort_keys=False)
            elif format.lower() == 'json':
                json.dump(data, f, indent=2)
            else:
                raise ValueError(f"Unsupported format: {format}. Use 'yaml' or 'json'.")

    def _validate_mapping(self):
        """
        Validate molecular mapping configuration for consistency and completeness.
        """
        required_bead_fields = ['name', 'residue', 'atoms', 'type']
        
        for bead in self.beads:
            missing_fields = [field for field in required_bead_fields if field not in bead]
            if missing_fields:
                raise ValueError(f"Bead missing required fields: {missing_fields}")
            
            if not isinstance(bead['atoms'], list):
                raise ValueError(f"Atoms for bead {bead['name']} must be a list")
                
        self._validate_connectivity()
    
    def _validate_connectivity(self):
        """
        Validate molecular connectivity by ensuring all bond and angle references exist.
        """
        all_bead_names = set(self.bead_dict.keys())
        
        # Check bonds
        for bond in self.bonds:
            if len(bond) != 3:
                raise ValueError(f"Invalid bond format: {bond}. Expected [bead1, bead2, length]")
            if bond[0] not in all_bead_names or bond[1] not in all_bead_names:
                raise ValueError(f"Bond references non-existent bead: {bond}")
                
        # Check angles
        for angle in self.angles:
            if len(angle) != 4:
                raise ValueError(f"Invalid angle format: {angle}. Expected [bead1, bead2, bead3, angle]")
            if any(bead not in all_bead_names for bead in angle[:3]):
                raise ValueError(f"Angle references non-existent bead: {angle}")

    def get_bead_info(self, bead_name):
        """
        Retrieve detailed information about a specific molecular bead.

        Parameters
        ----------
        bead_name : str
            Identifier of the bead

        Returns
        -------
        dict
            Detailed bead configuration or empty dict if not found
        """
        return self.bead_dict.get(bead_name, {})
    
    def is_valid_residue(self, res_name):
        """
        Check if a residue is supported by this molecular mapping.

        Parameters
        ----------
        res_name : str
            Name of the residue to validate

        Returns
        -------
        bool
            True if residue is supported in the mapping, False otherwise
        """
        return not self.residue_names or res_name in self.residue_names
    
    def get_beads_for_residue(self, residue):
        """
        Retrieve all bead definitions associated with a specific residue.

        Parameters
        ----------
        residue : str
            Name of the molecular residue

        Returns
        -------
        list
            Collection of bead configurations for the specified residue
        """
        return [bead for bead in self.beads if bead['residue'] == residue]
    
    def get_bonds_containing_bead(self, bead_name):
        """
        Find all molecular bonds involving a specific bead.

        Parameters
        ----------
        bead_name : str
            Identifier of the bead

        Returns
        -------
        list
            All bond definitions containing the specified bead
        """
        return [bond for bond in self.bonds if bead_name in bond[:2]]
    
    def get_angles_containing_bead(self, bead_name):
        """
        Find all molecular angles involving a specific bead.

        Parameters
        ----------
        bead_name : str
            Identifier of the bead

        Returns
        -------
        list
            All angle definitions containing the specified bead
        """
        return [angle for angle in self.angles if bead_name in angle[:3]]
    
    def get_connected_beads(self, bead_name):
        """
        Identify all beads directly bonded to a specific bead.

        Parameters
        ----------
        bead_name : str
            Identifier of the bead

        Returns
        -------
        set
            Collection of bead identifiers directly connected to the specified bead
        """
        connected = set()
        for bond in self.bonds:
            if bond[0] == bead_name:
                connected.add(bond[1])
            elif bond[1] == bead_name:
                connected.add(bond[0])
        return connected
    
    def calculate_mass(self, bead_name):
        """
        Calculate the total mass of a molecular bead.

        Parameters
        ----------
        bead_name : str
            Identifier of the bead

        Returns
        -------
        float
            Total mass of the bead (0.0 if not specified)
        """
        bead = self.get_bead_info(bead_name)
        if not bead:
            return 0.0
            
        if bead.get('mass', 0.0) != 0.0:
            return bead['mass']
            
        weights = bead.get('weights', [])
        if weights:
            return sum(weights)
            
        return 0.0  # Default if no mass information is available
    
    def get_bead_types(self):
        """
        Retrieve all unique bead types defined in the molecular mapping.

        Returns
        -------
        set
            Collection of unique bead type identifiers
        """
        return set(bead['type'] for bead in self.beads if 'type' in bead)
    
    def to_dict(self):
        """
        Convert the molecular mapping to a dictionary format.

        Returns
        -------
        dict
            Complete mapping configuration as a dictionary
        """
        return {
            'beads': self.beads,
            'bonds': self.bonds,
            'angles': self.angles
        }
    
    def __str__(self):
        """
        Generate a string representation of the molecular mapping.

        Returns
        -------
        str
            Summary of the mapping configuration
        """
        return (f"MoleculeMapper with {len(self.beads)} beads, {len(self.bonds)} bonds, "
                f"and {len(self.angles)} angles for residues: {', '.join(self.residue_names)}")
    
    def validate_atom_names(self, available_atoms):
        """
        Verify that all referenced atoms exist in the available atom set.

        Parameters
        ----------
        available_atoms : set
            Set of valid atom identifiers

        Returns
        -------
        tuple
            (bool, list) - Validation success status and list of missing atoms
        """
        missing_atoms = []
        for bead in self.beads:
            for atom in bead['atoms']:
                if atom not in available_atoms:
                    missing_atoms.append((bead['name'], atom))
        return len(missing_atoms) == 0, missing_atoms
