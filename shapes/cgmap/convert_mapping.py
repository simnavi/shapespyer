import json

class MartiniMappingConverter:
    def __init__(self):
        self.beads = {}
        self.bonds = []
        self.angles = []
        self.residue_name = ""

    def parse_map_file(self, map_content: str):
        """Parse .map file content to extract mapping information."""
        current_section = ""
        bead_map = {}
        
        for line in map_content.split('\n'):
            line = line.strip()
            if not line or line.startswith(';'):
                continue
                
            if line.startswith('['):
                current_section = line.strip('[]').strip()
                continue
                
            if current_section == "molecule":
                self.residue_name = line.strip()
            elif current_section == "atoms":
                parts = line.split()
                if len(parts) >= 3:
                    atom_num = parts[0]
                    atom_name = parts[1]
                    bead_names = parts[2:]
                    
                    for bead_name in bead_names:
                        if bead_name not in bead_map:
                            bead_map[bead_name] = []
                        bead_map[bead_name].append(atom_name)
        
        # Initialize beads dictionary
        for bead_name, atoms in bead_map.items():
            self.beads[bead_name] = {
                "atoms": atoms,
                "weights": [],
                "type": "",
                "charge": 0.0,
                "mass": 0.0
            }

    def parse_itp_file(self, itp_content: str):
        """Parse .itp file content to extract additional information."""
        current_section = ""
        
        for line in itp_content.split('\n'):
            line = line.strip()
            if not line or line.startswith(';'):
                continue
                
            if line.startswith('['):
                current_section = line.strip('[]').strip()
                continue
                
            if current_section == "atoms":
                parts = line.split()
                if len(parts) >= 7:
                    bead_name = parts[4]
                    bead_type = parts[1]
                    charge = float(parts[6])
                    
                    if bead_name in self.beads:
                        self.beads[bead_name]["type"] = bead_type
                        self.beads[bead_name]["charge"] = charge
                        
            elif current_section == "bonds":
                parts = line.split()
                if len(parts) >= 4:
                    # Convert 1-based to 0-based indexing for the bond indices
                    idx1, idx2 = int(parts[0])-1, int(parts[1])-1
                    length = float(parts[3])
                    # We need to map indices back to bead names using the atom order
                    bead_names = list(self.beads.keys())
                    if idx1 < len(bead_names) and idx2 < len(bead_names):
                        self.bonds.append([bead_names[idx1], bead_names[idx2], length])
                        
            elif current_section == "angles":
                parts = line.split()
                if len(parts) >= 5:
                    # Convert 1-based to 0-based indexing for the angle indices
                    idx1, idx2, idx3 = int(parts[0])-1, int(parts[1])-1, int(parts[2])-1
                    angle = float(parts[3])
                    # Map indices back to bead names
                    bead_names = list(self.beads.keys())
                    if all(idx < len(bead_names) for idx in [idx1, idx2, idx3]):
                        self.angles.append([
                            bead_names[idx1],
                            bead_names[idx2],
                            bead_names[idx3],
                            angle
                        ])

    def to_json(self) -> dict:
        """Convert the parsed data to the required JSON format."""
        return {
            "beads": {
                "residue": self.residue_name,
                **{k: v for k, v in self.beads.items()}
            },
            "bonds": self.bonds,
            "angles": self.angles
        }

def convert_mapping_files(map_file_path: str, itp_file_path: str, output_path: str):
    """Convert Martini mapping files to JSON format."""
    converter = MartiniMappingConverter()
    
    # Read and parse map file
    with open(map_file_path, 'r') as f:
        converter.parse_map_file(f.read())
    
    # Read and parse itp file
    with open(itp_file_path, 'r') as f:
        converter.parse_itp_file(f.read())
    
    # Convert to JSON and save
    output_data = converter.to_json()
    with open(output_path, 'w') as f:
        json.dump(output_data, f, indent=2)

# Example usage
if __name__ == "__main__":
    
    map_file = "DMPC.gromos.map"
    itp_file = "martini_v2.0_DLPC_01.itp"
    output_file = "dmpc_mapping.json"
    
    try:
        convert_mapping_files(map_file, itp_file, output_file)
        print(f"Successfully converted mapping files to {output_file}")
    except Exception as e:
        print(f"Error during conversion: {str(e)}")
