"""
.. module:: protolattice
   :platform: Linux - tested, Windows [WSL Ubuntu] - tested
   :synopsis: provides classes for generating molecular structures on 3D lattices

.. moduleauthor:: Dr Andrey Brukhno <andrey.brukhno[@]stfc.ac.uk>

The module contains class Lattice(object)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:

import sys
from math import sin, cos  #, sqrt, acos, asin
from numpy import array, dot #, ndarray , arange, random, arccos  #, cross, double
from numpy.linalg import norm

from shapes.basics.globals import TINY, Degs2Rad #, Pi, TwoPi, PiOver2  #, Rad2Degs
from shapes.basics.functions import get_mins, get_maxs
from shapes.stage.protovector import Vec3
#from shapes.stage.protospecies import Atom, Molecule, MoleculeSet
from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
from shapes.stage.protomoleculeset import MoleculeSet #as MolSet


class Lattice(object):

    """
    Class **Lattice(object)** - generates a set of molecules or molecular shapes arranged in a lattice.

    Parameters
    ----------
    nx, ny, nz : int
        Number of nodes on three primary axes
    ovec : float
        Radius-vector for the *origin* (centre) of the created 'ring'
    mols_inp : MoleculeSet
        A minimal set of *distinct* molecular species (input)
    mols_out : list
        The *final* set (list) of molecules arranged in a ring configuration in XY plane (output)
    """

    def __init__(self, nx: int = 0, ny: int = 0, nz: int = 0,
                 ovec: Vec3 = None, mols_inp: [] = None, mols_out: [] = None):
        self.nx = 1
        self.ny = 1
        self.nz = 1
        if nx > 0 : self.nx = nx
        if ny > 0 : self.ny = ny
        if nz > 0 : self.nz = nz
        self.mols_inp = mols_inp
        self.mols_out = mols_out

    def __del__(self):
        if self.mols_inp is not None: del self.mols_inp
        if self.mols_out is not None: del self.mols_out

    def make(self, gbox: [] = None, nx: int = 0, ny: int = 0, nz: int = 0,
             mols_inp: [] = None, mols_out: [] = None,
             alpha: float = 0.0, theta: float = 0.0,
             hbuf: float = 0.0, ishape: int = 6, be_verbose=False):

        mmols = sum(mols.nitems for mols in mols_inp)
        matms = sum(mol.nitems for mols in mols_inp for mol in mols)

        print(f"{self.__class__.__name__}.make(): Got {mmols} molecules of {len(mols_inp)} two species "
              f"with {matms} atoms in total ...\n")

        atoms = [a for molset in mols_inp for mol in molset.items for a in mol.items]
        atms0 = [a.getName() for a in atoms]
        axyz0 = [a.getRvec() for a in atoms]
        matms = len(atms0)

        print(f"{self.__class__.__name__}.make(): Got {matms} =?= {len(atoms)} atoms with {len(atms0)} names "
              f" and {len(axyz0)} positions ...\n")

        hbox = 0.0
        if len(gbox) > 0:
            hbox = array(gbox) * 0.5
        else:
            print(f"{self.__class__.__name__}.make(): Missing box dimensions - FULL STOP!!!\n")
            sys.exit(5)

        if nx > 0 : self.nx = nx
        if ny > 0 : self.ny = ny
        if nz > 0 : self.nz = nz

        dbx = gbox[0] / float(self.nx)
        dby = gbox[1] / float(self.ny)
        dbz = gbox[2] / float(self.nz)
        hbx = dbx / 2.0
        hby = dby / 2.0
        hbz = dbz / 2.0

        vmin = []
        imin = []
        vmax = []
        imax = []

        if abs(alpha) > TINY or abs(theta) > TINY:
            alpha = alpha * Degs2Rad  # initial rotation angle on XY plane   (azimuth)
            theta = theta * Degs2Rad  # initial rotation angle from XY plane (altitude)
            cosa = cos(alpha)
            sina = sin(alpha)
            cost = cos(theta)
            sint = sin(theta)

            vmin, imin = get_mins(axyz0)
            vmax, imax = get_maxs(axyz0)

            print(f"{self.__class__.__name__}.make(): Box(xyz) = {gbox}")
            print(f"{self.__class__.__name__}.make(): Hbx(xyz) = {hbox}")
            print(f"{self.__class__.__name__}.make(): Min(xyz) = {vmin} @ {imin}")
            print(f"{self.__class__.__name__}.make(): Max(xyz) = {vmax} @ {imax}\n")

            mint = imin[2]
            mext = imax[2]

            vec0 = array(axyz0[mext]) - array(axyz0[mint])  # the primary alignment vector for molecule
            vec2 = array([cosa * cost, sina * cost, sint])  # the initial alignment vector (director)
            rotM = vec0.getMatrixAligningTo(vec2)  # rotation matrix to align vec0 || vec2 (no scaling)

            # the 'origin' is midway between the tips
            vorg = (array(axyz0[mext]) + array(axyz0[mint])) * 0.5

            # place the read-in molecule(s) in correct orientation
            for i in range(matms) :
                vec1 = array(axyz0[i]) #-vorg
                vec3 = dot(rotM,vec1)
                diff = norm(vec1) - norm(vec3)
                if diff*diff > TINY :
                   print(f"{self.__class__.__name__}.make(): Vector diff upon rotation ({i}) = {diff}\n")
                axyz0[i] = list(vec3)

        vmin, imin = get_mins(axyz0)
        vmax, imax = get_maxs(axyz0)

        print(f"{self.__class__.__name__}.make(): Box(xyz) = {gbox}")
        print(f"{self.__class__.__name__}.make(): Hbx(xyz) = {hbox}")
        print(f"{self.__class__.__name__}.make(): Min(xyz) = {vmin} @ {imin}")
        print(f"{self.__class__.__name__}.make(): Max(xyz) = {vmax} @ {imax}\n")

        # put first COM in the box origin (i.e. subtract from all the coordinates)

        vcom = array([0.0, 0.0, 0.0])
        for i in range(matms):
            vcom += array(axyz0[i])
        vcom = vcom / float(matms)

        for i in range(matms):
            axyz0[i][0] = axyz0[i][0] - vcom[0]
            axyz0[i][1] = axyz0[i][1] - vcom[1]
            axyz0[i][2] = axyz0[i][2] - vcom[2]

        vmin, imin = get_mins(axyz0)
        vmax, imax = get_maxs(axyz0)

        cbox = array([vmax[0] - vmin[0] + hbuf, vmax[1] - vmin[1] + hbuf, vmax[2] - vmin[2] + hbuf])
        lbox = array([cbox[0] * float(self.nx), cbox[1] * float(self.ny), cbox[2] * float(self.nz)])

        gbox[0] = lbox[0]
        gbox[1] = lbox[1]
        gbox[2] = lbox[2]

        print(f"{self.__class__.__name__}.make(): Lbox(xyz) = {list(lbox)}")
        print(f"{self.__class__.__name__}.make(): Cbox(xyz) = {list(cbox)}")
        print(f"{self.__class__.__name__}.make(): Min(xyz) = {vmin} @ {imin}")
        print(f"{self.__class__.__name__}.make(): Max(xyz) = {vmax} @ {imax}\n")

        mmols = len(mols_inp)

        # generate the lattice
    
        if abs(ishape) == 6:
    
            dxyz = array(-(lbox - cbox) * 0.5)
            for i in range(self.nx):
                dxyz[0] += cbox[0]
                dxyz[1] = -(lbox[1] - cbox[1]) * 0.5
                for j in range(self.ny):
                    dxyz[1] += cbox[1]
                    dxyz[2] = -(lbox[2] - cbox[2]) * 0.5
                    for k in range(self.nz):
                        dxyz[2] += cbox[2]
                        l = 0
                        #for m in range(len(mols_inp)):
                        for m in range(mmols):
                            if m >= len(mols_out):
                                mols_out.append(MoleculeSet(m, 0,
                                                            sname=mols_inp[m].name,
                                                            stype='output'))
                            for n in range(len(mols_inp[m])):
                                mlast = len(mols_out[m])
                                mols_out[m].addItem(Molecule(mindx=mlast,
                                                             aname=mols_inp[m][n].name,
                                                             atype='output'))
                                matms = mols_inp[m][0].nitems
                                for ia in range(matms):
                                    #l += 1
                                    mols_out[m].items[mlast].addItem(
                                        Atom(aname=mols_inp[m][n][ia].name,
                                             atype=mols_inp[m][n][ia].type,
                                             aindx=ia,
                                             #arvec=list(array(mols_inp[m][n][ia].getRvec() + dxyz))))
                                             arvec=list(array(axyz0[l]) + dxyz)))
                                    l += 1

            if ishape < 0:
                dxyz = array(-lbox * 0.5)
                for i in range(self.nx):
                    dxyz[0] += cbox[0]
                    dxyz[1] = -lbox[1] * 0.5
                    for j in range(self.ny):
                        dxyz[1] += cbox[1]
                        dxyz[2] = -lbox[2] * 0.5
                        for k in range(self.nz):
                            dxyz[2] += cbox[2]
                            l = 0
                            #for m in range(len(mols_inp)):
                            for m in range(mmols):
                                if m >= len(mols_out):
                                    mols_out.append(MoleculeSet(m, 0,
                                                                sname=mols_inp[m].name,
                                                                stype='output'))
                                for n in range(len(mols_inp[m])):
                                    mlast = len(mols_out[m])
                                    mols_out[m].addItem(Molecule(mindx=mlast,
                                                                 aname=mols_inp[m][n].name,
                                                                 atype='output'))
                                    matms = mols_inp[m][0].nitems
                                    for ia in range(matms):
                                        #l += 1
                                        mols_out[m].items[mlast].addItem(
                                            Atom(aname=mols_inp[m][n][ia].name,
                                                 atype=mols_inp[m][n][ia].type,
                                                 aindx=ia,
                                                 #arvec=list(array(mols_inp[m][n][ia].getRvec() + dxyz))))
                                                 arvec=list(array(axyz0[l]) + dxyz)))
                                        l += 1

        else:
    
            cos30 = 0.866025403784
    
            cell = min(cbox)
            cbox[0] = cell
            cbox[1] = cell
            cbox[2] = cell
            lbox = array([cbox[0]*float(self.nx), cbox[1]*float(self.ny)*cos30, cbox[2]*float(self.nz)*cos30])
    
            gbox[0] = lbox[0]
            gbox[1] = lbox[1]
            gbox[2] = lbox[2]
    
            dxyz = array(-cbox * 0.5)
    
            dxyz[2] = -cbox[2] * cos30 * 0.5
            for k in range(self.nz):
                dxyz[2] += cbox[2] * cos30
                dxyz[1] = -cbox[1] * cos30 * 0.5
                dx = 0.0
                nyd = k % 2
                if nyd == 0:
                    dxyz[1] += cbox[1] * cos30 / 3.0  # *0.25
                    # dx = cbox[0]*0.25
                else:
                    dxyz[1] -= cbox[1] * cos30 / 3.0  # *0.25
                    # dx = -cbox[0]*0.25
                for j in range(self.ny):
                    dxyz[1] += cbox[1] * cos30
    
                    dxyz[0] = -cbox[0] * 0.5
                    nxd = j % 2
                    if nxd == 0:
                        dxyz[0] += cbox[0] * 0.25
                    else:
                        dxyz[0] -= cbox[0] * 0.25
                    for i in range(self.nx):  # range(self.nx+nxd) :
                        dxyz[0] += cbox[0]
    
                        l = 0
                        #for m in range(len(mols_inp)):
                        for m in range(mmols):
                            if m >= len(mols_out):
                                mols_out.append(MoleculeSet(m, 0,
                                                            sname=mols_inp[m].name,
                                                            stype='output'))
                            for n in range(len(mols_inp[m])):
                                mlast = len(mols_out[m])
                                mols_out[m].addItem(Molecule(mindx=mlast,
                                                             aname=mols_inp[m][n].name,
                                                             atype='output'))
                                matms = mols_inp[m][0].nitems
                                for ia in range(matms):
                                    #l += 1
                                    mols_out[m].items[mlast].addItem(
                                        Atom(aname=mols_inp[m][n][ia].name,
                                             atype=mols_inp[m][n][ia].type,
                                             aindx=ia,
                                             #arvec=list(array(mols_inp[m][n][ia].getRvec() + dxyz))))
                                             arvec = list(array(axyz0[l]) + dxyz)))
                                    l += 1

    # end Lattice.make(...)

# end of class Lattice()


