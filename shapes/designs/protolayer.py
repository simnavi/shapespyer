"""
.. module:: protolayer
   :platform: Linux - tested, Windows [WSL Ubuntu] - tested
   :synopsis: provides classes for generating lamellae molecular structures

.. moduleauthor:: Dr Andrey Brukhno <andrey.brukhno[@]stfc.ac.uk>

The module contains class Bilayer(object)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: Dr Valeria Losasso (c) 2024          #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (Bilayer class and relevant tests)    #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:

import sys
#from math import sqrt, sin, cos  #, acos, asin
from numpy import array, ndarray #, arange, dot, random, arccos  #, cross, double
from shapes.basics.globals import TINY #, Pi, TwoPi, Degs2Rad, PiOver2  #, Rad2Degs
from shapes.stage.protovector import Vec3
#from shapes.stage.protospecies import Atom, Molecule #, MoleculeSet
from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
#from shapes.stage.protomoleculeset import MoleculeSet #as MolSet

class Bilayer (object):

    """
    Class **Bilayer(object)** - generates a set of molecules arranged in a 'bilayer' configuration.

    If `zsep = 0` (default), the two leaflets are separated by distance `dmin` (nm)
    between the *interior* atoms defined by `mint` indices for each species.

    In contrast, if `zsep > 0`, the two leaflets are arranged so that distance `zsep` (nm)
    is maintained between their exterior atoms defined by `mext` indices for each species.

    Parameters
    ----------
    zsep : float
        z distance between the two layers of a bilayer
    dmin : float
        spacing in x-y between bilayer molecules
    nside : int
        Number of molecules on each side of the bilayer
    mols_inp : MoleculeSet list
        A minimal set of *distinct* molecular species (input)
    mols_out : MoleculeSet list
        The *final* set (list) of molecules arranged in a bilayer configuration in XY plane (output)
    """

    def __init__(self, zsep: float = 0.0, dmin: float = 0.5, nside: int = 0,
                 mols_inp: [] = None, mols_out: [] = None):
        self.zsep  = zsep
        self.dmin  = dmin
        self.nside = nside
        self.mols_inp = mols_inp
        self.mols_out = mols_out
        self.zsep = zsep 
        self.dmin = dmin

    def __del__(self):
        if self.mols_inp is not None: del self.mols_inp
        if self.mols_out is not None: del self.mols_out

    def make(self, zsep: float = 0.0, nside: int = 0, dmin: float = 0.0,
             mols_inp: [] = None, mols_out: [] = None, invVec = [1., -1., -1.]):
        """
        Takes a minimal set of distinct molecules as input and populates
        a Bilayer object (``self``) with molecules placed accordingly.

        :param nside: as above
        :param dmin: as above
        :param zsep: as above
        :param mols_inp: as above
        :param mols_out: as above

        :return: None (`mols_out = self.mols_out`)
        """

        # AB: reset attributes of self if meaningful input is provided
        # AB: otherwise attempt using attributes of self (if meaningful)

        if isinstance(mols_inp, list) and len(mols_inp) > 0:  # reset self.mols_inp with the current input
            self.mols_inp = mols_inp
        elif isinstance(self.mols_inp, list) and len(self.mols_inp) > 0:  # reset local mols_inp
            mols_inp = self.mols_inp
        else:  # AB: raise Exception(message) and halt execution
            raise Exception(f"{self.__class__.__name__}.make(): "
                            f"Incorrect type of 'mols_inp' - must be non-empty list")

        if isinstance(mols_out, list) and len(mols_out) > 0:  # reset self.mols_out with the current input
            self.mols_out = mols_out
        elif isinstance(self.mols_out, list) and len(self.mols_out) > 0:  # reset local mols_out
            mols_out = self.mols_out
        else:  # AB: raise Exception(message) and halt execution
            raise Exception(f"{self.__class__.__name__}.make(): "
                            f"Incorrect type of 'mols_out' - must be non-empty list")

        if zsep > TINY:
            self.zsep = zsep
        elif self.zsep > TINY:
            zsep = self.zsep
        # AB: zsep can be 0!
        # else:
        #     raise Exception(f"{self.__class__.__name__}.make(): "
        #                     f"Incorrect input for 'zsep' = {zsep} - must be > 0")

        if nside > 0:
           self.nside = nside
        elif self.nside > 0:
           nside = self.nside
        else:
            raise Exception(f"{self.__class__.__name__}.make(): "
                            f"Incorrect input for 'nside' = {nside} - must be > 0")

        if dmin > TINY:
           self.dmin = dmin
        elif self.dmin > TINY:
           dmin = self.dmin
        else:
            raise Exception(f"{self.__class__.__name__}.make(): "
                            f"Incorrect input for 'dmin' = {dmin} - must be > 0")

        # AB: make sure the inversion list (or array) is suitable
        if (isinstance(invVec, list) or isinstance(invVec, ndarray)) and len(invVec) == 3:
            for iv in invVec:
                if iv > TINY:    iv = 1.0
                elif iv < -TINY: iv =-1.0
                else:            iv = 0.0
            invVec[2] = -1.0
            if isinstance(invVec, list):
                invVec = array(invVec)
        else:
            invVec = array([1.0, 1.0, -1.0])

        isZext = False
        zini = 0.0
        if zsep < TINY:
            zini = 0.5*dmin
        else:
            zini = 0.5*zsep
            isZext = True

        z_axis = array([0.0, 0.0, 1.0])
        for m in range(len(mols_inp)):
            # AB: get reference atom index
            mref = mols_inp[m][0].getBoneInt()
            if isZext:
                mref = mols_inp[m][0].getBoneExt()
            for n in range(len(mols_inp[m])):
                # align the molecule to the z axis
                # upon alignment mint-th atom is placed at the origin
                mols_inp[m][n].alignBoneToVec(z_axis)
                # AB: get the Z shift
                z_mint = zini - mols_inp[m][n].items[mref].rvec[2]
                # AB: translate the molecule
                mols_inp[m][n].moveBy(Vec3(0.0, 0.0, z_mint))

        # create upper monolayer from single molecule input
        # MS: converting all distances from angstroms to nm
        self.create_monolayer(nside, dmin, mols_inp, mols_out)

        mols_inp_inv = mols_inp.copy()

        for m in range(len(mols_inp_inv)):
            for n in range(len(mols_inp_inv[m])):
                # AB: apply coordinates inversion
                for i in range (len(mols_inp_inv[m][n])):
                    mols_inp_inv[m][n][i].rvec[0] = invVec[0]*mols_inp_inv[m][n][i].rvec[0]
                    mols_inp_inv[m][n][i].rvec[1] = invVec[1]*mols_inp_inv[m][n][i].rvec[1]
                    mols_inp_inv[m][n][i].rvec[2] = invVec[2]*mols_inp_inv[m][n][i].rvec[2]

        # create second layer
        self.create_monolayer(nside, dmin, mols_inp_inv, mols_out)
    # end of make()

    def create_monolayer(self, nside, dmin, mols_inp, mols_out):
        natms = 0
        for m in range(len(mols_inp)):
            # AB: in general, all template molecules in list mols_inp[m] must be the same (except their positions)
            # AB: but for the bilayer scenario there is only one template molecule per species
            matms = len(mols_inp[m][0])
            for k in range(nside):  # iterate over molecules on each side of the monolayer
                mlast = mols_out[m].nitems
                mols_out[m].addItem(Molecule(mindx=mlast, aname=mols_inp[m].name, atype='output'))
                for i in range(matms):
                    vec0 = mols_inp[m][0].items[i].getRvec()
                    # replicate over x
                    #vec1 = array([(k+1)*dmin, 0, 0])
                    vec1 = Vec3((k+1)*dmin, 0.0, 0.0)
                    vec2 = vec0 + vec1
                    mols_out[m].items[mlast].addItem(
                        Atom(aname=mols_inp[m][0].items[i].name,
                             atype=mols_inp[m][0].items[i].type,
                             aindx=natms,
                             arvec=Vec3(*vec2)))
                             #arvec=[vec2[0], vec2[1], vec2[2]]))
                    natms += 1
                for k1 in range (nside-1):  # expand along y
                    mlast = mols_out[m].nitems  # len(mols_out[m].items)
                    mols_out[m].addItem(Molecule(mindx=mlast,
                                                 aname=mols_inp[m].name,
                                                 atype='output'))
                    for i in range(matms):
                        vec0 = mols_inp[m][0].items[i].getRvec()
                        # replicate all the molecules in x over y
                        #vec1 = array([(k+1)*dmin, (k1+1)*dmin, 0])
                        vec1 = Vec3((k+1)*dmin, (k1+1)*dmin, 0.0)
                        vec2 = vec0 + vec1
                        mols_out[m].items[mlast].addItem(
                             Atom(aname=mols_inp[m][0].items[i].name,
                                  atype=mols_inp[m][0].items[i].type,
                                  aindx=natms,
                                  arvec=Vec3(*vec2)))
                                  #arvec = [vec2[0], vec2[1], vec2[2]]))
                        natms += 1
    # end of create_monolayer()

# end of class Bilayer(object)
