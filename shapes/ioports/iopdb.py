"""
.. module:: iopdb
   :platform: Linux - tested, Windows (WSL Ubuntu) - tested
   :synopsis: provides classes for PDB oriented input/output

.. moduleauthor:: Dr Valeria Losasso <valeria.losasso[@]stfc.ac.uk>

The module contains class pdbFile(ioFile)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                # 
#  Shapespyer - soft matter structure generator  # 
#                                                # 
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  # 
#                                                #
#  Contrib: Dr Valeria Losasso (c) 2024          #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (PDB file IO and relevant tests)      #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:

# import os
# import sys
# import time

from shapes.basics.globals import *
from shapes.ioports.iofiles import ioFile
from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
from shapes.stage.protomoleculeset import MoleculeSet as MolSet
#from shapes.stage.protomolecularsystem import MolecularSystem as MolSys


class pdbFile(ioFile):

    """
    Class **pdbFile(ioFile)** abstracts I/O operations on PDB files.

    Parameters
    ----------
    fname : string
        Full name of the file, possibly including the path to it
    fmode : string
        Mode for file operations, must be in ['r','w','a']
    try_open : boolean
        Flag to open the file upon creating the file object
    """

    # def __init__(self, fname: str, fmode='r', try_open=False):
    def __init__(self, *args, **keys):
        super(pdbFile, self).__init__( *args, **keys)

        if self._ext != '.pdb':
            print("{self.__class__.__name__}:: Wrong extension '" +
                  self._ext + "' for PDB file '" + self._fname + "' - FULL STOP!!!")
            sys.exit(1)
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''

        if self._fmode not in ['r','w','a']:
            print(f"{self.__class__.__name__}.readInMols(): Oops! Unkown mode '{self._fmode}' "
                  f"for file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)
    # end of __init__()

    def readInMols(self, rems=[], mols=[], box=[], resnames=(), resids=(), lenscale=0.1):
        if not self.is_open():
            self.open(fmode='r')
            print(f"{self.__class__.__name__}.readInMols(): Ready for reading PDB file '{self._fname}' ...")
        if not self.is_rmode():
            print(f"{self.__class__.__name__}.readInMols(): Oops! Wrong mode '{self._fmode}' "
                  f"(file in rmode = {self._is_rmode}) for reading file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

        print(f"{self.__class__.__name__}.readInMols(): Reading PDB file '{self._fname}' "
              f"from line # {str(self._lnum)} (file is_open = {self.is_open()})...")

        line  = ""
        nrems = 0
        isread= True
        if self._lnum == 0:
            line = self._fio.readline().rstrip()
            while line[:6] in {"HEADER", "TITLE ", "REMARK"}:
                #if "REMARK" in line: # checking if the file starts with comments
                self._remark += line[9:].lstrip() + ' '
                self._lnum += 1
                nrems += 1
                print(f"{self.__class__.__name__}.readInMols(): PDB title: '{self._remark}'\n")
                rems.append(line[9:].lstrip())
                line = self._fio.readline().rstrip()
                isread = False
            if line[:5] == "CRYST":
                nrems += 1
                self._lnum += 1
                lbox = line[6:].split()
                # MS: convert box size (unit cube) from angstoems to nm, but keep angles in degrees
                for lb in range(min(6,len(lbox))):
                    box.append(float(lbox[lb])*(lenscale if lb<3 else 1.0))
                if len(lbox) > 6:
                    lb = line.rfind(str(lbox[5]))
                    box.append(line[lb:])
                # box.append(float(lbox[0]))
                # box.append(float(lbox[1]))
                # box.append(float(lbox[2]))
                print(f"{self.__class__.__name__}.readInMols(): PDB CRYST (box): '{box}'\n")
                line = self._fio.readline().rstrip()
                isread = False

        matms = 0
        ierr  = 0
        nout  = 1
        natms = 0
        mmols = 0
        nmols = 0
        mspec = 0
        resip = 0
        resix = 0
        resnp = "none"
        resnm = "none"

        resname = resnames[0]
        resid   = resids[0]
        mres   = len(resnames)
        mids   = len(resids)
        mfound = 0

        # AB: helper lists for molecular species
        # previously declared in globals.py, now only used locally here, in iopdb.readInMols()
        mnatm = []  # helper list of atom numbers per molecule
        molnm = []  # helper list of molecule names
        mnmol = []  # helper list of molecular species to be read in

        # arrange for abnormal EOF handling
        if self._lnum == nrems:
            is_molin = False   
            mlast = 0
            #for i in range(matms):
            while True:
                # do not lstrip here - relaying on field widths in PDB files!
                if isread:
                    line = self._fio.readline().rstrip()
                if not line:  # or len(line.split())!=4 :
                    break
                if not (line[:4] == "ATOM" or line[:6] == "HETATM"):
                    continue
                isread = True
                self._lnum += 1
                matms += 1

                # AB: reference for 'ATOM' / 'HETATM' records:
                # https://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM

                resip = resix
                resix = int(line[22:26].strip())

                # AB: PDB format expects residue name of only 3 characters [18-20]
                # AB: but we allow for residue names of 5 characters [17-21],
                # AB: including 'altLoc' character before it [17] (Alternate location indicator)
                # AB: and a space after it = before 'chainID' character [22] (Chain identifier)
                resnp = resnm
                #resnm = line[16:22].strip()
                resnm = line[16:21].strip()
                is_another = (resnm != resnp) # another molecular species

                #print(f"{self.__class__.__name__}.readInMols(): Read-in resix = {resix} "
                #      f", resnm = {resnm} at line # {self._lnum}\n")

                if is_another or resix != resip:
                # if resnm != resnp or resix != resip:
                    # another molecule / residue found in input
                    is_molin = resnm in resnames or resname == 'ALL' or \
                               (resname == 'ANY' and resix in resids)
                    if is_molin:

                        if is_another:
                        # AB: a species with a name different from the previous one

                            if nmols > 0:
                                print(f"{self.__class__.__name__}.readInMols(): In total {nmols+1} '{resnp}' "
                                      f"molecule(s) of {natms} atom(s) found ... ")

                            if len(molnm) > 0 and resnm in molnm:
                                # AB: a previously encountered molecular species
                                mspec = molnm.index(resnm)
                                nmols = mnmol[mspec]
                                print(f"{self.__class__.__name__}.readInMols(): It's not a new species "
                                      f"- {len(mols)}, mspec = {mspec}, nmols = {nmols}")
                            else:
                            # AB: another molecular species encountered
                                print(f"{self.__class__.__name__}.readInMols(): Adding new molecular species "
                                      f"- {len(mols)}, mspec = {mspec}, nmols = {nmols}")
                                molnm.append(resnm)
                                if natms > 0:
                                    mnatm.append(natms)
                                # if nmols > 0:
                                mnmol.append(nmols)
                                nmols = 0
                                mspec += 1
                                mfound += 1
                                mols.append(MolSet(mspec, 0, sname=resnm, stype='input'))
                        else:
                            nmols += 1
                        mols[mspec-1].addItem(Molecule(nmols, resnm, 'input'))
                        mlast  = len(mols[mspec-1].items)-1
                        mmols += 1

                        natms = 0

                if is_molin: 
                    natms += 1
                    atmix = int(line[6:11].lstrip().rstrip())
                    #aindx = int(line[6:11].lstrip().rstrip())
                    # print(f"{self.__class__.__name__}.readInMols(): Read-in resix = "+str(resix)+", resnm = "+resnm+", atnm = "+atms[i]+ \
                    #      ", atmid = "+str(atmix)+"\n")

                    # AB: PDB format expects atom names of only 4 characters [13-16]
                    # AB: but we allow for an extra character in atom names [12-16]
                    # AB: which otherwise would be taken by space before it
                    #atype = line[12:16].strip()
                    atype = line[11:16].strip()
                    aname = line[11:16].strip()

                    # AB: atom coordinates in format Real(8:3) [31-54]
                    latm = line[30:54].split()
                    # print(f"{self.__class__.__name__}.readInMols(): Read-in resix = "+str(resix)+", resnm = "+resnm+", atnm = "+atms[i]+ \
                    #      ", coords = "+'({:>8.3f},{:>8.3f},{:>8.3f})'.format(*axyz[i])+"\n")

                    # MS: convert atom coordinates from angstroms to nm for assignment
                    mols[mspec-1].items[mlast].addItem(Atom(aname, atype, aindx=natms,
                                                           arvec=[lenscale*float(latm[0]), lenscale*float(latm[1]), lenscale*float(latm[2])]))

                    if nmols <= nout:
                        print(f"{self.__class__.__name__}.readInMols(): "
                              f"{mols[mspec-1].items[mlast].items[len(mols[mspec-1].items[mlast].items)-1]}")
                    elif nmols == nout+1 and natms < 2:
                        print(f"{self.__class__.__name__}.readInMols(): "
                              f"More than {nout} '{resnm}' molecule(s) ... ")
            if mmols > 0:
                mnmol[0] =  sum(mnmol[1:])
                if natms > 0 :
                    mnatm.append(natms)
                    print(f"{self.__class__.__name__}.readInMols(): In total {nmols+1} '{resnp}' "
                          f"of {natms} atom(s) found ... ")
                natms = sum(mnatm)

                print(f"\n{self.__class__.__name__}.readInMols(): Read-in Mmols = {str(mmols)}, "
                      f"Matms = {str(mnatm)}, MolNames = {str(molnm)}")
            else:
                print(f"\n{self.__class__.__name__}.readInMols(): Read-in Mmols = {str(mmols)}, "
                      f"no molecule '{resname}' with resid in {str(resids)} found - FULL STOP!!!")
                sys.exit(2)

        natms = len([a for mset in mols  for mol in mset.items for a in mol.items])
        if matms != natms:
            print(f"\n{self.__class__.__name__}.readInMols(): Total number of atoms: {matms} =/= {natms} number of atoms kept...")
            # print(f"\n{self.__class__.__name__}.readInMols(): Oops! Inconsistent number of atoms: {matms} =/= {natms}"
            #      f" - FULL STOP!!!")
            # sys.exit(4)

        if ierr == 0:
            print(f"{self.__class__.__name__}.readInMols(): File '{self._fname}' successfully read: "
                  f"lines = {str(self._lnum)} & natms = {str(natms)}\n")

        return (ierr == 0)
#   end of readInMols(...)

    def writeOutMols(self, rems=[], gbox=[],  mols=[]):
    ### NEED TO REFACTOR - ??? ###

        if not self._is_open:
            self.open(fmode='w')
            print(f"{self.__class__.__name__}.writeOutMols(): Ready for writing PDB file '{self._fname}' ...")
        if not self._is_wmode:
            print(f"{self.__class__.__name__}.writeOutMols(): Oops! Wrong mode '{self._fmode}' "
                  f"for writing file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

        print(f"{self.__class__.__name__}.writeOutMols(): Writing PDB file '{self._fname}' "
              f"from line # {str(self._lnum)} ...")

        imols = len(mols)                    # mnmol[0] - number of molecule types / sets of unique molecules
        mmols = sum(len(ms) for ms in mols)  # mnmol[len(mnmol) - 1] - total number of molecules in all sets

        nout = 0  # total number of atoms to output
        for mts in mols:
            for mol in mts:
                nout += len(mol)

        ierr  = 0
        nlines = 0
        natms  = 0
        resid  = 0
        is_fin = False
        for m in range(imols):
            # matms = len(mols[m][0])  # mnatm[m] - number of atoms per molecule of type m
            nmols = len(mols[m])     # int(len(atms[m]) / matms) - number of molecules of type / in set m
            for k in range(nmols):
                resnm = mols[m][k].name  # molnm[m] - name of molecules of type m / in set m
                resid += 1
                if resid == 1:
                    print(f"{self.__class__.__name__}.writeOutMols(): Writing molecule '{str(resid)+resnm}' "
                          f"into PDB file '{self._fname}' ...")
                    if isinstance(rems, list):
                        if len(rems) > 0:
                            self._fio.write("HEADER    " + rems[0] + "\n")
                            st = ' '
                            ns = 1
                            for rem in rems[1:]:
                                self._fio.write("TITLE    " + st + rem + "\n")
                                ns += 1
                                st = str(ns) + ' '
                            nlines += len(rems)
                    elif isinstance(rems, str):
                        self._fio.write("HEADER    " + rems + "\n")
                        nlines += 1

                    if isinstance(gbox, list) or isinstance(gbox, np.ndarray):
                        self._fio.write('CRYST1')
                        if len(gbox) == 3:
                            self._fio.write('{:>9.3f}{:>9.3f}{:>9.3f}'.format(*gbox)) # + "\n")
                            self._fio.write("  90.00  90.00  90.00 P 1           1\n")
                        elif len(gbox) >= 6:
                            self._fio.write('{:>9.3f}{:>9.3f}{:>9.3f}'.format(*gbox[:3]))
                            self._fio.write('{:>7.2f}{:>7.2f}{:>7.2f}'.format(*gbox[3:6]))
                            if len(gbox) > 6:
                                lend = str(gbox[7:]).join(' ')
                                self._fio.write(lend + "\n")
                        nlines += 1
                    rems = None
                elif resid < 10 or (resid < 100 and resid % 10 == 0) or \
                    (resid < 1000 and resid % 100 == 0) or (resid % 1000 == 0) or resid == mmols:
                    print(f"{self.__class__.__name__}.writeOutMols(): Appending molecule '{str(resid)+resnm}' "
                          f"to file '" + self._fname + "' ...")

                matms = len(mols[m][k])  # mnatm[m] - number of atoms in molecule

                for i in range(matms):
                    nlines += 1
                    natms += 1
                    iprn = natms % 100000

                    rvec = [0.0 if abs(elem)<TINY else elem for elem in mols[m][k][i].rvec] # rounds tiny negative coordinates up to zero: avoids printing -0.0
                    line = "ATOM  " + '{:>5}{:>5}'.format(iprn,  mols[m][k][i].name) + \
                    '{:>5} {:>4}'.format(resnm, resid) + ''.join('{:>12.3f}{:>8.3f}{:>8.3f}'.format(*rvec))
                    #" " + '{:>5}{:>4}'.format(resnm, resid) + ''.join('{:>12.3f}{:>8.3f}{:>8.3f}'.format(*rvec))
                    self._fio.write(line + "\n")

                    # print("File '"+self._fname+"' : successfully written n_lines = "+str(nlines)+ \
                    #      " : n_mols = " + str(resid) + " & n_atms = "+str(natms)+" / "+str(nout)+"\n")

        if ierr == 0 and is_fin:
            print(f"{self.__class__.__name__}.writeOutMols(): File '{self._fname}' "
                  f"successfully written: lines = {str(nlines)} : n_mols = {str(resid)} "
                  f"& n_atms = {str(natms)} / {str(nout)}\n")
        return (ierr == 0)
    # end of writeOutMols()

    def close(self):
        super(pdbFile, self).close()
        # self._fio.close()
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''

    def __del__(self):
        # super(ioFile, self).__del__()
        self.close()

# end of Class pdbFile
