"""
.. module:: iotraj
   :platform: Linux - tested, Windows (WSL Ubuntu) - NOT TESTED 
   :synopsis: Trajectory handling for molecular dynamics data

.. moduleauthor:: Saul Beck <saul.beck[@]stfc.ac.uk>

The module contains class Trajectory(ABC) and DCDTrajectory
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: Dr Saul Beck (c) 2024                #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (DCD file IO and relevant tests)      #
#                                                #
##################################################

from abc import ABC, abstractmethod
import numpy as np
import numpy.typing as npt
from pathlib import Path
from typing import Dict, Iterator, Union, List, Type, Tuple
from dataclasses import dataclass
import struct
import os
from ioports.ioframe import Frame, CellParameters


class Trajectory(ABC):
    """
    Abstract base class for molecular trajectories.

    This class defines the core interface and functionality that all trajectory implementations
    must provide. It handles reading, writing, and analysing trajectory data across multiple
    file formats.
    """

    _trajectory_types = {}  # Registry for trajectory implementations
    
    def __init__(self):
        """Initialise base trajectory attributes."""
        self._frames: List[Frame] = []  # List to store Frame objects
        
    @abstractmethod
    def _initialize(self, *args, **kwargs) -> None:
        """
        Initialise trajectory-specific attributes.
        
        Must be implemented by derived classes to handle format-specific initialization.
        """
        pass

    @classmethod
    def register_trajectory_type(cls, extension: str):
        """
        Decorator to register trajectory implementations.

        Parameters
        ----------
        extension : str
            File extension to associate with this trajectory type (e.g., '.dcd')

        Returns
        -------
        callable
            Decorator function that registers the trajectory class
        """
        def decorator(trajectory_class: Type['Trajectory']):
            cls._trajectory_types[extension.lower()] = trajectory_class
            return trajectory_class
        return decorator

    @classmethod
    def from_file(cls, filepath: Union[str, Path], mode: str = 'r') -> 'Trajectory':
        """
        Factory method to create trajectory from file.

        Parameters
        ----------
        filepath : Union[str, Path]
            Path to the trajectory file
        mode : str
            File mode ('r' for read, 'w' for write)

        Returns
        -------
        Trajectory
            Appropriate trajectory instance for the file type

        Raises
        ------
        ValueError
            If the file type is not supported
        """
        filepath = Path(filepath)
        extension = filepath.suffix.lower()

        trajectory_class = cls._trajectory_types.get(extension)
        if trajectory_class is None:
            supported = ", ".join(cls._trajectory_types.keys())
            raise ValueError(
                f"Unsupported file type: {extension}. "
                f"Supported types are: {supported}"
            )

        return trajectory_class(filepath, mode)

    @property
    @abstractmethod
    def number_of_atoms(self) -> int:
        """
        Get the number of atoms in the system.

        Returns
        -------
        int
            Number of atoms in each frame
        """
        pass

    @property
    @abstractmethod
    def number_of_frames(self) -> int:
        """
        Get the total number of frames in the trajectory.

        Returns
        -------
        int
            Total number of frames
        """
        pass

    @abstractmethod
    def read(self) -> None:
        """
        Read the entire trajectory file.

        This method should handle all file reading operations and populate
        the internal frame list.
        """
        pass

    @abstractmethod
    def write(self, frames: List[Frame]) -> None:
        """
        Write frames to trajectory file.

        Parameters
        ----------
        frames : List[Frame]
            List of frames to write to file
        """
        pass

    @abstractmethod
    def close(self) -> None:
        """
        Close the trajectory file and clean up resources.
        """
        pass

    @property
    def frames(self) -> List[Frame]:
        """
        Get all frames in the trajectory.

        Returns
        -------
        List[Frame]
            List of all frames in the trajectory
        """
        return self._frames

    def __iter__(self) -> Iterator[Tuple[CellParameters, npt.NDArray]]:
        """
        Iterate over frames, yielding tuples of (cell_parameters, coordinates).

        Returns
        -------
        Iterator[Tuple[CellParameters, npt.NDArray]]
            Iterator yielding cell parameters and coordinates for each frame
        """
        for frame in self._frames:
            yield frame.cell_parameters, frame.coordinates

    def __getitem__(self, index: Union[int, slice]) -> Union[Frame, List[Frame]]:
        """
        Get frame(s) by index or slice.

        Parameters
        ----------
        index : Union[int, slice]
            Index or slice to retrieve

        Returns
        -------
        Union[Frame, List[Frame]]
            Single frame or list of frames
        """
        return self._frames[index]

    def get_frame(self, index: int) -> Frame:
        """
        Get a specific frame object.

        Parameters
        ----------
        index : int
            Frame index

        Returns
        -------
        Frame
            Frame object at the specified index
        """
        return self._frames[index]

    @abstractmethod
    def get_volume_statistics(self) -> Dict[str, float]:
        """
        Calculate volume statistics across all frames.

        Returns
        -------
        Dict[str, float]
            Dictionary containing:
            - 'mean': Average volume
            - 'std': Standard deviation
            - 'min': Minimum volume
            - 'max': Maximum volume
        """
        pass

    @abstractmethod
    def print_statistics(self) -> None:
        """Print statistical information about the entire trajectory."""
        pass

# end of Trajectory class


@Trajectory.register_trajectory_type('.dcd')
class DCDTrajectory(Trajectory):
    """
    DCD-specific trajectory implementation.
    
    Handles reading and writing of DCD. Implements all methods required 
    by the Trajectory base class.
    """

    # Constants for DCD file processing
    INITIAL_HEADER_SIZE: int = 24
    CORD_VELD_SIZE: int = 4
    NUM_SIMULATION_PARAMS: int = 9
    NUM_TIMESTEP_FLOATS: int = 1
    NUM_RESERVED_INTS: int = 10
    TITLE_LINE_SIZE: int = 80
    BYTES_PER_FLOAT: int = 4
    NUM_COORDINATES: int = 3
    CELL_RECORD_SIZE: int = 56
    EXPECTED_ENDIAN_VALUE: int = 84
    RECORD_MARKER_SIZE = 4
    LITTLE_ENDIAN_PREFIX: str = "<"
    BIG_ENDIAN_PREFIX: str = ">"

    def __init__(self, filepath: Union[str, Path], mode: str = 'r'):
        """
        Initialize DCD trajectory.

        Parameters
        ----------
        filepath : Union[str, Path]
            Path to the DCD file
        mode : str
            File mode ('r' for read, 'w' for write)

        Raises
        ------
        ValueError
            If file extension is not .dcd or mode is invalid
        """
        super().__init__()  # Initialize base class
        self._initialize(filepath, mode)
        
    def _initialize(self, filepath: Union[str, Path], mode: str) -> None:
        """
        Initialise DCD-specific attributes.
        
        Parameters
        ----------
        filepath : Union[str, Path]
            Path to the DCD file
        mode : str
            File mode ('r' for read, 'w' for write)
        """
        self._filepath = Path(filepath)
        self._mode = mode
        self._number_of_atoms: int = 0  # Number of atoms per frame
        self._file = None  # File handle
        self._is_big_endian: bool = None  # Endianness flag

        if self._filepath.suffix.lower() != '.dcd':
            raise ValueError(f"Invalid file extension: {self._filepath.suffix}")

        if mode not in ['r', 'w']:
            raise ValueError(f"Invalid mode: {mode}")

        # Read file if in read mode
        if mode == 'r':
            self.read()

    @property
    def number_of_atoms(self) -> int:
        """Get total number of atoms."""
        return self._number_of_atoms

    @property
    def number_of_frames(self) -> int:
        """Get total number of frames."""
        return len(self._frames)

    def read(self) -> None:
        """
        Read the entire trajectory.
        
        Opens the DCD file, reads header information and all frames.
        Stores frames internally for later access.
        """
        with open(self._filepath, 'rb') as self._file:
            self._read_header()
            self._read_frames()

    def write(self, frames: List[Frame]) -> None:
        """
        Write frames to DCD file.

        Parameters
        ----------
        frames : List[Frame]
            List of Frame objects to write
            
        Raises
        ------
        IOError
            If file not opened in write mode
        ValueError
            If no frames provided or inconsistent atom counts
        """
        if self._mode != 'w':
            raise IOError("File not opened in write mode")
        
        if not frames:
            raise ValueError("No frames provided to write")
            
        # Set number of atoms from first frame
        self._number_of_atoms = len(frames[0].coordinates)
        
        # Verify all frames have same number of atoms
        if any(len(frame.coordinates) != self._number_of_atoms for frame in frames):
            raise ValueError("All frames must have the same number of atoms")
        
        print(f"Writing {len(frames)} frames with {self._number_of_atoms} atoms each")
        
        # Open file and write data
        with open(self._filepath, 'wb') as fout:
            self._write_dcd_header(fout, len(frames))
            for frame in frames:
                self._write_frame(fout, frame)

    def _write_dcd_header(self, fout, num_frames: int) -> None:
        """
        Write the DCD file header.

        Parameters
        ----------
        fout : file object
            Open file handle in binary write mode
        num_frames : int
            Number of frames to write
        """
        # First record: simulation parameters
        rec1_data = []
        rec1_data.extend([ord(c) for c in 'CORD'])  # File type identifier
        rec1_data.extend([
            num_frames,  # Number of frames in file
            0,          # Starting timestep
            1,          # Timestep frequency 
            num_frames, # Total timesteps
            0, 0, 0, 0, 0  # Padding
        ])
        rec1_data.append(1.0)  # Timestep size
        rec1_data.extend([2] + [0] * 8 + [410])  # System flags and CHARMM version

        # Write records
        self._write_fortran_record(fout, [rec1_data], '4B 9i f 10i ')

        # Second record: title information  
        title = "DCD file created by IOTraj".ljust(80)
        remark = "Generated by DCDTrajectory class".ljust(80)
        rec2_data = []
        rec2_data.append(2)  # Number of title lines
        rec2_data.extend([ord(c) for c in title])
        rec2_data.extend([ord(c) for c in remark])
        
        self._write_fortran_record(fout, [rec2_data], 
                                 f'i {len(title)}B {len(remark)}B ')

        # Write number of atoms
        self._write_fortran_record(fout, [[self._number_of_atoms]], 'i ')

    def _write_frame(self, fout, frame: Frame) -> None:
        """
        Write a single frame to the DCD file.

        Parameters
        ----------
        fout : file object
            Open file handle in binary write mode
        frame : Frame
            Frame object containing coordinates and cell parameters
        """
        # Write unit cell parameters
        cp = frame.cell_parameters
        cell_array = [cp.a, cp.gamma, cp.b, cp.beta, cp.alpha, cp.c]
        self._write_fortran_record(fout, [cell_array], '6d ')

        # Write coordinates by component (x, y, z)
        coords = frame.coordinates
        ffmt = f'{len(coords)}f '
        
        for i in range(3):
            coord_component = coords[:, i].tolist()
            self._write_fortran_record(fout, [coord_component], ffmt)

    def _write_fortran_record(self, fout, data, format_string: str) -> None:
        """
        Write a Fortran-style record to binary file.

        Parameters
        ----------
        fout : file object
            Open file handle in binary write mode
        data : list
            Data to write
        format_string : str
            Format string specifying data types
        """
        # Calculate record length
        format_string = format_string.replace(',', ' ')
        record_length = struct.calcsize(format_string)

        # Determine endianness prefix
        prefix = '<' if not self._is_big_endian else '>'
        
        # Write record length marker
        fout.write(struct.pack(f'{prefix}i', record_length))

        # Write data according to format
        if len(data) == 1 and hasattr(data[0], "__len__"):
            fmt = prefix + format_string.strip()
            fout.write(struct.pack(fmt, *data[0]))
        else:
            fmt = prefix + format_string.strip()
            fout.write(struct.pack(fmt, *data))

        # Write closing record length marker
        fout.write(struct.pack(f'{prefix}i', record_length))

    def close(self) -> None:
        """Close the trajectory file if open."""
        if self._file and not self._file.closed:
            self._file.close()

    def get_volume_statistics(self) -> Dict[str, float]:
        """
        Calculate volume statistics across all frames.

        Returns
        -------
        Dict[str, float]
            Dictionary containing volume statistics
        """
        volumes = [frame.get_volume() for frame in self._frames]
        return {
            'mean': float(np.mean(volumes)),
            'std': float(np.std(volumes)),
            'min': float(np.min(volumes)),
            'max': float(np.max(volumes)),
            'volumes': volumes
        }

    def print_statistics(self) -> None:
        """Print trajectory statistics."""
        print("\n=== Trajectory Statistics ===")
        print(f"Number of frames: {self.number_of_frames}")
        print(f"Number of atoms: {self.number_of_atoms}")

        vol_stats = self.get_volume_statistics()
        print("\nVolume Statistics:")
        print(f"Average volume: {vol_stats['mean']:.2f}")
        print(f"Standard deviation: {vol_stats['std']:.2f}")
        print(f"Minimum volume: {vol_stats['min']:.2f}")
        print(f"Maximum volume: {vol_stats['max']:.2f}")

    def _check_endianness(self) -> bool:
        """
        Check if the file is in little-endian format.

        Returns
        -------
        bool
            True if file is little-endian, False otherwise
        """
        # Read first 4-byte integer value to check endianness
        with open(self._filepath, "rb") as f:
            value = struct.unpack("<i", f.read(4))[0]
        return value == self.EXPECTED_ENDIAN_VALUE

    # end of _check_endianness()

    def _read_header(self) -> None:
        """
        Read and process the DCD file header.

        Reads file metadata including:
        - File endianness
        - Title information
        - Number of atoms
        - System parameters

        Raises
        ------
        RuntimeError
            If file not opened
        ValueError
            If header data is invalid
        """
        if self._file is None:
            raise RuntimeError("File not opened")

        # Determine endianness and reset file position
        self._is_big_endian = not self._check_endianness()
        self._file.seek(0)

        # Set endian prefix for struct operations
        endian_prefix = self.BIG_ENDIAN_PREFIX if self._is_big_endian else self.LITTLE_ENDIAN_PREFIX

        # Skip first record (we'll read details later if needed)
        record_length = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]
        self._file.seek(record_length + 4, os.SEEK_CUR)

        # Read title record
        record_length = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]
        num_titles = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]

        # Validate number of titles
        if num_titles < 0:
            raise ValueError(f"Invalid number of titles: {num_titles}")

        # Read title content
        titles = []
        for _ in range(num_titles):
            title_data = self._file.read(self.TITLE_LINE_SIZE)
            title = struct.unpack(f"{endian_prefix}{self.TITLE_LINE_SIZE}s", title_data)[0]
            titles.append(title.decode('utf-8', errors='ignore').strip())

        # Skip end marker
        _ = self._file.read(4)

        # Read number of atoms
        _ = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]  # record marker
        self._number_of_atoms = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]
        _ = self._file.read(4)  # end marker

        # Validate number of atoms
        if self._number_of_atoms <= 0:
            raise ValueError(f"Invalid number of atoms: {self._number_of_atoms}")

        # Print debug info
        print(f"Reading DCD file with {self._number_of_atoms} atoms")
        if titles:
            print("DCD Titles:")
            for title in titles:
                print(f"  {title}")

    def _read_frames(self) -> None:
        """
        Read all frames from the file.

        Processes frame data including:
        - Cell parameters
        - Atomic coordinates
        - Frame metadata

        Raises
        ------
        RuntimeError
            If file not opened
        struct.error
            If binary data cannot be unpacked
        """
        if self._file is None:
            raise RuntimeError("File not opened")

        # Set endian prefix for struct operations
        endian_prefix = self.BIG_ENDIAN_PREFIX if self._is_big_endian else self.LITTLE_ENDIAN_PREFIX

        # Calculate file positions
        current_pos = self._file.tell()
        self._file.seek(0, os.SEEK_END)
        file_size = self._file.tell()
        self._file.seek(current_pos)

        # Calculate frame size and number
        frame_size = self._calculate_frame_size()
        remaining_size = file_size - current_pos
        num_frames = remaining_size // frame_size

        # Read each frame
        for _ in range(num_frames):
            try:
                # Read frame data
                cell_params = self._read_cell_parameters()
                coords = self._read_coordinates()
                
                # Create and store frame object
                self._frames.append(Frame.from_coordinates(
                    cell_params,
                    coords,
                    frame_type='.dcd'
                ))
            except struct.error as e:
                print(f"Error reading frame: {e}")
                break

    def _read_cell_parameters(self) -> CellParameters:
        """
        Read cell parameters from current file position.

        Returns
        -------
        CellParameters
            Cell parameters for current frame
        """
        endian_prefix = self.BIG_ENDIAN_PREFIX if self._is_big_endian else self.LITTLE_ENDIAN_PREFIX

        # Read record markers and data
        _ = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]
        raw_params = struct.unpack(f"{endian_prefix}6d", self._file.read(6 * 8))
        _ = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]

        # Unpack parameters
        a, gamma, b, beta, alpha, c = raw_params

        # Handle zero angles (set to 90 degrees)
        alpha = 90.0 if alpha == 0.0 else alpha
        beta = 90.0 if beta == 0.0 else beta
        gamma = 90.0 if gamma == 0.0 else gamma

        return CellParameters(a, b, c, alpha, beta, gamma)

    def _read_coordinates(self) -> npt.NDArray:
        """
        Read coordinates for current frame.

        Returns
        -------
        npt.NDArray
            Nx3 array of atomic coordinates
        """
        endian_prefix = self.BIG_ENDIAN_PREFIX if self._is_big_endian else self.LITTLE_ENDIAN_PREFIX
        coords = np.zeros((self._number_of_atoms, 3))

        # Read x, y, z components
        for i in range(3):
            # Read record marker
            _ = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]
            
            # Read coordinate data
            coord_data = self._file.read(4 * self._number_of_atoms)
            coords[:, i] = np.frombuffer(coord_data, dtype=f'{endian_prefix}f')
            
            # Read end marker
            _ = struct.unpack(f"{endian_prefix}i", self._file.read(4))[0]

        return coords

    def _calculate_frame_size(self) -> int:
        """
        Calculate the size of each frame in bytes.

        Returns
        -------
        int
            Size of frame in bytes
        """
        return (
            56 +  # Cell parameters (48 bytes + 8 bytes record markers)
            3 * (self._number_of_atoms * 4 + 8)  # Coordinates (3 dimensions * (4 bytes per float + 8 bytes record markers))
        )

# end of DCDTrajectory class
