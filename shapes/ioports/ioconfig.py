"""
.. module:: ioconfig
   :platform: Linux - tested, Windows (WSL Ubuntu) - tested
   :synopsis: provides classes for DL_POLY/DL_MESO CONFIG input/output

.. moduleauthor:: Dr Michael Seaton <michael.seaton[@]stfc.ac.uk>

The module contains class CONFIGFile(ioFile)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: Dr Michael Seaton (c) 2024           #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (DL_POLY / DL_MESO DPD workflows)     #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:

from shapes.basics.globals import *
from shapes.ioports.iofiles import ioFile
#from shapes.stage.protospecies import Atom, Molecule
#from shapes.stage.protospecies import MoleculeSet as MolSet
from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
from shapes.stage.protomoleculeset import MoleculeSet as MolSet


class CONFIGFile(ioFile):

    """
    Class **CONFIGFile(ioFile)** abstracts I/O operations on DL_POLY/DL_MESO CONFIG files.

    Parameters
    ----------
    fname : string
        Full name of the file, possibly including the path to it
    fmode : string
        Mode for file operations, must be in ['r','w','a']
    try_open : boolean
        Flag to open the file upon creating the file object
    """

    def __init__(self, *args, **keys):
        super(CONFIGFile, self).__init__( *args, **keys)
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''
        if self._fmode not in ['r','w','a']:
            print(f"{self.__class__.__name__}.readInMols(): Oops! Unknown mode '{self._fmode}' "
                  f"for file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

    #end of __init__()


    def readInMols(self, rems=[], mols=[], box=[], mol_name=(), lenscale=0.1):
        # note lenscale is scaling factor to convert distances to nm 
        # (set to 0.1 by default for DL_POLY to convert from angstroms,
        # can be any value set by user for DL_MESO to use its arbitrary length units)
        if not self.is_open():
            self.open(fmode='r')
            print(f"{self.__class__.__name__}.readInMols(): Ready for reading CONFIG file '{self._fname}' ...")
        if not self.is_rmode():
            print(f"{self.__class__.__name__}.readInMols(): Oops! Wrong mode '{self._fmode}' "
                  f"(file in rmode = {self._is_rmode}) for reading file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)


        print(f"{self.__class__.__name__}.readInMols(): Reading CONFIG file '{self._fname}' "
              f"from line # {str(self._lnum)} (file is_open = {self.is_open()})...")

        # first line is simulation name (remark) to identify file contents

        if self._lnum == 0:
            line = self._fio.readline().strip()
            self._remark = line
            self._lnum += 1
            print(f"{self.__class__.__name__}.readInMols(): CONFIG title: '{self._remark}'\n")
            rems.append(line)


        # the second line gives information level and 
        # boundary conditions (box shape): it might
        # also give number of particles

        ierr = 0
        natms = 0
        levcfg = 0
        incom = 0

        line = self._fio.readline()
        self._lnum += 1

        words = line.replace(',',' ').replace('\t',' ').lower().split()
        if len(words)>1:
            levcfg = int(words[0])
            incom = int(words[1])
            if len(words)>2:
                natms = int(words[2])
            else:
                print(f"\n{self.__class__.__name__}.readInMols(): "
                    f"no number of atoms/particles found - FULL STOP!!!")
                sys.exit(2)

        else:
            print(f"\n{self.__class__.__name__}.readInMols(): "
                f"no information level or boundary conditions found - FULL STOP!!!")
            sys.exit(2)

        # if boundary condition != 0, next three lines
        # give box dimensions as vectors - assume
        # orthorhombic boundary conditions apply and
        # ignore tilt if using parallelpiped
            
        if incom !=0:
            line = self._fio.readline()
            words = line.replace(',',' ').replace('\t',' ').lower().split()
            box.append(float(words[0])*lenscale)
            self._lnum +=1
            line = self._fio.readline()
            words = line.replace(',',' ').replace('\t',' ').lower().split()
            box.append(float(words[1])*lenscale)
            self._lnum +=1
            line = self._fio.readline()
            words = line.replace(',',' ').replace('\t',' ').lower().split()
            box.append(float(words[2])*lenscale)
            self._lnum +=1
  
        # read in particle data: name (type) based on input (first name only), 
        # index and position: keep track of maximum extent of positions for 
        # box size if needed and check number of available atoms 
        # (against value read in header)

        print(f"{self.__class__.__name__}.readInMols(): Adding new molecular species "
             f"- {len(mols)}, mspec = 0, nmols = 0")
        mols.append(MolSet(1, 0, sname=mol_name[0], stype='input'))
        mols[0].addItem(Molecule(1, mol_name[0], 'input'))

        min_x = max_x = min_y = max_y = min_z = max_z = 0.0
        matms = 0
        for i in range(natms):
            line = self._fio.readline().rstrip()
            if not line or len(line.split())<2:
                break
            self._lnum += 1
            words = line.replace(',',' ').replace('\t',' ').split()
            name = words[0]
            gindex = int(words[1])
            line = self._fio.readline().rstrip()
            if not line or len(line.split())<3:
                break
            self._lnum += 1
            words = line.replace(',',' ').replace('\t',' ').lower().split()
            min_x = min(min_x, float(words[0])*lenscale)
            max_x = max(max_x, float(words[0])*lenscale) 
            min_y = min(min_y, float(words[1])*lenscale)
            max_y = max(max_y, float(words[1])*lenscale) 
            min_z = min(min_z, float(words[2])*lenscale)
            max_z = max(max_z, float(words[2])*lenscale) 
            mols[0].items[0].addItem(Atom(name, mol_name[0], aindx=gindex, arvec=[float(words[0])*lenscale, float(words[1])*lenscale, float(words[2])*lenscale]))
            print(f"{self.__class__.__name__}.readInMols(): "
                              f"{mols[0].items[0].items[len(mols[0].items[0].items)-1]}")
            matms +=1
            # skip past velocities and forces if supplied in CONFIG file
            if levcfg>0:
                line = self._fio.readline().rstrip()
                if not line:
                    break
                self._lnum +=1
            if levcfg>1:
                line = self._fio.readline().rstrip()
                if not line:
                    break
                self._lnum +=1

        print(f"{self.__class__.__name__}.readInMols(): In total 1 '{mol_name[0]}' "
                f"of {matms} atom(s) found ... ")
        
        # check number of atoms/particles read in against value obtained in header
        
        if matms != natms:
            print(f"\n{self.__class__.__name__}.readInMols(): Total number of atoms: {matms} =/= {natms} number of atoms kept...")
            ierr = 1

        # if boundary condition = 0, set box dimensions using maximum particle position extents
        
        if incom==0:
            box.append(2.0*max(max_x, abs(min_x)))
            box.append(2.0*max(max_y, abs(min_y)))
            box.append(2.0*max(max_z, abs(min_z)))
            
        # error checking and message if completed successfully

        if ierr == 0:            
            print(f"\n{self.__class__.__name__}.readInMols(): Read-in Mmols = 1, "
                      f"Matms = {str(matms)}, MolNames = {mol_name[0]}")            
            print(f"{self.__class__.__name__}.readInMols(): File '{self._fname}' successfully read: "
                  f"lines = {str(self._lnum)} & natms = {str(natms)}\n")

        return (ierr == 0)

    def writeOutMols(self, rem, gbox, mols):
    ### NEED TO REFACTOR - ??? ###
        if not self._is_open:
            self.open(fmode='w')
            print(f"{self.__class__.__name__}.writeOutMols(): Ready for writing CONFIG file '{self._fname}' ...")
        if not self._is_wmode:
            print(f"{self.__class__.__name__}.writeOutMols(): Oops! Wrong mode '{self._fmode}' "
                  f"for writing file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

        print(f"{self.__class__.__name__}.writeOutMols(): Writing CONFIG file '{self._fname}' ...")

        imols = len(mols)                    # mnmol[0] - number of molecule types / sets of unique molecules
        mmols = sum(len(ms) for ms in mols)  # mnmol[len(mnmol) - 1] - total number of molecules in all sets

        nout = 0  # total number of atoms to output
        for mts in mols:
            for mol in mts:
                nout += len(mol)

        ierr  = 0
        natms  = 0
        #resid  = 0

        # write simulation title (remark)

        self._fio.write('{0:80s}\n'.format(rem))

        # write information level (fixed to positions only), boundary condition 
        # (set to orthorhombic box) and number of particles

        self._fio.write('0\t\t\t2\t\t\t{0:d}\n'.format(nout))
                        
        # write simulation box size as unit vectors

        self._fio.write('{0:16.8f}{1:16.8f}{2:16.8f}\n'.format(gbox[0], 0.0, 0.0))
        self._fio.write('{0:16.8f}{1:16.8f}{2:16.8f}\n'.format(0.0, gbox[1], 0.0))
        self._fio.write('{0:16.8f}{1:16.8f}{2:16.8f}\n'.format(0.0, 0.0, gbox[2]))

        nlines = 5

        # loop through molecule types to add to file
        
        is_fin = False
        resid = 0
        for m in range(imols):
            #matms = len(mols[m][0])  # mnatm[m] - number of atoms per molecule of type m
            nmols = len(mols[m])     # int(len(atms[m]) / matms) - number of molecules of type / in set m
            for k in range(nmols):
                resnm = mols[m][k].name  # molnm[m] - name of molecules of type m / in set m
                resid += 1
                if resid == 1:
                    print(f"{self.__class__.__name__}.writeOutMols(): Writing molecule {resid} ('{resnm}') "
                          f"into CONFIG file '{self._fname}' ...")
                elif resid < 10 or (resid < 100 and resid % 10 == 0) or \
                    (resid < 1000 and resid % 100 == 0) or (resid % 1000 == 0) or resid == mmols:
                    print(f"{self.__class__.__name__}.writeOutMols(): Appending molecule {resid} ('{resnm}') "
                          f"to file '" + self._fname + "' ...")

                matms = len(mols[m][k])  # mnatm[m] - number of atoms in molecule
                for i in range(matms):
                    nlines += 2
                    natms += 1
                    rvec = [0.0 if abs(elem)<TINY else elem for elem in mols[m][k][i].rvec] # rounds tiny negative coordinates up to zero: avoids printing -0.0
                    line = '{0:8s}        {1:d}\n'.format(mols[m][k][i].name, natms)
                    self._fio.write(line)
                    line = '{0:16.8f}{1:16.8f}{2:16.8f}\n'.format(*rvec)
                    self._fio.write(line)
        is_fin = True

        if ierr == 0 and is_fin:
            print(f"{self.__class__.__name__}.writeOutMols(): File '{self._fname}' "
                  f"successfully written: lines = {str(nlines)} : n_mols = {str(nmols)} "
                  f"& n_atms = {str(natms)} / {str(nout)}\n")
        return (ierr == 0)

    #end of writeOutMols()


    def close(self):
        super(CONFIGFile, self).close()
        #self._fio.close()
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''

    def __del__(self):
        #super(ioFile, self).__del__()
        self.close()

