"""
.. module:: ioframe
   :platform: Linux - tested, Windows (WSL Ubuntu) - NOT TESTED
   :synopsis: Trajectory and Frame classes for molecular dynamics data handling

.. moduleauthor:: Saul Beck <saul.beck[@]stfc.ac.uk>

The module contains classes Frame(ABC) and DCDFrame 
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: Dr Saul Beck (c) 2024                #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#          (DCD file IO and relevant tests)      #
#                                                #
##################################################


from abc import ABC, abstractmethod
import numpy as np
import numpy.typing as npt
from dataclasses import dataclass
from typing import Dict, Any, Union, Type, List, Optional
import numpy as np

@dataclass
class CellParameters:
    """Container for cell parameters.

    Parameters
    ----------
    a : float
        Length of first cell vector
    b : float
        Length of second cell vector  
    c : float
        Length of third cell vector
    alpha : float
        Angle between b and c vectors (degrees)
    beta : float
        Angle between a and c vectors (degrees)
    gamma : float
        Angle between a and b vectors (degrees)
    """
    a: float
    b: float
    c: float
    alpha: float
    beta: float  
    gamma: float

    def validate(self) -> None:
        """
        Validate cell parameters.

        Raises
        ------
        ValueError
            If any parameters are invalid
        """
        # Cell lengths must be positive
        if any(param <= 0 for param in [self.a, self.b, self.c]):
            raise ValueError("Cell lengths must be positive")

        # Angles must be between 0 and 180 degrees 
        if any(not 0 <= angle <= 180 for angle in [self.alpha, self.beta, self.gamma]):
            raise ValueError("Angles must be between 0 and 180 degrees")

    def to_dict(self) -> Dict[str, float]:
        """
        Convert to dictionary representation.
        
        Returns
        -------
        Dict[str, float]
            Dictionary containing cell parameters
        """
        return {
            'a': self.a,
            'b': self.b, 
            'c': self.c,
            'alpha': self.alpha,
            'beta': self.beta,
            'gamma': self.gamma
        }

    @classmethod 
    def from_dict(cls, data: Dict[str, float]) -> 'CellParameters':
        """
        Create from dictionary representation.
        
        Parameters
        ----------
        data : Dict[str, float]
            Dictionary containing cell parameters
            
        Returns
        -------
        CellParameters
            New cell parameters instance
        """
        return cls(**data)

# end of CellParameters

class Frame(ABC):
    """
    Abstract base class for trajectory frames.

    This class defines the interface that specific frame implementations must follow.
    The interface includes methods for handling coordinates, cell parameters, and frame metadata.
    """

    _frame_types = {}  # Registry for frame implementations 

    @classmethod
    def register_frame_type(cls, extension: str):
        """
        Decorator to register frame implementations.

        Parameters
        ----------
        extension : str
            File extension to associate with this frame type
        """
        def decorator(frame_class: Type['Frame']):
            cls._frame_types[extension.lower()] = frame_class
            return frame_class
        return decorator

    @classmethod
    def from_coordinates(cls,
                       cell_parameters: CellParameters,
                       coordinates: npt.NDArray,
                       frame_type: Optional[str] = None) -> 'Frame':
        """
        Factory method to create frames from coordinates.

        Parameters
        ----------
        cell_parameters : CellParameters
            Unit cell parameters
        coordinates : npt.NDArray
            Atomic coordinates (Nx3 array)
        frame_type : Optional[str]
            Frame type to create (e.g., '.dcd'). If None, uses default.

        Returns
        -------
        Frame
            Frame instance of appropriate type

        Raises
        ------
        ValueError
            If frame type not supported or coordinates invalid
        """
        # Validate input
        if not isinstance(cell_parameters, CellParameters):
            raise ValueError("cell_parameters must be a CellParameters instance") 

        if not isinstance(coordinates, np.ndarray):
            raise ValueError("coordinates must be a numpy array")

        if coordinates.ndim != 2 or coordinates.shape[1] != 3:
            raise ValueError("coordinates must be an Nx3 array")

        # Determine frame class to use
        if frame_type is None:
            frame_class = cls._frame_types.get('default', DCDFrame)
        else:
            frame_class = cls._frame_types.get(frame_type.lower())
            if frame_class is None:
                supported = ", ".join(cls._frame_types.keys())
                raise ValueError(
                    f"Unsupported frame type: {frame_type}. "
                    f"Supported types are: {supported}"
                )

        return frame_class(cell_parameters, coordinates)

    @abstractmethod
    def __init__(self, cell_parameters: CellParameters, coordinates: npt.NDArray):
        """
        Initialize frame.

        Parameters
        ----------
        cell_parameters : CellParameters
            Unit cell parameters
        coordinates : npt.NDArray
            Atomic coordinates (Nx3 array)
        """
        pass

    @property
    @abstractmethod
    def cell_parameters(self) -> CellParameters:
        """Get cell parameters."""
        pass

    @property
    @abstractmethod
    def coordinates(self) -> npt.NDArray:
        """Get atomic coordinates."""
        pass

    @abstractmethod
    def get_volume(self) -> float:
        """
        Calculate unit cell volume.

        Returns
        -------
        float
            Cell volume
        """
        pass

    @abstractmethod
    def get_coordinate_bounds(self) -> Dict[str, float]:
        """
        Get coordinate bounds in each dimension.

        Returns
        -------
        Dict[str, float]
            Min/max coordinates in each dimension
        """
        pass

    @abstractmethod
    def print_frame_details(self) -> None:
        """Print detailed frame information."""
        pass

    @abstractmethod
    def to_dict(self) -> Dict[str, Any]:
        """
        Convert frame to dictionary representation.

        Returns
        -------
        Dict[str, Any]
            Dictionary representation of frame
        """
        pass

    @classmethod
    @abstractmethod
    def from_dict(cls, data: Dict[str, Any]) -> 'Frame':
        """
        Create frame from dictionary representation.

        Parameters
        ----------
        data : Dict[str, Any]
            Dictionary representation of frame

        Returns
        -------
        Frame
            New frame instance
        """
        pass

# end of Frame class

@Frame.register_frame_type('.dcd')
class DCDFrame(Frame):
    """DCD-specific frame implementation."""

    def __init__(self, cell_parameters: CellParameters, coordinates: npt.NDArray):
        """
        Initialize DCD frame.

        Parameters
        ----------
        cell_parameters : CellParameters
            Unit cell parameters
        coordinates : npt.NDArray
            Atomic coordinates (Nx3 array)

        Raises
        ------
        ValueError
            If input validation fails
        """
        # Validate cell parameters
        if not isinstance(cell_parameters, CellParameters):
            raise ValueError("cell_parameters must be a CellParameters instance")
        cell_parameters.validate()

        # Validate coordinates
        if not isinstance(coordinates, np.ndarray):
            raise ValueError("coordinates must be a numpy array")
        if coordinates.ndim != 2 or coordinates.shape[1] != 3:
            raise ValueError("coordinates must be an Nx3 array")

        self._cell_parameters = cell_parameters
        self._coordinates = coordinates.copy()  # Make a copy to prevent modification

    @property
    def cell_parameters(self) -> CellParameters:
        """Get cell parameters."""
        return self._cell_parameters

    @property
    def coordinates(self) -> npt.NDArray:
        """Get atomic coordinates."""
        return self._coordinates.copy()  # Return a copy to prevent modification

    def get_volume(self) -> float:
        """
        Calculate the volume using the triclinic cell formula.
        
        Returns
        -------
        float
            Cell volume
        """
        cp = self._cell_parameters
        volume = (
                cp.a * cp.b * cp.c *
                np.sqrt(
                    1 - np.cos(np.radians(cp.alpha)) ** 2 -
                    np.cos(np.radians(cp.beta)) ** 2 -
                    np.cos(np.radians(cp.gamma)) ** 2 +
                    2 * np.cos(np.radians(cp.alpha)) *
                    np.cos(np.radians(cp.beta)) *
                    np.cos(np.radians(cp.gamma))
                )
        )
        return float(volume)  # Ensure float output

    def get_coordinate_bounds(self) -> Dict[str, float]:
        """
        Get coordinate bounds in each dimension.
        
        Returns
        -------
        Dict[str, float]
            Min/max coordinates in each dimension
        """
        coords_min = np.min(self._coordinates, axis=0)
        coords_max = np.max(self._coordinates, axis=0)
        return {
            'x_min': float(coords_min[0]),
            'y_min': float(coords_min[1]),
            'z_min': float(coords_min[2]),
            'x_max': float(coords_max[0]),
            'y_max': float(coords_max[1]),
            'z_max': float(coords_max[2])
        }

    def print_frame_details(self) -> None:
        """Print detailed frame information."""
        print("\n=== Frame Details ===")
        
        # Cell parameters
        cp = self._cell_parameters
        print("\nUnit Cell Parameters:")
        print(f"a = {cp.a:.3f}")
        print(f"b = {cp.b:.3f}")
        print(f"c = {cp.c:.3f}")
        print(f"α = {cp.alpha:.2f}")
        print(f"β = {cp.beta:.2f}")
        print(f"γ = {cp.gamma:.2f}")

        # Volume
        print(f"\nBox volume: {self.get_volume():.2f}")

        # Coordinate bounds
        bounds = self.get_coordinate_bounds()
        print("\nCoordinate Statistics:")
        print(f"X range: {bounds['x_min']:.3f} to {bounds['x_max']:.3f}")
        print(f"Y range: {bounds['y_min']:.3f} to {bounds['y_max']:.3f}")
        print(f"Z range: {bounds['z_min']:.3f} to {bounds['z_max']:.3f}")

        # Basic coordinate statistics
        print(f"\nNumber of atoms: {len(self._coordinates)}")

    def to_dict(self) -> Dict[str, Any]:
        """
        Convert frame to dictionary representation.
        
        Returns
        -------
        Dict[str, Any]
            Dictionary containing cell parameters and coordinates
        """
        return {
            'cell_parameters': self._cell_parameters.to_dict(),
            'coordinates': self._coordinates.tolist()
        }

    @classmethod
    def from_dict(cls, data: Dict[str, Any]) -> 'DCDFrame':
        """
        Create frame from dictionary representation.
        
        Parameters
        ----------
        data : Dict[str, Any]
            Dictionary containing cell parameters and coordinates
            
        Returns
        -------
        DCDFrame
            New frame instance
        """
        cell_params = CellParameters.from_dict(data['cell_parameters'])
        coordinates = np.array(data['coordinates'])
        return cls(cell_params, coordinates)

    def __repr__(self) -> str:
        """
        String representation of frame.
        
        Returns
        -------
        str
            Frame information
        """
        return (f"DCDFrame(atoms={len(self._coordinates)}, "
                f"volume={self.get_volume():.2f})")
                
# end of DCDFrame class
