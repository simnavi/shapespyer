"""
.. module:: iogro
   :platform: Linux - tested, Windows (WSL Ubuntu) - tested
   :synopsis: provides classes for Gromacs oriented input/output

.. moduleauthor:: Dr Andrey Brukhno <andrey.brukhno[@]stfc.ac.uk>

The module contains class groFile(ioFile)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:
# AB: This is an insightful comment describing a crucial or difficult to follow piece of code

#import os
#import sys
#import time

from shapes.basics.globals import *
from shapes.ioports.iofiles import ioFile
from shapes.stage.protoatom import Atom
from shapes.stage.protomolecule import Molecule
from shapes.stage.protomoleculeset import MoleculeSet as MolSet
#from shapes.stage.protomolecularsystem import MolecularSystem as MolSys


class groFile(ioFile):

    """
    Class **groFile(ioFile)** abstracts I/O operations on Gromacs files.

    Parameters
    ----------
    fname : string
        Full name of the file, possibly including the path to it
    fmode : string
        Mode for file operations, must be in ['r','w','a']
    try_open : boolean
        Flag to open the file upon creating the file object
    """

    #def __init__(self, fname: str, fmode='r', try_open=False):
    def __init__(self, *args, **keys):
        super(groFile, self).__init__( *args, **keys)
        #self._fio = None
        #self._is_open = False
        #self._is_rmode = False
        #self._is_wmode = False
        #self._is_amode = False
        # self._fname = fname
        # self._name, self._ext = os.path.splitext(os.path.basename(fname))
        if self._ext != '.gro':
            print("{self.__class__.__name__}:: Wrong extension '" +
                  self._ext + "' for GRO file '" + self._fname + "' - FULL STOP!!!")
            sys.exit(1)
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''
        #self._fmode = fmode
        #if fmode == 'r':
        #    self._is_rmode = True
        #elif fmode == 'w':
        #    self._is_wmode = True
        #elif fmode == 'a':
        #    self._is_amode = True
        #else:
        #    print("Oops! Unknown mode '" + self._fmode + "' for file '" + self._fname + "' - FULL STOP!!!")
        #    sys.exit(1)
        #if try_open:
        #    self.open()
        if self._fmode not in ['r','w','a']:
            print(f"{self.__class__.__name__}.readInMols(): Oops! Unknown mode '{self._fmode}' "
                  f"for file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

    #end of __init__()


#    def open(self):
#        if fmode not in ['r','w','a']:
#            print(f"{self.__class__.__name__}:: Oops! Unknown mode '{fmode}' "
#                  f"while trying to open file '{self._fname}' - FULL STOP!!!")
#            sys.exit(1)
#        if self._is_open:
#            print(f"{self.__class__.__name__}:: Warning: Reopening I/O file '{self._fname}' "
#                  f"in '{fmode}' mode ...")
#            self.close()
#        try:
#            self._fio = open(self._fname, self._fmode, encoding='utf-8')
#        except (IOError, EOFError) as err:
#            print("Oops! Could not open file '" + self._fname + "' in mode '" + self._fmode +
#                  "' - FULL STOP!!!\n")
#            sys.exit(2)
#        except:
#            print("Oops! Unknown error while opening file '" + self._fname + "' in mode '" + self._fmode +
#                  "' - FULL STOP!!!\n")
#            sys.exit(3)
#        self._is_open = True
#
#    #end of open()


    def readInMols(self, rems=[], mols=[], box=[], resnames=(), resids=()):
        if not self.is_open():
            self.open(fmode='r')
            print(f"{self.__class__.__name__}.readInMols(): Ready for reading GRO file '{self._fname}' ...")
        if not self.is_rmode():
            print(f"{self.__class__.__name__}.readInMols(): Oops! Wrong mode '{self._fmode}' "
                  f"(file in rmode = {self._is_rmode}) for reading file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

        print(f"{self.__class__.__name__}.readInMols(): Reading GRO file '{self._fname}' "
              f"from line # {str(self._lnum)} (file is_open = {self.is_open()})...")

        if self._lnum == 0:
            line = self._fio.readline().strip()
            self._remark = line
            self._lnum += 1
            print(f"{self.__class__.__name__}.readInMols(): GRO title: '{self._remark}'\n")
            rems.append(line)
        line = self._fio.readline().strip()
        self._lnum += 1
        control = line.split()
        matms = int(control[0])

        ierr  = 0
        nout  = 1
        nrems = 1
        natms = 0
        mmols = 0
        nmols = 0
        mspec = 0
        resip = 0
        resix = 0
        resnp = "none"
        resnm = "none"

        resname = resnames[0]
        resid   = resids[0]

        mres   = len(resnames)
        mids   = len(resids)
        mfound = 0

        # AB: helper lists for molecular species
        # previously declared in globals.py, now only used locally here, in iogro.readInMols()
        mnatm = []  # helper list of atom numbers per molecule
        molnm = []  # helper list of molecule names
        mnmol = []  # helper list of molecular species to be read in

        # arrange for abnormal EOF handling
        if self._lnum == nrems + 1:
            is_molin = False
            mlast = 0
            for i in range(matms):
                # do not lstrip here - relaying on field widths in GRO files!
                line = self._fio.readline().rstrip()
                if not line:  # or len(line.split())!=4 :
                    break
                self._lnum += 1

                resip = resix
                resix = int(line[0:5].lstrip().rstrip())
                resnp = resnm
                resnm = line[5:10].lstrip().rstrip()
                is_another = (resnm != resnp)

                # print(f"{self.__class__.__name__}.readInMols(): Read-in resix = "+str(resix)+", resnm = "+resnm+"\n")

                if is_another or resix != resip:
                #if resnm != resnp or resix != resip:
                    # another molecule / residue found in input
                    #is_molin = ((resnm in resnames or resname == 'ANY') or resix in resids) \
                    is_molin = resnm in resnames or resname == 'ALL' or \
                               (resname == 'ANY' and resix in resids)

                    if is_molin:
                        if is_another:
                        #AB: a species with a name different from the previous one

                            if nmols > 0:
                                print(f"{self.__class__.__name__}.readInMols(): In total {nmols+1} '{resnp}' "
                                      f"molecule(s) of {natms} atom(s) found ... ")

                            if len(molnm) > 0 and resnm in molnm:
                                # AB: a previously encountered molecular species
                                mspec = molnm.index(resnm)
                                nmols = mnmol[mspec]
                                print(f"{self.__class__.__name__}.readInMols(): It's not a new species "
                                      f"- {len(mols)}, mspec = {mspec}, nmols = {nmols}")
                            else:
                            #AB: another molecular species encountered
                                print(f"{self.__class__.__name__}.readInMols(): Adding new molecular species "
                                      f"- {len(mols)}, mspec = {mspec}, nmols = {nmols}")
                                molnm.append(resnm)
                                if natms > 0:
                                    mnatm.append(natms)
                                #if nmols > 0:
                                mnmol.append(nmols)
                                nmols = 0
                                mspec += 1
                                mfound += 1
                                mols.append(MolSet(mspec, 0, sname=resnm, stype='input'))
                        else:
                            nmols += 1
                        mols[mspec-1].addItem(Molecule(nmols, resnm, 'input'))
                        mlast  = len(mols[mspec-1].items)-1
                        mmols += 1

                    natms = 0

                if is_molin:
                    natms += 1

                    atmix = int(line[15:20].lstrip().rstrip())

                    # print(f"{self.__class__.__name__}.readInMols(): Read-in resix = "+str(resix)+", resnm = "+resnm+", atnm = "+atms[i]+ \
                    #      ", atmid = "+str(atmix)+"\n")

                    latm = line[20:].split()

                    # print(f"{self.__class__.__name__}.readInMols(): Read-in resix = "+str(resix)+", resnm = "+resnm+", atnm = "+atms[i]+ \
                    #      ", coords = "+'({:>8.3f},{:>8.3f},{:>8.3f})'.format(*axyz[i])+"\n")

                    atype = line[5:10].strip()
                    #aname, aindx = line[10:20].strip().split()
                    aname = line[10:15].strip()
                    aindx = line[15:20].strip()

                    mols[mspec-1].items[mlast].addItem(Atom(aname, atype, aindx=natms,
                                                           arvec=[float(latm[0]), float(latm[1]), float(latm[2])]))
                    if nmols <= nout:
                        print(f"{self.__class__.__name__}.readInMols(): "
                              f"{mols[mspec-1].items[mlast].items[len(mols[mspec-1].items[mlast].items)-1]}")
                    elif nmols == nout+1 and natms < 2:
                        print(f"{self.__class__.__name__}.readInMols(): "
                              f"More than {nout} '{resnm}' molecule(s) ... ")

            if mmols > 0:
                mnmol[0] =  sum(mnmol[1:])
                if natms > 0 :
                    mnatm.append(natms)
                    print(f"{self.__class__.__name__}.readInMols(): In total {nmols+1} '{resnp}' "
                          f"of {natms} atom(s) found ... ")
                natms = sum(mnatm)

                print(f"\n{self.__class__.__name__}.readInMols(): Read-in Mmols = {str(mmols)}, "
                      f"Matms = {str(mnatm)}, MolNames = {str(molnm)}")
            else:
                print(f"\n{self.__class__.__name__}.readInMols(): Read-in Mmols = {str(mmols)}, "
                      f"no molecule '{resname}' with resid in {str(resids)} found - FULL STOP!!!")
                sys.exit(2)

            line = self._fio.readline().rstrip()
            lbox = line.split()
            box.append(float(lbox[0]))
            box.append(float(lbox[1]))
            box.append(float(lbox[2]))

            # arrange for abnormal EOF handling
            if self._lnum != nrems + matms + 1:
                ierr = 1
                print(f"\n{self.__class__.__name__}.readInMols(): Oops! Unexpected EOF or format in '{self._fname}' "
                      f"(line {str(self._lnum + 1)}) - FULL STOP!!!")
                sys.exit(4)
        else:  # self._lnum != nrems+1
            ierr = 1
            print(f"\n{self.__class__.__name__}.readInMols(): Oops! Unexpected EOF or empty line in '{self._fname}' "
                  f"(line {str(self._lnum + 1)}) - FULL STOP!!!")
            sys.exit(4)

        natms = len([a for mset in mols  for mol in mset.items for a in mol.items])
        if matms != natms:
            print(f"\n{self.__class__.__name__}.readInMols(): Total number of atoms: {matms} =/= {natms} number of atoms kept...")
            #print(f"\n{self.__class__.__name__}.readInMols(): Oops! Inconsistent number of atoms: {matms} =/= {natms}"
            #      f" - FULL STOP!!!")
            #sys.exit(4)

        if ierr == 0:
            print(f"{self.__class__.__name__}.readInMols(): File '{self._fname}' successfully read: "
                  f"lines = {str(self._lnum)} & natms = {str(natms)}\n")

        return (ierr == 0)

#   end of readInMols(...)


    def writeOutMols(self, rem, gbox, mols):
    ### NEED TO REFACTOR - ??? ###
        if not self._is_open:
            self.open(fmode='w')
            print(f"{self.__class__.__name__}.writeOutMols(): Ready for writing GRO file '{self._fname}' ...")
        if not self._is_wmode:
            print(f"{self.__class__.__name__}.writeOutMols(): Oops! Wrong mode '{self._fmode}' "
                  f"for writing file '{self._fname}' - FULL STOP!!!")
            sys.exit(1)

        print(f"{self.__class__.__name__}.writeOutMols(): Writing GRO file '{self._fname}' "
              f"from line # {str(self._lnum)} ...")

        imols = len(mols)                    # mnmol[0] - number of molecule types / sets of unique molecules
        mmols = sum(len(ms) for ms in mols)  # mnmol[len(mnmol) - 1] - total number of molecules in all sets

        nout = 0  # total number of atoms to output
        for mts in mols:
            for mol in mts:
                nout += len(mol)

        ierr  = 0
        nlines = 0
        natms  = 0
        resid  = 0
        is_fin = False
        for m in range(imols):
            #matms = len(mols[m][0])  # mnatm[m] - number of atoms per molecule of type m
            nmols = len(mols[m])     # int(len(atms[m]) / matms) - number of molecules of type / in set m
            for k in range(nmols):
                resnm = mols[m][k].name  # molnm[m] - name of molecules of type m / in set m
                resid += 1
                if resid == 1:
                    print(f"{self.__class__.__name__}.writeOutMols(): Writing molecule '{str(resid)+resnm}' "
                          f"into GRO file '{self._fname}' ...")
                    self._fio.write(rem + "\n")
                    self._fio.write(str(nout) + "\n")
                    nlines += 2
                    rem = None
                elif resid < 10 or (resid < 100 and resid % 10 == 0) or \
                    (resid < 1000 and resid % 100 == 0) or (resid % 1000 == 0) or resid == mmols:
                    print(f"{self.__class__.__name__}.writeOutMols(): Appending molecule '{str(resid)+resnm}' "
                          f"to file '" + self._fname + "' ...")

                matms = len(mols[m][k])  # mnatm[m] - number of atoms in molecule
                for i in range(matms):
                    nlines += 1
                    natms += 1
                    iprn = natms % 100000

                    #rvec = mols[m][k][i].rvec.arr3() + np.array(gbox)*0.5
                    rvec = mols[m][k][i].rvec + gbox*0.5
                    line = '{:>5}{:<5}{:>5}{:>5}'.format(resid, resnm, mols[m][k][i].name, iprn) + \
                           ''.join('{:>8.3f}{:>8.3f}{:>8.3f}'.format(*rvec))

                    self._fio.write(line + "\n")

                    # print("File '"+self._fname+"' : successfully written n_lines = "+str(nlines)+ \
                    #      " : n_mols = " + str(resid) + " & n_atms = "+str(natms)+" / "+str(nout)+"\n")

                if resid == mmols:
                    self._fio.write('{:>10.5f}{:>10.5f}{:>10.5f}'.format(*gbox) + "\n")
                    nlines += 1
                    is_fin = True

        if ierr == 0 and is_fin:
            print(f"{self.__class__.__name__}.writeOutMols(): File '{self._fname}' "
                  f"successfully written: lines = {str(nlines)} : n_mols = {str(resid)} "
                  f"& n_atms = {str(natms)} / {str(nout)}\n")
        return (ierr == 0)

    #end of writeOutMols()


    def close(self):
        super(groFile, self).close()
        #self._fio.close()
        self._lnum = 0
        self._lfrm = 0
        self._remark = ''

    def __del__(self):
        #super(ioFile, self).__del__()
        self.close()

#end of Class groFile