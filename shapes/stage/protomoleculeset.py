"""
.. module:: protosmoleculeset
       :platform: Linux - tested, Windows [WSL Ubuntu] - tested
       :synopsis: contributes to the hierarchy of classes:
        Atom > AtomSet > Molecule > MoleculeSet > MolecularSystem

.. moduleauthor:: Dr Andrey Brukhno <andrey.brukhno[@]stfc.ac.uk>

The module contains class MoleculeSet(object)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:

#import os, sys, re #, yaml
#import importlib.util

#from math import sqrt, sin, cos #, acos
from numpy import sum #, array, dot, cross, random, double
from numpy.linalg import norm

from shapes.basics.globals import TINY, Pi #, InvM1
from shapes.basics.functions import pbc #, isVec3Like, pbc_rect #, pbc_cube
from shapes.basics.mendeleyev import Chemistry
from shapes.stage.protovector import Vec3
#from shapes.stage.protoatom import Atom
#from shapes.stage.protoatomset import AtomSet
from shapes.stage.protomolecule import Molecule


class MoleculeSet(object):

    def __init__(self, sindx: int = 0, sitems: int = 0, sname='empty', stype='empty', mols=None, iscopy=True,
                 *args, **keys) -> None:
        self.indx = sindx
        self.mass = 0.0
        self.chrg = 0.0
        self.items = []
        self.nitems = 0
        self.rvec   = None
        self.rcog   = None
        self.sample = None
        if mols is not None:
            try:
                iterator = iter(mols)
            except TypeError: # non-iterable => use a sample / template molecule
                if isinstance(mols, Molecule):
                    self.sample = mols.copy()
                    if sitems > 0:  # create a set of same molecules (copies of sample)
                        for i in range(sitems):
                            self.items.append(mols.copy(i))
                        self.nitems = len(self.items)
                        self.name = self.items[0].name
                        self.type = self.items[0].type
            else:  # iterable
                if isinstance(mols[0], Molecule): # use the given set of molecules
                    self.sample = mols[0].copy()
                    if iscopy:
                        mitems = min(sitems, len(mols))
                        for i in range(mitems):
                            self.items.append(mols[i].copy())
                    else:
                        self.items = mols[:min(sitems, len(mols))]
                    self.nitems = len(self.items)
                    self.name = self.items[0].name
                    self.type = self.items[0].type
        elif sitems > 0: # create a set of same molecules defined by (*args, **keys)
            self.sample = Molecule(0, *args, **keys)
            for i in range(sitems):
                self.items.append(Molecule(i, *args, **keys))
            self.nitems = len(self.items)
            self.name = self.items[0].name
            self.type = self.items[0].type
        else:
            self.name = sname  # 'empty'
            self.type = stype  # 'empty'
        self.refresh()

    def __del__(self) -> None:
        while len(self.items) > 0:
            del self.items[len(self)-1]
        del self.items
        del self.rvec

    def __repr__(self) -> str:
        return '{self.__class__.__name__} => {{ index: {self.indx}, name: \'{self.name}\', type: \'{self.type}\', '\
               'mass: {self.mass}, charge: {self.chrg}, nitems: {self.nitems};\n rvec: {self.rvec};\n rcog: {self.rcog} }}'.format(self=self)
               #'mass: {self.mass}, charge: {self.chrg}, nitems: {self.nitems}; rvec: {self.rvec} }}'.format(self=self)

    def __len__(self) -> int:
        return len(self.items)

    def __getitem__(self, i) -> list:
        return self.items[i]

    def addItem(self, mol=None, be_verbose=False) -> None:
        if isinstance(mol, Molecule):
            self.items.append(mol)  # .copy()
            self.nitems = len(self.items)
            if self.name == 'empty':
                self.name = mol.name
            if self.type == 'empty':
                self.type = mol.type
        elif be_verbose:
            print(f"{self.__class__.__name__}.addItem(): Input {mol} does not qualify as "
                  f"Molecule(AtomSet) - skipped (item not added)!")

    def setSample(self, mol=None, be_verbose=False) -> None:
        if isinstance(mol, Molecule):
            if self.sample is not None:
                del self.sample
            self.sample = mol.copy()
            if self.name == 'empty':
                self.name = mol.name
            if self.type == 'empty':
                self.type = mol.type
        elif be_verbose:
            print(f"{self.__class__.__name__}.addItem(): Input {mol} does not qualify as "
                  f"Molecule(AtomSet) - skipped (sample not set)!")

    def setMass(self, isupdate=False) -> None:
        self.mass = 0.0
        self.nitems = len(self.items)
        if self.nitems > 0:
            for mol in self.items:
                self.mass += mol.getMass(isupdate)

    def setMassElems(self) -> bool:
        success = False
        self.nitems = len(self.items)
        if self.nitems > 0:
            success = True
            mass = 0.0
            for mol in self.items:
                if not mol.setMassElems():
                    success = False
                    break
                mass += mol.getMass()
            if success:
                self.mass = mass
        self.isMassElems = success
        return success

    def getMass(self, isupdate=False) -> float:
        if isupdate:
            self.setMass(isupdate)
        return self.mass

    def setCharge(self, isupdate=False) -> None:
        self.chrg = 0.0
        self.nitems = len(self.items)
        if self.nitems > 0:
            for mol in self.items:
                self.chrg += mol.getCharge(isupdate)

    def getCharge(self, isupdate=False) -> float:
        if isupdate:
            self.setCharge(isupdate)
        return self.chrg

    def refresh(self, **kwargs) -> None:
        if self.rvec is not None:
            del self.rvec
        if self.rcog is not None:
            del self.rcog
        self.mass = 0.0
        self.chrg = 0.0
        self.nitems = len(self.items)
        if self.nitems > 0:
            self.rvec = Vec3()
            self.rcog = Vec3()
            for mol in self.items:
                mol.refresh(**kwargs)
                self.mass += mol.mass
                self.chrg += mol.chrg
                self.rcog += mol.rcog
                self.rvec += mol.rvec * mol.mass
            self.rvec /= self.mass
            self.rcog /= float(self.nitems)

    def updateRcom(self, **kwargs) -> None: # center of mass
        if self.rvec is not None:
            del self.rvec
        self.mass = 0.0
        self.nitems = len(self.items)
        if self.nitems > 0:
            self.rvec = Vec3()
            for mol in self.items:
                mol.updateRcom(**kwargs)
                self.mass += mol.mass
                self.rvec += mol.rvec * mol.mass
            self.rvec /= self.mass

    def getRvec(self, isupdate=False, **kwargs) -> Vec3: # center of mass
        if isupdate:
            self.updateRcom(**kwargs)
        return self.rvec

    def getRcom(self, isupdate=False, **kwargs) -> Vec3: # center of mass
        return self.getRvec(isupdate, **kwargs)

    def getRcomPBC(self, box = 1.0) -> Vec3:
        return pbc(self.getRvec().copy(), box)

    def updateRcog(self, **kwargs) -> Vec3: # center of geometry
        if self.rcog is not None:
            del self.rcog
        self.nitems = len(self.items)
        if self.nitems > 0:
            self.rcog = Vec3()
            for mol in self.items:
                mol.updateRcog(**kwargs)
                self.rcog += mol.rcog
            self.rcog /= float(self.nitems)

    def getRcog(self, isupdate=False, **kwargs) -> Vec3: # center of geometry
        if isupdate:
            self.updateRcog(**kwargs)
        return self.rcog

    def getRcogPBC(self, box = 1.0) -> Vec3:
        return pbc(self.getRcog().copy(), box)

    def updateRvecs(self, **kwargs) -> None: # center of mass
        if self.rvec is not None:
            del self.rvec
        if self.rcog is not None:
            del self.rcog
        self.mass = 0.0
        self.nitems = len(self.items)
        if self.nitems > 0:
            self.rvec = Vec3()
            self.rcog = Vec3()
            box = None
            isMolPBC = False
            isMolSetPBC = False
            if 'box' in kwargs.keys():
                # AB: check if PBC to be applied to molecules or the molecule set
                box = kwargs['box']
                if 'isMolPBC' in kwargs.keys():
                    isMolPBC = kwargs['isMolPBC']
                isMolSetPBC = (box is not None and not isMolPBC)
            if isMolSetPBC:  # AB: applying PBC to the molecular set
                print(f"\nApplying PBC to set of molecules '{self.name}': "
                      f"box = {box} & isMolPBC = {isMolPBC}")
                rcom0 = self.items[0].getRcom(True, **kwargs).copy()
                rcog0 = self.items[0].getRcog().copy()
                self.mass += self.items[0].getMass()
                self.rvec += rcom0 * self.mass
                self.rcog += rcog0
                for mol in self.items[1:]:
                    rcom, rcog = mol.getRvecs(True, **kwargs)
                    rvec = rcom0 + pbc(rcom-rcom0, box)
                    rcom0 = rcom.copy()
                    mol.moveBy(rvec - rcom)
                    self.mass += mol.getMass()
                    self.rvec += mol.getRvec() * mol.getMass()
                    self.rcog += mol.getRcog()
                self.rvec /= self.mass
                self.rcog /= self.nitems
            else:  # AB: possibly applying PBC to molecules (depending on kwargs)
                if box is not None:
                    print(f"\nApplying PBC to molecules in set '{self.name}': "
                          f"box = {box} & isMolPBC = {isMolPBC}")
                for mol in self.items:
                    mol.updateRvecs(**kwargs)
                    self.mass += mol.mass
                    self.rvec += mol.rvec * mol.mass
                    self.rcog += mol.rcog
                self.rvec /= self.mass
                self.rcog /= self.nitems

    def getRvecs(self, isupdate: bool = False, **kwargs) -> Vec3:
        if isupdate:
            self.updateRvecs(**kwargs)
        return self.rvec, self.rcog

    def moveBy(self, dvec: Vec3 = Vec3(), be_verbose: bool = False) -> None:
        if isinstance(dvec, Vec3):
            self.nitems = len(self.items)
            if self.nitems > 0:
                for mol in self.items:
                    mol.moveBy(dvec)
                self.rvec += dvec
                self.rcog += dvec
        elif be_verbose:
            print(f"{self.__class__.__name__}.moveBy(): Input {dvec} does not qualify "
                  f"as Vec3(float, float, float) - skipped (no change)!")

    def radialDensities(self, rorg=Vec3(), rmin=0.0, rmax=0.0, dbin=0.0, clist=[],
                        dlist=[], bname = None, is_com=True) -> None:
        if rmax < TINY or rmax-rmin < TINY or dbin < TINY:
            print(f"{self.__class__.__name__}.radialDensities(): ill-defined range "
                  f"[{rmin}, {rmax}; {dbin}] - skipping calculation ...")
            return
        nbins = round((rmax-rmin)/dbin)
        if nbins < 5:
            print(f"{self.__class__.__name__}.radialDensities(): too few points ({nbins} < 5) "
                  f"in [{rmin}, {rmax}; {dbin}] - skipping calculation ...")
            return

        # emass  = dict( D=2.014, H=1.0078, C=12.011, N=14.007, O=15.999, P=30.974, S=32.065 )
        emass = { elem:elem["mau"] for elem in Chemistry.etable }
        elems_csl = Chemistry.ecsl # dict( D=6.674, H=-3.741, C=6.648, N=9.360, O=5.805, P=5.130, S=2.847 )
        elems_sld = Chemistry.esld # dict( D=2.823, H=-1.582, C=7.000, N=3.252, O=2.491, P=1.815, S=1.107 )

        alist  = list(set(dlist))
        flist = [name.casefold() for name in alist]
        if 'all'.casefold() in flist:
            alist.pop(flist.index('all'.casefold()))
            print(f"\n{self.__class__.__name__}.radialDensities(): Will calculate "
                  f"ALL density contributions ...")
        if 'hist'.casefold() in flist:
            alist.pop(flist.index('hist'.casefold()))
            print(f"\n{self.__class__.__name__}.radialDensities(): Will output "
                  f"histogram(s) ...")
        if 'nden'.casefold() in flist:
            alist.pop(flist.index('nden'.casefold()))
            print(f"\n{self.__class__.__name__}.radialDensities(): Will output "
                  f"N-density(ies) ...")
        if 'mden'.casefold() in flist:
            alist.pop(flist.index('mden'.casefold()))
            print(f"\n{self.__class__.__name__}.radialDensities(): Will output "
                  f"M-density(ies) ...")
        if 'nsld'.casefold() in flist:
            alist.pop(flist.index('nsld'.casefold()))
            print(f"\n{self.__class__.__name__}.radialDensities(): Will output "
                  f"SLD(s) ...")

        not_found = [ atom[0] for atom in alist if atom[0] not in emass.keys() ]
        if len(not_found) > 0:
            if len(not_found) > 0:
                print(f"{self.__class__.__name__}.radialDensities(): Unsupported atoms {not_found} "
                      f"in requested atom list {dlist} - skipping calculation ...")
                return

        import numpy as np
        dbin2 = dbin/2.0
        drange= np.arange(0, nbins, dtype=float) * dbin + dbin2 + rmin
        dbinV = 4.0 * Pi * drange**2 * dbin # * 1.e3
        dbinM = dbinV * 602.2 # 0.6022  # 6.022e23 / 1.e24  # 1.e21
        print(f"Will collect histograms in range [{drange[0]} ... {drange[-1]}] "
              f"with dbin = {dbin} -> {nbins} bins...\n")
        hwater = np.zeros(nbins)
        atms = [a for mol in self.items for a in mol.items if mol.name in clist]
        anms = [a.getName() for a in atms]
        axyz = [ (a.getRvec()-rorg).arr3() for a in atms]

        print(f"\nCollecting histograms for {len(atms)} atoms on species {clist}:\n")
              #f"{axyz[:10]}")

        halst = []
        hlist = []
        rxyz = np.array([0.0, 0.0, 0.0])
        aprev = ''
        gprev = ''
        gmass = 0.0
        aother = []
        nother = []
        for ia, aname in enumerate(anms):
            atmlist = [hatm[0][0] for hatm in halst]
            if aname[0] in atmlist:
                la = atmlist.index(aname[0])
                ibin = int(np.linalg.norm(axyz[ia])/dbin)
                #ibin = int(np.linalg.norm(axyz[ia]-rorg)/dbin)
                if -1 < ibin < nbins:
                    halst[la][0][0]  = aname[0]
                    halst[la][0][1] += 1
                    halst[la][1][ibin] += 1.0
            else:
                halst.append([[aname[0], 1], np.zeros(nbins)])
                ibin = int(np.linalg.norm(axyz[ia])/dbin)
                #ibin = int(np.linalg.norm(axyz[ia]-rorg)/dbin)
                if -1 < ibin < nbins:
                    halst[-1][1][ibin] = 1.0

            if aname[0] == 'H':  # add hydrogen to the COM group
                if 'H' in aother:
                    nother[aother.index('H')] += 1
                else:
                    aother.append('H')
                    nother.append(1)
                mass = atms[ia].getMass()
                gmass += mass
                rxyz += axyz[ia]*mass
            elif len(aname)>1 and len(gprev)>0 and aname[1] == gprev[0]:
                # found another atom in the previous COM group
                if aname[0] in aother:
                   nother[aother.index(aname[0])] += 1
                else:
                   aother.append(aname[0])
                   nother.append(1)
                mass = atms[ia].getMass()
                gmass += mass
                rxyz += axyz[ia]*mass
            else:
            #elif aname != gprev: # increment the count in histogram
                #pass  # initiate Rvec for new COM group
                atmlist = [hatm[0][0] for hatm in hlist]
                if len(gprev) > 0:
                    agrp = gprev[0]
                    if len(aother) > 0:
                        for io, ao in enumerate(aother):
                            agrp = agrp + ao + str(nother[io])
                    #print(f"Counting for atom group {gprev} {ia}...")
                    if gprev in atmlist:
                        la = atmlist.index(gprev)
                        ibin = int(np.linalg.norm(rxyz/gmass)/dbin)
                        #ibin = int(np.linalg.norm(rxyz/gmass-rorg)/dbin)
                        if -1 < ibin < nbins:
                            hlist[la][0][1] = agrp
                            hlist[la][1][ibin] += 1.0
                if aname not in atmlist:
                    print(f"Seeding a new atom group {aname} {ia}...")
                    hlist.append([[aname,aname], np.zeros(nbins)])
                gmass = atms[ia].getMass()
                rxyz  = axyz[ia]*gmass
                gprev = aname
                aother = []
                nother = []

            if ia == len(anms)-1:  # increment the count in histogram
                agrp = gprev[0]
                if len(aother)>0:
                    for io, ao in enumerate(aother):
                        agrp = agrp + ao + str(nother[io])
                atmlist = [hatm[0][0] for hatm in hlist]
                if gprev in atmlist:
                    la = atmlist.index(gprev)
                    ibin = int(np.linalg.norm(rxyz/gmass)/dbin)
                    #ibin = int(np.linalg.norm(rxyz/gmass-rorg)/dbin)
                    if -1 < ibin < nbins:
                        hlist[la][0][1] = agrp
                        hlist[la][1][ibin] += 1.0

        ntot  = 0
        antot = []
        hatot = []
        gntot = []
        hgtot = []
        for ih, hist in enumerate(hlist):
            if ih > 0:
                if hist[0][1] == hlist[ih-1][0][1]:
                    hgtot[-1] += hist[1]
                else:
                    gntot.append(hist[0][1])
                    hgtot.append(hist[1])
            else:
                gntot.append(hist[0][1])
                hgtot.append(hist[1])
            ntot += sum(hist[1])
            print(f"\nHistogram for group '{hist[0][1]}' @ atom {hist[0][0]} "
                  f"on species {self.name} => {sum(hist[1])} counts:\n",
                  f"{hist[1].T}\n")

        is_all = 'ALL'  in dlist or 'All' in dlist or 'all' in dlist
        printH = 'hist' in dlist
        printN = 'nden' in dlist
        printS = 'nsld' in dlist #or is_all
        printM = True #'mden' in dlist or is_all
        checkA = [is_all]
        checkA.extend([ True for aname in anms if aname[0] in dlist])
        countA = checkA.count(True) > 1 or is_all
        #print(f"\ncountA = {checkA} -> {countA} (for all = {is_all})")
        checkG = [is_all]
        checkG.extend([ True for gname in gntot if gname in dlist])
        countG = checkG.count(True) > 1 or is_all
        #print(f"\ncountG = {checkG} -> {countG} (for all = {is_all})")

        histSG = np.zeros(nbins)
        histMG = np.zeros(nbins)
        if any(checkG):  # AB: Group contributions
            for ih, hist in enumerate(hgtot):
                gchsl = 0.0
                chsl  = 0.0
                gmass = 0.0
                mass  = 0.0
                edigs = ''
                elems = gntot[ih]
                for ic in range(len(elems)):
                    if elems[ic].isdigit():
                        edigs += elems[ic]
                        if ic == len(elems)-1:
                            gchsl += chsl*float(edigs)
                            gmass += mass*float(edigs)
                        elif not elems[ic+1].isdigit():
                            gchsl += chsl*float(edigs)
                            gmass += mass*float(edigs)
                            edigs = ''
                    elif elems[ic] in emass.keys():
                        if ic == len(elems)-1:
                            gchsl += elems_csl[elems[ic]]
                            gmass += emass[elems[ic]]
                        elif not elems[ic+1].isdigit():
                            gchsl += elems_csl[elems[ic]]
                            gmass += emass[elems[ic]]
                        else:
                            chsl = elems_csl[elems[ic]]
                            mass = emass[elems[ic]]
                print(f"\nHistogram for group '{gntot[ih]}' of {sum(hist)} counts, "
                      f"mass = {gmass}, CSL = {gchsl}:\n",
                      *np.column_stack((drange, hist+0.0)), sep='\n')

                if isinstance(bname, str):
                    gname = gntot[ih]
                    if is_all or gname in dlist:
                        if printH:
                            np.savetxt(bname+'_hist_'+gntot[ih]+'.dat',
                                       np.column_stack((drange, hist+0.0)), fmt='%-0.3f %10.7f')
                        histN = hist / dbinV
                        if printN:
                            np.savetxt(bname + '_nden_' + gntot[ih] + '.dat',
                                       np.column_stack((drange, histN+0.0)), fmt='%-0.3f %10.7f')
                        histS   = histN * gchsl * 0.01
                        histSG += histS
                        if printS:
                            np.savetxt(bname + '_nsld_' + gntot[ih] + '.dat',
                                       np.column_stack((drange, histS+0.0)), fmt='%-0.3f %10.7f')
                        histM   = hist * gmass / dbinM
                        histMG += histM
                        if printM:
                            np.savetxt(bname + '_mden_' + gntot[ih] + '.dat',
                                       np.column_stack((drange, histM+0.0)), fmt='%-0.3f %10.7f')
            print(f"\nOverall number of groups = {ntot} \n")

        if isinstance(bname,str):
            if countG:  # AB: Groups totals
                if printS:
                    np.savetxt(bname + '_nsld_GRP.dat',
                               np.column_stack((drange, histSG+0.0)), fmt='%-0.3f %10.7f')
                if printM:
                    np.savetxt(bname + '_mden_GRP.dat',
                               np.column_stack((drange, histMG+0.0)), fmt='%-0.3f %10.7f')

            if any(checkA):  # AB: Atom contributions
                natot = 0
                histSA = np.zeros(nbins)
                histMA = np.zeros(nbins)
                for ih, hist in enumerate(halst):
                    natot += sum(hist[1])
                    print(f"\nHistogram for atoms '{hist[0]}' of {sum(hist[1])} counts:\n",
                          *np.column_stack((drange, hist[1]+0.0)), sep='\n')
                    aname = hist[0][0]
                    if is_all or aname in dlist:
                        if printH:
                            np.savetxt(bname+'_hist_'+hist[0][0]+'.dat',
                                       np.column_stack((drange, hist[1]+0.0)), fmt='%-0.3f %10.7f')
                        histN = hist[1] / dbinV
                        if printN:
                            np.savetxt(bname+'_nden_' + hist[0][0] + '.dat',
                                       np.column_stack((drange, histN+0.0)), fmt='%-0.3f %10.7f')
                        histS   = histN * elems_csl[hist[0][0]] * 0.01
                        histSA += histS
                        if printS:
                            np.savetxt(bname + '_nsld_' + hist[0][0] + '.dat',
                                       np.column_stack((drange, histS+0.0)), fmt='%-0.3f %10.7f')
                        histM   = hist[1] * emass[hist[0][0]] / dbinM
                        histMA += histM
                        if printM:
                            np.savetxt(bname + '_mden_' + hist[0][0] + '.dat',
                                       np.column_stack((drange, histM+0.0)), fmt='%-0.3f %10.7f')

                if countA:  # Atoms totals
                    if printS:
                        np.savetxt(bname + '_nsld_ATM.dat',
                                   np.column_stack((drange, histSA+0.0)), fmt='%-0.3f %10.7f')
                    if printM:
                        np.savetxt(bname + '_mden_ATM.dat',
                                   np.column_stack((drange, histMA+0.0)), fmt='%-0.3f %10.7f')
                print(f"\nOverall number of atoms in (sub-)system {self.name} = {natot} \n")
    # end of radialDensities()

# end of class MoleculeSet
