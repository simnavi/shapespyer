"""
.. module:: input
   :platform: Linux - tested, Windows (WSL Ubuntu) - tested
   :synopsis: abstraction class for reading in input options and arguments in YAML format (for shape.py)

.. moduleauthor:: MSc Mariam Demir <mariam.demir[@]stfc.ac.uk>

The module contains class InputParser(object)
"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
#  Contrib: MSc Mariam Demir (c) Oct - Dec 2023  #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.7 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by developer's initials as follows:

# system modules for parsing arguments & options
import os, sys, getopt, argparse, re #, yaml
import importlib.util

from shapes.basics.help import *

class InputParser(object):
    """
    Class **InputParser()** - parses and stores the input (arguments and flags) from CLI or Yaml file

    Parameters
    ----------
    _options['input']['path'] : str
        Directory containing the input file
    _options['output']['path'] : str
        Directory containing the output file

    _options['input']['file'] : str
        Full name of input file containing structural information (to be only used per se)
    _options['input']['base'] : str
        Basename of input file (to be only used with ``ext`` option)
    _options['input']['ext'] : str
        Extension of input file (to be only used with ``base`` option)
    _options['input']['box'] : str
        Input file specifying box dimensions (3 values) or cell matrix (9 values)

    _options['output']['file'] : str
        Full name of output configuration file (to be only used per se)
    _options['output']['base'] : str
        Basename of the output configuration file (to be only used with 'ext' option)
    _options['output']['ext'] : str
        Extension of the output configuration file (to be only used with 'base' option)

    _options['molecule']['names'] : list
        Name(s) of molecules (species) to pick up from input
    _options['molecule']['mint'] : list
        Interior atom indices (one per input species), placed closer to the center
    _options['molecule']['mext'] : list
        Exterior atom indices (one per input species), placed farther from the center
    _options['molecule']['fracs'] : list
        Fractions of molecular species in the output structure (i.e. its composition)
    _options['molecule']['molids'] : list
        Index(ices) of molecular species to pick up from input (only if 'names' unspecified)

    _options['shape']['type'] : str
        Type of geometrical shape to create {ring*/rod/ball/ves/lat/lat2/lat3/smiles}
    _options['shape']['radius'] : float
        Radius of the internal cavity in the centre of the output structure
    _options['shape']['lring'] : int
        Number of molecules in the largest ring within the output structure (e.g. equator in a ball)
    _options['shape']['nmols'] : int
        Number of molecules in the output ball structure (i.e. relevant only for 'ball' and 'ves' shapes)
    _options['shape']['turns'] : int
        Number of full turns in a 'rod' (i.e. stack of rings)
    _options['shape']['layers'] : int
        Number of (mono-)layers in the output vesicle structure (i.e. relevant only for shape='ves')

    _options['angle']['alpha'] : float
        Initial azimuth angle (in XY plane)
    _options['angle']['theta'] : float
        Initial altitude angle (w.r.t. OZ axis)

    _options['membrane']['nside'] : int
        Number of molecules on the side of a bilayer
    _options['membrane']['zsep'] : float
        Z distance between lipid heads

    _options['other']['minDist'] : float
        Minimum distance between 'bone' atoms in the generated structure
    _options['other']['slvBuff'] : float
        Solvation buffer size around the generated structure
    _options['other']['nlatt'] : int
        3D lattice number, i.e. same number of nodes in each dimension on a cubic 3D lattice
    _options['other']['nlatx'] : int
        Lattice number in X, i.e. number of nodes in X dimension on a rectangular 3D lattice
    _options['other']['nlaty'] : int
        Lattice number in Y, i.e. number of nodes in Y dimension on a rectangular 3D lattice
    _options['other']['nlatz'] : int
        Lattice number in Z, i.e. number of nodes in Z dimension on a rectangular 3D lattice
    _options['other']['ldpd'] : float
        DPD length scale for input/output files in nm (only needed for DPD/DL_MESO inputs/outputs)

    _options['flags']['fxz'] : bool
        Flag for using XZ-flattened initial molecule orientation to minimise its spread along Y axis
    _options['flags']['rev'] : bool
        Flag for reversing the order of 'bone' atoms (i.e. 'mint' becomes 'mext' and vice versa)
    _options['flags']['alignz'] : bool
        Align initial molecule configuration along Z axis (only valid with 'smiles' input for shape)
    _options['flags']['smiles'] : bool
        Flag for using SMILES string from input file (set ON with the option 'shape=smiles')
    """
    # _options['molecule']['npick'] : int
    #     Number of molecules (species) to pick up from input (only used internally)
    # _options['shape']['nstep'] : int
    #     Number of layer steps per turn in a pitched band (ad hoc, do not use!)
    # _options['flags']['wav'] : bool
    #     Reserved (to be defined)
    # _options['flags']['pin'] : bool
    #     Flag for creating a 'pinned-through' ball or vesicle, i.e. with holes on its sides (ad hoc, do not use!)
    # _options['flags']['inpbox'] : bool
    #     Flag for box dimensions or a cell matrix read from a separate input file 'shape.box' (not implemented yet)
    # _options['flags']['random'] : bool
    #     Flag for random population of molecular species in a multicomponent structure (the only option for now)

    # DEFAULTS & CONVENTIONS
    _dinp = '.'
    _dout = '.'
    _sxyz = '.xyz'
    _spdb = '.pdb'
    _sgro = '.gro'
    _sdlp = '.dlp'
    _sdlm = '.dlm'
    _ssml = '.sml'

    _iexts  = {_sgro, _sxyz, _ssml, _spdb, _sdlp, _sdlm} #, _sdpd]  # supported file extensions so far
    _oexts  = {_sgro, _sxyz, _spdb, _sdlp, _sdlm} #, _sdpd]  # supported file extensions so far
    _shapes = ['ring', 'rod', 'ball', 'ves', 'balp', 'vesp', 'lat', 'lat2', 'lat3',
               'smiles', 'waves', 'bilayer', 'dens']  #, 'band', 'mix'
    _sfill  = ['rings0','rings','fibo','mesh']
    _origin = {'cog', 'com', 'cob',}

    # AB: default surface XYZ file
    _fwav = 'config_wav.xyz'

    # AB: default GROMACS INPUT / OUTPUT
    # _finp = 'config_inp.gro'
    # _fout = 'config_out.gro'
    # _fsml = 'smiles.sml'

    # AB: default DL_POLY INPUT / OUTPUT
    # _fcfg = 'CONFIG'
    # _fhst = 'HISTORY'
    # _ftrj = 'TRAJOUT'

    def __init__(self):
        self._options = {# INPUT path/file
                         # AB: default: the full file name is expected
                         # AB: consider adding similar logic for dealing with base and extension as for OUTPUT
                         'input': {'path': '.',  # 'inp'          # default
                                   'file': 'config_inp.gro',      # default
                                   'base': None,  # 'config_inp'  # default
                                   'ext' : None,  # '.gro'        # default
                                   'box' : None,  # 'config.box'  # default
                                   'isbase': False                # default (auxilary)
                                   },
                         # OUTPUT path/file
                         # AB: if the full file name is given, use it as is, i.e. don't add any suffices
                         # AB: if the file name base and extension are given separately, add specs-inspired suffices
                         # AB: default - the file name base and extension are dealt with separately
                         'output': {'path': '.',   # 'out'             # default
                                    'file': None,  # 'config_out.gro'  # default
                                    'base': 'config_out',  # default => 'config_out_<suffices>.gro'
                                    'ext' : '.gro',        # default => 'config_out_<suffices>.gro'
                                    'isbase': True         # default (auxilary)
                                    },
                         # MOLECULE(s) to pick up from the input and use for creating the shape
                         'molecule': {'resnm'  : 'ANY',  # default (auxilary) {'ANY', 'ALL'}
                                      'names'  : [],  # names of molecules / species to pick up
                                      'mint'   : [],  # indices of interior atoms (one per molecule)
                                      'mext'   : [],  # indices of exterior atoms (one per molecule)
                                      'fracs'  : [],  #None,  # fractions of molecular species in the shape
                                      'molids' : [],  # indices of molecules / species to pick up
                                      'molid'  : 1,   # index of the first input molecule to pick up (deprecated?)
                                      'npick'  : 1    # how many molecules to pick up (deprecated?)
                                      #'smiles' : []  # AB: consider adding SMILES directly in YAML input
                                      },
                         # SHAPE specs
                         'shape': {'type'  : 'ring',  # default (see _shapes above)
                                   'radius': 0.25,    # internal radius of the shape / nm
                                   'lring' : 0,       # number of molecules in the largest (equatorial) ring
                                   'turns' : 0,       # number of turns (rings) in ball and rod shapes
                                   'nmols' : 0,       # number of molecules in (inner) ball shape
                                   'layers': [1,1,1], # number of layers in vesicle and scaling factors for dmin and layers' radii {[4, 1.15, 0.975]}
                                   'nstep' : 0,
                                   'fill'  : 'rings0', # type of molecules' placement on the surface ['rings','area','fibo','mesh']
                                   'origin': 'cog',    # the origin is either COG, COM or COB (center of bounding box)
                                   'offset': []    # the origin is either COG, COM or COB (center of bounding box)
                                   },
                         # PITCH ANGLES for molecules or other subunits within the shape
                         'angle': {'alpha': 0.0,
                                   'theta': 0.0
                                   },
                         # MEMBRANE parameters
                         'membrane': {'nside': 10,
                                      'zsep' : 0
                                   },
                         # AUXILARY parameters
                         'other': {'minDist': 0.5,    # default min distance between bone atoms / nm
                                   'slvBuff': 1.0,    # default solvation buffer around the shape / nm
                                   'nlatt'  : 0,
                                   'nlatx'  : 0,
                                   'nlaty'  : 0,
                                   'nlatz'  : 0,
                                   'ldpd'   : 0.1     # DPD length unit / nm (defaults to 0.1 to assume angstroms in use)
                                   },
                         # FLAGS
                         'flags':{ 'fxz': False,
                                   'rev': False,
                                   'pin': False,
                                   'wav': False,
                                   'alignz': False,
                                   'smiles': False,
                                   'inpbox': False,
                                   'random': False  # True
                                   },
                         #'density': {'names' : []}  # default {'ANY', 'ALL'}
                         'density': {'names' : 'NONE'}  # default {'ANY', 'ALL'}
                         }

    # INPUT / OUTPUT

    def getOptions(self) -> dict:
        return self._options

    def getInpPath(self) -> str:
        return self._options['input']['path']

    def setInpPath(self, value: str) -> None:
        self._options['input']['path'] = value

    def getOutPath(self) -> str:
        return self._options['output']['path']

    def setOutPath(self, value: str) -> None:
        self._options['output']['path'] = value

    def getInpFile(self) -> str:
        return self._options['input']['file']

    def setInpFile(self, value: str) -> None:
        self._options['input']['file'] = value

    def getInpBase(self) -> str:
        return self._options['input']['base']

    def setInpBase(self, value: str) -> None:
        self._options['input']['base'] = value

    def getInpExt(self) -> str:
        return self._options['input']['ext']

    def setInpExt(self, value: str) -> None:
        self._options['input']['ext'] = value

    def getInpBox(self) -> (str):
        return self._options['input']['box']

    def setInpBox(self, value: str) -> None:
        if isinstance(value, str):
            self._options['input']['box'] = value
        else:
            print(f"\n{self.__class__.__name__}.setInpBox('{value}'): "
                  f"ERROR! Incorrect input for '-b/--box' (box file name) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)

    def getOutIsBase(self) -> bool:
        return self._options['output']['isbase']

    def setOutIsBase(self, val: bool = True) -> None:
        self._options['output']['isbase'] = val

    def getOutFile(self) -> str:
        return self._options['output']['file']

    def setOutFile(self, value: str) -> None:
        if isinstance(value, str):
            self._options['output']['file'] = value
        else:
            print(f"\n{self.__class__.__name__}.setOutFile('{value}'): "
                  f"ERROR! Incorrect input for '-o/--out' (output file name) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)

    def getOutBase(self) -> str:
        return self._options['output']['base']

    def setOutBase(self, value: str) -> None:
        if isinstance(value, str):
            self._options['output']['base'] = value
        else:
            print(f"\n{self.__class__.__name__}.setOutBase('{value}'): "
                  f"ERROR! Incorrect input for '-o/--out' (output base name) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)

    def getOutExt(self) -> str:
        return self._options['output']['ext']

    def setOutExt(self, value: str) -> None:
        if isinstance(value, str):
            value = value.lower()
            if value not in self._oexts:
                print(f"\n{self.__class__.__name__}.setOutExt('{value}'): "
                      f"ERROR! Unsupported input for '-x/--xout' (output file extension) "
                      f"- FULL STOP!!!")
                sys.exit(1)
        else:
            print(f"\n{self.__class__.__name__}.setOutExt('{value}'): "
                  f"ERROR! Incorrect input for '-x/--xout' (output file extension) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)
        self._options['output']['ext'] = value

    # MOLECULE(s)

    def getMolResnm(self) -> str:
        return self._options['molecule']['resnm']

    def setMolResnm(self, value: str) -> None:
        if isinstance(value, str):
            self._options['molecule']['resnm'] = value
        else:
            print(f"\n{self.__class__.__name__}.setMolResnm('{value}'): "
                  f"ERROR! Incorrect input for '-r/--rnames' (residue/molecule name) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)

    def getMolNames(self) -> (list, str):
        return self._options['molecule']['names']

    def setMolNames(self, value: (list, str) = None, index: int = -1) -> None:
        if isinstance(value, str):
            lopt = -1
            if not isinstance(self._options['molecule']['names'], list):
                self._options['molecule']['names'] = []
            lopt = len(self._options['molecule']['names'])
            if index > -1:
                if index < lopt:
                    self._options['molecule']['names'][index] = value
                else:
                    print(f"\n{self.__class__.__name__}.setMolNames('{value}'): "
                          f"ERROR! Trying to reset residue/molecule name # {index} - "
                          f"out of range [{0}, {lopt}) - FULL STOP!!!")
                    sys.exit(1)
            else:
                self._options['molecule']['names'].append(value)
        elif isinstance(value, list):
            if isinstance(self._options['molecule']['names'], list):
                del(self._options['molecule']['names'])
                self._options['molecule']['names'] = value
        else:
            print(f"\n{self.__class__.__name__}.setMolNames('{value}'): "
                  f"ERROR! Incorrect input for '-r/--rnames' (residue/molecule names) "
                  f"- not a string nor a list - FULL STOP!!!")
            sys.exit(1)

    def delMolNames(self) -> None:
        if self._options['molecule']['names'] is not None:
            self._options['molecule']['names'].clear()

    def getDensNames(self) -> (list, str):
        return self._options['density']['names']

    def setDensNames(self, value: (list, str), index: int = -1) -> None:
        if isinstance(value, str) or isinstance(value, list):
            lopt = -1
            if not isinstance(self._options['density']['names'], list):
                self._options['density']['names'] = []
            lopt = len(self._options['density']['names'])
            if index > -1:
                if index < lopt:
                    self._options['density']['names'][index] = value
                else:
                    print(f"\n{self.__class__.__name__}.setDensNames('{value}'): "
                          f"ERROR! Trying to reset residue/molecule name # {index} - "
                          f"out of range [{0}, {lopt}) - FULL STOP!!!")
                    sys.exit(1)
            else:
                self._options['density']['names'].append(value)
        else:
            print(f"\n{self.__class__.__name__}.setDensNames('{value}'): "
                  f"ERROR! Incorrect input for '-r/--rnames' (residue/molecule names) "
                  f"- not a string nor a list - FULL STOP!!!")
            sys.exit(1)

    def delDensNames(self) -> None:
        if self._options['density']['names'] is not None:
            self._options['density']['names'].clear()

    def getMolFracs(self) -> list:
        return self._options['molecule']['fracs']

    def setMolFracsFromStr(self, arg: str) -> None:
        fracs = []
        flist = arg.strip()
        print(f"\n{self.__class__.__name__}.setMolFracsFromStr(0): flist = '{flist}'")
        if flist.count('[')>2 and flist.count(']')>2:
            flist = flist.strip()[1:-1].strip()
            flist = re.split(r"\s*\]\s*,\s*\[\s*|\s*\]\s*:\s*\[\s*|\s*\]\s*\[\s*", flist)
            flist = [ re.sub(r"\[|\]",'',frc).strip() for frc in flist ]
            #print(f"{self.__class__.__name__}.setMolFracsFromStr(1): flist = {flist}")
        elif flist.count('[')==1 and flist.count(']')==1:
            flist = re.sub(r"\s*\[\s*|\s*\]\s*",'',flist).strip().split(':')
            #print(f"{self.__class__.__name__}.setMolFracsFromStr(1): flist = {flist}")
        else:
            flist = flist.strip().split(':')
            #print(f"{self.__class__.__name__}.setMolFracsFromStr(1): flist = {flist}")
        for frcl in flist:
            fracs.append([])
            frcs = frcl.strip().replace('[', '').replace(']', '').split(',')
            for frc in frcs:
                fracs[-1].append(float(frc))
        self._options['molecule']['fracs'] = fracs
        print(f"{self.__class__.__name__}.setMolFracsFromStr(2): Species' fractions = "
              f"{self._options['molecule']['fracs']}")

    def delMolFracs(self) -> None:
        if self._options['molecule']['fracs'] is not None:
            self._options['molecule']['fracs'].clear()

    def getMolMint(self) -> list:
        return self._options['molecule']['mint']

    def setMolMint(self, value: (list, int), index: int = -1) -> None:
        if index > -1:
            if index < len(self._options['molecule']['mint']):
                self._options['molecule']['mint'][index] = value
            else:
                print(f"\n{self.__class__.__name__}.setMolMint(): "
                      f"Trying to reset input molecule mint # {index} - out of range!")
                sys.exit(1)
        else:
            self._options['molecule']['mint'].append(value)

    def delMolMint(self) -> None:
        if self._options['molecule']['mint'] is not None:
            self._options['molecule']['mint'].clear()

    def getMolMext(self) -> list:
        return self._options['molecule']['mext']

    def setMolMext(self, value: (list, int), index: int = -1) -> None:
        if index > -1:
            if index < len(self._options['molecule']['mext']):
                self._options['molecule']['mext'][index] = value
            else:
                print(f"\n{self.__class__.__name__}.setMolMext(): "
                      f"Trying to reset input molecule mext # {index} - out of range!")
                sys.exit(1)
        else:
            self._options['molecule']['mext'].append(value)

    def delMolMext(self) -> None:
        if self._options['molecule']['mext'] is not None:
            self._options['molecule']['mext'].clear()

    def getMolIDs(self) -> int:
        return self._options['molecule']['molids']

    def setMolIDs(self, value: int, index: int = -1) -> None:
        if index > -1:
            if index < len(self._options['molecule']['molids']):
                self._options['molecule']['molids'][index] = value
            else:
                print(f"\n{self.__class__.__name__}.setMolIDs(): "
                      f"Trying to reset input molecule ID # {index} - out of range!")
                sys.exit(1)
        else:
            self._options['molecule']['molids'].append(value)

    def delMolIDs(self) -> None:
        if self._options['molecule']['molids'] is not None:
            self._options['molecule']['molids'].clear()

    def getMolID(self) -> int:
        return self._options['molecule']['molid']

    def setMolID(self, value: int) -> None:
        self._options['molecule']['molid'] = value

    def getMolNpick(self) -> int:
        return self._options['molecule']['npick']

    def setMolNpick(self, value: int) -> None:
        self._options['molecule']['npick'] = value

    # SHAPE specs

    def getShapeType(self) -> str:
        return self._options['shape']['type']

    def setShapeType(self, value: str) -> None:
        if isinstance(value, str):
            value = value.lower()
            if value not in self._shapes:
                print(f"\n{self.__class__.__name__}.setShapeType('{value}'): "
                      f"ERROR! Unknown input value for '-s/--shape' (shape type) "
                      f"- FULL STOP!!!")
                sys.exit(1)
        else:
            print(f"\n{self.__class__.__name__}.setShapeType('{value}'): "
                  f"ERROR! Incorrect input for '-s/--shape' (shape type) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)
        self._options['shape']['type'] = value

    def getShapeFill(self) -> str:
        return self._options['shape']['fill']

    def setShapeFill(self, value: str) -> None:
        if isinstance(value, str):
            value = value.lower()
            if value not in self._sfill:
                print(f"\n{self.__class__.__name__}.setShapeFill('{value}'): "
                      f"WARNING! Unknown input value for '-f/--fill' (shape filling algo) "
                      f"- using default: '{self._options['shape']['fill']}' ...")
                return
                #value = self._sfill[0]
        else:
            print(f"\n{self.__class__.__name__}.setShapeFill('{value}'): "
                  f"ERROR! Incorrect input for '-f/--fill' (shape filling algo) "
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)
        self._options['shape']['fill'] = value

    def getShapeRadius(self) -> float:
        if isinstance(self._options['shape']['radius'], list):
            return self._options['shape']['radius'][0]
        elif isinstance(self._options['shape']['radius'], float):
            return self._options['shape']['radius']
        else:
            print(f"\n{self.__class__.__name__}.getShapeRadius(): "
                  f"ERROR! The value set with option '--cavr'"
                  f"is not a list nor a float - FULL STOP!!!")
            sys.exit(1)

    def getDensRange(self) -> list:
        rrange = self._options['shape']['radius']
        if isinstance(rrange, list):
            if len(rrange) > 2:
                return rrange[:3]
            elif len(rrange)==2:
                return [rrange[0], 0.0, rrange[1]]
            else:
                return [rrange[0], 0.0, 0.1]
        elif isinstance(self._options['shape']['radius'], float):
            return [self._options['shape']['radius'], 0.0, 0.1]
        else:
            print(f"\n{self.__class__.__name__}.getDensRange(): "
                  f"ERROR! The value set with option '--cavr'"
                  f"is not a list nor a float - FULL STOP!!!")
            sys.exit(1)

    def setShapeRadius(self, value: (float, str, list) = 0.5) -> None:
        if isinstance(value, float):
            self._options['shape']['radius'] = value
        elif isinstance(value, list):
            value = [ float(val) for val in value ]
            if len(value) < 2:
                self._options['shape']['radius'] = value[0]
            elif len(value) > 3:
                self._options['shape']['radius'] = value[:3]
            else:
                self._options['shape']['radius'] = value
        elif isinstance(value, str):
            strl = value.strip().replace('[', '').replace(']', '').split(',')
            dist = []
            for lstr in strl:
                dist.append(float(lstr))
            self.setShapeRadius(dist)
        else:
            print(f"\n{self.__class__.__name__}.setShapeRadius('{value}'): "
                  f"ERROR! Incorrect input for '--cavr' (the structure inner radius)"
                  f"- not a string, float or list - FULL STOP!!!")
            sys.exit(1)

    def getShapeLring(self) -> int:
        return self._options['shape']['lring']

    def setShapeLring(self, value: int) -> None:
        self._options['shape']['lring'] = int(value)

    def getShapeTurns(self) -> int:
        return self._options['shape']['turns']

    def setShapeTurns(self, value: int) -> None:
        self._options['shape']['turns'] = int(value)

    def getShapeNmols(self) -> int:
        return self._options['shape']['nmols']

    def setShapeNmols(self, value: int) -> None:
        self._options['shape']['nmols'] = int(value)

    def getShapeLayers(self) -> []:
        return self._options['shape']['layers']

    def setShapeLayers(self, value: (int, list, str)) -> None:
        if isinstance(value, int):
            self._options['shape']['layers'] = [value, 1.0, 1.0]
        elif isinstance(value, list):
            value = [ float(val) for val in value ]
            if len(value) < 2:
                self._options['shape']['layers'] = [int(value[0]), 1.0, 1.0]
            elif len(value) == 2:
                self._options['shape']['layers'] = [int(value[0]), value[1], 1.0]
            else:
                self._options['shape']['layers'] = [int(value[0]), value[1], value[2]]
        elif isinstance(value, str):
            strl = value.strip().replace('[', '').replace(']', '').split(',')
            spec = []
            for lstr in strl:
                spec.append(float(lstr))
            self.setShapeLayers(spec)
        else:
            print(f"\n{self.__class__.__name__}.setShapeLayers('{value}'): "
                  f"ERROR! Incorrect input for '--layers' (the vesicle layers specs)"
                  f"- not a string, list or integer - FULL STOP!!!")
            sys.exit(1)
        #
        # if isinstance(value, list):
        #     self._options['shape']['layers'] = value
        # elif isinstance(value, str):
        #     self._options['shape']['layers'] = value
        # else:
        #     self._options['shape']['layers'] = int(value)

    def getShapeNstep(self) -> int:
        return self._options['shape']['nstep']

    def setShapeNstep(self, value: int) -> None:
        self._options['shape']['nstep'] = int(value)

    def getShapeOrigin(self) -> str:
        return self._options['shape']['origin']

    def setShapeOrigin(self, value: str = 'cog') -> None:
        if isinstance(value, str):
            value = value.lower()
            if value not in self._origin:
                print(f"\n{self.__class__.__name__}.setShapeOrigin('{value}'): "
                      f"WARNING! Unknown input value for '--origin' (algo for the structure origin) "
                      f"- using default: '{self._options['shape']['origin']}' ...")
                return
                #value = self._origin[0]
        else:
            print(f"\n{self.__class__.__name__}.setShapeOrigin('{value}'): "
                  f"ERROR! Incorrect input for '--origin' (algo for the structure origin)"
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)
        self._options['shape']['origin'] = value

    def getShapeOffset(self) -> list:
        return self._options['shape']['offset']

    def setShapeOffset(self, value: (list, str) = None) -> None:
        if isinstance(value, list):
            value = [ float(val) for val in value ]
            if len(value) < 4:
                while len(value) < 3:
                    value.append(0.0)
                self._options['shape']['offset'] = value
            else:
                self._options['shape']['offset'] = value[:3]
        elif isinstance(value, str):
            strl = value.strip().replace('[', '').replace(']', '').split(',')
            shift = []
            for lstr in strl:
                shift.append(float(lstr))
            self.setShapeOffset(shift)
        else:
            print(f"\n{self.__class__.__name__}.setShapeOffset('{value}'): "
                  f"ERROR! Incorrect input for '--offset' (the structure origin's offset)"
                  f"- not a string - FULL STOP!!!")
            sys.exit(1)

    # PITCH ANGLES

    def getAlpha(self) -> float:
        return self._options['angle']['alpha']

    def setAlpha(self, value: float) -> None:
        self._options['angle']['alpha'] = value

    def getTheta(self) -> float:
        return self._options['angle']['theta']

    def setTheta(self, value: float) -> None:
        self._options['angle']['theta'] = value

    # OTHER / AUXILARY

    def getSolvBuff(self) -> float:
        return self._options['other']['slvBuff']

    def setSolvBuff(self, value: float) -> None:
        self._options['other']['slvBuff'] = value

    def getMinDist(self) -> float:
        return self._options['other']['minDist']

    def setMinDist(self, value: float) -> None:
        self._options['other']['minDist'] = value

    def getNlatt(self) -> int:
        return self._options['other']['nlatt']

    def setNlatt(self, value: int) -> None:
        self._options['other']['nlatt'] = value

    def getNside(self) -> int:
        return self._options['membrane']['nside']

    def setNside(self, value: int) -> None:
        self._options['membrane']['nside'] = value

    def getZsep(self) -> float:
        return self._options['membrane']['zsep']
    
    def setZsep(self, value: float) -> None:
        self._options['membrane']['zsep'] = value

    def getNlatx(self) -> int:
        return self._options['other']['nlatx']

    def setNlatx(self, value: int) -> None:
        self._options['other']['nlatx'] = value

    def getNlaty(self) -> int:
        return self._options['other']['nlaty']

    def setNlaty(self, value: int) -> None:
        self._options['other']['nlaty'] = value

    def getNlatz(self):
        return self._options['other']['nlatz']

    def setNlatz(self, value: int) -> None:
        self._options['other']['nlatz'] = value

    def getLenDPD(self) -> float:
        return self._options['other']['ldpd']
    
    def setLenDPD(self, value: float) -> None:
        self._options['other']['ldpd'] = value
    
    # FLAGS

    def getFlagRev(self) -> bool:
        return self._options['flags']['rev']

    def setFlagRev(self, value: bool) -> None:
        self._options['flags']['rev'] = value

    def getFlagPin(self) -> bool:
        return self._options['flags']['pin']

    def setFlagPin(self, value: bool) -> None:
        self._options['flags']['pin'] = value

    def getFlagFxz(self) -> bool:
        return self._options['flags']['fxz']

    def setFlagFxz(self, value: bool) -> None:
        self._options['flags']['fxz'] = value

    def getFlagAlignZ(self) -> bool:
        return self._options['flags']['alignz']

    def setFlagAlignZ(self, value: bool) -> None:
        self._options['flags']['alignz'] = value

    def getFlagWav(self) -> bool:
        return self._options['flags']['wav']

    def setFlagWav(self, value: bool) -> None:
        self._options['flags']['wav'] = value

    def getFlagBox(self) -> bool:
        return self._options['flags']['inpbox']

    def setFlagBox(self, value: bool) -> None:
        self._options['flags']['inpbox'] = value

    def getFlagSmiles(self) -> bool:
        return self._options['flags']['smiles']

    def setFlagSmiles(self, value: bool) -> None:
        self._options['flags']['smiles'] = value

    def getFlagRandom(self) -> bool:
        return self._options['flags']['random']

    def setFlagRandom(self, value: bool) -> None:
        self._options['flags']['random'] = value

    def dumpYaml(self, fname: str = 'input-dump.yaml') -> None:
        # AB: making sure PyYaml module is found and loading correctly!
        pyyaml = 'yaml'
        if pyyaml in sys.modules:
            print(f"{self.__class__.__name__}.dumpYaml(): {pyyaml!r} found in sys.modules - proceeding ...")
        #elif (spec := importlib.util.find_spec(pyyaml)) is not None:
        else:
            spec = importlib.util.find_spec(pyyaml)
            if (spec) is not None:
                # here we do the actual 'import module'
                # module = importlib.util.module_from_spec(spec)
                # sys.modules[pyyaml] = module
                # spec.loader.exec_module(module)
                yaml = importlib.util.module_from_spec(spec)
                sys.modules[pyyaml] = yaml
                spec.loader.exec_module(yaml)
                print(f"{self.__class__.__name__}.dumpYaml(): {pyyaml!r} has been successfully imported - proceeding ...")
            else:
                print(f"{self.__class__.__name__}.dumpYaml(): ERROR! {pyyaml!r} module was not found! - FULL STOP!!!")
                print(f"{self.__class__.__name__}.dumpYaml(): Please use the command line options instead! ")
                sys.exit(1)
        
        yaml = sys.modules[pyyaml]

        yamlName = "input-dump.yaml"
        if fname != '':
            yamlName = fname
        if os.path.isfile(yamlName):
            print(f"\n{self.__class__.__name__}.dumpYaml(): : File '{yamlName}' exists - overwriting!..")

        with open(yamlName, 'w') as yamlFile:
            yaml.dump(self._options, yamlFile, default_flow_style=False)

    # Reading YAML input file

    def readYaml(self, fname: str = 'input.yaml') -> None:
        """
        Takes a .yaml file containing a set of parameters, and updates the options dictionary (``self``)
        with these parameters accordingly.

        :param yamlName: a file containing a set of parameters
        :return: None
        """

        # AB: making sure PyYaml module is found and loading correctly!
        pyyaml = 'yaml'
        if pyyaml in sys.modules:
            print(f"{self.__class__.__name__}.readYaml(): {pyyaml!r} found in sys.modules - proceeding ...")
        else:
            spec = importlib.util.find_spec(pyyaml)
            if (spec) is not None:
                # here we do the actual 'import module'
                # module = importlib.util.module_from_spec(spec)
                # sys.modules[pyyaml] = module
                # spec.loader.exec_module(module)
                yaml = importlib.util.module_from_spec(spec)
                sys.modules[pyyaml] = yaml
                spec.loader.exec_module(yaml)
                print(f"{self.__class__.__name__}.readYaml(): {pyyaml!r} has been successfully imported - proceeding ...")
            else:
                print(f"{self.__class__.__name__}.readYaml(): ERROR! {pyyaml!r} module was not found! - FULL STOP!!!")
                print(f"{self.__class__.__name__}.readYaml(): Please use command line options instead!..")
                sys.exit(1)

        yaml = sys.modules[pyyaml]

        yamlName = "input.yaml"
        if fname != '':
            yamlName = fname

        with open(yamlName, 'r') as yamlFile:
            argd = yaml.safe_load(yamlFile)

        print(f"\n{self.__class__.__name__}.readYaml(0): argd = {argd}")

        # reads input from yaml file
        # goes through each key and updates the dictionary attribute to these new values, using setattr
        #print ("argd=", argd)
        for categ, opt in argd.items():
            #print ("categ, opt=", categ, opt)
            for opt, val in argd[categ].items():
                #print ("opt, val=", opt, val)
                if (self._options[categ][opt] is not None
                  and isinstance(self._options[categ][opt], list)):
                    if isinstance(val, list):  # and len(val) > 0:
                        for v in val:
                            self._options[categ][opt].append(v)
                    else:
                        self._options[categ][opt].append(val)
                else:
                    self._options[categ][opt] = val

        print(f"\n{self.__class__.__name__}.readYaml(1): options = {self._options}")

        #self._options['output']['base'] = self._options['output']['file'][:-4]
        #self._options['output']['ext'] = self._options['output']['file'][-4:]

        self._options['molecule']['npick'] = max(abs(int(self._options['molecule']['npick'])), 1)
        #self._options['shape']['lring'] = int(self._options['shape']['lring'])
        #self._options['shape']['nmols'] = int(self._options['shape']['nmols'])

        if self._options['molecule']['names'] == ['***']:
            self._options['molecule']['resnm'] = 'ALL'

        if len(self._options['molecule']['fracs']) == 1:
            self._options['molecule']['fracs'] = str(self._options['molecule']['fracs'][0])
        if isinstance(self._options['molecule']['fracs'], str):
            print(f"\n{self.__class__.__name__}.readYaml(2): setting fracs from string "
                  f"'{self._options['molecule']['fracs']}'")
            self.setMolFracsFromStr(self._options['molecule']['fracs'])

        self._options['other']['nlatx'] = self._options['other']['nlatt']
        self._options['other']['nlaty'] = self._options['other']['nlatt']
        self._options['other']['nlatz'] = self._options['other']['nlatt']

        print(f"\n{self.__class__.__name__}.readYaml(2): options = {self._options}")

    # end of readYaml

    # CLI parsing

    def parseCLI(self, argv) -> None:
        """
        Takes a set of command-line arguments, and updates the options dictionary
        with the parameters accordingly (see ``self._options``).

        :param argv: a list of the user-specified command-line arguments (ref ``shape.py --help``)
        :return: None
        """

        sname = os.path.basename(argv[0])

        hlp = help()
        tb  = beauty()
    
        if argv[1] in ('-g', '--gopt'):
            argv.pop(1)
            #print(f"{self.__class__.__name__}.parseCLI(): Using 'getopt' ...\n")
    
            if argv[1] in ('-h', '--help'):
                hlp.show(sname)
                print(f"End of help (nothing done)")
                sys.exit(0)
    
            try:
                opts, args = getopt.getopt(argv[1:], "d:b:i:o:x:s:p:l:m:n:r:t:c:w:",
                             ["rev", "pin", "fxz", "alignz", "smiles", "dio=", "box=", "inp=", "out=", "xout=", "frc=", "waves=",
                              "shape=", "layers=", "lring=", "nmols=", "molids=", "rnames=", "turns=", "alpha=", "theta=",
                              "ldpd=", "dmin=", "cavr=", "nside=", "zsep=", "sbuff=", "mint=", "mext=", "rmb=", "rme=", "nl=", "nx=", "ny=", "nz=",
                              "fill=", "origin=", "offset=", "dnames="])  #, "npick=", "shift="])  #, "tstep=" - deprecated
            except getopt.GetoptError:
                print(f"{self.__class__.__name__}.parseCLI(): Too few options: " + argv + "\n")
                print(f"{self.__class__.__name__}.parseCLI(): Try: " + sname + " --help\n")
                sys.exit(1)
    
            for opt, arg in opts:
                #print ("options=", options)
                if opt in ("-d", "--dio"):
                    #dio = arg.split(',')
                    dio = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    if len(dio) > 1:
                        if len(dio) > 2:
                            print(f"\n{self.__class__.__name__}.parseCLI(): More than two IO directory names given - only two are taken, the rest is ignored!\n")
                        self.setInpPath(str(dio[0].rstrip()))
                        self.setOutPath(str(dio[1].rstrip()))
                        print(f"{self.__class__.__name__}.parseCLI(): Got inp_path = '{self.getInpPath()}' and out_path = '{self.getOutPath()}'")
                    else:
                        self.setInpPath(str(arg.rstrip()))
                        self.setOutPath(self.getInpPath())
                        print(f"{self.__class__.__name__}.parseCLI(): Got inp_path = '{self.getInpPath()}' == out_path = '{self.getOutPath()}'")
                elif opt in ("-b", "--box"):
                    self.setFlagBox(True)
                    self.setInpBox(arg.rstrip())  #AB: TODO - reading the box/cell specs from a separate file?
                elif opt in ("-i", "--inp"):
                    self.setInpFile(arg.rstrip())
                elif opt in ("-o", "--out"):
                    fout = arg.rstrip()
                    if fout[:6] == 'CONFIG':
                        # MS: user provided standard name for DL_POLY/DL_MESO output (on its own or at start): use as full file name
                        self.setOutFile(fout)
                        self.setOutIsBase(False)
                    elif len(fout) > 4 and fout[-4:] in self._oexts:
                        # AB: user provided the full file name
                        self.setOutFile(fout)
                        self.setOutIsBase(False)
                    else:
                        # AB: user provided the file name base (make sure the '-x/--xout' is not used)
                        self.setOutBase(fout)  # OutBase + OutExt = OutFile
                        self.setOutIsBase(True)
                elif opt in ("-x", "--xout"):
                    self.setOutExt(arg.rstrip())
                elif opt in ("-s", "--shape"):
                    self.setShapeType(arg.rstrip())
                # elif opt in ("-p", "--npick"):
                #     self.setMolNpick(max(abs(int(arg)), 1))
                elif opt in ("-l", "--lring"):
                    self.setShapeLring(int(arg))
                elif opt in ("-t", "--turns"):
                    self.setShapeTurns(int(arg))
                elif opt in ("-n", "--nmols"):
                    self.setShapeNmols(int(arg))
                elif opt in ("-m", "--molids"):
                    #idsl = arg.split(',')
                    idsl = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    if len(idsl) > 1:
                        self.setMolID(max(abs(int(idsl[0])), 0))
                        for id in idsl:
                            self.setMolIDs((max(abs(int(id)), 0)))
                    else:
                        self.setMolID(max(abs(int(arg)), 0))
                elif opt in ("-r", "--rnames"):
                    #names = arg.replace('[','').replace(']','').split(',')
                    names = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    if len(names) > 1:
                        self.setMolResnm(str(names[0].rstrip()))
                        for nm in names:
                            self.setMolNames((str(nm.rstrip())))
                    else:
                        self.setMolResnm(str(arg.rstrip()))
                        self.setMolNames(str(arg.rstrip()))
                    if self.getMolResnm() == '***': self.setMolResnm('ALL')
                elif opt in ("--dnames"):
                    names = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    if len(names) > 1:
                        for nm in names:
                            self.setDensNames((str(nm.rstrip())))
                    else:
                        #self.setMolResnm(str(arg.rstrip()))
                        self.setDensNames(str(arg.rstrip()))
                elif opt in ("-c", "--cavr"):
                    self.setShapeRadius(arg.strip())
                    #self.setShapeRadius(float(arg))
                elif opt in ("-f", "--fill"):
                    self.setShapeFill(arg.rstrip())
                elif opt in ("--layers"):
                    self.setShapeLayers(arg)
                elif opt in ("--nside"):
                    self.setNside(abs(int(arg)))
                elif opt in ("--zsep"):
                    self.setZsep(abs(float(arg)))
                elif opt in ("--origin"):
                    self.setShapeOrigin(arg.rstrip())
                elif opt in ("--offset"):
                    self.setShapeOffset(arg.strip())
                    # is_shift = True
                    # strl = arg.strip().replace('[', '').replace(']', '').split(',')
                    # for lstr in strl:
                    #     shift.append(float(lstr))
                #AB: deprecated - pitched 'band' = rod with a spiral pitch between turns
                #elif opt in ("--tstep"):
                #    shape = 'ring'
                #    nstep = int(arg)
                #    if abs(nstep) > 0: shape = 'band'
                elif opt in ("--alpha"):
                    self.setAlpha(float(arg))
                elif opt in ("--theta"):
                    self.setTheta(float(arg))
                elif opt in ("--dmin"):
                    self.setMinDist(float(arg))
                elif opt in ("--sbuff"):
                    self.setSolvBuff(float(arg))
                elif opt in ("--fxz"):
                    self.setFlagFxz(True)
                elif opt in ("--alignz"):
                    self.setFlagAlignZ(True)
                elif opt in ("--pin"):
                    self.setFlagPin(True)
                elif opt in ("--rev"):
                    self.setFlagRev(True)
                elif opt in ("--frc"):
                    self.setFlagRandom(True)
                    self.setMolFracsFromStr(arg)
                    # fracs = []
                    # frclist = arg.strip().replace('[', '').replace(']', '').split(':')
                    # for frcl in frclist:
                    #     fracs.append([])
                    #     frcs = frcl.strip().replace('[', '').replace(']', '').split(',')
                    #     for frc in frcs:
                    #         fracs[-1].append(float(frc))
                    # self._options['molecule']['fracs'] = fracs
                    #print(f"{self.__class__.__name__}.parseCLI(gopt): Species' fractions = "
                    #      f"{self._options['molecule']['fracs']}")
                elif opt in ("--mint"):
                    #nums = arg.split(',')
                    nums = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    for num in nums:
                        self.setMolMint((max(abs(int(num)), 0)))
                elif opt in ("--mext"):
                    #nums = arg.split(',')
                    nums = re.sub(r'[\[\]\(\)]', '', arg).split(',')
                    for num in nums:
                        self.setMolMext((max(abs(int(num)), 0)))
                elif opt in ("--nl"):
                    self.setNlatt(abs(int(arg)))
                    self.setNlatx(self.getNlatt())
                    self.setNlaty(self.getNlatt())
                    self.setNlatz(self.getNlatt())
                elif opt in ("--nx"):
                    self.setNlatx(abs(int(arg)))
                elif opt in ("--ny"):
                    self.setNlaty(abs(int(arg)))
                elif opt in ("--nz"):
                    self.setNlatz(abs(int(arg)))
                elif opt in ("--ldpd"):
                    # MS: new option to set length scale (in nm) for DPD inputs/outputs
                    self.setLenDPD(float(arg))
                else:
                    print(f"{self.__class__.__name__}.parseCLI(): Unrecognised option(s): " + arg + "\n")
                    print(f"{self.__class__.__name__}.parseCLI(): Try: " + sname + " --help\n")
                    sys.exit(1)

            # if self.getOutIsBase():
            #     #self.setOutFile(str(self.getOutBase() + self.getOutExt()))
            #     if self.getOutExt() not in self._oexts:
            #         print(f"{self.__class__.__name__}.parseCLI(): Unsupported output file extension: '{self.getOutExt()}' "
            #               f"- FULL STOP!!!\n")
            #         sys.exit(1)
            # else:
            #     self.setOutBase(self.getOutFile()[:-4])
            #     self.setOutExt(self.getOutFile()[-4:])

        elif argv[1] in ('-y', '--yaml'):

            # MD: Check for flag, and if the third argument is 'yaml' (i.e. follows by file name), read it
            if ('.yaml') or ('.yml') in argv[2]:
                self.readYaml(argv[2])

            if self.getMolIDs() == 1:
                self.setMolIDs([1])

        else:
            #argv.pop(1)
            #print(f"{self.__class__.__name__}.parseCLI(): Using 'argparser' ...\n")
    
            if argv[1] in ('-h','--help'):
                hlp.header()
            parser = argparse.ArgumentParser(prog=sname,
                                             usage=tb.BOLD + sname + " -i <INP> -o <OUT> -s <SHAPE> [options]" + tb.END,
                                             description=None,
                                             epilog="End of help (nothing done)")
    
            # IO
            parser.add_argument('-g','--gopt', action="store_false", required=False,
                                help=hlp.GETOPT) #"switch to use 'getopt' for parsing the sname arguments")
            # verbosity
            parser.add_argument("-v","--verbose", action="store_true", required=False,
                                help=hlp.VERBOSE) #"verbosity switch [OFF]") # a switch
    
            parser.add_argument('-d','--dio', default='.', required=False, metavar='IODIR',
                                help=hlp.DIRIO) #"directory(ies) for input/output files {'.'*}")
            parser.add_argument('-b','--box', default=None, required=False, metavar='IBOX',
                                help=hlp.BOXINP) #"input file specifying box dimensions (3 values) or cell matrix (9 values) "
                                     #"{shape.box*/<name>.box}")
            parser.add_argument('-i','--inp', default='config_inp.gro', required=True, metavar='IFILE',
                                help=hlp.FINAME) ##"input configuration file containing molecule(s) coordinates "
                                     #"{config_inp.gro*/<name>.gro/<name>.xyz}")
            parser.add_argument('-o', '--out', default='config_out', required=True, metavar='ONAME',
                                help=hlp.FONAME) #"'base_name' of the output configuration file without extension {config_out*}")
            parser.add_argument('-x', '--xout', default='.gro', required=False, metavar='OEXT',
                                help=hlp.FOEXT) #"output file extension {.gro*/.xyz/.pdb?}")
            # parameters
            parser.add_argument('-r', '--rnames', default='ANY', required=False,
                                help=hlp.RNAMES) #"list of molecule names to pick up from input {ALL/ANY/SDS,CTAB/etc}")
            parser.add_argument("-m", "--molids", default='1',
                                help=hlp.MOLIDS) #"list of molecule indices to pick up from input {1*/2,5/etc}")
            # parser.add_argument("-p", "--npick", type=int, default=1,
            #                     help=hlp.NPICK) #"number of molecules (residues) to pick up from input")
            #shapes = ['ring', 'rod', 'band', 'ball', 'ves', 'balp', 'vesp', 'mix', 'lat', 'lat2', 'lat3']
            parser.add_argument('-s', '--shape', default='ring', required=True,
                                help=hlp.SHAPE) #"shape of the output structure to create {ring*/rod/band/ball/ves/balp/vesp/mix/lat/lat2/lat3}")
            parser.add_argument('-f', '--fill', default='rings0', required=False,
                                help=hlp.FILL) #"type of filling in the shape {rings*/area/fibo/mesh}")
            parser.add_argument("-c", "--cavr", type=str, default='0.25',
                                help=hlp.CAVR) #"radius of internal cavity {0.5*}")
            parser.add_argument("-n", "--nmols", type=int, default=0,
                                help=hlp.NMOLS) #"number of molecules in the largest ring within the output structure (>9)")
            parser.add_argument("-l", "--lring", type=int, default=0,
                                help=hlp.LRING) #"number of molecules in the largest ring within the output structure (>9)")
            parser.add_argument("-t", "--turns", type=int, default=0,
                                help=hlp.TURNS) #"number of full 'turns' in a 'rod', i.e. stack of rings {1*}")
            parser.add_argument("--dnames", default='NONE', required=False,
                                help=hlp.DNAMES) #"list of molecule names to pick up from input {ALL/ANY/SDS,CTAB/etc}")
            parser.add_argument("--layers", type=str, default='1',
                                help=hlp.LAYERS) #"number of (mono-)layers in the vesicle structure (>0)")
            parser.add_argument("--nside", type=int, default=10,
                                help=hlp.NSIDE) #"number of molecules in the side of the bilayer")
            parser.add_argument("--zsep", type=float, default=0,
                                help=hlp.ZSEP) #"xy distance between molecule in a layer")
            parser.add_argument("--origin", type=str, default='cog',
                                help=hlp.ORIGIN) #"initial azimuth for each molecule in degrees {0.0*}")
            parser.add_argument("--offset", type=str, default="",
                                help=hlp.OFFSET) #"initial azimuth for each molecule in degrees {0.0*}")
            # parser.add_argument("--shift", type=str, default="",
            #                     help=hlp.OFFSET) #"initial azimuth for each molecule in degrees {0.0*}")

            #AB: deprecated - pitched 'band' = rod with a spiral pitch between turns
            #parser.add_argument("--tstep", type=int, default=1,
            #                    help=hlp.TSTEP) #"number of 'steps' (open rings) per 'turn' in a 'rod' or a pitched 'band' {1*}")
    
            parser.add_argument("--alpha", type=float, default=0.0,
                                help=hlp.ALPHA) #"initial azimuth for each molecule in degrees {0.0*}")
            parser.add_argument("--theta", type=float, default=0.0,
                                help=hlp.THETA) #"initial altitude for each molecule in degrees {0.0*}")
            parser.add_argument("--sbuff", type=float, default=1.0,
                                help=hlp.SBUFF) #
            parser.add_argument("--dmin", type=float, default=0.5,
                                help=hlp.DMIN) #
            parser.add_argument('--ldpd', type=float, required=False, default=0.1,
                                help=hlp.LDPD) #"DPD length scale in nm used in input/output files {0.1}"
            parser.add_argument("--mint", default='', required=False, #type=list,
                                help=hlp.MINT) #
            parser.add_argument("--mext", default='', required=False, #type=list,
                                help=hlp.MEXT) #
            parser.add_argument("--frc", default=[], required=False, #type=list,
                                help=hlp.FRC)
            parser.add_argument("--nl", type=int, default=0,
                                help=hlp.NL) #
            parser.add_argument("--nx", type=int, default=0,
                                help=hlp.NX) #
            parser.add_argument("--ny", type=int, default=0,
                                help=hlp.NY) #
            parser.add_argument("--nz", type=int, default=0,
                                help=hlp.NZ) #

            # switches
            parser.add_argument("--rev", action="store_true", required=False,
                                help=hlp.REVERSE) #"use reverse atom indexing, i.e. opposite to the input indexing [OFF]") # a switch!
            parser.add_argument("--pin", action="store_true", required=False,
                                help=hlp.PINNED) #"create a 'pinned-through' vesicle, i.e. with holes on its six sides [OFF]") # a switch!
            parser.add_argument("--fxz", action="store_true", required=False,
                                help=hlp.FLATTEN) #"use 'XZ-flattened' initial molecule orientation to minimise its 'spread' along Y axis "
                                #"before its further reorientation and adding to a 'ring' formation around Z axis [OFF]") # a switch!
            parser.add_argument("--alignz", action="store_true", required=False,
                                help=hlp.ALIGNZ) #"align molecule configuration from SMILES along Z axis"

            args = parser.parse_args()
            #print(f"\n{self.__class__.__name__}.parseCLI(parse_args):\n{args}\n")
    
            if args.dio :
                #dio = args.dio.split(',')
                dio = re.sub(r'[\[\]\(\)]', '', args.dio).split(',')
                if len(dio) > 1:
                    if len(dio) > 2:
                        print(f"\n{self.__class__.__name__}.parseCLI(): More than two IO directory names given - only two are taken, the rest is ignored!\n")
                    self.setInpPath(str(dio[0].rstrip()))
                    self.setOutPath(str(dio[1].rstrip()))
                    print(f"\n{self.__class__.__name__}.parseCLI(): Got inp_path = '{self.getInpPath()}' and out_path = '{self.getOutPath()}'")
                else:
                    self.setInpPath(str(args.dio.rstrip()))
                    self.setOutPath(self.getInpPath())
                    print(f"\n{self.__class__.__name__}.parseCLI(): Got inp_path = '{self.getInpPath()}' == out_path = '{self.getOutPath()}'")
    
            if args.box :
                self.setFlagBox(True)
                self.setInpBox(args.box.rstrip())
                #AB: TODO - reading the box/cell specs from a separate file?
    
            if args.inp :
                self.setInpFile(args.inp)
    
            if args.ldpd:
                self.setLenDPD(float(args.ldpd))

            if args.out :
                fout = args.out.rstrip()
                if fout[:6] == 'CONFIG':
                    # MS: user provided CONFIG as (full) file name for DL_POLY/DL_MESO
                    self.setOutFile(fout)
                    self.setOutIsBase(False)
                elif len(fout) > 4 and fout[-4:] in self._oexts:
                    # AB: user provided the full file name
                    self.setOutFile(fout)
                    self.setOutIsBase(False)
                else:
                    # AB: user provided the file name base (make sure the '-x/--xout' is not used)
                    self.setOutBase(fout)  # OutBase + OutExt = OutFile
                    self.setOutIsBase(True)

            if args.xout :
                self.setOutExt(args.xout.rstrip())
                self.setOutIsBase(True)
            else:
                self.setOutIsBase(False)

            if args.shape :
                self.setShapeType(args.shape.rstrip())

            if args.fill :
                self.setShapeFill(args.fill.rstrip())

            # if args.npick :
            #     self.setMolNpick(max(abs(int(args.npick)), 1))
    
            if args.lring :
                self.setShapeLring(int(args.lring))

            if args.turns :
                self.setShapeTurns(args.turns)

            if args.nmols:
                self.setShapeNmols(int(args.nmols))

            if args.layers:
                self.setShapeLayers(args.layers)

            if args.nside:
                self.setNside(abs(int(args.nside)))
          
            if args.zsep:
                self.setZsep(abs(float(args.zsep)))

            if args.molids :
                self.delMolIDs()
                #idsl = args.molids.split(',')
                idsl = re.sub(r'[\[\]\(\)]', '', args.molids).split(',')
                if len(idsl)>1:
                    self.setMolID(max(abs(int(idsl[0])), 0))
                    for idm in idsl:
                        self.setMolIDs((max(abs(int(idm)), 0)))
                else:
                    self.setMolID(max(abs(int(idsl[0])), 0))

            if args.rnames :
                #names = args.rnames.replace('[','').replace(']','').split(',')
                names = re.sub(r'[\[\]\(\)]', '', args.rnames).split(',')
                if len(names)>1:
                    self.setMolResnm(str(names[0].rstrip()))
                    for nm in names:
                        self.setMolNames((str(nm.rstrip())))
                else:
                    self.setMolResnm(str(args.rnames.rstrip()))
                    self.setMolNames(str(args.rnames.rstrip()))
                if self.getMolResnm() == '***': self.setMolResnm('ALL')

            if args.dnames :
                names = re.sub(r'[\[\]\(\)]', '', args.dnames).split(',')
                if len(names) > 1:
                    for nm in names:
                        self.setDensNames((str(nm.rstrip())))
                else:
                    self.setDensNames(str(args.dnames.rstrip()))

            if args.cavr :
                self.setShapeRadius(args.cavr.strip())

            if args.dmin :
                self.setMinDist(float(args.dmin))

            if args.origin:
                self.setShapeOrigin(args.origin.strip())

            if args.offset:
                self.setShapeOffset(args.offset)
                print(f"{self.__class__.__name__}.parseCLI(argp): "
                      f"Read-in offset = {args.offset} ({type(args.offset)})")

            # if args.shift:
            #     self.setShapeOffset(args.shift)

            # AB: deprecated - pitched 'band' = rod with a spiral pitch between turns
            #if args.tstep :  # elif opt in ("--tstep"):
            #    #shape = 'ring'
            #    nstep = int(args.tstep)
            #    if abs(nstep) > 0: shape = 'band'
    
            if args.alpha :
                self.setAlpha(float(args.alpha))

            if args.theta :
                self.setTheta(float(args.theta))
    
            if args.sbuff :
                self.setSolvBuff(float(args.sbuff))
    
            if args.fxz :
                self.setFlagFxz(True)

            if args.alignz:
                self.setFlagAlignZ(True)

            if args.pin :
                self.setFlagPin(True)
    
            if args.rev :
                self.setFlagRev(True)
    
            if args.frc :
                self.setFlagRandom(True)
                self.setMolFracsFromStr(args.frc)
                # if isinstance(args.frc, str):
                    # fracs = []
                    # frclist = args.frc.strip().replace('[', '').replace(']', '').split(':')
                    # print(f"{self.__class__.__name__}.parseCLI(argp): Species' fractions list = {frclist}")
                    # for frcl in frclist:
                    #     fracs.append([])
                    #     frcs = frcl.strip().replace('[', '').replace(']', '').split(',')
                    #     for frc in frcs:
                    #         fracs[-1].append(float(frc))
                    # self._options['molecule']['fracs'] = fracs
                    # print(f"{self.__class__.__name__}.parseCLI(argp): Species' fractions = "
                    #       f"{self._options['molecule']['fracs']}")

            if args.mint :
                #nums = args.mint.split(',')
                nums = re.sub(r'[\[\]\(\)]', '', args.mint).split(',')
                for num in nums:
                    self.setMolMint((max(abs(int(num)), 0)))

            if args.mext :
                #nums = args.mext.split(',')
                nums = re.sub(r'[\[\]\(\)]', '', args.mext).split(',')
                for num in nums:
                    self.setMolMext((max(abs(int(num)), 0)))

            if args.nl :
                self.setNlatt(abs(args.nl)) #abs(int(args.nl))
                self.setNlatx(self.getNlatt())
                self.setNlaty(self.getNlatt())
                self.setNlatz(self.getNlatt())
    
            if args.nx :
                if self.getNlatt() > 1:
                    self.setNlatx(self.getNlatt())
                else:
                    self.setNlatx(abs(args.nx)) #abs(int(args.nx))
            if args.ny :
                if self.getNlatt() > 1:
                    self.setNlatx(self.getNlatt())
                else:
                    self.setNlaty(abs(args.ny)) #abs(int(args.ny))
            if args.nz :
                if self.getNlatt() > 1:
                    self.setNlatx(self.getNlatt())
                else:
                    self.setNlatz(abs(args.nz)) #abs(int(args.nz))

            if args.verbose :
                print(f"\n{self.__class__.__name__}.parseCLI(): Verbose mode is ON")
            else:
                print(f"\n{self.__class__.__name__}.parseCLI(): Verbose mode is OFF")

        # ARGUMENTS & HELP - END

        if self.getOutIsBase():
            #self.setOutFile(str(self.getOutBase() + self.getOutExt()))
            if self.getOutExt() not in self._oexts:
                print(f"{self.__class__.__name__}.parseCLI(): ERROR! Unsupported output file extension: '{self.getOutExt()}' "
                      f"- FULL STOP!!!\n")
                sys.exit(1)
        elif self.getOutFile()[:6]=='CONFIG':
            # MS: no need to do anything to setOutBase or setOutExt for CONFIG files,
            #     but need to make sure nothing is actually done and no error is thrown!
            pass
        elif isinstance(self.getOutFile(),str) and len(self.getOutFile()) > 4:
            self.setOutBase(self.getOutFile()[:-4])
            self.setOutExt(self.getOutFile()[-4:])
        elif isinstance(self.getOutBase(), str) and len(self.getOutBase()) > 0 and self.getOutExt() in self._oexts:
            self.setOutIsBase(True)
        else:
            print(f"{self.__class__.__name__}.parseCLI(): ERROR! Inconsistent input for "
                  f"output file name base and/or extension: '{self.getOutFile()}' =?= "
                  f"'{self.getOutBase()}' + '{self.getOutExt()}' - FULL STOP!!!\n")
            sys.exit(1)

        if isinstance(self.getOutBase(),str) and len(self.getOutBase()) > 4:
            if self.getOutBase()[-4:] in self._oexts and self.getOutIsBase():
                print(f"{self.__class__.__name__}.parseCLI(): ERROR! Inconsistent input for "
                      f"output file name base and extension: '{self.getOutBase()}' +"
                      f"'{self.getOutExt()}' - FULL STOP!!!\n")
                sys.exit(1)

        if self.getShapeType() == 'ball' and self.getShapeFill() != self._sfill[0]:
            #self.setShapeLring(0)
            if self.getShapeFill() == self._sfill[1]:
                print(f"{self.__class__.__name__}.parseCLI(): WARNNING! "
                      f"Will generate 'ball' with {self.getShapeLring()} molecules projected on 'equator' ring - "
                      f"option '--fill=rings' (second variant) ...")
                      #f"- experimental feature!..")
            else:
                print(f"{self.__class__.__name__}.parseCLI(): WARNNING! "
                      f"Will generate 'ball' of {self.getShapeNmols()} molecules covering its surface uniformly - "
                      f"option '--fill=fibo' (Fibonacci spiral) ...")
                      #f"- experimental feature!..")
            if self.getShapeRadius() < 0.5:
                print(f"{self.__class__.__name__}.parseCLI(): WARNNING! "
                      f"Generating a 'ball' of radius Rmin = {self.getShapeRadius()} "
                      f" < 0.5 nm ...")

    # end of parseCLI

# end of InputParser
