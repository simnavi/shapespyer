"""
.. module:: functions
   :platform: Linux - tested, Windows (WSL Ubuntu) - tested
   :synopsis: helper functions unfit for inclusion in any abstraction class

.. moduleauthor:: Dr Andrey Brukhno <andrey.brukhno[@]stfc.ac.uk>

"""

# This software is provided under The Modified BSD-3-Clause License (Consistent with Python 3 licenses).
# Refer to and abide by the Copyright detailed in LICENSE file found in the root directory of the library!

##################################################
#                                                #
#  Shapespyer - soft matter structure generator  #
#                                                #
#  Author: Dr Andrey Brukhno (c) 2020 - 2024     #
#          Daresbury Laboratory, SCD, STFC/UKRI  #
#                                                #
##################################################

##from __future__ import absolute_import
__author__ = "Andrey Brukhno"
__version__ = "0.1.6 (Beta)"

# TODO: unify the coding style:
# TODO: CamelNames for Classes, camelNames for functions/methods & variables (where meaningful)
# TODO: hint on method/function return data type(s), same for the interface arguments
# TODO: one empty line between functions/methods & groups of interrelated imports
# TODO: two empty lines between Classes & after all the imports done
# TODO: classes and (lengthy) methods/functions must finish with a closing comment: '# end of <its name>'
# TODO: meaningful DocStrings right after the definition (def) of Class/method/function/module
# TODO: comments must be meaningful and start with '# ' (hash symbol followed by a space)
# TODO: insightful, especially lengthy, comments must be prefixed by develoer's initials as follows:


from shapes.basics.globals import *
from math import copysign

from functools import wraps
from time import time


def timing(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        ts = time()
        result = func(*args, **kwargs)
        te = time()
        print(f"function:{func.__name__} took: {te-ts} sec")
        # print('func:%r args:[%r, %r] took: %2.4f sec' % \
        #   (f.__name__, args, kw, te-ts))
        return result
    return wrap

def nint(x: float) -> int :
    return round(x+copysign(TNST,x))

def pbc_rect(dvec, box):
    dvec[0] -= box[0] * float(nint(dvec[0]/box[0]))
    dvec[1] -= box[1] * float(nint(dvec[1]/box[1]))
    dvec[2] -= box[2] * float(nint(dvec[2]/box[2]))
    return dvec

def pbc_cube(dvec, box):
    rbox = 1.0/box
    dvec[0] -= box * float(nint(dvec[0]*rbox))
    dvec[1] -= box * float(nint(dvec[1]*rbox))
    dvec[2] -= box * float(nint(dvec[2]*rbox))
    return dvec

def isVec3Like(vec):
    return (hasattr(vec, '__len__') and len(vec) == 3)

def pbc(dvec, box):
    #if hasattr(dvec, '__len__') and len(dvec) == 3:
    if isVec3Like(dvec):
        if hasattr(box, '__len__'):
            if len(box) == 3:
                #print(f'Box = {box} is a vector => using pbc_rect()')
                return pbc_rect(dvec, box)
            else:
                print(f'Box = {box} does not qualify as either scalar or 3D vector '
                      f'- FULL STOP!!!')
                sys.exit(1)
        else:
            #print(f'Box = {box} is a scalar => using pbc_cube()')
            return pbc_cube(dvec, box)
    else:
        print(f'dVec = {dvec} does not qualify as 3D vector - FULL STOP!!!')
        sys.exit(1)


def get_mins(axyz):
    vmin = [0., 0., 0.]
    imin = [-1, -1, -1]
    if axyz:
        mini = []
        vmin[0] = min(axyz, key=lambda x: x[0])[0]
        mini.append([i for i, xxx in enumerate(axyz) if vmin[0] == xxx[0]])
        print("\nget_mins(0): Min(x) = "+str(vmin[0])+" @ "+str(mini[0])) #+"\n")
        # " @ " +str( [(i, xxx.index(xmin)) for i, xxx in enumerate(axyz) if xmin in xxx] )+"\n")

        vmin[1] = min(axyz, key=lambda y: y[1])[1]
        mini.append([i for i, yyy in enumerate(axyz) if vmin[1] == yyy[1]])
        print("get_mins(1): Min(y) = "+str(vmin[1])+" @ "+str(mini[1])) #+"\n")
        # " @ " +str( [(i, yyy.index(ymin)) for i, yyy in enumerate(axyz) if ymin in yyy] )+"\n")

        vmin[2] = min(axyz, key=lambda z: z[2])[2]
        mini.append([i for i, zzz in enumerate(axyz) if vmin[2] == zzz[2]])
        print("get_mins(2): Min(z) = "+str(vmin[2])+" @ "+str(mini[2])+"\n")
        # " @ " +str( [(i, zzz.index(zmin)) for i, zzz in enumerate(axyz) if zmin in zzz] )+"\n")

        for k in range(len(mini)):
            imin[k] = mini[k][int(len(mini[k]) / 2)]
            # if len(mini[k]) > 1 :
            # mini[k][0] = mini[k][int(len(mini[k])/2)]
            # del mini[k][1:]
    return vmin, imin
# end of get_mins()

def get_maxs(axyz):
    vmax = [0., 0., 0.]  # [-HUGE,-HUGE,-HUGE]
    imax = [-1, -1, -1]
    if axyz:
        maxi = []
        vmax[0] = max(axyz, key=lambda x: x[0])[0]
        maxi.append([i for i, xxx in enumerate(axyz) if vmax[0] == xxx[0]])
        print("\nget_maxs(0): Max(x) = "+str(vmax[0])+" @ "+str(maxi[0])) #+"\n")
        # " @ " +str( [(i, xxx.index(xmax)) for i, xxx in enumerate(axyz) if xmax in xxx] )+"\n")

        vmax[1] = max(axyz, key=lambda y: y[1])[1]
        maxi.append([i for i, yyy in enumerate(axyz) if vmax[1] == yyy[1]])
        print("get_maxs(1): Max(y) = "+str(vmax[1])+" @ "+str(maxi[1])) #+"\n")
        # " @ " +str( [(i, yyy.index(ymax)) for i, yyy in enumerate(axyz) if ymax in yyy] )+"\n")

        vmax[2] = max(axyz, key=lambda z: z[2])[2]
        maxi.append([i for i, zzz in enumerate(axyz) if vmax[2] == zzz[2]])
        print("get_maxs(2): Max(z) = "+str(vmax[2])+" @ "+str(maxi[2])+"\n")
        # " @ " +str( [(i, zzz.index(zmax)) for i, zzz in enumerate(axyz) if zmax in zzz] )+"\n")

        for k in range(len(maxi)):
            imax[k] = maxi[k][int(len(maxi[k]) / 2)]
            # if len(maxi[k]) > 1 :
            # maxi[k][0] = maxi[k][int(len(maxi[k])/2)]
            # del maxi[k][1:]
    return vmax, imax
# end of get_maxs()
