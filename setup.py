# For extras refer to https://pythonhosted.org/an_example_pypi_project/
from setuptools import setup, find_packages

# Use README as the long_description for release
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name = 'Shapespyer',
      version = '0.1.7.dev',
      author = 'Andrey V. Brukhno',
      author_email = "andrey.brukhno_at_stfc.ac.uk",
      description = 'Python toolkit for generation and equilibration of complex soft matter systems',
      long_description = long_description,
      long_description_content_type='text/markdown',
      url = "https://gitlab.com/simnavi/shapespyer",
      license = "BSD-3-Clause License",
      keywords = "soft matter simulation molecular biomolecular structure self-assembly cluster analysis gyration",
      install_requires = ['numpy', 'matplotlib', 'pyyaml', 'docopt'],
      classifiers=[
        "Development Status :: 7 - Beta"
        "Intended Audience :: Science/Research"
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3.8+",
        "Topic :: Scientific/Software/Engineering/Workflow/Simulation"
      ],
      include_package_data = True,
      packages = find_packages()
)

